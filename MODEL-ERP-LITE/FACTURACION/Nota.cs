﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_nota", Schema = "facturacion")]
    public  class Nota
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("tipoNota")]
        public int idTipoNota { get; set; }
        [ForeignKey("comprobante")]

        public int idComprobanteVenta { get; set; }
        [ForeignKey("tipoComprobante")]

        public int idTipoComprobante { get; set; }
        [ForeignKey("cliente")]
        public int idCliente { get; set; }
        public double total { get; set; }
        public double totalExonerado { get; set; }
        public double igv { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }
        public string usuarioCreate { get; set; }
        public string usuarioUpdate { get; set; }
        public bool anulado { get; set; }
        public bool pagado { get; set; }
        public string serie { get; set; }
        public string motivo { get; set; }
        public string hash { get; set; }
        public int numero { get; set; }

        public bool esCredito { get; set; }
        public bool esDebito { get; set; }

        public virtual Cliente cliente { get; set; }
        public virtual TipoNota tipoNota { get; set; }
        public virtual ComprobantePago comprobante { get; set; }
        public virtual TipoComprobantePago tipoComprobante { get; set; }
        public virtual List<DetalleNota> detalles { get; set; }
    }
}
