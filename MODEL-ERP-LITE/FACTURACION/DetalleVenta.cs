﻿using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_detalle_venta", Schema = "facturacion")]
    public class DetalleVenta
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("venta")]
        public int idVenta { get; set; }
        [ForeignKey("productoPresentacion")]
        public int idProductoPresentacion { get; set; }
        public int idProducto { get; set; }
        [ForeignKey("movimiento")]
        public int idMovimiento { get; set; }
        [ForeignKey("tipoAfectacionIgv")]
        public int idTipoAfectacionIgv { get; set; }
        public string observacion { get; set; }
        public double cantidad { get; set; }
        public double factor { get; set; }
        public double igv { get; set; }
        public double descuentoN { get; set; }
        public bool anulado { get; set; }
        public double precio { get; set; }
        public double precioN { get; set; }
        public double cantidadN { get; set; }
        public double total { get; set; }
        public double descuentoItem { get; set; }
        // public double disminucionValor { get; set; }
        public virtual ProductoPresentacion productoPresentacion { get; set; }
        public virtual Venta venta { get; set; }
        public virtual DetalleMovimientoProducto movimiento { get; set; }

        public virtual  TipoAfectacionIGV tipoAfectacionIgv{get;set;}

    }
}
