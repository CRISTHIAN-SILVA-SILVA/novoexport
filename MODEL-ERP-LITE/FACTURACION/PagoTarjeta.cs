﻿using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_pagos_tarjeta", Schema = "facturacion")]
    public class PagoTarjeta
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string post { get; set; }
        public double porcentaje { get; set; }
        public DateTime fecha { get; set; }
        public double monto { get; set; }
        public bool esVenta { get; set; }
        public bool anulado { get; set; }
        public string comprobante { get; set; }
        [ForeignKey("medioPago")]
        public int idMedioPago { get; set; }
        [ForeignKey("movimiento")]
        public int idMovimiento { get; set; }

        public virtual MovimientoCaja movimiento { get; set; }
        [ForeignKey("cuentaBanco")]
        public int idCuenta { get; set; }

        public virtual CuentaBanco cuentaBanco { get; set; }
        public virtual MedioPago medioPago { get; set; }
    }
}
