﻿using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_venta", Schema = "facturacion")]
  public  class Venta
    {

        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("cliente")]
        public int idCliente { get; set; }
        [ForeignKey("tipoPago")]
        public int idTipoPago { get; set; }
        public int diasCredito { get; set; }
        public int idPedido{ get; set; }
        public int idCotizacion { get; set; }
        public double subTotal { get; set; }
        public double montoTotal { get; set; }
        public double opGravadas { get; set; }
        public double opGratuitas { get; set; }
        public double totalExonerado { get; set; }
        public double interes { get; set; }
        public double descuento { get; set; }
        public bool anulado { get; set; }
        public double igv { get; set; }
        public string usuarioCreate { get; set; }
        public string orden { get; set; }
        public string usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }


        public virtual TipoPago tipoPago { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual List<DetalleVenta> detalles { get; set; }
     
    }
}
