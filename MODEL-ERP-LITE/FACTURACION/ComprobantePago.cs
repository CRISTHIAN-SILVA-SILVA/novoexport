﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_comprobante_pago", Schema = "facturacion")]
  public  class ComprobantePago
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("cliente")]
        public int idCliente { get; set; }
        [ForeignKey("tipoComprobante")]
        public int idTipoComprobante { get; set; }
        [ForeignKey("venta")]
        public int idVenta { get; set; }
        [ForeignKey("medioPago")]
        public int idMedioPago { get; set; }
        [ForeignKey("tipoPago")]
        public int idTipoPago { get; set; }
        public string guia { get; set; }
        public double subTotal { get; set; }
        public double montoTotal { get; set; }
        public double montoDetraccion { get; set; }
        public double cobrosExtras { get; set; }
        public double descuentosGlobales { get; set; }
        public double montoN{ get; set; }
        public double montoPagado { get; set; }
        public double interesOtros { get; set; }
        public bool anulado { get; set; }
        public bool pagado { get; set; }
        public bool enviado { get; set; }
        public bool generaDetraccion { get; set; }
        public string serie { get; set; }
        public string hash { get; set; }
        public int numero { get; set; }
        public string usuarioCreate { get; set; }
       // public string guia { get; set; }
        public string usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaPago { get; set; }
     public DateTime fechaVencimiento { get; set; }

        public virtual TipoPago tipoPago { get; set; }
        public virtual MedioPago medioPago { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual TipoComprobantePago tipoComprobante{ get; set; }
        public virtual Venta venta { get; set; }

        public virtual List<Nota> notasCredito { get; set;}
    }
}
