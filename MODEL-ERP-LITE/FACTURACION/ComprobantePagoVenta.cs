﻿using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_comprobante_pago_ventas", Schema = "facturacion")]
    public  class ComprobantePagoVenta
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("cliente")]
        public int idCliente { get; set; }
        public string codTipoComprobante { get; set; }
        public string tipoR { get; set; }
        public string docCliente { get; set; }
        public string codTipoDocCliente { get; set; }
        public DateTime fechaEmision { get; set; }
        public string fechaVencimiento { get; set; }
        public string serie { get; set; }
        public string fechaReferencia { get; set; }
        public string serieReferencia { get; set; }
        public bool anulado { get; set; }
        public double totalExonerado { get; set; }
        public int numero { get; set; }
        public int numeroReferencia { get; set; }
        public double baseImponible { get; set; }
        public double tipoCambio { get; set; }
        public double total { get; set; }
        public virtual Cliente cliente { get; set; }
    }
}
