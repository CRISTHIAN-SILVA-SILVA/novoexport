﻿using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_detalle_nota", Schema = "facturacion")]
    public   class DetalleNota
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("nota")]
        public int idNota { get; set; }
        [ForeignKey("presentacion")]
        public int idPresentacion { get; set; }
        [ForeignKey("tipoAfectacionIgv")]
        public int idTipoAfectacionIgv { get; set; }
        public bool anulado { get; set; }
        public int idProducto { get; set; }
        public int idMovimiento { get; set; }
        public double cantidad { get; set; }
        public double factor { get; set; }
        public double igv { get; set; }
        public double precio { get; set; }
        public double total { get; set; }
        public virtual ProductoPresentacion presentacion { get; set; }
        public virtual Nota nota { get; set; }

        public virtual TipoAfectacionIGV tipoAfectacionIgv { get; set; }
    }
}
