﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_guia_remision", Schema = "facturacion")]
    public class GuiaRemision
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("motivoTranslado")]
        public int idMotivo{ get; set; }
        [ForeignKey("transportista")]
        public int idTransportista { get; set; }
        [ForeignKey("tipoComprobante")]
        public int idTipoComprobante { get; set; }
        public int idComprobante { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        public string usuarioCreate { get; set; }
        public bool esNota { get; set; }
        public bool anulado { get; set; }
        public double monto { get; set; }
        public string licencia { get; set; }
        public string conductor { get; set; }
        public string serieNumeroComprobante { get; set; }
        public int numero { get; set; }
        public string serie { get; set; }
        public string placa { get; set; }

        public string direccionPartida { get; set; }
        public string direccionLlegada { get; set; }

        public virtual Transportista transportista { get; set; }
        public virtual MotivoTranslado motivoTranslado { get; set; }
        public virtual TipoComprobantePago tipoComprobante { get; set; }


    }
}
