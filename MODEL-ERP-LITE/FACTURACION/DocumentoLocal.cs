﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.FACTURACION
{
    [Table("tbl_documentos_locales", Schema = "facturacion")]
    public class DocumentoLocal
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string serie { get; set; }
        [ConcurrencyCheck]
        public int numero { get; set; }
       
        public int numeroInicio { get; set; }
        public bool anulado { get; set; }
        public string nombre { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }

        public DateTime fecha { get; set; }

    }
}
