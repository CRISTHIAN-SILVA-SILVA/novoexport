﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
  public  class JoinDetalleCompra
    {
        public int id { get; set; }
        public int idMovimiento { get; set; }
        public int idCompra { get; set; }
        public DateTime fecha { get; set; }
        public string proveedor { get; set; }

        public string serie { get; set; }
        public int numero  { get; set; }
        public double cantidad { get; set; }
        public double costo { get; set; }
        public double total { get; set; }
        public double totalMov{ get; set; }
    }
}
