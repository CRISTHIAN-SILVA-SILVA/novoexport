﻿using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
    [Table("tbl_detalle_compra", Schema = "compras")]
    public  class DetallCompra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }


        [ForeignKey("compra")]
        public int idCompra { get; set; }
        [ForeignKey("presentacion")]
        public int idPresentacion { get; set; }
        public int idProducto { get; set; }
        [ForeignKey("movimiento")]
        public int idMovimiento { get; set; }

        public double cantidad { get; set; }
        public double igv { get; set; }
        public double total{ get; set; }
        public double costo { get; set; }
        public double factor { get; set; }
        public double descuento { get; set; }

        public bool anulado { get; set; }

        public virtual Compra compra { get; set; }
        public virtual DetalleMovimientoProducto movimiento { get; set; }
        public virtual ProductoPresentacion presentacion { get; set; }
    }
}
