﻿using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
    [Table("tbl_detalle_nota_compra", Schema = "compras")]
  public  class DetalleNotaCompra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("presentacion")]
        public int idPresentacion { get; set; }
        public int idProducto { get; set; }
        public int idMovimiento{ get; set; }
        [ForeignKey("nota")]
        public int idNota { get; set; }

        public double cantidad { get; set; }
        public double igv { get; set; }
        public double total { get; set; }
        public double valorUnitario { get; set; }
        public double factor { get; set; }

        public virtual ProductoPresentacion presentacion { get; set; }
        public virtual NotaCompra nota { get; set; }
    }
}
