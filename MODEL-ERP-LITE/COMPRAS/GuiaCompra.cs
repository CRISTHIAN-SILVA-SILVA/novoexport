﻿using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
    [Table("tbl_guia_remision_compra", Schema = "compras")]
    public  class GuiaCompra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("compra")]
        public int idComprobante { get; set; }
        [ForeignKey("transportista")]
        public int idTranporte { get; set; }
        public double monto { get; set; }
        public string numero { get; set; }
        public bool esNota { get; set; }
        public bool esSalida { get; set; }
        public virtual Compra compra { get; set; }
        public virtual Transportista transportista { get; set; }
    }
} 
