﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
    [Table("tbl_nota_compra", Schema = "compras")]
    public class NotaCompra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("tipoNota")]
        public int idTipoNota { get; set; }
        [ForeignKey("compra")]
        public int idCompra { get; set; }
        [ForeignKey("tipoComprobante")]
        public int idTipoComprobante { get; set; }
        [ForeignKey("proveedor")]
        public int idProveedor { get; set; }

        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        public string usuarioCreate { get; set; }
        public DateTime fechaEmision { get; set; }

        public double igv { get; set; }
        public double total { get; set; }
        public double totalExonerado { get; set; }
        public double opGravadas { get; set; }

        public string serie { get; set; }
        public int numero { get; set; }

        public bool anulado { get; set; }

        public bool esCredito { get; set; }

        public virtual Proveedor proveedor { get; set; }
        public virtual TipoNota tipoNota{ get; set; }
        public virtual Compra compra { get; set; }
        public virtual TipoComprobantePago tipoComprobante { get; set; }
        public virtual List<DetalleNotaCompra> detalle { get; set; }

    }
}
