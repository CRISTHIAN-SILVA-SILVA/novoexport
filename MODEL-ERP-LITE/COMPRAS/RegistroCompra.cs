﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
    [Table("tbl_registro_compra", Schema = "compras")]
    public class RegistroCompra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("moneda")]
        public int idMoneda { get; set; }
        [ForeignKey("medioPago")]
      
        public int idMedioPago { get; set; }
        public double descuento { get; set; }
        public DateTime fechaEmision { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public string docIdentidad { get; set; }
        public string razonSocial { get; set; }
        public int numeroComprobante { get; set; }
        public double igv { get; set; }
        public double montoDetraccion { get; set; }
        public bool esGasto { get; set; }
        public double valorNoGravadas { get; set; }
        public double opGravadas { get; set; }
        public double opExoneradas { get; set; }
        public double total { get; set; }
        public double tipoCambio { get; set; }
        public string numeroDetraccion { get; set; }
        public string fechaDetraccion { get; set; }
        public string fechaNota { get; set; }
        public string serieNota { get; set; }
        public int numeroNota { get; set; }
        public string serie { get; set; }
        public bool anulado { get; set; }
        public int idComprobante { get; set; }
        [ForeignKey("proveedor")]
        public int idProveedor { get; set; }
        public string tipoNota { get; set; }
        public string tipoComprobante { get; set; }
        public string tipoDocIdentidad { get; set; }

        public virtual Proveedor proveedor { get; set; }
        public virtual TipoMoneda moneda { get; set; }
        public virtual MedioPago medioPago { get; set; }
    }
}
