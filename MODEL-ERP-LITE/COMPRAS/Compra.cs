﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{

    [Table("tbl_compra", Schema = "compras")]
    public class Compra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        public bool anulado { get; set; }
        public bool completa { get; set; }
        public bool pagado { get; set; }
        [ForeignKey("tipoComprobante")]
        public int idTipoComprobante { get; set; }
        [ForeignKey("proveedor")]
        public int idProveedor { get; set; }
        [ForeignKey("tipoMoneda")]
        public int idMoneda { get; set; }
        [ForeignKey("tipoPago")]
        public int idTipoPago { get; set; }
        [ForeignKey("medioPago")]
        public int idMedioPago { get; set; }

        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fecha { get; set; }


        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaPago { get; set; }
        public DateTime fechaEmision { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public double cambio { get; set; }
        public double descuentoGlobal { get; set; }
        public double montoTotal { get; set; }
        public double intereses { get; set; }
        public double opExoneradas { get; set; }
        public double igv { get; set; }
        public double montoN { get; set; } 
    public double montoPagado { get; set; }
    public double opGravadas { get; set; }
        public double detraccion{ get; set; }
        public string usuarioCreate { get; set; }
        public string usuarioUpdate { get; set; }
        public string guia { get; set; }
        public string serie { get; set; }
        public int numero{ get; set; }

        public virtual TipoComprobantePago tipoComprobante { get; set; }
        public virtual TipoMoneda tipoMoneda { get; set; }
        public virtual TipoPago tipoPago { get; set; }
        public virtual Proveedor proveedor { get; set; }
        public virtual MedioPago medioPago    { get; set; }

        public virtual List<DetallCompra> detallesCompra { get; set; }
        public virtual List<LetraCompra> letrasCompra { get; set; }
        public virtual List<NotaCompra> notasCompras { get; set; }
    }
}
