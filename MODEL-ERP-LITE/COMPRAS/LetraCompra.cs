﻿using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.COMPRAS
{
    [Table("tbl_letra_compra", Schema = "compras")]
    public   class LetraCompra
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("compra")]
        public int idCompra { get; set; }
        public int numero{ get; set; }
        public bool anulado { get; set; }
        public bool pagado { get; set; }
        public string serieNumero { get; set; }
        public double monto { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaPago { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }

        public virtual Compra compra { get; set; }
    }
}
