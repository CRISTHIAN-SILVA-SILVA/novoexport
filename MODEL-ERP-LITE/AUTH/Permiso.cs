﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.AUTH
{
    [Table("tbl_permiso", Schema = "auth")]
    public class Permiso
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("rol")]
        public int idRol { get; set; }


        [ForeignKey("menu")]
        public int idMenu { get; set; }
        public string usuario { get; set; }
        public string usuarioMod { get; set; }
        public string ip { get; set; }
        public string ipMod { get; set; }

        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaCreacion { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaMod { get; set; }
        public bool anulado { get; set; }
        public virtual Rol rol { get; set; }
        public virtual Menuu menu { get; set; }
    }
}
