﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.AUTH
{
        [Table("tbl_usuario", Schema = "auth")]
        public class Usuario
        {
            [Key]
            [Column("id")]
            public int id { get; set; }
            public string nombres { get; set; }
            public string apellidos { get; set; }
            public string password { get; set; }
            public string antiguaPassword { get; set; }
            public string usuario { get; set; }
        public string cuentaDetraccion { get; set; }
        public string bancoDetraccion { get; set; }
        public string email { get; set; }
        public string usuarioMod { get; set; }
            [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            public DateTime fechaCreacion { get; set; }
            [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
            public DateTime fechaMod { get; set; }
            public string ip { get; set; }
            public string ipMod { get; set; }
            public string dni { get; set; }
            public string userName { get; set; }
            public bool anulado { get; set; }
            [ForeignKey("rol")]
            public int idRol { get; set; }
            public virtual Rol rol { get; set; }
        }
}
