﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.AUTH
{
    [Table("tbl_menu", Schema = "auth")]
    public class Menuu
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("modulo")]
        public int idModulo { get; set; }
        public int idPadre { get; set; }
        public string nombre { get; set; }
        public string icono { get; set; }
        public bool anulado { get; set; }
        public virtual List<Permiso> permisos { get; set; }
        public virtual Modulo modulo { get; set; }
    }
}
