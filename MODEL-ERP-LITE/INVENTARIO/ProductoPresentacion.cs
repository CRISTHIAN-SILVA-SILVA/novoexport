﻿using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_presentacion_producto", Schema = "inventario")]
    public class ProductoPresentacion
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("unidad")]
        public int idUnidadMedida { get; set; }

        [ForeignKey("producto")]
        public int idProducto { get; set; }
        public double factor { get; set; }
        public double precio { get; set; }
        public string nombre { get; set; }
        public string usuarioCreate { get; set; }
        public string usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaCreate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaUpdate { get; set; }
        public bool anulado { get; set; }
        public virtual Producto producto { get; set; }
        public virtual UnidadMedida unidad { get; set; }
        public virtual List<DetalleVenta> detalleVentas { get; set; }
        public virtual List<DetalleNota> detallesnotas { get; set; }
        public virtual List<DetalleTransferenciaProducto> detallesTranferencia{ get; set; }
        public virtual List<DetalleMerma> detallesMermas { get; set; }
        public virtual List<DetallCompra> detallesCompra { get; set; }
        public virtual List<DetalleNotaCompra> detallesNota { get; set; }
    }
}
