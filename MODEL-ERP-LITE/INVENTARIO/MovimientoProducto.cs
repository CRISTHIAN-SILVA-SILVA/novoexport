﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
  
    public class MovimientoProducto
    {

        public int id { get; set; }
        public DateTime fechaCreate { get; set; }
        public String usuarioCreate { get; set; }
        public DateTime fechaUpdate { get; set; }
        public String usuarioUpdate { get; set; }
        public bool esIngreso { get; set; }
        public bool esSalida { get; set; }
        public bool esMerma { get; set; }
        public double total { get; set; }
    }
}
