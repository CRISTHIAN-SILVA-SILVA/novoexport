﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_categoria", Schema = "inventario")]
    public class Categoria
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaUpdate { get; set; }
        public String usuarioUpdate { get; set; }
        public virtual List<Producto> productos
        { get; set; }
    }
}
