﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_detalle_merma", Schema = "inventario")]
    public class DetalleMerma
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("detalleMovimiento")]
        public int idDetalleMovimiento { get; set; }
        public virtual DetalleMovimientoProducto detalleMovimiento { get; set; }
        [ForeignKey("presentacion")]
        public int idPresentacion { get; set; }

        public double cantidad { get; set; }
        public double factor { get; set; }
             
        public int idProducto { get; set; }
        [ForeignKey("merma")]

        public int idMerma { get; set; }
        [ForeignKey("almacen")]
        public int idAlmacen { get; set; }

        public virtual Almacen almacen { get; set; }
        public virtual ProductoPresentacion presentacion { get; set; }
        public virtual Merma merma { get; set; }

    }
}
