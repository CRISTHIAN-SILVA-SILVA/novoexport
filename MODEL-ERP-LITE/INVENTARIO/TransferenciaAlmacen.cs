﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_transferencia_almacen", Schema = "inventario")]
    public class TransferenciaAlmacen
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("almacen")]
        public int idAlmacen { get; set; }
        public virtual Almacen almacen { get; set; }
        [ForeignKey("transferencia")]
        public int idTransferencia { get; set; }
        public bool esOrigen { get; set; }
        public bool esDestino { get; set; }
        public virtual TransferenciaProducto transferencia { get; set; }
    }
}
