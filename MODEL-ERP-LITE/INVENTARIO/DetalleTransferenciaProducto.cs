﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_detalle_transferencia_producto", Schema = "inventario")]
    public class DetalleTransferenciaProducto
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("transferencia")]
        public int idTransferencia { get; set; }
        public virtual TransferenciaProducto transferencia { get; set; }
        [ForeignKey("presentacion")]
        public int idPresentacion { get; set; }
        public int idProducto { get; set; }
        public double total { get; set; }
        public virtual ProductoPresentacion presentacion { get; set; }
        public double cantidad { get; set; }
        public double factor { get; set; }
        public double precio { get; set; }

    }
}
