﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_transferencia_producto", Schema = "inventario")]
    public class TransferenciaProducto
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string placaVehiculo { get; set; }
        public string conductor { get; set; }
        public bool anulado { get; set; }
        public double total { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }
        public String usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        public String usuarioCreate { get; set; }
        public virtual List<DetalleTransferenciaProducto> detalles { get; set; }
        public virtual List<TransferenciaAlmacen> almacenesTransferencia { get; set; }
    }
}
