﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_almacen", Schema = "inventario")]
    public class Almacen
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string encargado { get; set; }
        public bool esPrincipal { get; set; }
        public bool anulado { get; set; }
        public string usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaUpdate { get; set; }
        public virtual List<AlmacenProducto> almacenesProducto { get; set; }
        public virtual List<TransferenciaAlmacen> almacenesTransferencia { get; set; }
        public virtual List<DetalleMerma> detallesMermas { get; set; }
    }
}
