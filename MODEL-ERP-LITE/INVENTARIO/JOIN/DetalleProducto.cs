﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO.JOIN
{
  public  class DetalleProducto
    {
        public int IDPRODUCTO { get; set; }
        public int IDPRESENTACION { get; set; }
        public string PRESENTACION { get; set; }
        public double CANTIDAD_ALMACEN { get; set; }

        public double PRECIO { get; set; }
        public double FACTOR { get; set; }
        public bool ES_EXONERADO { get; set; }

    }
}
