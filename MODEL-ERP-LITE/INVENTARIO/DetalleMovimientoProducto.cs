﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_detalle_movimiento_producto", Schema = "inventario")]
    public class DetalleMovimientoProducto
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public int idAnterior { get; set; }
        public double nuevoStock { get; set; }
        public double cantidad { get; set; }
        [ForeignKey("producto")]
        public int idProducto { get; set; }
        public virtual Producto producto { get; set; }

        public double total{ get; set; }
        public double costoUnitario { get; set; }
        public double nuevoCostoUnitario { get; set; }
        public bool anulado { get; set; }
        public bool esIngreso{ get; set; }
        public DateTime fecha { get; set; }
        public string serie { get; set; }
        public int numero { get; set; }
        public string tbl10 { get; set; }
        public string tbl12 { get; set; }

    }
}
