﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_merma", Schema = "inventario")]
    public class Merma
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string motivo { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaCreate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaUpdate { get; set; }
        public string usuarioCreate { get; set; }
        public string usuarioUpdate { get; set; }
        public double total { get; set; }
        public bool anulado { get; set; }
        public virtual List<DetalleMerma> detalles { get; set; }
    }
}
