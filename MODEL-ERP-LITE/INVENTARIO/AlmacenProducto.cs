﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_almacen_producto", Schema = "inventario")]
    public class AlmacenProducto
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("producto")]
        public int idProducto { get; set; }
        [ForeignKey("almacen")]
        public int idAlmacen { get; set; }
        public double cantidadFisica { get; set; }
        public double cantidadLogica { get; set; }
        public virtual Producto producto { get; set; }
        public virtual Almacen almacen { get; set; }
    }
}
