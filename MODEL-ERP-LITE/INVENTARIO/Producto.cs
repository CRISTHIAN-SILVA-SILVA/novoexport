﻿using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.INVENTARIO
{
    [Table("tbl_producto", Schema = "inventario")]
    public class Producto
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        public double stock { get; set; }
        public double precio { get; set; }
        public double costo{ get; set; }
        public double costoInicial { get; set; }
        public double stockInicial { get; set; }
        public double montoDetraccion { get; set; }
        public double porcentajeDetraccion { get; set; }
        [ForeignKey("categoria")]
        public int idCategoria { get; set; }
        [ForeignKey("marca")]
        public int idMarca { get; set; }
        [ForeignKey("unidad")]
        public int idUnidadMedida { get; set; }
        public string usuarioCreate { get; set; }
        public string usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
        public string codigoBarra { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }
        public bool anulado { get; set; }
        public bool esDetraccion { get; set; }
       public int idMovimiento { get; set; }
        public bool esExonerado { get; set; }
        public virtual Marca marca { get; set; }
        public virtual Categoria categoria { get; set; }
        public virtual UnidadMedida unidad { get; set; }
        public virtual List<ProductoPresentacion> presentaciones { get; set; }
        public virtual List<AlmacenProducto> almacenesProducto { get; set; }
        public virtual List<DetalleMovimientoProducto> detalleMovimientos { get; set; }
    }
}
