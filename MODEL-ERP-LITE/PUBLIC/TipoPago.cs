﻿using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_forma_pago", Schema = "public")]
    public  class TipoPago
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        public bool habilitado { get; set; }
        public virtual List<ComprobantePago> comprobantesPago { get; set; }
        public virtual List<Venta> ventas { get; set; }
    }
}
