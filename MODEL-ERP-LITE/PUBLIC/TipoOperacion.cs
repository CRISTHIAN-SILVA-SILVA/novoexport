﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_tipo_operacion", Schema = "public")]
    public class TipoOperacion
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string descripcion { get; set; }
        public string codigo_sunat { get; set; }
        public bool esPerdida { get; set; }
    }
}
