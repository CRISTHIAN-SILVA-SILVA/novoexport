﻿using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_motivo_translado", Schema = "public")]
    public class MotivoTranslado
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string descripcion { get; set; }
        public string codigo_sunat { get; set; }
        public virtual List<GuiaRemision> guias { get; set; }
    }
}
