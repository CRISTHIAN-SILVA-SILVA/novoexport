﻿using MODEL_ERP_LITE.COMPRAS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_tipo_moneda", Schema = "public")]
    public  class TipoMoneda
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string codigo_sunat { get; set; }
        public string  descripcion { get; set; }
        public string pais_zona { get; set; }
        public bool anulado { get; set; }
        public virtual List<Compra> compras { get; set; }
    }
}
