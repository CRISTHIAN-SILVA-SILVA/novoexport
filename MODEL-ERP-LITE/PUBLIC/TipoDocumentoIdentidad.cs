﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_tipo_documento_identidad", Schema = "public")]
    public  class TipoDocumentoIdentidad
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string descripcion { get; set; }
        public string codigo_sunat { get; set; }
        public string descripcion_abreviada { get; set; }
    }
}
