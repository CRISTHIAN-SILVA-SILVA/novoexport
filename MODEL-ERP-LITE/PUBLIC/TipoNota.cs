﻿using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_tipo_nota", Schema = "public")]
    public  class TipoNota
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string descripcion { get; set; }
        public string codigo_sunat { get; set; }
        public bool esCredito { get; set; }
        public bool esDebito { get; set; }
        public virtual List<Nota> notasCredito { get; set; }
        public virtual List<NotaCompra> notasCompras { get; set; }
    }
}
