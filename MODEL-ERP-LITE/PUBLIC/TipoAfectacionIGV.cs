﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_tipo_afectacion_igv", Schema = "public")]
    public  class TipoAfectacionIGV
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public string codigoTributo { get; set; }
        public string codigoInternacionalTributo { get; set; }
        public string nombreTributo { get; set; }
        public string descripcionTributo { get; set; }
        public bool exonerado{ get; set; }
        public bool gravado{ get; set; }
        public bool inafecto { get; set; }
     
    }
}
