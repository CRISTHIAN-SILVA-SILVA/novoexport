﻿using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_unidad_medida", Schema = "public")]
    public class UnidadMedida
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        public string codigo { get; set; }
        public string codComun { get; set; }
        public virtual List<Producto> productos
        { get; set; }

        public virtual List<ProductoPresentacion> presentaciones
        { get; set; }
    }
}
