﻿using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PUBLIC
{
    [Table("tbl_medio_pago", Schema = "public")]
    public class MedioPago
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        public bool habilitado { get; set; }
        public bool cobroExtra { get; set; }
        public bool esBanco{ get; set; }
        public virtual  List<ComprobantePago> comprobantesPago { get; set; }
        public virtual List<Compra> compras { get; set; }
        public virtual List<MovimientoCaja> movimientoCaja { get; set; }
    }
}
