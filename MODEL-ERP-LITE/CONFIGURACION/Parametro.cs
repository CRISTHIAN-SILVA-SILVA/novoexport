﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CONFIGURACION
{
    [Table("tbl_parametros", Schema = "configuracion")]
    public  class Parametro
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string nombre { get; set; }
        public double valorDouble   { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaUpdate { get; set; }
        public string usuarioUpdate { get; set; }
        public string valorCadena { get; set; }
    }
}
