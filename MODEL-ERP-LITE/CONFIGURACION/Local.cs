﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CONFIGURACION
{
    [Table("tbl_local", Schema = "configuracion")]
    public    class Local
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string razonSocial { get; set; }
        public string cuenta { get; set; }
        public string banco { get; set; }
        public string direccion { get; set; }
        public string nombreComercial { get; set; }
        public string telefono { get; set; }
        public string ruc { get; set; }
        public string email { get; set; }
        public int idTipoContribuyente { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]

        public DateTime fechaUpdate{ get; set; }
        public string usuarioUpdate { get; set; }
    }
}
