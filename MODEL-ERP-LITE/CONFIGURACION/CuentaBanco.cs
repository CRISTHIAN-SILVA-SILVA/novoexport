﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CONFIGURACION
{
    [Table("tbl_cuenta_bancos", Schema = "configuracion")]
    public  class CuentaBanco
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("banco")]
        public int idBanco { get; set; }
        public string cuenta { get; set; }
        public bool anulado { get; set; }
        public virtual Banco banco { get; set; }
    }
}
