﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CONFIGURACION
{
    [Table("tbl_banco", Schema = "configuracion")]
    public class Banco
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string banco { get; set; }
        public bool anulado { get; set; }
    }
}
