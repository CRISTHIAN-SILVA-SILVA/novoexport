﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.EFAC
{
  public  class NotaCreditoEfac
    {
        public string _D = "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2";
        public string _S = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
        public string _B = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
        public string _E = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";

        public List<CreditNote> CreditNote = new List<CreditNote>();
      
    }
}
