﻿using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.EFAC
{//https://ose-gw1.efact.pe:443/api-efact-ose/oauth/token
 //https://ose-gw1.efact.pe:443/api-efact-ose/v1/document
 //https://ose-gw1.efact.pe:443/api-efact-ose/v1/cdr/{ticket}
 //https://ose-gw1.efact.pe:443/api-efact-ose/v1/xml/{ticket}
 //https://ose-gw1.efact.pe:443/api-efact-ose/v1/pdf/{ticket}


    //PRODUCFCCION
    //	https://ose.efact.pe/api-efact-ose/oauth/token
    //https://ose.efact.pe/api-efact-ose/v1/document   
    //https://ose.efact.pe/api-efact-ose/v1/cdr/{ticket}
    //https://ose.efact.pe/api-efact-ose/v1/xml/{ticket}
    //	https://ose.efact.pe/api-efact-ose/v1/pdf/{ticket}
    public class Invoice
    {
        public List<Object> UBLVersionID = new List<object>();
        public List<Object> CustomizationID = new List<object>();

        public List<ID> ID = new List<ID>();
        public List<Object> IssueDate = new List<object>();

        public List<Object> IssueTime = new List<object>();
     //   public List<Object> ReceiptDocumentReference = new List<object>();
        public List<Object> DueDate = new List<object>();

        public List<Object> InvoiceTypeCode = new List<object>();
        public List<Object> Note = new List<object>();
        public List<Object> DocumentCurrencyCode = new List<object>();
        public List<Object> LineCountNumeric = new List<object>();
        public List<Object> Signature = new List<Object>();
        public List<AccountingSupplierParty> AccountingSupplierParty = new List<AccountingSupplierParty>(); 
        public List<AccountingCustomerParty> AccountingCustomerParty = new List<AccountingCustomerParty>();
        public List<PaymentMeans> PaymentMeans = new List<PaymentMeans>();
        public List<TaxTotal> TaxTotal = new List<TaxTotal>();
        public List<LegalMonetaryTotal> LegalMonetaryTotal = new List<LegalMonetaryTotal>();


   

        public List<Object> InvoiceLine= new List<Object>();

        public  string NormalizarCampo(string campo, int numeroMaximoCaracteres)
        {

            int longitudCampo = campo.Length;
            if (longitudCampo > numeroMaximoCaracteres)
            {
                throw new Exception("La longitud del campo es mayor que el numero maximo de caracteres indicados");
            }
            int diferencia = numeroMaximoCaracteres - longitudCampo;
            string caracteresAgregar = "";
            for (int i = 0; i < diferencia; i++)
            {
                caracteresAgregar += "0";
            }
            return caracteresAgregar + campo;
        }
        public Invoice(ComprobantePago c, Venta venta, List<DetalleVenta> detalles, Local local, List<ProductoPresentacion> presentaciones,string codDocIdentidad,string montoLetras,double igv) {
            UBLVersionID UB = new UBLVersionID { IdentifierContent = "2.1" };
            UBLVersionID.Add(UB);
            CustomizationID Cu = new CustomizationID { IdentifierContent = "2.0" };
            CustomizationID.Add(Cu);

            ID ID1 = new ID { IdentifierContent = c.serie +"-"+ NormalizarCampo(c.numero.ToString(),8) };    ID.Add(ID1);              //SERIE-NUMERO
           var IssueDat = new IssueDate { DateContent = c.fechaCreate.ToString("yyyy-MM-dd") };        IssueDate.Add(IssueDat);                             //FECHA EMISION
           var  IssueTim = new IssueTime { DateTimeContent = c.fechaCreate.ToString("hh:mm:ss") };     IssueTime.Add(IssueTim);

            var fechavencimiento = new DueDate { DateContent = c.fechaVencimiento.ToString("yyyy-MM-dd") }; DueDate.Add(fechavencimiento);
            var idPago = new IDCONDICIONPAGO { IdentifierContent= "Condicion de pago", IdentificationSchemeIdentifier="CP"};
            var condicionPago = new ReceiptDocumentReference { };condicionPago.ID.Add(idPago);
    
        //    ReceiptDocumentReference.Add(condicionPago);
            
            //HORA DE EMISION
          var  InvoiceTypeCod = new InvoiceTypeCode {
                CodeContent = c.tipoComprobante.codigo_sunat,
                CodeListNameText = "Tipo de Documento",
                CodeListSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo51",
                CodeListIdentifier="0101",
                CodeNameText= "Tipo de Operacion",
                CodeListUniformResourceIdentifier= "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01",
                CodeListAgencyNameText = "PE:SUNAT"
            };              InvoiceTypeCode.Add(InvoiceTypeCod);                                //TIPO DE DOCUMENTO   

           var Not = new Note { LanguageLocaleIdentifier = "1000",
                TextContent = montoLetras
            };
            Note.Add(Not);
                       //L**************************************EYENDA C 52   --->         FALTA
         var   DocumentCurrencyCod = new DocumentCurrencyCode {
                CodeContent = "PEN",
             CodeListIdentifier = "ISO 4217 Alpha",
             CodeListAgencyNameText = "United Nations Economic Commission for Europe",
              
                CodeListNameText = "Currency"
         };               DocumentCurrencyCode.Add(DocumentCurrencyCod);                                                //TIPO DE MONEDA  
         var   LineCountNumeri = new LineCountNumeric { NumericContent = detalles.Count };                LineCountNumeric.Add(LineCountNumeri);                       //NUMERO DE ITEMS
            var Signatur = new SignatureF();                                                                          //INFORMACION DE LA FIRMA DIGITAL
            Signatur.ID.Add(new ID() { IdentifierContent = "IDSignature" });

           
           var SPF = new SignatoryPartyF();
            var PI = new PartyIdentificationF();
            var IDT = new IDText { TextContent = local.ruc };
            PI.ID.Add(IDT);
            SPF.PartyIdentification.Add(PI);
            var PN = new PartyName();
            var  Nam = new Name { TextContent = local.razonSocial };
            PN.Name.Add(Nam);
            SPF.PartyName.Add(PN);

         
            Signatur.SignatoryParty.Add(SPF);

            var DS = new DigitalSignatureAttachment();
            var EXR = new ExternalReference();
            var U = new Uri {  TextContent= "IDSignature" };
            EXR.URI.Add(U);
            DS.ExternalReference.Add(EXR);
            Signatur.DigitalSignatureAttachment.Add(DS);
 
    Signature.Add(Signatur);



            //INFO DE LA FIRMA DIGITAL
            AccountingSupplierParty AS = new AccountingSupplierParty();
            Party p = new Party();
            PartyIdentification PAI = new PartyIdentification();
            IDE id2 = new IDE
            {
                IdentifierContent = local.ruc,
                IdentificationSchemeIdentifier = "6",
                IdentificationSchemeNameText = "Documento de Identidad",
                IdentificationSchemeAgencyNameText = "PE:SUNAT",
                IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"
            };
            PAI.ID.Add(id2);
           
            PartyLegalEntity PLE = new PartyLegalEntity();
            RegistrationAddress ra = new RegistrationAddress();
            AddressLine al = new AddressLine();
            al.Line.Add(new Line { TextContent = local.direccion });
            ra.AddressLine.Add(al);
            ra.AddressTypeCode.Add(new AddressTypeCode
            {
                CodeContent = "0000",
                CodeListAgencyNameText = "PE:SUNAT",
                CodeListNameText = "Establecimientos anexos"
            });
            ra.CityName.Add(new CityName { TextContent = "PIURA" });
            Country cou = new Country();
            cou.IdentificationCode.Add(new IdentificationCode {
                CodeContent = "PE",
                CodeListIdentifier = "ISO 3166-1",
                CodeListAgencyNameText = "United Nations Economic Commission for Europe",
                CodeListNameText = "Country"
            });
            ra.Country.Add(cou);
            ra.CountrySubentity.Add(new CountrySubentity { TextContent = "PIURA" });
           
            ra.District.Add(new District { TextContent = "PIURA" });
            IDRegistrationAddress id3 = new IDRegistrationAddress
            {
                IdentifierContent = "200101",
                
                IdentificationSchemeAgencyNameText = "PE:INEI",
                IdentificationSchemeNameText = "Ubigeos",
            };
            ra.ID.Add(id3);
            PLE.RegistrationAddress.Add(ra);
            RegistrationName rn = new RegistrationName { TextContent = local.razonSocial };
           
            PLE.RegistrationName.Add(rn);
            p.PartyIdentification.Add(PAI);
            p.PartyLegalEntity.Add(PLE);
            PartyName pn1 = new PartyName();
            pn1.Name.Add(new Name { TextContent = local.nombreComercial });
            p.PartyName.Add(pn1);
            AS.Party.Add(p);
            AccountingSupplierParty.Add(AS);

            AccountingCustomerParty ac = new AccountingCustomerParty();
            PartyC pc = new PartyC();
            PartyName pnc = new PartyName();
            pnc.Name.Add(new Name { TextContent = c.cliente.razonSocial.Replace("&", "Y") });
            pc.PartyName.Add(pnc);
            PartyLegalEntityC pl = new PartyLegalEntityC();
            RegistrationAddressC rc = new RegistrationAddressC();
            AddressLine adc = new AddressLine();
            adc.Line.Add(new Line { TextContent = c.cliente.direccion.Replace("&","Y") });
            rc.AddressLine.Add(adc);

            rc.CityName.Add(new CityName {  TextContent="" });
            Country cc = new Country();
            cc.IdentificationCode.Add(new IdentificationCode {
                CodeContent = "PE",
                CodeListIdentifier = "ISO 3166-1",
                CodeListAgencyNameText = "United Nations Economic Commission for Europe",
                CodeListNameText = "Country"
            });
            rc.Country.Add(cc);
            rc.CountrySubentity.Add(new CountrySubentity {  TextContent=""});
            rc.District.Add(new District {  TextContent=""});
          
            rc.ID.Add(new IDRegistrationAddress {
                IdentifierContent = "200101",
               
                IdentificationSchemeAgencyNameText = "PE:INEI",
                 IdentificationSchemeNameText = "Ubigeos",
            });
            pl.RegistrationAddress.Add(rc);
            RegistrationName rnc = new RegistrationName { TextContent = c.cliente.razonSocial.Replace("&", "Y") };
            
            pl.RegistrationName.Add(rnc);
            pc.PartyLegalEntity.Add(pl);
            PartyIdentification pic = new PartyIdentification();
            string docCliente = "";
            if (codDocIdentidad == "6")
                docCliente = c.cliente.ruc;
            else docCliente = c.cliente.dniRepresentante;
            pic.ID.Add(new IDE {
                IdentifierContent = docCliente,
                IdentificationSchemeIdentifier = codDocIdentidad,
                IdentificationSchemeNameText = "Documento de Identidad",
                IdentificationSchemeAgencyNameText = "PE:SUNAT",
                IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"
            });
            pc.PartyIdentification.Add(pic);
            Contact ct = new Contact();
            ct.ElectronicMail.Add(new ElectronicMail { TextContent = c.cliente.email });
            pc.Contact.Add(ct);
            ac.Party.Add(pc);
            AccountingCustomerParty.Add(ac);

            //INFORMACION DEL CLIENTE

           
            TaxTotal tx = new TaxTotal();
            tx.TaxAmount.Add(new TaxAmount {
                AmountContent = string.Format("{0:0.00}", venta.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            TaxSubtotal tt = new TaxSubtotal();
            TaxableAmount tta = new TaxableAmount();
           
            tt.TaxableAmount.Add(new TaxableAmount {
                AmountContent = string.Format("{0:0.00}", c.montoTotal - venta.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            tt.TaxAmount.Add(new TaxAmount {
                AmountContent = string.Format("{0:0.00}", venta.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            TaxCategory tc = new TaxCategory();
            TaxScheme ts = new TaxScheme();
            ts.ID.Add(new IDTaxScheme {
                IdentifierContent = "1000",
                IdentificationSchemeNameText = "Codigo de tributos",
                IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05",
                IdentificationSchemeAgencyNameText = "PE:SUNAT"
            });
            ts.Name.Add(new Name { TextContent = "IGV" });
            ts.TaxTypeCode.Add(new TaxTypeCode { CodeContent = "VAT" });
            tc.TaxScheme.Add(ts);
            tt.TaxCategory.Add(tc);
            tx.TaxSubtotal.Add(tt);
            TaxTotal.Add(tx);

            //TOTALES E IMPUESTOS

            LegalMonetaryTotal lmt = new LegalMonetaryTotal();
            lmt.LineExtensionAmount.Add(new LineExtensionAmount {
                AmountContent = string.Format("{0:0.00}", c.montoTotal - venta.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            lmt.PayableAmount.Add(new PayableAmount {
                AmountContent = string.Format("{0:0.00}", c.montoTotal),
                AmountCurrencyIdentifier = "PEN"

            });
            lmt.TaxInclusiveAmount.Add(new TaxInclusiveAmount {
                AmountContent = string.Format("{0:0.00}", c.montoTotal),
                AmountCurrencyIdentifier = "PEN"
            });
                
            LegalMonetaryTotal.Add(lmt);

      //      DocumentTypeCode d;                                                        //TOTALES


            InvoiceLineD i;int index = 0;                                                                               
            ProductoPresentacion pp = null;

            String codDetraccion = "";
            string porcentajeDetraccion = "";

        


            foreach (var x in detalles) {
              
                foreach (var pro in presentaciones)
                    if (x.idProductoPresentacion == presentaciones[index].id)
                        pp = presentaciones[index];
                index++;

                porcentajeDetraccion =string.Format("{0:0.00}", pp.producto.porcentajeDetraccion);
                codDetraccion = pp.producto.codigoBarra;
                i = new InvoiceLineD();
                i.ID.Add(new IDEntero {
                    IdentifierContent = index
                });
                i.Note.Add(new NoteInvoiceLine { TextContent = pp.unidad.nombre, });
                i.InvoicedQuantity.Add(new InvoicedQuantity {
                    QuantityContent = x.cantidad.ToString(), ///-------------------------------------------------------------------------------------------PREGUNTA
                    QuantityUnitCode = pp.unidad.codigo,
                    QuantityUnitCodeListIdentifier = "UN/ECE rec 20",
                    QuantityUnitCodeListAgencyNameText = "United Nations Economic Commission for Europe",
            
                });
                i.LineExtensionAmount.Add(new LineExtensionAmount {
                    AmountContent = String.Format("{0:0.00}", x.total-x.igv),
                    AmountCurrencyIdentifier = "PEN"
                }
                );
                PricingReference ppr = new PricingReference();
                AlternativeConditionPrice alp = new AlternativeConditionPrice();
                alp.PriceAmount.Add(new PriceAmount {
                    AmountContent = String.Format("{0:0.00}", x.precio),
                    AmountCurrencyIdentifier = "PEN"
                });
                alp.PriceTypeCode.Add(new PriceTypeCode { CodeContent="01",
                    CodeListNameText = "Tipo de Precio",
                    CodeListAgencyNameText = "PE:SUNAT",
                    CodeListUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16"
                });
                ppr.AlternativeConditionPrice.Add(alp);
                i.PricingReference.Add(ppr);
                TaxTotalImpuesto txi = new TaxTotalImpuesto();
                
                txi.TaxAmount.Add(new TaxAmount {
                    AmountContent = string.Format("{0:0.00}", x.igv),
                    AmountCurrencyIdentifier = "PEN"
                });
                
                TaxSubtotalImpuesto tsi = new TaxSubtotalImpuesto();
                tsi.TaxableAmount.Add(new TaxableAmount { AmountContent = String.Format("{0:0.00}",x.total-x.igv), AmountCurrencyIdentifier = "PEN" });
                tsi.TaxAmount.Add(new TaxAmount {
                    AmountContent = String.Format("{0:0.00}", x.igv),
                    AmountCurrencyIdentifier = "PEN"
                });
                TaxCategoryImpuesto tci = new TaxCategoryImpuesto();
                tci.Percent.Add(new Percent {  NumericContent=igv*100});
                tci.TaxExemptionReasonCode.Add(new TaxExemptionReasonCode {
                    CodeContent = "10",
                    CodeListAgencyNameText = "PE:SUNAT",
                    CodeListNameText = "Afectacion del IGV",
                    CodeListUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"
                });
                TaxSchemeI thi = new TaxSchemeI();
                thi.ID.Add(new IDTaxScheme {
                    IdentifierContent = "1000",
                    IdentificationSchemeNameText = "Codigo de tributos",
                    IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05",
                    IdentificationSchemeAgencyNameText = "PE:SUNAT",
                });
                thi.Name.Add(new Name { TextContent = "IGV", });
                thi.TaxTypeCode.Add(new TaxTypeCode { CodeContent = "VAT" });
                tci.TaxScheme.Add( thi);
                tsi.TaxCategory.Add(tci);
                txi.TaxSubtotal.Add(tsi);
                i.TaxTotal.Add(txi);
                Item it = new Item();
                it.Description.Add(new Description { TextContent = pp.nombre });
                SellersItemIdentification ist = new SellersItemIdentification();
                ist.ID.Add(new ID {
                    IdentifierContent =NormalizarCampo( pp.idProducto.ToString(),4),
                });
                it.SellersItemIdentification.Add(ist);
                i.Item.Add(it);
                Price pri = new Price();
                pri.PriceAmount.Add(new PriceAmount
                {
                    AmountContent = string.Format("{0:0.00}", x.precio/(igv+1)),
                    AmountCurrencyIdentifier = "PEN",
                });
                i.Price.Add(pri);
                                                                                                 
                InvoiceLine.Add(i);
            }

            /*    if (c.montoDetraccion>0||c.generaDetraccion)
        {
            PaymentMeans pay = new PaymentMeans();
            pay.PayeeFinancialAccount.ID.IdentifierContent =local.cuenta;
            pay.PaymentTerms.PaymentPercent.NumericContent = porcentajeDetraccion;
            pay.PaymentTerms.PaymentMeansID.IdentifierContent = codDetraccion;
            PaymentMeans.Add(pay);
    var notaDetraccion = new Notaa
                    {
                        TextContent = "OPERACION SUJETA AL SISTEMA DE PAGOS DE OBLIGACIONES TRIBUTARIAS SPOT, N° DE CTA BANCO DE LA NACION 00631-180094"
                    };
                    Note.Add(notaDetraccion);
                }*/
        }
    }
    public class PaymentMeans
    {
        public PaymentMeansCode PaymentMeansCode = new PaymentMeansCode();
        public PayeeFinancialAccount PayeeFinancialAccount = new PayeeFinancialAccount();
        public PaymentTerms PaymentTerms = new PaymentTerms(); }
    public class PaymentMeansCode
    {
        public string CodeContent = "001";
        public string CodeListAgencyNameText = "PE:SUNAT";
        public string CodeListNameText = "Medio de pago";
        public string CodeListUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo59";
    } 
    public class PayeeFinancialAccount
    {
        public ID ID = new ID() { IdentifierContent = "", };
    }
    public class PaymentTerms
    {
       public PaymentMeansID PaymentMeansID = new PaymentMeansID();
      public  PaymentPercent PaymentPercent = new PaymentPercent();
    }
    public class PaymentMeansID
    {
        public string IdentifierContent  { get;set; }
        public string IdentificationSchemeNameText = "Codigo de detraccion";
        public string IdentificationSchemeAgencyNameText = "PE:SUNAT";
        public string IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo54"  ;
    }
    public class PaymentPercent { public string NumericContent { get; set; } }



    public class UBLVersionID { public string IdentifierContent { get; set; } }
    public class CustomizationID{ public string IdentifierContent { get; set; } }
    //SERIE-NUMERO FACTURA 
    public class ID { public string IdentifierContent { get; set; } }
    public class IDText { public string TextContent { get; set; } }
    public class IDEntero { public int IdentifierContent { get; set; } }
    //FECHA EMISION 2019-02-11
    public class IssueDate { public string DateContent { get; set; } }
    //HORA EMISION 16:58:51
    public class IssueTime { public string DateTimeContent { get; set; } }
    public class ProfileID { public string IdentifierContent { get; set; }
        public string IdentificationSchemeNameText { get; set; }
        public string IdentificationSchemeAgencyNameText { get; set; }
        public string IdentificationSchemeUniformResourceIdentifier { get; set; }
      //  public string urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo17 { get; set; }
    }
    public class InvoiceTypeCode {
        public string CodeContent { get; set; }
        public string CodeListNameText { get; set; }
        public string CodeListSchemeUniformResourceIdentifier { get; set; }
        public string CodeListIdentifier { get; set; }
        public string CodeNameText { get; set; }
        public string CodeListUniformResourceIdentifier { get; set; }
        public string CodeListAgencyNameText { get; set; }
    }
    public class Note
    {
        public string TextContent { get; set; }
        public string LanguageLocaleIdentifier { get; set; }
    }
    public class Notaa
    {
        public string TextContent { get; set; }
    }
    public class DocumentCurrencyCode {
        public string CodeContent { get; set; }
        public string CodeListIdentifier { get; set; }
        public string CodeListNameText { get; set; }
        public string CodeListAgencyNameText { get; set; }
    }
    //numero de items
    public class LineCountNumeric { public int NumericContent { get; set; } }
    public class Signature {
        public List<IDSignature> ID =new List<IDSignature>();
        public List<SignatoryParty> SignatoryParty = new List<SignatoryParty>();
    }
    public class SignatureF
    {
        public List<ID> ID = new List<ID>();
        public List<SignatoryPartyF> SignatoryParty = new List<SignatoryPartyF>();
        public List<DigitalSignatureAttachment> DigitalSignatureAttachment = new List<DigitalSignatureAttachment>();
    }

    public class IDSignature { public string IdentifierContent { get; set; } }
    public class SignatoryPartyF {
        public List<PartyIdentificationF> PartyIdentification = new List<PartyIdentificationF>();
        public List<PartyName> PartyName = new List<PartyName>();
    }
    public class SignatoryParty
    {
        public List<PartyIdentification> PartyIdentification = new List<PartyIdentification>();
        public List<PartyName> PartyName = new List<PartyName>();
    }
    //RUC EMISOR
    public class PartyIdentificationF { public List<IDText> ID = new List<IDText>(); }
    //   public class IDPartyIdentification { public string TextContent { get; set; } }
    //EMISOR
    //  public class PartyName { public Name name { get; set; } }
    //  public class Name { public string TextContent { get; set; } }
    public class IDET
    {
        public string IdentifierContent { get; set; }
        public string IdentificationSchemeIdentifier { get; set; }
      //  public string IdentificationSchemeNameText { get; set; }
        public string IdentificationSchemeAgencyNameText { get; set; }
        //    public string IdentificationSchemeUniformResourceIdentifier { get; set; }
    }
    public class DueDate {
        public string DateContent { get; set; }
    }
    public class ReceiptDocumentReference {
        public List<IDCONDICIONPAGO> ID = new List<IDCONDICIONPAGO>();
    }
    public class IDCONDICIONPAGO {
        public string IdentifierContent { get; set; }
        public string IdentificationSchemeIdentifier { get; set; }
    }
    public class CompanyID
    {
        public string IdentifierContent { get; set; }
        public string IdentificationSchemeIdentifier { get; set; }
        public string IdentificationSchemeNameText { get; set; }
        public string IdentificationSchemeAgencyNameText { get; set; }
        public string IdentificationSchemeUniformResourceIdentifier { get; set; }
    }

    public class DigitalSignatureAttachment { public List<ExternalReference> ExternalReference = new List<ExternalReference>(); }
    public class ExternalReference { public List<Uri> URI = new List<Uri>(); }
    //Idignature
    public class Uri { public string TextContent { get; set; } }

    //INFORMACION EMISOR
    public class AccountingSupplierParty { public List<Party> Party = new List<Party>(); }
        public class Party {
        public List<PartyIdentification> PartyIdentification = new List<PartyIdentification>();
        public List<PartyName> PartyName = new List<PartyName>();
        //  public CompanyID CompanyID{ get; set; }
        public List<PartyLegalEntity> PartyLegalEntity = new List<PartyLegalEntity>();
            }

   

    public class PartyIdentification { public List<IDE> ID = new List<IDE>(); }
                public class IDE {
                    public string IdentifierContent { get; set; }
                    public string IdentificationSchemeIdentifier { get; set; }
                    public string IdentificationSchemeNameText { get; set; }
                    public string IdentificationSchemeAgencyNameText { get; set; }
                    public string IdentificationSchemeUniformResourceIdentifier { get; set; }
                    }
            public class PartyName { public List<Name> Name = new List<Name>(); }
                public class Name { public string TextContent { get; set; } }
            public class PartyLegalEntity {
        public List<RegistrationName> RegistrationName = new List<RegistrationName>();
        public List<RegistrationAddress> RegistrationAddress = new List<RegistrationAddress>(); }
    public class PartyLegalEntityC
    {
        public List<RegistrationName> RegistrationName = new List<RegistrationName>();
        public List<RegistrationAddressC> RegistrationAddress = new List<RegistrationAddressC>();
    }
    public class RegistrationName { public string TextContent { get; set; } }
                public class RegistrationAddress {
        public List<IDRegistrationAddress> ID = new List<IDRegistrationAddress>();
        public List<AddressTypeCode> AddressTypeCode = new List<AddressTypeCode>();
        public List<CityName> CityName = new List<CityName>();
        public List<CountrySubentity> CountrySubentity = new List<CountrySubentity>();
        public List<District> District = new List<District>();
        public List<AddressLine> AddressLine = new List<AddressLine>();
        public List<Country> Country = new List<Country>();
                }
    public class RegistrationAddressC
    {
        public List<IDRegistrationAddress> ID = new List<IDRegistrationAddress>();
        public List<CityName> CityName = new List<CityName>();
        public List<CountrySubentity> CountrySubentity = new List<CountrySubentity>();
        public List<District> District = new List<District>();
        public List<AddressLine> AddressLine = new List<AddressLine>();
        public List<Country> Country = new List<Country>();
    }
    public class IDRegistrationAddress {
                        public string IdentifierContent { get; set; }
                        public string IdentificationSchemeAgencyNameText { get; set; }
                        public string IdentificationSchemeNameText { get; set; }
                        }
                    public class AddressTypeCode {
                        public string CodeContent { get; set; }
                        public string CodeListAgencyNameText { get; set; }
                        public string CodeListNameText { get; set; } }
                    public class CityName { public string TextContent { get; set; } }
                    public class CountrySubentity { public string TextContent { get; set; } }
                    public class District { public string TextContent { get; set; } }
                    public class AddressLine { public List<Line> Line = new List<Line>(); }
                    public class Line { public string TextContent { get; set; } }
                    public class Country { public List<IdentificationCode> IdentificationCode = new List<IdentificationCode>(); }
                        public class IdentificationCode {
                            public string CodeContent { get; set; }
                            public string CodeListIdentifier { get; set; }
                            public string CodeListAgencyNameText { get; set; }
                            public string CodeListNameText { get; set; }}
    //INFORMACION CLIENTE
    public class AccountingCustomerParty { public List<PartyC> Party = new List<PartyC>(); }
        public class PartyC
    {
        public List<PartyIdentification> PartyIdentification = new List<PartyIdentification>();
        public List<PartyName> PartyName = new List<PartyName>();
        public List<PartyLegalEntityC> PartyLegalEntity = new List<PartyLegalEntityC>();
        public List<Contact> Contact = new List<Contact>();
    }
            public class Contact { public List<ElectronicMail> ElectronicMail = new List<ElectronicMail>(); }
                public class ElectronicMail { public string TextContent { get; set; } }

    //TOTALES
    public class TaxTotal { public List<TaxAmount> TaxAmount = new List<TaxAmount>(); public List<TaxSubtotal> TaxSubtotal = new List<TaxSubtotal>(); }
        public class TaxAmount {
            public string AmountContent { get; set; }
            public string AmountCurrencyIdentifier { get; set; }}//PEN 
        public class TaxSubtotal {
        public List<TaxableAmount> TaxableAmount = new List<TaxableAmount>();
        public List<TaxAmount> TaxAmount = new List<TaxAmount>();
        public List<TaxCategory> TaxCategory = new List<TaxCategory>();
    }
            public class TaxableAmount { public string AmountContent { get; set; } public string AmountCurrencyIdentifier { get; set; } }
            public class TaxCategory { public List<TaxScheme> TaxScheme = new List<TaxScheme>(); }
                public class TaxScheme
            {
        public List<IDTaxScheme> ID = new List<IDTaxScheme>();
        public List<Name> Name = new List<Name>();
        public List<TaxTypeCode> TaxTypeCode = new List<TaxTypeCode>();
            }
    public class TaxSchemeI
    {
        public List<IDTaxScheme> ID = new List<IDTaxScheme>();
        public List<Name> Name = new List<Name>();
        public List<TaxTypeCode> TaxTypeCode = new List<TaxTypeCode>();
    }
    public class IDTaxScheme {
        public string IdentifierContent { get; set; }
        public string IdentificationSchemeNameText { get; set; }
        public string IdentificationSchemeUniformResourceIdentifier { get; set; }
        public string IdentificationSchemeAgencyNameText { get; set; }
    }
    public class IDTaxSchemeI
    {
        public string IdentifierContent { get; set; }
        public string IdentificationSchemeNameText { get; set; }
        public string IdentificationSchemeIdentifier { get; set; }
        public string IdentificationSchemeAgencyNameText { get; set; }
    }
    public class TaxTypeCode { public string CodeContent { get; set; } }

    public class LegalMonetaryTotal {
        public List<LineExtensionAmount> LineExtensionAmount = new List<LineExtensionAmount>();
        public List<TaxInclusiveAmount> TaxInclusiveAmount = new List<TaxInclusiveAmount>();
        public List<PayableAmount> PayableAmount = new List<PayableAmount>(); }
        public class LineExtensionAmount { public string AmountContent { get; set; } public string AmountCurrencyIdentifier { get; set; } }
        public class TaxInclusiveAmount { public string AmountContent { get; set; } public string AmountCurrencyIdentifier { get; set; } }
        public class PayableAmount { public string AmountContent { get; set; } public string AmountCurrencyIdentifier { get; set; } }

    public class InvoiceLineD {
        public List<IDEntero> ID = new List<IDEntero>();
        public List<NoteInvoiceLine> Note = new List<NoteInvoiceLine>();
        public List<InvoicedQuantity> InvoicedQuantity = new List<InvoicedQuantity>();
        public List<LineExtensionAmount> LineExtensionAmount = new List<LineExtensionAmount>();
        public List<PricingReference> PricingReference = new List<PricingReference>();
        public List<TaxTotalImpuesto> TaxTotal = new List<TaxTotalImpuesto>();
        public List<Item> Item = new List<Item>();
        public List<Price> Price = new List<Price>();
    }
        public class NoteInvoiceLine { public string TextContent { get; set; } }
        public class InvoicedQuantity {
            public string QuantityContent { get; set; }
            public string QuantityUnitCode { get; set; }
            public string QuantityUnitCodeListIdentifier { get; set; }
            public string QuantityUnitCodeListAgencyNameText { get; set; }
        }
        public class PricingReference { public List<AlternativeConditionPrice> AlternativeConditionPrice = new List<AlternativeConditionPrice>(); }
            public class AlternativeConditionPrice { public List<PriceAmount> PriceAmount = new List<PriceAmount>(); public List<PriceTypeCode> PriceTypeCode = new List<PriceTypeCode>(); }
                public class PriceAmount { public string AmountContent { get; set; } public string AmountCurrencyIdentifier { get; set; } }
                public class PriceTypeCode {
                    public string CodeContent { get; set; }
                    public string CodeListNameText { get; set; }
                    public string CodeListAgencyNameText { get; set; }
                    public string CodeListUniformResourceIdentifier { get; set; }
                }

    public class TaxTotalImpuesto {
        public List<TaxAmount> TaxAmount = new List<TaxAmount>();
        public List<TaxSubtotalImpuesto> TaxSubtotal = new List<TaxSubtotalImpuesto>();

    }
    public class TaxSubtotalImpuesto {
        public List<TaxableAmount> TaxableAmount = new List<TaxableAmount>();
        public List<TaxAmount> TaxAmount = new List<TaxAmount>();
        public List<TaxCategoryImpuesto> TaxCategory = new List<TaxCategoryImpuesto>();
    }

    public class TaxCategoryImpuesto {
        public List<Percent> Percent = new List<Percent>();
        public List<TaxExemptionReasonCode> TaxExemptionReasonCode = new List<TaxExemptionReasonCode>();
        public List<TaxSchemeI> TaxScheme = new List<TaxSchemeI>();
    }
    public class Percent { public double NumericContent { get; set; } }

    public class TaxExemptionReasonCode { public string CodeContent { get; set; }
        public string CodeListAgencyNameText { get; set; }
        public string CodeListNameText { get; set; }
        public string CodeListUniformResourceIdentifier { get; set; }
    }
    public class Item {
        public List<Description> Description = new List<Description>();
        public List<SellersItemIdentification> SellersItemIdentification = new List<SellersItemIdentification>();
    }
    public class Description { public string TextContent { get; set; } }
    public class SellersItemIdentification { public List<ID> ID = new List<ID>(); }
    public class Price { public List<PriceAmount> PriceAmount = new List<PriceAmount>(); }
}
