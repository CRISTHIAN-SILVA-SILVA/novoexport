﻿using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.EFAC
{
   public class CreditNote
    {
        public List<Object> UBLVersionID = new List<object>();
        public List<Object> CustomizationID = new List<object>();

        public List<ID> ID = new List<ID>();
        public List<Object> IssueDate = new List<object>();
        public List<Object> IssueTime = new List<object>();
    //    public List<Object> InvoiceTypeCode = new List<object>();
        public List<Object> Note = new List<object>();
        public List<Object> DocumentCurrencyCode = new List<object>();
        public List<Object> DiscrepancyResponse = new List<object>();
        public List<Object> BillingReference = new List<object>();

      //  public List<Object> LineCountNumeric = new List<object>();
        public List<Object> Signature = new List<Object>();
        public List<AccountingSupplierParty> AccountingSupplierParty = new List<AccountingSupplierParty>();
        public List<AccountingCustomerParty> AccountingCustomerParty = new List<AccountingCustomerParty>();
        public List<TaxTotal> TaxTotal = new List<TaxTotal>();
        public List<LegalMonetaryTotal> LegalMonetaryTotal = new List<LegalMonetaryTotal>();
        public List<Object> CreditNoteLine = new List<Object>();
        public string NormalizarCampo(string campo, int numeroMaximoCaracteres)
        {

            int longitudCampo = campo.Length;
            if (longitudCampo > numeroMaximoCaracteres)
            {
                throw new Exception("La longitud del campo es mayor que el numero maximo de caracteres indicados");
            }
            int diferencia = numeroMaximoCaracteres - longitudCampo;
            string caracteresAgregar = "";
            for (int i = 0; i < diferencia; i++)
            {
                caracteresAgregar += "0";
            }
            return caracteresAgregar + campo;
        }
        public CreditNote(Nota c,  Local local, List<ProductoPresentacion> presentaciones, string codDocIdentidad, string montoLetras, double igv,string tipoComprobanteR)
        {
            UBLVersionID UB = new UBLVersionID { IdentifierContent = "2.1" };
            UBLVersionID.Add(UB);
            CustomizationID Cu = new CustomizationID { IdentifierContent = "2.0" };
            CustomizationID.Add(Cu);

            ID ID1 = new ID { IdentifierContent = c.serie + "-" + NormalizarCampo(c.numero.ToString(), 8) }; ID.Add(ID1);              //SERIE-NUMERO
            var IssueDat = new IssueDate { DateContent = c.fechaCreate.ToString("yyyy-MM-dd") }; IssueDate.Add(IssueDat);                             //FECHA EMISION
            var IssueTim = new IssueTime { DateTimeContent = c.fechaCreate.ToString("hh:mm:ss") }; IssueTime.Add(IssueTim);                      //HORA DE EMISION
                                   //TIPO DE DOCUMENTO
            var Not = new Note
            {
                LanguageLocaleIdentifier = "1000",
                TextContent = montoLetras
            }; Note.Add(Not);            
            
            //L**************************************EYENDA C 52   --->         FALTA
            var DocumentCurrencyCod = new DocumentCurrencyCode
            {
                CodeContent = "PEN",
                CodeListIdentifier = "ISO 4217 Alpha",
                CodeListAgencyNameText = "United Nations Economic Commission for Europe",

                CodeListNameText = "Currency"
            }; DocumentCurrencyCode.Add(DocumentCurrencyCod);                                                //TIPO DE MONEDA  

            var dis = new DiscrepancyResponse {};
            var des =new Description{  TextContent=c.tipoNota.descripcion  };
            var responseCode = new ResponseCode { CodeContent = c.tipoNota.codigo_sunat };
            dis.Description.Add(des);
            dis.ResponseCode.Add(responseCode);
            DiscrepancyResponse.Add(dis);

            var bill = new BillingReference { };
            var doc = new InvoiceDocumentReference {  };
            var id5 = new ID {  IdentifierContent=c.comprobante.serie+"-"+NormalizarCampo(c.comprobante.numero.ToString(),8)};
            var fechaR = new IssueDate { DateContent = c.comprobante.fechaCreate.ToString("yyyy-MM-dd") };
            var dp = new DocumentTypeCode {  };
            dp.CodeContent = tipoComprobanteR;
            doc.ID.Add(id5);
            doc.IssueDate.Add(fechaR);
            doc.DocumentTypeCode.Add(dp);
            bill.InvoiceDocumentReference.Add(doc);
            BillingReference.Add(bill);




         //   var LineCountNumeri = new LineCountNumeric { NumericContent = detalles.Count }; LineCountNumeric.Add(LineCountNumeri);                       //NUMERO DE ITEMS
            var Signatur = new SignatureF();                                                                          //INFORMACION DE LA FIRMA DIGITAL
            Signatur.ID.Add(new ID() { IdentifierContent = "IDSignature" });


            var SPF = new SignatoryPartyF();
            var PI = new PartyIdentificationF();
            var IDT = new IDText { TextContent = local.ruc };
            PI.ID.Add(IDT);
            SPF.PartyIdentification.Add(PI);
            var PN = new PartyName();
            var Nam = new Name { TextContent = local.razonSocial };
            PN.Name.Add(Nam);
            SPF.PartyName.Add(PN);


            Signatur.SignatoryParty.Add(SPF);



            var DS = new DigitalSignatureAttachment();
            var EXR = new ExternalReference();
            var U = new Uri { TextContent = "IDSignature" };
            EXR.URI.Add(U);
            DS.ExternalReference.Add(EXR);
            Signatur.DigitalSignatureAttachment.Add(DS);

            Signature.Add(Signatur);

 
            //INFO DE LA FIRMA DIGITAL
            AccountingSupplierParty AS = new AccountingSupplierParty();
            Party p = new Party();
            PartyIdentification PAI = new PartyIdentification();
            IDE id2 = new IDE
            {
                IdentifierContent = local.ruc,
                IdentificationSchemeIdentifier = "6",
                IdentificationSchemeNameText = "Documento de Identidad",
                IdentificationSchemeAgencyNameText = "PE:SUNAT",
                IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"
            };
            PAI.ID.Add(id2);

            PartyLegalEntity PLE = new PartyLegalEntity();
            RegistrationAddress ra = new RegistrationAddress();
            AddressLine al = new AddressLine();
            al.Line.Add(new Line { TextContent = local.direccion });
            ra.AddressLine.Add(al);
            ra.AddressTypeCode.Add(new AddressTypeCode
            {
                CodeContent = "0000",
                CodeListAgencyNameText = "PE:SUNAT",
                CodeListNameText = "Establecimientos anexos"
            });
            ra.CityName.Add(new CityName { TextContent = "PIURA" });
            Country cou = new Country();
            cou.IdentificationCode.Add(new IdentificationCode
            {
                CodeContent = "PE",
                CodeListIdentifier = "ISO 3166-1",
                CodeListAgencyNameText = "United Nations Economic Commission for Europe",
                CodeListNameText = "Country"
            });
            ra.Country.Add(cou);
            ra.CountrySubentity.Add(new CountrySubentity { TextContent = "PIURA" });

            ra.District.Add(new District { TextContent = "PIURA" });
            IDRegistrationAddress id3 = new IDRegistrationAddress
            {
                IdentifierContent = "200101",

                IdentificationSchemeAgencyNameText = "PE:INEI",
                IdentificationSchemeNameText = "Ubigeos",
            };
            ra.ID.Add(id3);
            PLE.RegistrationAddress.Add(ra);
            RegistrationName rn = new RegistrationName { TextContent = local.razonSocial };

            PLE.RegistrationName.Add(rn);
            p.PartyIdentification.Add(PAI);
            p.PartyLegalEntity.Add(PLE);
            PartyName pn1 = new PartyName();
            pn1.Name.Add(new Name { TextContent = local.nombreComercial });
            p.PartyName.Add(pn1);
            AS.Party.Add(p);
            AccountingSupplierParty.Add(AS);

            AccountingCustomerParty ac = new AccountingCustomerParty();
            PartyC pc = new PartyC();
            PartyName pnc = new PartyName();
            pnc.Name.Add(new Name { TextContent = c.cliente.razonSocial.Replace("&", "Y") });
            pc.PartyName.Add(pnc);
            PartyLegalEntityC pl = new PartyLegalEntityC();
            RegistrationAddressC rc = new RegistrationAddressC();
            AddressLine adc = new AddressLine();
            adc.Line.Add(new Line { TextContent = c.cliente.direccion.Replace("&", "Y") });
            rc.AddressLine.Add(adc);

            rc.CityName.Add(new CityName { TextContent = "" });
            Country cc = new Country();
            cc.IdentificationCode.Add(new IdentificationCode
            {
                CodeContent = "PE",
                CodeListIdentifier = "ISO 3166-1",
                CodeListAgencyNameText = "United Nations Economic Commission for Europe",
                CodeListNameText = "Country"
            });
            rc.Country.Add(cc);
            rc.CountrySubentity.Add(new CountrySubentity { TextContent = "" });
            rc.District.Add(new District { TextContent = "" });

            rc.ID.Add(new IDRegistrationAddress
            {
                IdentifierContent = "200101",

                IdentificationSchemeAgencyNameText = "PE:INEI",
                IdentificationSchemeNameText = "Ubigeos",
            });
            pl.RegistrationAddress.Add(rc);
            RegistrationName rnc = new RegistrationName { TextContent = c.cliente.razonSocial.Replace("&", "Y") };

            pl.RegistrationName.Add(rnc);
            pc.PartyLegalEntity.Add(pl);
            PartyIdentification pic = new PartyIdentification();
            pic.ID.Add(new IDE
            {
                IdentifierContent = c.cliente.ruc,
                IdentificationSchemeIdentifier = codDocIdentidad,
                IdentificationSchemeNameText = "Documento de Identidad",
                IdentificationSchemeAgencyNameText = "PE:SUNAT",
                IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06"
            });
            pc.PartyIdentification.Add(pic);
            Contact ct = new Contact();
            ct.ElectronicMail.Add(new ElectronicMail { TextContent = c.cliente.email });
            pc.Contact.Add(ct);
            ac.Party.Add(pc);
            AccountingCustomerParty.Add(ac);

            //INFORMACION DEL CLIENTE

            TaxTotal tx = new TaxTotal();
            tx.TaxAmount.Add(new TaxAmount
            {
                AmountContent = string.Format("{0:0.00}", c.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            TaxSubtotal tt = new TaxSubtotal();
            TaxableAmount tta = new TaxableAmount();

            tt.TaxableAmount.Add(new TaxableAmount
            {
                AmountContent = string.Format("{0:0.00}", c.total - c.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            tt.TaxAmount.Add(new TaxAmount
            {
                AmountContent = string.Format("{0:0.00}", c.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            TaxCategory tc = new TaxCategory();
            TaxScheme ts = new TaxScheme();
            ts.ID.Add(new IDTaxScheme
            {
                IdentifierContent = "1000",
                IdentificationSchemeNameText = "Codigo de tributos",
                IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05",
                IdentificationSchemeAgencyNameText = "PE:SUNAT"
            });
            ts.Name.Add(new Name { TextContent = "IGV" });
            ts.TaxTypeCode.Add(new TaxTypeCode { CodeContent = "VAT" });
            tc.TaxScheme.Add(ts);
            tt.TaxCategory.Add(tc);
            tx.TaxSubtotal.Add(tt);
            TaxTotal.Add(tx);

            //TOTALES E IMPUESTOS

            LegalMonetaryTotal lmt = new LegalMonetaryTotal();
            lmt.LineExtensionAmount.Add(new LineExtensionAmount
            {
                AmountContent = string.Format("{0:0.00}", c.total - c.igv),
                AmountCurrencyIdentifier = "PEN"
            });
            lmt.PayableAmount.Add(new PayableAmount
            {
                AmountContent = string.Format("{0:0.00}", c.total),
                AmountCurrencyIdentifier = "PEN"

            });
            lmt.TaxInclusiveAmount.Add(new TaxInclusiveAmount
            {
                AmountContent = string.Format("{0:0.00}", c.total),
                AmountCurrencyIdentifier = "PEN"
            });

            LegalMonetaryTotal.Add(lmt);

            //TOTALES


            CreditNoteLine i; int index = 0;
            ProductoPresentacion pp = null;
            foreach (var x in c.detalles)
            {

                foreach (var pro in presentaciones)
                    if (x.idPresentacion == presentaciones[index].id)
                        pp = presentaciones[index];
                index++;

                i = new CreditNoteLine();
                i.ID.Add(new IDEntero
                {
                    IdentifierContent = index
                });
                i.Note.Add(new NoteInvoiceLine { TextContent = pp.unidad.nombre, });
                i.CreditedQuantity.Add(new InvoicedQuantity
                {
                    QuantityContent = x.cantidad.ToString(), ///-------------------------------------------------------------------------------------------PREGUNTA
                    QuantityUnitCode = pp.unidad.codigo,
                    QuantityUnitCodeListIdentifier = "UN/ECE rec 20",
                    QuantityUnitCodeListAgencyNameText = "United Nations Economic Commission for Europe"
                });
                i.LineExtensionAmount.Add(new LineExtensionAmount
                {
                    AmountContent = String.Format("{0:0.00}", x.total - x.igv),
                    AmountCurrencyIdentifier = "PEN"
                }
                );
                PricingReference ppr = new PricingReference();
                AlternativeConditionPrice alp = new AlternativeConditionPrice();
                alp.PriceAmount.Add(new PriceAmount
                {
                    AmountContent = String.Format("{0:0.00}", x.precio),
                    AmountCurrencyIdentifier = "PEN"
                });
                alp.PriceTypeCode.Add(new PriceTypeCode
                {
                    CodeContent = "01",
                    CodeListNameText = "Tipo de Precio",
                    CodeListAgencyNameText = "PE:SUNAT",
                    CodeListUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16"
                });
                ppr.AlternativeConditionPrice.Add(alp);
                i.PricingReference.Add(ppr);
                TaxTotalImpuesto txi = new TaxTotalImpuesto();

                txi.TaxAmount.Add(new TaxAmount
                {
                    AmountContent = string.Format("{0:0.00}", x.igv),
                    AmountCurrencyIdentifier = "PEN"
                });

                TaxSubtotalImpuesto tsi = new TaxSubtotalImpuesto();
                tsi.TaxableAmount.Add(new TaxableAmount { AmountContent = String.Format("{0:0.00}", x.total - x.igv), AmountCurrencyIdentifier = "PEN" });
                tsi.TaxAmount.Add(new TaxAmount
                {
                    AmountContent = String.Format("{0:0.00}", x.igv),
                    AmountCurrencyIdentifier = "PEN"
                });
                TaxCategoryImpuesto tci = new TaxCategoryImpuesto();
                tci.Percent.Add(new Percent { NumericContent = igv * 100 });
                tci.TaxExemptionReasonCode.Add(new TaxExemptionReasonCode
                {
                    CodeContent = "10",
                    CodeListAgencyNameText = "PE:SUNAT",
                    CodeListNameText = "Afectacion del IGV",
                    CodeListUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07"
                });
                TaxSchemeI thi = new TaxSchemeI();
                thi.ID.Add(new IDTaxScheme
                {
                    IdentifierContent = "1000",
                    IdentificationSchemeNameText = "Codigo de tributos",
                    IdentificationSchemeUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05",
                    IdentificationSchemeAgencyNameText = "PE:SUNAT",
                });
                thi.Name.Add(new Name { TextContent = "IGV", });
                thi.TaxTypeCode.Add(new TaxTypeCode { CodeContent = "VAT" });
                tci.TaxScheme.Add(thi);
                tsi.TaxCategory.Add(tci);
                txi.TaxSubtotal.Add(tsi);
                i.TaxTotal.Add(txi);
                Item it = new Item();
                it.Description.Add(new Description { TextContent = pp.nombre });
                SellersItemIdentification ist = new SellersItemIdentification();
                ist.ID.Add(new ID
                {
                    IdentifierContent = NormalizarCampo(pp.idProducto.ToString(), 4),
                });
                it.SellersItemIdentification.Add(ist);
                i.Item.Add(it);
                Price pri = new Price();
                pri.PriceAmount.Add(new PriceAmount
                {
                    AmountContent = string.Format("{0:0.00}", x.precio / (igv + 1)),
                    AmountCurrencyIdentifier = "PEN",
                });
                i.Price.Add(pri);

                CreditNoteLine.Add(i);
            }
        }
    }


    public class DiscrepancyResponse
    {
     public   List<ResponseCode> ResponseCode = new List<ResponseCode>();
     public   List<Description> Description = new List<Description>();
    }

    public class ResponseCode
    {
        public string CodeContent = "";
        public string CodeListAgencyNameText = "PE:SUNAT";
        public string CodeListNameText = "Tipo de nota de credito";
        public string CodeListUniformResourceIdentifier = "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo09";
    }

    public class BillingReference
    {
        public List<InvoiceDocumentReference> InvoiceDocumentReference = new List<InvoiceDocumentReference>();

    }
    public class InvoiceDocumentReference {
        public List<ID> ID = new List<ID>();
        public List<IssueDate> IssueDate = new List<IssueDate>();
        public List<DocumentTypeCode> DocumentTypeCode = new List<DocumentTypeCode>();
    }

    public class CreditNoteLine
    {
        public List<IDEntero> ID = new List<IDEntero>();
        public List<NoteInvoiceLine> Note = new List<NoteInvoiceLine>();
        public List<InvoicedQuantity> CreditedQuantity = new List<InvoicedQuantity>();
        public List<LineExtensionAmount> LineExtensionAmount = new List<LineExtensionAmount>();
        public List<PricingReference> PricingReference = new List<PricingReference>();
        public List<TaxTotalImpuesto> TaxTotal = new List<TaxTotalImpuesto>();
        public List<Item> Item = new List<Item>();
        public List<Price> Price = new List<Price>();
    }
}
