﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.EFAC
{
  public  class EfacOrden
    {
        public string _D = "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2";
        public string _S = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
        public string _B = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
        public string _E = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";

        public List<InvoiceNota> Invoice = new List<InvoiceNota>();
    }
}
