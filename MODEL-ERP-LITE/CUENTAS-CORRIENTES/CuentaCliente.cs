﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CUENTAS_CORRIENTES
{
public    class CuentaCliente
    {
     public int   idCliente {get;set;}
        public string nombre { get; set; }
        public string dni { get; set; }
        public string ruc { get; set; }
        public double monto { get; set; }
    }
}
