﻿using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CAJA
{
    [Table("tbl_movimientos_varios", Schema = "caja")]
    public class MovimientoVarios
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("movimientoCaja")]
        public int idMovimientoCaja { get; set; }
        [ForeignKey("concepto")]
        public int idConcepto { get; set; }
        public double descuento { get; set; }
        public bool anulado { get; set; }
        public bool esDolar { get; set; }
        public string numero { get; set; }
        public string serie { get; set; }
        [ForeignKey("proveedor")]
        public int idProveedor { get; set; }
        [ForeignKey("tipoComprobante")]
        public int idTipoComprobante { get; set; }
     
        public int idRegistro { get; set; }
        public bool esDetraccion { get; set; }

        public DateTime fechaComprobante { get; set; }
        public double valorNoGravadas { get; set; }
     
        public double pocentajeDetraccion { get; set; }
        public double montoDetraccion { get; set; }

        public virtual TipoComprobantePago tipoComprobante { get; set; }
        public virtual Proveedor proveedor { get; set; }
        public virtual ConceptoMovimientoVarios concepto { get; set; }
        public virtual MovimientoCaja movimientoCaja { get; set; }
    }
}
