﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CAJA
{
    [Table("tbl_caja_diaria", Schema = "caja")]
    public    class CajaDiaria
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fecha { get; set; }
        public DateTime dia { get; set; }
        public bool estaAbierta { get; set; }
        public double montoInicial{ get; set; }
        public double montoFinal { get; set; }

        public virtual List<MovimientoCaja> movimientos { get; set; }
    }
}
