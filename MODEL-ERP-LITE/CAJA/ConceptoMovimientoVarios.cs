﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CAJA

{
    [Table("tbl_conceptos_movimientos_varios", Schema = "caja")]
    public class ConceptoMovimientoVarios
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string descripcion { get; set; }
        public bool anulado { get; set; }
        public virtual List<MovimientoVarios> movimientosVarios { get; set; } 
    }
}
