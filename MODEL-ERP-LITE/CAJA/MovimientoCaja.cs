﻿using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CAJA
{
    [Table("tbl_movimiento_caja", Schema = "caja")]
    public  class MovimientoCaja
    {

        [Key]
        [Column("id")]
        public int id { get; set; }
        public int idComprobante{ get; set; }
        [ForeignKey("cajaDiaria")]
        public int idDiaCaja { get; set; }
        public string serieNumeroComprobante{ get; set; }
        public string concepto { get; set; }
        public bool esPorVenta { get; set; }
        public bool esPorCompra { get; set; }
        public bool esSalidaVarios { get; set; }
        public bool esIngresoVarios { get; set; }
        public bool esPorNotaCredito { get; set; }
        public bool esPorNotaDebito { get; set; }
        public double monto { get; set; }
        public bool anulado { get; set; }
        public DateTime fecha{ get; set; }
        [ForeignKey("medioPago")]
        public int idMedioPago { get; set; }
        public virtual MedioPago medioPago { get; set; }
        public virtual CajaDiaria cajaDiaria { get; set; }
    }
}
