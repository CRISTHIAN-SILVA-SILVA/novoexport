﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.CAJA
{
  public  class JoinMovCajaCliente
    {
        public int idMov { get; set; }
        public DateTime fechaMov { get; set; }
        public string comprobante { get; set; }
        public double monto { get; set; }
        public string medioPago { get; set; }
        public string descripcionPago { get; set; }
        
    }
}
