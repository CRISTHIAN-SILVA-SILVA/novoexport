﻿using MODEL_ERP_LITE.AUTH;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE
{
  public  class BaseDeDatos : DbContext
    {
        private readonly string schema;

        public BaseDeDatos(string schema) : base("conexion")
        {
            this.schema = schema;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // PostgreSQL uses the public schema by default - not dbo.
            modelBuilder.HasDefaultSchema(schema);
            base.OnModelCreating(modelBuilder);
        }
        //AUTENTIFICACION
        public DbSet<Rol> roles { get; set; }
        public DbSet<Menuu> menus { get; set; }
        public DbSet<Permiso> permisos { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Modulo> modulos { get; set; }

        //PUBLICO
        public DbSet<MotivoTranslado> motivosTranslado { get; set; }
        public DbSet<UnidadMedida> unidades { get; set; }
        public DbSet<TipoAfectacionIGV> tiposAfectacioIGV { get; set; }
        public DbSet<TipoMoneda> tiposMoneda { get; set; }
        public DbSet<TipoPago> tiposPago { get; set; }
        public DbSet<TipoNota> tiposNotaCredito { get; set; }
        public DbSet<MedioPago> mediosPago { get; set; }
        public DbSet<TipoOperacion> TiposOperacion { get; set; }
        public DbSet<TipoDocumentoIdentidad> TipoDocumentosIdentidad { get; set; }
        public DbSet<TipoComprobantePago> tiposComprobantePago { get; set; }

        //INVENTARIO
        public DbSet<Almacen> almacenes { get; set; }
        public DbSet<AlmacenProducto> almacenesProductos { get; set; }
        public DbSet<Marca> marcas { get; set; }
        public DbSet<Merma> mermas { get; set; }
        public DbSet<Categoria> categorias { get; set; }      
        public DbSet<Producto> productos { get; set; }
        public DbSet<DetalleMovimientoProducto> detallesMovimientos { get; set; }
        public DbSet<MovimientoProducto> movimientosProductos { get; set; }
        public DbSet<ProductoPresentacion> presentanciones { get; set; }
        public DbSet<TransferenciaProducto> transferenciasProductos { get; set; }
        public DbSet<DetalleTransferenciaProducto> detallesTransferencia { get; set; }

        //PEDIDOS
        public DbSet<Pedido> pedidos { get; set; }
        public DbSet<Cotizacion> cotizaciones { get; set; }
        public DbSet<DetalleCotizacion> detallesCotizacion { get; set; }
        public DbSet<DetallePedido> detallesPedidos { get; set; }

        //FACTURACION
        public DbSet<DocumentoLocal> documentosLocales { get; set; }
        public DbSet<Venta> ventas { get; set; }
        public DbSet<Nota> notas { get; set; }
        public DbSet<DetalleNota> detalleNotas { get; set; }
        public DbSet<DetalleVenta> detallesVenta { get; set; }
        public DbSet<ComprobantePago> comprobantesPago { get; set; }
        public DbSet<ComprobantePagoVenta> registroVentas { get; set; }
        public DbSet<GuiaRemision> guiasRemision { get; set; }
        public DbSet<PagoTarjeta> pagosTarjeta{ get; set; }
        //RELACIONES
        public DbSet<Cliente> clientes { get; set; }
        public DbSet<Proveedor> proveedores { get; set; }
        public DbSet<Transportista> transportistas { get; set; }
        //CONFIGURACION
        public DbSet<Parametro> parametros { get; set; }
        public DbSet<Local> locales { get; set; }
        public DbSet<Banco> bancos { get; set; }
        public DbSet<CuentaBanco> cuetasBanco { get; set; }

        public DbSet<CuentaProveedor> cuentasProveeedores { get; set; }
        //CAJA
        public DbSet<MovimientoCaja> movimientosCaja { get; set; }
        public DbSet<CajaDiaria> cajasDiarias { get; set; }
        public DbSet<ConceptoMovimientoVarios> conceptosMovimientosVarios { get; set; }
        public DbSet<MovimientoVarios> movimientosCajaVarios { get; set; }

        //COMPRAS
        public DbSet<RegistroCompra> registroCompras { get; set; }
        public DbSet<Compra> compras { get; set; }
        public DbSet<DetallCompra> detallesCompras { get; set; }
        public DbSet<GuiaCompra> guiasEmision { get; set; }
        public DbSet<DetalleNotaCompra> detallesNotaCompra { get; set; }
        public DbSet<NotaCompra> notasCompra { get; set; }
        public DbSet<LetraCompra> letrasCompra { get; set; }
    }
}
