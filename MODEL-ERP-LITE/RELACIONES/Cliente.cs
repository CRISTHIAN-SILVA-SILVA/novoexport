﻿using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.RELACIONES
{
    [Table("tbl_clientes", Schema = "relaciones")]
    public class Cliente
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string razonSocial { get; set; }
        public string ruc { get; set; }
        public string dniRepresentante { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string email { get; set; }
        public string tipoContribuyente{ get; set; }
        public DateTime fechaInscripcion { get; set; }
        public int idTipoContribuyente { get; set; }
        public bool anulado { get; set; }
        public bool activoSUNAT { get; set; }
        public bool esEmpresa { get; set; }

        public virtual List<ComprobantePago> comprobantesPago { get; set; }
        public virtual List<ComprobantePagoVenta> registroVentas { get; set; }
        public virtual List<Venta> ventas { get; set; }
        public virtual List<Nota> notasCredito { get; set; }
    }
}
