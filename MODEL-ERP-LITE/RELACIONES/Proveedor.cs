﻿using MODEL_ERP_LITE.COMPRAS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.RELACIONES
{
    [Table("tbl_proveedores", Schema = "relaciones")]
   public class Proveedor
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string razonSocial { get; set; }
        public string ruc { get; set; }
        public string dniRepresentante { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string email { get; set; }
        public string tipoContribuyente { get; set; }
        public DateTime fechaInscripcion { get; set; }
        public int idTipoContribuyente { get; set; }
        public bool anulado { get; set; }
        public bool activoSUNAT { get; set; }

       public virtual List<Compra> compras { get; set; }
        public virtual List<NotaCompra> notasCompra { get; set; }
        public virtual List<CuentaProveedor> cuentasProveedor{ get; set; }
    }
}
