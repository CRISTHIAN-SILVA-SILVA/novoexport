﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.RELACIONES
{
    [Table("tbl_cuenta_proveedor", Schema = "relaciones")]
    public  class CuentaProveedor
    {

        [Key]
        [Column("id")]
        public int id { get; set; }
        public string numeroCuenta { get; set; }
        public string banco { get; set; }
        [ForeignKey("proveedor")]
        public int idProveedor { get; set; }
        public bool anulado { get; set; }

        public virtual Proveedor proveedor { get; set; }
    }
}
