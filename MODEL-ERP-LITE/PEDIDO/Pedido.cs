﻿using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PEDIDO
{
    [Table("tbl_pedido", Schema = "pedidos")]
    public  class Pedido
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public double total { get; set; }
        public double igv { get; set; }
        public int numeroPedido { get; set; }
        public bool anulado{ get; set; }
        public bool esBoleta { get; set; }
        public bool completado { get; set; }
        public int idVendedor { get; set; }
        [ForeignKey("cliente")]
        public int idCliente { get; set; }
        public string usuarioCreate { get; set; }
        public int idCotizacion { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }
   
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }
        public virtual List<DetallePedido> detalles { get; set; }
        public virtual Cliente cliente { get; set; }
    }
}
