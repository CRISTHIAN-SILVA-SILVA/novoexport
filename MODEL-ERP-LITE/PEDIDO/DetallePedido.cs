﻿using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PEDIDO
{
    [Table("tbl_detalle_pedido", Schema = "pedidos")]
    public class DetallePedido
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("producto")]
        public int idProducto { get; set; }
        [ForeignKey("pedido")]
        public int idPedido { get; set; }
        public double cantidad { get; set; }
        public double factor { get; set; }
        public double precio { get; set; }
        public double total { get; set; }
        public string observacion { get; set; }
        public double igv { get; set; }
        public bool anulado { get; set; }
        public virtual ProductoPresentacion producto{get;set;}
        public virtual Pedido pedido { get; set; }
    }
}
