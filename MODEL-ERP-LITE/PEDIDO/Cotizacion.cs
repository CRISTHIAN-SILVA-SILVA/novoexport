﻿using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PEDIDO
{
    [Table("tbl_cotizacion", Schema = "pedidos")]
  public  class Cotizacion
    {

        [Key]
        [Column("id")]
        public int id { get; set; }
        public double total { get; set; }
        public double igv { get; set; }
        public double diasCredito { get; set; }
        public double porcentajeInteres { get; set; }
        public bool anulado { get; set; }
        [ForeignKey("cliente")]
        public int idCliente { get; set; }
        [ForeignKey("tipoPago")]
        public int idTipoPago { get; set; }
        public string usuarioCreate { get; set; }
        public int numero { get; set; }
        public string serie { get; set; }
        public string usuarioUpdate { get; set; }
        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaCreate { get; set; }

        [DefaultValue(typeof(DateTime), "{yyyy/MM/dd hh:mm:ss}"), DisplayFormat(DataFormatString = "{yyyy/MM/dd hh:mm:ss}", ApplyFormatInEditMode = true), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime fechaUpdate { get; set; }
        public virtual List<DetalleCotizacion> detalles { get; set; }
        public virtual Cliente cliente { get; set; }
        public virtual TipoPago tipoPago { get; set; }
    }
}
