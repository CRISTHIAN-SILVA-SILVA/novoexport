﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
  public  class FacturaNoPagada
    {
        public string fecha { get; set; }
        public string fechaPago { get; set; }
        public string tipo { get; set; }
        public string serie { get; set; }
        public string numero{ get; set; }
        public string cliente { get; set; }
        public string baseImponible { get; set; }
        public string igv { get; set; }
        public string exonerado { get; set; }
        public string total { get; set; }
    }
}
