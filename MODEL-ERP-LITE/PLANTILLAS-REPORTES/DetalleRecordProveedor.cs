﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
  public  class DetalleRecordProveedor
    {
        public string cliente { get; set; }
        public string documento { get; set; }
        public int numeroVentas { get; set; }
        public double monto { get; set; }
    }
}
