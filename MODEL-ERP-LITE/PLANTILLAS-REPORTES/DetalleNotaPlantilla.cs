﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
 public   class DetalleNotaPlantilla
    {
       public string cantidad { get; set; }
        public string unidad { get; set; }
        public string descripcion { get; set; }
        public string valorUnitario { get; set; }
    }
}
