﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
    public class DetalleRVResumenDiario
    {
        public DateTime fecha { get; set; }
        public double montoFacturas { get; set; }
        public double montoBoletas { get; set; }
        public double montoNotaCredito { get; set; }
        public double montoNotaDebito { get; set; }
        public double total { get; set; }

    }
}
