﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
    public class DetalleRVenta
    {
        public DateTime fecha { get; set; }
        public string fechaS { get; set; }
        public string cliente { get; set; }
        public string serie { get; set; }
        public int numero { get; set; }
        public double importe{get;set;} 
    }
}
