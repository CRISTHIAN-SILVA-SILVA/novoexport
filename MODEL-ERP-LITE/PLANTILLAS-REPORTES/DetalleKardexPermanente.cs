﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
  public  class DetalleKardexPermanente
    {
        public string ID { get; set; }


        public string fecha { get; set; }
        public string tipoComprobante { get; set; }
        public string serie { get; set; }
        public string numero { get; set; }
        public string tipOperacion { get; set; }

        public string entradas { get; set; }
        public string COSTOE { get; set; }
        public string TOTALE { get; set; }

        public string salidas { get; set; }
        public string COSTOS { get; set; }
        public string  TOTALS { get; set; }

        public string saldo { get; set; }
        public string COSTON { get; set; }
        public string TOTALN { get; set; }
    }
}
