﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
  public  class DetalleFactura
    {
       public string cantidad { get; set; }
        public string unidad { get; set; }
        public string codigo { get; set; }
        public string descricion { get; set; }
        public string valorUnitario { get; set; }
    }
}
