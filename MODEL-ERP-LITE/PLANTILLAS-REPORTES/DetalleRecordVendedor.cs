﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
   public class DetalleRecordVendedor
    {
        public string vendedor { get; set; }
        public int numeroVentas { get; set; }
        public double montoVendido { get; set; }
    }
}
