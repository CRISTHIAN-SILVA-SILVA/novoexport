﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL_ERP_LITE.PLANTILLAS_REPORTES
{
  public  class DetalleRVSerie
    {
        public string nombreTipoComprobante { get; set; }
        public string codigoTipo { get; set; }
        public string serie { get; set; }
        public double baseImpl { get; set; }
        public double exonerado { get; set; }
        public double igv { get; set; }
        public double total { get; set; }
    }
}
