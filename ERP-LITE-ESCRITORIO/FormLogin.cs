﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.AUTH;
using MODEL_ERP_LITE.EFAC;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using Newtonsoft.Json;
using SERVICE_ERP_LITE;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
//using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ERP_LITE_DESKTOP
{
    public partial class FormLogin : MaterialForm
    {
        public static Usuario user = null;
        public UsuarioService susuario = new UsuarioImp();
        public ComprobantePagoService cpservice = new ComprobantePagoImpl();
        public VentaService vservice = new VentaImpl();
        NotaService nservice = new NotaImpl();
        public FormLogin()
        {

            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
           //    DetalleMovimientoProductoImpl d = new DetalleMovimientoProductoImpl();
            //  d.multiplicarTipoCambio();
           // d.normalizarFechaCompras();
            //    CompraImpl cl = new CompraImpl();
            //  cl.normalizarCompras();
                  try
                   {
                       //d.ordenarKardex();

                       LocalImpl.getInstancia();
                             ParametroImpl.getInstancia();



                          //   ConfigurarFileWatcherForRPTAPath();
                            // ConfigurarFileWatcherForFIRMAPath();
                         }
                         catch (Exception e) {
                this.Close();
                         } 
         
                  
        }
        public async Task archivo() {
            string json = JsonConvert.SerializeObject(new AccesToken { access_token = "123", expires_in = "in", token_type = "1" }, Newtonsoft.Json.Formatting.Indented);
            string file = "p.json";
        
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");

            byte[] bytes = await (sc.ReadAsByteArrayAsync());
            File.WriteAllBytes(@"D:\jsonEfac\" + file,bytes);
        }
        public async Task getPDF(string token) {
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            cliente.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            var response = await cliente.GetAsync("https://ose-gw1.efact.pe/api-efact-ose/oauth/document");
            var r = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                MessageBox.Show(JsonConvert.DeserializeObject<AccesToken>(r).access_token);
            else MessageBox.Show("NOSE PUDOOOOOOOOOOOO" + response.ToString());   
            /*
        using (FileStream stream = System.IO.File.Create(tu_ruta_de_ficheros + obj_pdf[0]))
    { 
    byte[] todecode_byte = Convert.FromBase64String(data); // data es el string en BASE64
    stream.Write(todecode_byte, 0, todecode_byte.Length);
    }
        */
        }


        private void ConfigurarFileWatcherForFIRMAPath()
        {
            // Creamos un nuevo vigilante y sus propiedades 
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = @"D:\SFS_v1.2\sunat_archivos\sfs\FIRMA";
            /* Vigilar por el último cambio de los archivos. */
           watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite    | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            //Sólo vigilar los archivos zip.
           watcher.Filter = "*.xml";
            // Añadir manejadores de eventos.
            //watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChangeOnFIRMAPath);
            //watcher.Deleted += new FileSystemEventHandler(OnChanged);
            //watcher.Renamed += new RenamedEventHandler(OnRenamed);
            // Comenzar la vigilancia.
            watcher.EnableRaisingEvents = true; }
       private void ConfigurarFileWatcherForRPTAPath()
        {
            // Creamos un nuevo vigilante y sus propiedades 
           FileSystemWatcher watcher = new FileSystemWatcher();
           watcher.Path = @"D:\SFS_v1.2\sunat_archivos\sfs\RPTA";
            /* Vigilar por el último cambio de los archivos. */
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite  | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            //Sólo vigilar los archivos zip.
            watcher.Filter = "*.zip";
            // Añadir manejadores de eventos.
            //watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChangeOnRPTAPath);
            //watcher.Deleted += new FileSystemEventHandler(OnChanged);
            //watcher.Renamed += new RenamedEventHandler(OnRenamed);
            // Comenzar la vigilancia.
            watcher.EnableRaisingEvents = true;
       }

        private void OnChangeOnFIRMAPath(object source, FileSystemEventArgs e) {
            string archivo = e.FullPath;

            Thread.Sleep(1500);
            //  try {
            FileInfo DET = new FileInfo(@archivo);
            String hash = "";
            //  MessageBox.Show(hash);
            if (DET.Exists) {
                XmlDocument doc = new XmlDocument();
                XmlNode elemento;
                XmlNodeList nodo, nodo2, nodo3, nodo4, nodo5, nodo6, nodo7;
                //try {
                    string value = File.ReadAllText(archivo);
                    doc.LoadXml(value);
                    nodo = doc.GetElementsByTagName("ext:UBLExtensions");
                    if (nodo.Count > 0) {
                        nodo2 = nodo.Item(0).ChildNodes;
                        if (nodo2.Count > 0) {
                            nodo3 = nodo2.Item(0).ChildNodes;
                            if (nodo3.Count > 0) {
                                nodo4 = nodo3.Item(0).ChildNodes;
                                if (nodo4.Count > 0) {
                                    nodo5 = nodo4.Item(0).ChildNodes;
                                    if (nodo5.Count > 0) {
                                        string i = nodo5.Item(2).InnerXml;
                                        nodo6 = nodo5.Item(0).ChildNodes;
                                        if (nodo6.Count > 2) {
                                            nodo7 = nodo6.Item(2).ChildNodes;
                                            if (nodo6.Count > 2)
                                                hash = nodo6.Item(2).InnerText; } } } } } }
             //   }
             //   catch { }
            }
       //     MessageBox.Show(hash);
            string nombre = e.Name;
            string serie = e.Name.Substring(15, 4);
            string tipoComprobante = e.Name.Substring(12, 2);
            int numero = 0;
           // MessageBox.Show(serie);
            try { numero = Convert.ToInt32(e.Name.Substring(20, 8)); } catch { }
            //  SI SERIE ES NC O ND ENTONCS CONSULTA NOTA SINO ->
            if (CodigosSUNAT.NOTA_CREDITO.Equals(tipoComprobante) || CodigosSUNAT.NOTA_DEBITO.Equals(tipoComprobante))
            {
                Nota nota = nservice.buscarPorserieNumero(serie, numero);
                if (nota != null) {
                    nota.hash = hash;
                    nota.cliente = null;
                    nota.comprobante = null;
                    nota.detalles = null;
                    nota.tipoNota = null;
                    nota.tipoComprobante = null;
                    nservice.editar(nota);
                }
            }
            else
            {
                ComprobantePago cp = cpservice.listarPorSerieNumero(serie, numero);
                Venta v = vservice.buscar(cp.idVenta);

                if (cp != null && v != null)
                {
                    //    MessageBox.Show("1");
                    //  MessageBox.Show(hash);
                    cp.hash = hash;
                  ///  TicketService.imprimirTicket(cp, v, cp.hash);
                    //    MessageBox.Show("2");
                    cp.venta = null;
                    cp.tipoComprobante = null;
                    cp.cliente = null;
                    cp.medioPago = null;
                    cp.tipoPago = null;
                    cpservice.editar(cp);
                }
            }
        //  catch { }
    }
        private void OnChangeOnRPTAPath(object source, FileSystemEventArgs e)
        {
            string archivo = e.FullPath;
                string nombre = e.Name;
                string serie = e.Name.Substring(16, 4);
                int numero = 0;

           

                try { numero = Convert.ToInt32(e.Name.Substring(21, 8)); } catch { }
                ComprobantePago cp = cpservice.listarPorSerieNumero(serie, numero);
                if (cp != null)
                {
                    cp.enviado = true;
                    cp.venta = null;
                    cp.tipoComprobante = null;
                    cp.cliente = null;
                    cp.medioPago = null;
                    cp.tipoPago = null;
                    cpservice.editar(cp);
                }
        }
    

        private void FormLogin_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtUsername.Focus();
        }

        private void checkVerContrasena_CheckedChanged(object sender, EventArgs e)
        {
            if (checkVerContrasena.Checked)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else txtPassword.UseSystemPasswordChar = true;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            iniciar();
         
        }
        void iniciar() {

            if (Util.esDNI(txtUsername.Text.Trim()))
            {
                if (Util.validaCadena(txtPassword.Text))
                {
                    user = susuario.iniciarSesion(txtUsername.Text.Trim(), txtPassword.Text.Trim());
                    if (user != null)
                    {
                        this.Hide();
                        Principal form = new Principal();
                        form.Show();
                    }
                    else MessageBox.Show("USUARIO INCORRECTO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show("INGRESE CONTRASEÑA", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else MessageBox.Show("DNI INCORRECTO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        private void txtUsername_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter) iniciar();
                
        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) iniciar();
        }
    }
    
}
