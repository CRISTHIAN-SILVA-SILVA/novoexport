﻿using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM
{
    public partial class FormPagosCliente : Form
    {
        Cliente cliente;
        MovimientoCajaImpl mcservice = new MovimientoCajaImpl();
        DataTable table = new DataTable();
        PagosBancoImpl pservice = new PagosBancoImpl();

        public FormPagosCliente(Cliente c)
        {
            InitializeComponent();
            cliente = c;
            lblCliente.Text = c.razonSocial;
            table.Columns.Add("ID");
            table.Columns.Add("FECHA");
            table.Columns.Add("COMPROBANTE");
            table.Columns.Add("MONTO");
            table.Columns.Add("MEDIO PAGO");
            table.Columns.Add("DESCRIPCION PAGO");
            grid.DataSource = table;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 80;
            grid.Columns[2].Width = 120;
            grid.Columns[3].Width = 80;
            grid.Columns[4].Width = 142;
            grid.Columns[5].Width = 300;

        }

        private void FormPagosCliente_Load(object sender, EventArgs e)
        {
            string descripcion = "";
            foreach (var x in mcservice.listarPorCLiente200(cliente.id))
            {
                if (!x.medioPago.Equals("EFECTIVO"))
                {
                    PagoTarjeta pt = pservice.buscarPorMov(x.idMov);
                    if (pt != null) descripcion = pt.cuentaBanco.cuenta + " " + pt.post;
                }
                else descripcion = "";
                    table.Rows.Add(x.idMov, x.fechaMov.ToString("yyyy-MM-dd"), x.comprobante, x.monto, x.medioPago, descripcion);
            }
            
        
          
        }
    }
}
