﻿using ERP_LITE_DESKTOP.FACTURACION_FORM;
using ERP_LITE_ESCRITORIO.FACTURACION_FORM;
using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM
{
    public partial class FormComprobantesClientes : MaterialForm
    {
        DataTable tabla = new DataTable();
        Cliente cliente = null;
        public delegate void pasar();
        public event pasar pasado;
        ClienteService cservice = new ClienteImpl();
        ComprobantePagoService cpservice = new ComprobantePagoImpl();
        VentaService vservice = new VentaImpl();
        MedioPagoService mpService = new MedioPagoImpl();
        public FormComprobantesClientes(int idCliente)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
     
            cliente = cservice.buscar(idCliente);
            lblCliente.Text = cliente.razonSocial;

            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA E.");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("FECHA V.");
            tabla.Columns.Add("ORDEN COMPRA");
            grid.DataSource = tabla;
            grid.Columns[0].Visible =false;
            grid.Columns[1].Width = 200;
            grid.Columns[2].Width = 150;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 142;
            grid.Columns[5].Width = 120;

            double total = 0;
            foreach (var x in cpservice.listarPorDeudaCliente(cliente.id))
            {
                tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyy/MM/dd"), x.serie, x.numero, String.Format("{0:0.00}", x.montoN-x.montoPagado),x.fechaVencimiento.ToString("yyy/MM/dd"),x.venta.orden);
                total += x.montoTotal;
            }

            lblTotal.Text = string.Format("{0:0.00}", total);
        }
        void cargar() {
            double total = 0;
            tabla.Clear();
            foreach (var x in cpservice.listarPorDeudaCliente(cliente.id))
            {
                tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyy/MM/dd"), x.serie, x.numero, String.Format("{0:0.00}", x.montoN-x.montoPagado), x.fechaVencimiento.ToString("yyy/MM/dd"),x.venta.orden);
                total += x.montoN;
            }
           
            lblTotal.Text = string.Format("{0:0.00}", total);
            pasado();
        }
        private void FormComprobantesClientes_Load(object sender, EventArgs e)
        {
            
        }

        private void materialLabel10_Click(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            {
                ComprobantePago c = cpservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                FormPagoFactura form = new FormPagoFactura(c);
                form.pasado += new FormPagoFactura.pasar(cargar);
                form.ShowDialog();
            }
        }


        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void grid_DoubleClick(object sender, EventArgs e)
        {
        
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          
                ComprobantePago cp = cpservice.buscar(Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
                Venta venta = vservice.buscar(cp.idVenta);
                FormDetalleVenta form = new FormDetalleVenta(venta, cp, cp.tipoComprobante, cp.tipoPago.nombre, cp.medioPago.nombre, null, true);
                form.ShowDialog();
            
        }

        private void btnPagos_Click(object sender, EventArgs e)
        {
            FormPagosCliente form = new FormPagosCliente(cliente);
            form.Show();
        }
    }
}
