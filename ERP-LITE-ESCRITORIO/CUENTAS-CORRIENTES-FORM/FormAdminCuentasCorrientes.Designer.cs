﻿namespace ERP_LITE_DESKTOP.CUENTAS_CORRIENTES_FORM
{
    partial class FormAdminCuentasCorrientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdminCuentasCorrientes));
            this.contenedor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCuentasProveedores = new System.Windows.Forms.Button();
            this.btnCuentasClientes = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contenedor
            // 
            this.contenedor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.contenedor.Location = new System.Drawing.Point(224, 64);
            this.contenedor.Name = "contenedor";
            this.contenedor.Size = new System.Drawing.Size(1124, 634);
            this.contenedor.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.panel1.Controls.Add(this.btnCuentasProveedores);
            this.panel1.Controls.Add(this.btnCuentasClientes);
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 634);
            this.panel1.TabIndex = 7;
            // 
            // btnCuentasProveedores
            // 
            this.btnCuentasProveedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCuentasProveedores.FlatAppearance.BorderSize = 0;
            this.btnCuentasProveedores.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnCuentasProveedores.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnCuentasProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCuentasProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCuentasProveedores.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCuentasProveedores.Image = ((System.Drawing.Image)(resources.GetObject("btnCuentasProveedores.Image")));
            this.btnCuentasProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCuentasProveedores.Location = new System.Drawing.Point(0, 58);
            this.btnCuentasProveedores.Name = "btnCuentasProveedores";
            this.btnCuentasProveedores.Size = new System.Drawing.Size(228, 49);
            this.btnCuentasProveedores.TabIndex = 5;
            this.btnCuentasProveedores.Text = "          Cuentas Proveedores";
            this.btnCuentasProveedores.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCuentasProveedores.UseVisualStyleBackColor = true;
            this.btnCuentasProveedores.Click += new System.EventHandler(this.btnCuentasProveedores_Click);
            // 
            // btnCuentasClientes
            // 
            this.btnCuentasClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCuentasClientes.FlatAppearance.BorderSize = 0;
            this.btnCuentasClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnCuentasClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnCuentasClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCuentasClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCuentasClientes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCuentasClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnCuentasClientes.Image")));
            this.btnCuentasClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCuentasClientes.Location = new System.Drawing.Point(0, 3);
            this.btnCuentasClientes.Name = "btnCuentasClientes";
            this.btnCuentasClientes.Size = new System.Drawing.Size(228, 49);
            this.btnCuentasClientes.TabIndex = 1;
            this.btnCuentasClientes.Text = "          Cuentas Clientes";
            this.btnCuentasClientes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCuentasClientes.UseVisualStyleBackColor = true;
            this.btnCuentasClientes.Click += new System.EventHandler(this.btnCuentasClientes_Click);
            // 
            // FormAdminCuentasCorrientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 692);
            this.Controls.Add(this.contenedor);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "FormAdminCuentasCorrientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Módulo Cuentas Corrientes";
            this.Load += new System.EventHandler(this.FormAdminCuentasCorrientes_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel contenedor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCuentasProveedores;
        private System.Windows.Forms.Button btnCuentasClientes;
    }
}