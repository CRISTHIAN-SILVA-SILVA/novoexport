﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM
{
    public partial class FormLetraComprobante : MaterialForm
    {
        public delegate void pasar(Compra c);
        public event pasar pasado;
        public delegate void pasarCuenta();
        public event pasarCuenta pasadoCuenta;
        DataTable tabla = new DataTable();
        LetraService lservice=new LetraImpl();
        Compra compra = null;
        int ultimaLetra = 0;
        CompraService cservice = new CompraImpl();
        List<LetraCompra> letras;
        bool vienedeCuentas=false;
        public FormLetraComprobante(Compra c,bool vieneDeCuentas)
        {
            InitializeComponent();
            vienedeCuentas = vieneDeCuentas;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            compra = c;
            lblCliente.Text = c.proveedor.razonSocial;
            lblComprobante.Text = c.serie + "-" + Util.NormalizarCampo(c.numero.ToString(), 8);

            if (!vienedeCuentas) { btnPagar.Visible = false; }

            tabla.Columns.Add("ID");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("INICIO");
            tabla.Columns.Add("FIN");
            tabla.Columns.Add("PAGADO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 200;
            grid.Columns[2].Width = 150;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 100;
            grid.Columns[5].Width = 147;
            
        }

        void cargarLetras() {
            compra = cservice.buscar(compra.id);
            tabla.Clear();
            string pagado = "";
            lblTotal.Text = string.Format("{0:0.00}", compra.montoN - compra.montoPagado);
            letras = lservice.listarPorCompra(compra.id);
            foreach (var x in letras) {
                if (x.pagado) pagado = "PAGADO"; else pagado = "NO PAGADO";
                tabla.Rows.Add(x.id, x.numero, String.Format("{0:0.00}", x.monto), x.fechaInicio.ToShortDateString(), x.fechaFin.ToShortDateString(),pagado) ;
                if(ultimaLetra<x.numero)
                    ultimaLetra = x.numero;
            }
       }
        private void FormLetraComprobante_Load(object sender, EventArgs e)
        {
            cargarLetras();
        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            bool todo = false;
            if (letras.Count == 1) todo = true;
            foreach (var x in letras) {
                if (!x.pagado) {
                    if (lservice.pagar(x.id, todo))
                    {
                        cargarLetras();
                        return;
                    }
                    else { MessageBox.Show("ERROR INESPERADO");return; }
              
                }
            }
         
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {
            FormComvertirLetra form = new FormComvertirLetra(compra,ultimaLetra);
            form.pasado += new FormComvertirLetra.pasar(cargarLetras);
            form.ShowDialog();
        }

        private void FormLetraComprobante_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (vienedeCuentas)
                pasadoCuenta();

            else pasado(null);
        
        }
    }
}
