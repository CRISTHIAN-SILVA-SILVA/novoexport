﻿using ERP_LITE_ESCRITORIO.FACTURACION_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM
{
    public partial class FormPagoFactura : MaterialForm
    {
        MedioPagoService mpService = new MedioPagoImpl();
        public delegate void pasar();
        public event pasar pasado;
        ComprobantePagoService cpservice = new ComprobantePagoImpl();
        ComprobantePago cp;
        public FormPagoFactura(ComprobantePago c)
        {
            cp= c;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            comboMedioPago.DataSource = mpService.listarNoAnulados();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void cargar()
        {
            
            pasado();
        }
        private void btnPagar_Click(object sender, EventArgs e)
        {
            btnPagar.Enabled = false;

            if (Util.esDouble(txtMonto.Text))
            {
                MedioPago mp = mpService.buscar((int)comboMedioPago.SelectedValue);
                if (mp.esBanco)
                {
                    FormPagoTarjeta form = new FormPagoTarjeta(null, null, 0,cp. id, (int)comboMedioPago.SelectedValue, txtFecha.Value, Convert.ToDouble(txtMonto.Text),"");
                    form.pasado += new FormPagoTarjeta.pasar(cargar);
                    form.ShowDialog();
                }
                else
                {
                    if (MessageBox.Show("ESTAS SEGURO DE PAGAR EL COMPROBANTE", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (cpservice.pagar(cp.id, (int)comboMedioPago.SelectedValue, null, txtFecha.Value, Convert.ToDouble(txtMonto.Text)))
                        {
                            cargar();
                            this.Close();
                        }
                        else { MessageBox.Show("ERROR INESPERADO INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                    }
                }
            }
            else MessageBox.Show("MONTO INCORRECTO");

            btnPagar.Enabled = true;
        }

        private void FormPagoFactura_Load(object sender, EventArgs e)
        {
            this.Text="COMPROBANTE: "+cp.serie+"-"+Util.NormalizarCampo(cp.numero.ToString(),8);
            lblDeuda.Text = string.Format("{0:0.00}",cp.montoN-cp.montoPagado);
        }
    }
}
