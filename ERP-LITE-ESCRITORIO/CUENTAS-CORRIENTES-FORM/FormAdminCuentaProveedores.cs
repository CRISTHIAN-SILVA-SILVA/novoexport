﻿using ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM;
using SERVICE_ERP_LITE.CUENTAS_CORRIENTES_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CUENTAS_CORRIENTES_FORM
{
    public partial class FormAdminCuentaProveedores : Form
    {
        DataTable tabla = new DataTable();
        CuentaProveedorImpl scuenta = new CuentaProveedorImpl();
        double total = 0;
        public FormAdminCuentaProveedores()
        {

            InitializeComponent();
            tabla.Columns.Add("IDPROVEEDOR");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("RUC");
            tabla.Columns.Add("MONTO");
            grid.DataSource = tabla;
            grid.Columns[0].Width = 100;
            grid.Columns[1].Width = 700;
            grid.Columns[2].Width = 175;
            grid.Columns[3].Width = 100;
        }
        void cargar()
        {
            total = 0;
            tabla.Clear();
            foreach (var x in scuenta.listaDeudasProveedor())
            {
                tabla.Rows.Add(x.idCliente, x.nombre, x.ruc, x.monto);
                total += x.monto;

            }
            lblTotal.Text = string.Format("{0:0.00}", total);
        }
        private void FormAdminCuentaProveedores_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           if (grid.SelectedRows.Count == 1)
            {
                FormComprasProvedor form = new FormComprasProvedor(Convert.ToInt32(grid.SelectedRows[0].Cells["IDPROVEEDOR"].Value.ToString()));
                form.pasado += new FormComprasProvedor.pasar(cargar);
                form.ShowDialog();
            }
        }

        private void txtBuscarCliente_KeyUp(object sender, KeyEventArgs e)
        {
            total = 0;
            tabla.Clear();
            foreach (var x in scuenta.listaDeudasProveedorPorNombre(txtBuscarCliente.Text.ToUpper()))
            {
                tabla.Rows.Add(x.idCliente, x.nombre, x.ruc, x.monto);
                total += x.monto;

            }
            lblTotal.Text = string.Format("{0:0.00}", total);

        }
    }
}
