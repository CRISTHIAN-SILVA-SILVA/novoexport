﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM
{
    public partial class FormComvertirLetra : MaterialForm
    {
        public delegate void pasar();
        LetraService lservice = new LetraImpl();
        public event pasar pasado;
        int numeroInicio = 0;
        Compra compra = null;
        DataTable tabla = new DataTable();
        public FormComvertirLetra(Compra c,int ultimaLetra)
        {
            InitializeComponent();
            compra = c;
            numeroInicio = ultimaLetra+1;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            lblCliente.Text = c.proveedor.razonSocial;
            lblComprobante.Text = c.serie + "-"+Util.NormalizarCampo (c.numero.ToString(),8);
            lblTotal.Text =string.Format("{0:0.00}", c.montoN-c.montoPagado);


            tabla.Columns.Add("ID");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("FECHA INICIO");
            tabla.Columns.Add("FECHA FIN");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 200;
            grid.Columns[2].Width = 150;
            grid.Columns[3].Width = 140;
            grid.Columns[4].Width = 167;
        }

        private void FormComvertirLetra_Load(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Util.esDouble(txtMonto.Text))
            { 
                tabla.Rows.Add("","",string.Format("{0:0.00}",Convert.ToDouble(txtMonto.Text)),txtInicio.Value,txtFin.Value);
       
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int ultima = grid.RowCount-1;
            if(ultima>-1)
            tabla.Rows.RemoveAt(grid.SelectedRows[ultima].Index);
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("SEGURO DE CONVERTIR LETRAS","AVISO",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            btnConvertir.Enabled = false;
            if (grid.RowCount > 0)
            {
                List<LetraCompra> letras = new List<LetraCompra>();
                LetraCompra letra;
                double total = 0;
                for (int i=0;i<grid.RowCount;i++) {
                    letra = new LetraCompra {
                        fechaInicio = DateTime.Parse(grid.Rows[i].Cells["FECHA INICIO"].Value.ToString()),
                        fechaFin = DateTime.Parse(grid.Rows[i].Cells["FECHA FIN"].Value.ToString()),
                        numero = numeroInicio + i,
                        monto=Convert.ToDouble(grid.Rows[i].Cells["MONTO"].Value.ToString()),
                         idCompra=compra.id
                    };
                    total += letra.monto;      

                    letras.Add(letra);
                }
                if (lservice.guardarVarios(letras, numeroInicio))
                {
                    this.Close();
                    pasado();
                }
                else {
                    MessageBox.Show("ERROR INESPERADO");
                }
            }
            else { MessageBox.Show("NO HAY LETRAS"); }
            btnConvertir.Enabled = true
                ;
        }

        private void FormComvertirLetra_FormClosing(object sender, FormClosingEventArgs e)
        {
          
        }
    }
}
