﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CUENTAS_CORRIENTES_FORM
{
    public partial class FormAdminCuentasCorrientes : MaterialForm
    {
        public FormAdminCuentasCorrientes()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormAdminCuentasCorrientes_Load(object sender, EventArgs e)
        {
            abrirForm(new FormAdminCuentaClientes()); 
        }
        void abrirForm(Object formHijo)
        {
            if (this.contenedor.Controls.Count > 0)
                this.contenedor.Controls.RemoveAt(0);
            Form hijo = formHijo as Form;
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            this.contenedor.Controls.Add(hijo);
            this.contenedor.Tag = hijo;
            hijo.Show();
        }
        private void btnCuentasClientes_Click(object sender, EventArgs e)
        {
            abrirForm( new FormAdminCuentaClientes());
            
        }

        private void btnCuentasProveedores_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminCuentaProveedores());

        }
    }
}
