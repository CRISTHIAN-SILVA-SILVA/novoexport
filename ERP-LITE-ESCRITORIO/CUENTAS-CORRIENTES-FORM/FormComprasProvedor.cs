﻿using ERP_LITE_ESCRITORIO.COMPRAS_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM
{
    public partial class FormComprasProvedor : MaterialForm
    {
        DataTable tabla = new DataTable();
        Proveedor proveedor = null;
        public delegate void pasar();
        public event pasar pasado;
        ProveedorService pservice = new ProveedorImpl();
        CompraService cservice = new CompraImpl();
        public FormComprasProvedor(int idProveedor)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            proveedor = pservice.buscar(idProveedor);
            lblCliente.Text = proveedor.razonSocial;

            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("TIPO PAGO");
            tabla.Columns.Add("MONEDA");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 150;
            grid.Columns[2].Width = 150;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 147;
            grid.Columns[5].Width = 150;
            grid.Columns[6].Width = 100;
        }

        private void FormComprasProvedor_Load(object sender, EventArgs e)
        {
            cargar();
        }
        void cargar()
        {
            tabla.Clear();
            double total = 0;
            foreach (var x in cservice.listarPorDeudaProveedor(proveedor.id))
            {
                tabla.Rows.Add(x.id, x.fechaEmision.ToShortDateString(), x.serie, x.numero, String.Format("{0:0.00}", x.montoN-x.montoPagado),x.tipoPago.nombre,x.tipoMoneda.descripcion);
                total += x.montoN;
            }

            lblTotal.Text = string.Format("{0:0.00}", total);
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            btnPagar.Enabled = false;
            if (grid.SelectedRows.Count == 1)
            {
                if (grid.SelectedRows[0].Cells["ID"].Value.ToString() == "PAGO A LETRAS") {
                    MessageBox.Show("ESTE COMPROBANTE ESTA DIVIDIDO EN LETRAS","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                else
                if (cservice.pagar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString())))
                {
                    cargar();
                    pasado();
                }
            }
            btnPagar.Enabled = false; //PAGAR COMPRA SI ES A CREDITO SINO MOSTRAR MENSAJE ES PAGO A LETRAS
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            {
                Compra c = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null)
                {

                    if (!c.pagado)
                    {
                        FormLetraComprobante form = new FormLetraComprobante(c,true);

                        form.pasadoCuenta += new FormLetraComprobante.pasarCuenta(cargar);
                        form.ShowDialog();
                    }
                }
            }
        }
    }
}
