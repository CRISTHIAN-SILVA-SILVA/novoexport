﻿using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{
    public partial class FormAdminGuiasRemision : Form
    {
        GuiaRemisonService gservice = new GuiaRemisionImpl();
        DataTable tabla = new DataTable();
        public FormAdminGuiasRemision()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("COMPROBANTE");
            tabla.Columns.Add("TRANSPORTISTA");
            grid.DataSource = tabla;

            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 100;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 180;
            grid.Columns[5].Width = 600;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);

        }
        void cargar() {
            tabla.Clear();
            gservice.listarNoAnulados().ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(), x.serie, x.numero, x.serieNumeroComprobante, x.transportista.razonSocial));

        }
        private void FormAdminGuiasRemision_Load(object sender, EventArgs e)
        {
            cargar();
            habilitar(false,false);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cargar();
        }
        void habilitar(bool fecha,bool numero) {
            txtInicio.Visible = fecha;
            txtFin.Visible = fecha;
            txtNumero.Visible = numero;
        }
        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if(rbFecha.Checked)
                habilitar(true,false);
        }

        private void rbNumero_CheckedChanged(object sender, EventArgs e)
        {
            if(rbNumero.Checked)
            habilitar(false,true);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (rbNumero.Checked) {
                if (Util.esDouble(txtNumero.Text))
                {
                    GuiaRemision x = gservice.buscarPorNumero(Convert.ToInt32(txtNumero.Text));
                    tabla.Clear();
                 tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(), x.serie, x.numero,
                     x.serieNumeroComprobante, x.transportista.razonSocial); }

            }
            if (rbFecha.Checked) {
                tabla.Clear();
                gservice.buscarPorFecha(txtInicio.Value.Date,txtFin.Value.Date.AddDays(1)).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(), x.serie, x.numero, x.serieNumeroComprobante, x.transportista.razonSocial));

            }
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            FormReporteGuia form = new FormReporteGuia(Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            form.ShowDialog();
        }
    }
}
