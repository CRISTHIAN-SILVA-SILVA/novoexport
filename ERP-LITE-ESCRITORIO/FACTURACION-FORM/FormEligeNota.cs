﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.EFAC;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using Newtonsoft.Json;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    public partial class FormEligeNota : MaterialForm
    {
        public delegate void pasar();
        public event pasar pasado;
        ComprobantePago comprobante ;
        Parametro igv;
        TipoNotaImpl tnservice = new TipoNotaImpl();
        Venta venta;string cliente="";
        NotaImpl nservice = new NotaImpl();
        ProductoPresentacionImpl ppservice = new ProductoPresentacionImpl();
        ProductoService pservice = new ProductoImpl();
        void terminar() {
            pasado();
            this.Close();
        }
        public FormEligeNota(ComprobantePago cp, Venta v, string cliente,TipoComprobantePago t)
        {
            InitializeComponent();
            igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
            if (igv == null) this.Close();
            this.comprobante = cp;
            comprobante.tipoComprobante = t;
            this.venta = v;this.cliente = cliente;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            lblCliente.Text = cliente;
            lblTipoComprobante.Text = t.descripcion.ToUpper();
            lblSerie.Text = cp.serie + "-" +Util.NormalizarCampo( cp.numero.ToString(),8);
        }

        private void FormEligeNota_Load(object sender, EventArgs e)
        {
            comboNota.SelectedIndex = 0;
        }

        private void comboNota_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboNota.SelectedIndex == 0)
                comboTipoNota.DataSource = TipoNotaImpl.getInstancia().Where(x => x.esCredito).ToList();
            else comboTipoNota.DataSource = TipoNotaImpl.getInstancia().Where(x => x.esDebito).ToList();
        }

        private void txtSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSeguir_Click(object sender, EventArgs e)
        {
            btnSeguir.Visible = false;
            TipoComprobantePagoService tcpservice = new TipoComprobanteImpl();
            TipoComprobantePago tipoCredito = tcpservice.buscarPorNombre("Nota de credito");
            TipoComprobantePago tipoDebito = tcpservice.buscarPorNombre("Nota de debito");
            if (tipoCredito == null || tipoDebito == null) return;
            if (comboTipoNota.Text.Equals("ANULACION_DE_LA_OPERACION") || comboTipoNota.Text.Equals("ANULACION_POR_ERROR_EN_EL_RUC"))
            {
                if (MessageBox.Show("SEGURO DE ANULAR COMPROBANTE?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //SI YA EXISTEN NOTAS YA NO SE PUEDE ELIMINAR
                    if (nservice.listarPorComprobante(comprobante.id).Count > 0) { MessageBox.Show("NO SE PUEDE ANULAR EL COMPROBANTE YA QUE YA SE HA GENERADO UNA NOTA DE CREDITO O DEBITO ANTES, CONSULTE CON SUNAT", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); btnSeguir.Enabled = true; return; }
                    venta.anulado = true;

                    comprobante.anulado = true;
                    Nota nota = new Nota
                    {
                        esCredito = true,
                        idCliente = comprobante.idCliente,
                        idComprobanteVenta = comprobante.id,
                        idTipoComprobante = tipoCredito.id,
                        idTipoNota = (int)comboTipoNota.SelectedValue,
                        igv = venta.igv,
                        usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                        serie = tipoCredito.serie,
                        total = comprobante.montoTotal,totalExonerado=venta.totalExonerado
                        ,motivo=txtMotivo.Text,
                    };

                    DetalleNota detalle;
                    List<DetalleNota> detalles = new List<DetalleNota>();
                
                    foreach (var i in venta.detalles)
                    {
                     

                        detalle = new DetalleNota
                        {
                            cantidad = i.cantidad,
                            factor = i.factor,
                            idPresentacion = i.idProductoPresentacion,
                            idProducto = i.idProducto,
                            igv = i.igv,
                            total = i.total,
                            precio = i.precio, 
                             idTipoAfectacionIgv=i.idTipoAfectacionIgv,
                        };

                        detalles.Add(detalle);
                    }
                    nota.detalles = detalles;

                    if (nservice.guardarNotaCompleta(nota, venta, true, false, false, false, false))
                    {
                        nota.tipoNota = tnservice.buscar((int)comboTipoNota.SelectedValue);
                        List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
                        ProductoPresentacion ppp = null;
                        foreach (var d in venta.detalles)
                        {
                            ppp = ppservice.buscar(d.idProductoPresentacion);
                            presentaciones.Add(ppp);
                        }
                        string codDoc = CodigosSUNAT.RUC;
                       NotaCreditoEfac fac = new NotaCreditoEfac();
                        string json = "";
                        if (comprobante.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

                        nota.comprobante = comprobante;
                               CreditNote inv = new CreditNote(nota, LocalImpl.local, presentaciones, codDoc,
                                 Util.Convertir(string.Format("{0:0.00}", nota.total), true), igv.valorDouble,comprobante.tipoComprobante.codigo_sunat);
                                fac.CreditNote.Add(inv);
                                json = JsonConvert.SerializeObject(fac, Formatting.Indented);
                        nota.tipoComprobante = tipoCredito;
                         getToken(json,nota,this,btnSeguir);

                        
                    }
                }
            }
            else if (comboTipoNota.Text.Equals("DESCUENTO_GLOBAL") || comboTipoNota.Text.Equals("INTERES_POR_MORA") || comboTipoNota.Text.Equals("PENALIDADES_OTROS_CONCEPTOS"))
            {

            }
            else if (comboTipoNota.Text=="DESCUENTO_POR_ITEM"|| comboTipoNota.Text.Trim
                ().Equals("DEVOLUCION_POR_ITEM")|| comboTipoNota.Text.Trim().Equals("DISMINUCION_EN_EL_VALOR")
                || comboTipoNota.Text.Trim().Equals("AUMENTO_DE_VALOR")) {
                TipoComprobantePago tc = tipoCredito;
                if (comboTipoNota.Text.Equals("AUMENTO_DE_VALOR")) tc = tipoDebito;
                
                FormNuevaNotaCredito form = new FormNuevaNotaCredito(comprobante,venta,cliente,comboTipoNota.Text,tc);
                form.pasado +=new FormNuevaNotaCredito.pasar(terminar);
                form.ShowDialog();
            }
            
            //SI ES DEVOLUCION TOTAL
        }

        public async Task getToken(string json, Nota cp,MaterialForm formulario,MaterialRaisedButton boton)
        {//"20102536593-03-BF04-00000020" + ".json";
            string file = LocalImpl.getInstancia().ruc + "-" + cp.tipoComprobante.codigo_sunat + "-" + cp.serie + "-" + Util.NormalizarCampo(cp.numero.ToString(), 8) + ".json";
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");

            byte[] bytes = await (sc.ReadAsByteArrayAsync());
            File.WriteAllBytes(@"D:\jsonEfac\" + file, bytes);

            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);

            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token = JsonConvert.DeserializeObject<AccesToken>(r);
                try
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Clear();
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                    MultipartFormDataContent contenido = new MultipartFormDataContent();

                    // MultipartFormDataContent form = new MultipartFormDataContent();
                    HttpContent contnt;//= new StringContent("fileToUpload");
                                       //  form.Add(contnt, "fileToUpload");
                    HttpContent fileContent = new StreamContent(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)));
                    var stream = new FileStream(@"D:\jsonEfac\" + file, FileMode.Open);
                    contnt = new StreamContent(stream);
                    contnt.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        Name = "file",
                        FileName = file
                    };
                    contenido.Add(contnt);
                    contenido.Headers.Remove("Content_type");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);

                    client.DefaultRequestHeaders.Add("Accept", "*/*");
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                    var resp = await client.PostAsync("https://ose.efact.pe/api-efact-ose/v1/document", contenido);

                    if (resp.IsSuccessStatusCode)
                    {
                        var re = await resp.Content.ReadAsStringAsync();
                        TicketEfac tikcket = JsonConvert.DeserializeObject<TicketEfac>(re);
                        Nota cp2 = nservice.buscarSolo(cp.id);
                        cp.hash = tikcket.description;
                        cp2.hash = tikcket.description;
                      //  cp2.env= true;
                        nservice.editar(cp2);
                        HttpClient clien = new HttpClient();
                        clien.DefaultRequestHeaders.Accept.Clear();
                        clien.DefaultRequestHeaders.Clear();
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                        clien.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                        clien.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                        HttpResponseMessage respo = await clien.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + tikcket.description);
                        if (respo.IsSuccessStatusCode)
                        {
                            File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await respo.Content.ReadAsByteArrayAsync());
                            Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");
                            formulario.Close();
                        }
                        else MessageBox.Show("NOSE PUDO IMPRIMIR PDF" + respo.ToString());
                    }
                    else MessageBox.Show("NOSE PUDO ENVIAR ERROR DE CONEXION");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + e.StackTrace);
                }
            }
            else MessageBox.Show("ERROR DE ENVIO NO SE OBTUVO AUTENTIFICACION");

            boton.Visible = true;
        }

    }


}
