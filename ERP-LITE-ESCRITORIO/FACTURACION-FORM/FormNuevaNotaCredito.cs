﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.EFAC;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using Newtonsoft.Json;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    public partial class FormNuevaNotaCredito : MaterialForm
    {
        public delegate void pasar();
        public event pasar pasado;
        ComprobantePago comprobante = null;
        NotaImpl nservice = new NotaImpl();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        Venta venta = null;
        DataTable tablaN = new DataTable();
        DataTable tabla = new DataTable();
        Parametro igv;
        double total = 0;
        double nuevoTotalVenta = 0;
     double    totalExonerado=0;
        string tipoNota;
        TipoComprobantePago tipoComprobanteCD;
        TipoNota tipoNotaO;
        bool esDevolucion = false, esDisminucion = false, esAumento = false, esDescuento = false;
        public FormNuevaNotaCredito(ComprobantePago cp,Venta v,string cliente,string tipoNota,TipoComprobantePago tCD)
        {
            comprobante = cp;
            tipoNotaO = TipoNotaImpl.getInstancia().FirstOrDefault(x=>x.descripcion==tipoNota);
            if (tipoNotaO == null) this.Close();
            tipoComprobanteCD = tCD;
            this.tipoNota = tipoNota;
            igv = ParametroImpl.getInstancia().FirstOrDefault(x=>x.nombre=="IGV");
            if (igv == null) this.Close();
            venta = v;
            InitializeComponent();
            this.Text = tipoNota;
            lblTipoComprobante.Text = tipoComprobanteCD.descripcion.ToUpper() + " ELECTRONICA";
            lblTipoNota.Text = tipoNota;
            lblSerieNumero.Text = comprobante.tipoComprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(),8);

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
            lblCliente.Text = cliente;
            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("DESCUENTO");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("FACTOR");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Visible = false;
            grid.Columns[2].Visible = false;
            grid.Columns[3].Width = 550;
            grid.Columns[4].Width = 80;
            grid.Columns[5].Width = 80;
            grid.Columns[6].Width = 80;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 80;
            grid.Columns[9].Visible = false;
            tablaN.Columns.Add("ID");
            tablaN.Columns.Add("IDPRODUCTO");
            tablaN.Columns.Add("IDPRESENTACION");
            tablaN.Columns.Add("PRODUCTO");
            tablaN.Columns.Add("VALOR U.");
            tablaN.Columns.Add("CANTIDAD");
            tablaN.Columns.Add("IGV");
            tablaN.Columns.Add("TOTAL");
            tablaN.Columns.Add("FACTOR");
            tablaN.PrimaryKey = new DataColumn[] { tablaN.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tablaN;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 550;
            gridDetalle.Columns[4].Width = 100;
            gridDetalle.Columns[5].Width = 100;
            gridDetalle.Columns[6].Width = 100;
            gridDetalle.Columns[7].Width = 100;
            gridDetalle.Columns[8].Visible = false;

        }
        void llenar() {
         TipoNota tipoDescuentoItem=   TipoNotaImpl.getInstancia().FirstOrDefault(x=>x.descripcion=="DESCUENTO_POR_ITEM");
            foreach (var d in venta.detalles) {
                if (!d.anulado) {
                    double total = d.cantidadN * d.precioN;
                    ProductoPresentacion pp = ppservice.buscar(d.idProductoPresentacion);        
                    tabla.Rows.Add(d.id, d.idProducto, d.idProductoPresentacion,pp.nombre,d.precioN, 
                        String.Format("{0:0.0}",d.cantidadN),d.descuentoItem,d.igv
                    ,total-d.descuentoItem ,d.factor);
                }
            }
        }
        private void FormNuevaNotaCredito_Load(object sender, EventArgs e)
        {
            lblEmpresa.Text = LocalImpl.getInstancia().razonSocial;
            lblDireccionSede.Text = LocalImpl.getInstancia().direccion;
            lblRucEmpresa.Text = "RUC: " + LocalImpl.getInstancia().ruc;
            llenar();

            if (tipoNota.Equals("DESCUENTO_POR_ITEM"))  {    habilitar(true); lblCambio.Text = "Descontado (S/):"; esDescuento = true; }
            else if (tipoNota.Equals("DEVOLUCION_POR_ITEM")) { habilitar(true); lblCambio.Text = "Cantidad Devuelta:"; esDevolucion = true; }
            else if (tipoNota.Equals("DEVOLUCION_TOTAL")) { habilitar(false); esDevolucion = true; }
            else if (tipoNota.Equals("DISMINUCION_EN_EL_VALOR")) { habilitar(true); lblCambio.Text = "Disminución Valor (S/):"; esDisminucion = true; }
            else if (tipoNota.Equals("AUMENTO_DE_VALOR")) { habilitar(true); lblCambio.Text = "Aumento valor (S/):";esAumento = true; }
        }
        void habilitar(bool v) {
            txtCambio.Enabled = v;
            btnAplicar.Enabled = v;
            txtCambio.Focus();
            if (!v) for (int i=0;i<grid.RowCount;i++) tablaN.Rows.Add(grid.Rows[i].Cells["ID"].Value.ToString(),
                                                                        grid.SelectedRows[0].Cells["IDPRODUCTO"].Value.ToString(),
                                                                        grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString(),
                                                                        grid.SelectedRows[0].Cells["PRODUCTO"].Value.ToString(),
                                                                        grid.SelectedRows[0].Cells["PRECIO"].Value.ToString(),
                                                                        grid.SelectedRows[0].Cells["CANTIDAD"].Value.ToString(),
                                                                        Convert.ToDouble(grid.SelectedRows[0].Cells["TOTAL"].Value.ToString()) - Convert.ToDouble(grid.SelectedRows[0].Cells["TOTAL"].Value.ToString())/igv.valorDouble,
                                                                        grid.SelectedRows[0].Cells["TOTAL"].Value.ToString(),
                                                                        grid.SelectedRows[0].Cells["FACTOR"].Value.ToString());
            btnEliminar.Visible = v;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnAplicar_Click(object sender, EventArgs e)  {
            double igvItem = 0;
          double valor = 0;
                if (grid.SelectedRows.Count == 1)  { 
                  if (tipoNota.Equals("DESCUENTO_POR_ITEM")) {
                        double descuento = 0;
                        if (Util.esDouble(txtCambio.Text)) {
                            if (tablaN.Rows.Find(grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString()) == null) {
                                descuento = Convert.ToDouble(txtCambio.Text);
                                if (descuento <= 0) { MessageBox.Show("EL DESCUENTO NO PUEDE SER MENOR O IGUAL A 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                            if (descuento >= Convert.ToDouble(grid.SelectedRows[0].Cells["TOTAL"].Value.ToString())) { MessageBox.Show("EL DESCUENTO NO PUEDE SER MEYOR O IGUAL AL TOTAL", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                            if (Convert.ToDouble(grid.SelectedRows[0].Cells["IGV"].Value.ToString()) == 0) { valor = descuento; totalExonerado += descuento; } else { valor = descuento / (1 + igv.valorDouble); }
                            double cantidad = Convert.ToDouble(grid.SelectedRows[0].Cells["CANTIDAD"].Value.ToString());
                                tablaN.Rows.Add(grid.SelectedRows[0].Cells["ID"].Value.ToString(), grid.SelectedRows[0].Cells["IDPRODUCTO"].Value.ToString(), grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString(), grid.SelectedRows[0].Cells["PRODUCTO"].Value.ToString(),valor, cantidad,descuento-valor, descuento, grid.SelectedRows[0].Cells["FACTOR"].Value.ToString());
                                total = total + descuento;
                                lblTotal.Text = string.Format("{0:0.0000}", total);      } } }

                    else if (tipoNota.Equals("DEVOLUCION_POR_ITEM")) {
                        double cantidadItem = 0;
                        if (Util.esDouble(txtCambio.Text)) {
                            if (tablaN.Rows.Find(grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString()) == null) {
                                cantidadItem = Convert.ToDouble(txtCambio.Text);
                                if (cantidadItem <= 0) { MessageBox.Show("LA CANTIDAD NO PUEDE SER 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                                if (cantidadItem > Convert.ToDouble(grid.SelectedRows[0].Cells["CANTIDAD"].Value.ToString())) { MessageBox.Show("LA DEVOLUCION NO PUEDE MAYOR A LA CANTIDAD", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                                double precio = Convert.ToDouble(grid.SelectedRows[0].Cells["PRECIO"].Value.ToString());
                                double totalItem = cantidadItem * precio;
                            if (Convert.ToDouble(grid.SelectedRows[0].Cells["IGV"].Value.ToString()) == 0) { igvItem = 0; valor = precio; totalExonerado += totalItem; } else { igvItem = totalItem - totalItem / (1 + igv.valorDouble);valor = precio / (1 + igv.valorDouble); }

                            tablaN.Rows.Add(grid.SelectedRows[0].Cells["ID"].Value.ToString(), grid.SelectedRows[0].Cells["IDPRODUCTO"].Value.ToString(), grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString(), grid.SelectedRows[0].Cells["PRODUCTO"].Value.ToString(),valor , cantidadItem,igvItem,  totalItem, grid.SelectedRows[0].Cells["FACTOR"].Value.ToString());
                                total = total + totalItem;
                                lblTotal.Text = string.Format("{0:0.0000}", total); } } }
                    else if (tipoNota.Equals("DISMINUCION_EN_EL_VALOR") || tipoNota.Equals("AUMENTO_DE_VALOR")) {
                        double precioItem = 0;
                        if (Util.esDouble(txtCambio.Text)) {
                            if (tablaN.Rows.Find(grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString()) == null) {
                                precioItem = Convert.ToDouble(txtCambio.Text);
                                if (precioItem <= 0) { MessageBox.Show("LA VARIACION NO PUEDE SER 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                                if (precioItem >= Convert.ToDouble(grid.SelectedRows[0].Cells["PRECIO"].Value.ToString()) && tipoNota.Equals("DISMINUCION_EN_EL_VALOR")) { MessageBox.Show("LA DISMINUCION NO PUEDE SER IGUAL O MAYOR AL PRECIO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); return; }
                                double cantidad = Convert.ToDouble(grid.SelectedRows[0].Cells["CANTIDAD"].Value.ToString());
                                double totalI = cantidad * precioItem;
                            if (Convert.ToDouble(grid.SelectedRows[0].Cells["IGV"].Value.ToString()) == 0) { igvItem = 0;valor =precioItem; totalExonerado += totalI; } else { igvItem = totalI - totalI / (1 + igv.valorDouble); valor = precioItem / (1 + igv.valorDouble); }

                            tablaN.Rows.Add(grid.SelectedRows[0].Cells["ID"].Value.ToString(), grid.SelectedRows[0].Cells["IDPRODUCTO"].Value.ToString(), grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString(), grid.SelectedRows[0].Cells["PRODUCTO"].Value.ToString(),  valor,  cantidad, igvItem, totalI, grid.SelectedRows[0].Cells["FACTOR"].Value.ToString());
                                total = total + totalI;
                                lblTotal.Text = string.Format("{0:0.0000}", total); } } }
                    }        
            }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            btnGuardar.Visible = false;
            if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Nota nota = new Nota
                {
                    esCredito = true,
                    idCliente = comprobante.idCliente,
                    idComprobanteVenta = comprobante.id,
                    idTipoNota = tipoNotaO.id,
                    idTipoComprobante = tipoComprobanteCD.id,
                    serie = tipoComprobanteCD.serie,
                    total = Convert.ToDouble(lblTotal.Text),
                    usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                };
                double opGravadas = (nota.total-totalExonerado)/ (1 + igv.valorDouble);
                nota.igv =nota.total-opGravadas -totalExonerado;
                nota.totalExonerado = totalExonerado;
                DetalleNota detalle;
                List<DetalleNota> detalles = new List<DetalleNota>();
                for (int i = 0; i < gridDetalle.RowCount; i++)
                {
                    detalle = new DetalleNota
                    {
                        cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString()),
                        factor = Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString()),
                        idPresentacion = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString()),
                        idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRODUCTO"].Value.ToString()),
                        igv = Convert.ToDouble(gridDetalle.Rows[i].Cells["IGV"].Value.ToString()),
                        precio = Convert.ToDouble(gridDetalle.Rows[i].Cells["VALOR U."].Value.ToString()),
                        total = Convert.ToDouble(gridDetalle.Rows[i].Cells["TOTAL"].Value.ToString()),
                        idTipoAfectacionIgv = 1
                    };
                    detalles.Add(detalle);
                }
                nota.detalles = detalles;


                if (nservice.guardarNotaCompleta(nota, venta, false, esDisminucion, esDescuento, esAumento, esDevolucion))
                {
                    if (lblTipoComprobante.Text.Equals("NOTA DE CREDITO ELECTRONICA"))
                    {
                       // TxtSUNAT.ConstruirNotaCreditoDebito(nota, comprobante, true);
                        nota.tipoNota = tipoNotaO;
                        List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
                        ProductoPresentacion ppp = null;
                        foreach (var d in venta.detalles)
                        {
                            ppp = ppservice.buscar(d.idProductoPresentacion);
                            presentaciones.Add(ppp);
                        }
                        string codDoc = CodigosSUNAT.RUC;
                        NotaCreditoEfac fac = new NotaCreditoEfac();
                        string json = "";
                        if (comprobante.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

                        nota.comprobante = comprobante;
                        CreditNote inv = new CreditNote(nota, LocalImpl.local, presentaciones, codDoc,
                          Util.Convertir(string.Format("{0:0.00}", nota.total), true), igv.valorDouble, comprobante.tipoComprobante.codigo_sunat);
                        fac.CreditNote.Add(inv);
                        json = JsonConvert.SerializeObject(fac, Formatting.Indented);

                        getToken(json, nota, this,btnGuardar);


                    }
                    else TxtSUNAT.ConstruirNotaCreditoDebito(nota, comprobante, false);
                    pasado();
                    this.Close();
                }
                else MessageBox.Show("ERROR INESPERADO", "CONFIRMAR", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           

        }
        public async Task getToken(string json, Nota cp, MaterialForm formulario,MaterialRaisedButton boton)
        {//"20102536593-03-BF04-00000020" + ".json";
            string file = LocalImpl.getInstancia().ruc + "-" + cp.tipoComprobante.codigo_sunat + "-" + cp.serie + "-" + Util.NormalizarCampo(cp.numero.ToString(), 8) + ".json";
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");

            byte[] bytes = await (sc.ReadAsByteArrayAsync());
            File.WriteAllBytes(@"D:\jsonEfac\" + file, bytes);

            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);

            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token = JsonConvert.DeserializeObject<AccesToken>(r);
                try
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Clear();
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                    MultipartFormDataContent contenido = new MultipartFormDataContent();

                    // MultipartFormDataContent form = new MultipartFormDataContent();
                    HttpContent contnt;//= new StringContent("fileToUpload");
                                       //  form.Add(contnt, "fileToUpload");
                    HttpContent fileContent = new StreamContent(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)));
                    var stream = new FileStream(@"D:\jsonEfac\" + file, FileMode.Open);
                    contnt = new StreamContent(stream);
                    contnt.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        Name = "file",
                        FileName = file
                    };
                    contenido.Add(contnt);
                    contenido.Headers.Remove("Content_type");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);

                    client.DefaultRequestHeaders.Add("Accept", "*/*");
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                    var resp = await client.PostAsync("https://ose.efact.pe/api-efact-ose/v1/document", contenido);

                    if (resp.IsSuccessStatusCode)
                    {
                        var re = await resp.Content.ReadAsStringAsync();
                        TicketEfac tikcket = JsonConvert.DeserializeObject<TicketEfac>(re);
                        Nota cp2 = nservice.buscarSolo(cp.id);
                        cp.hash = tikcket.description;
                        cp2.hash = tikcket.description;
                        //  cp2.env= true;
                        nservice.editar(cp2);
                        HttpClient clien = new HttpClient();
                        clien.DefaultRequestHeaders.Accept.Clear();
                        clien.DefaultRequestHeaders.Clear();
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                        clien.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                        clien.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                        HttpResponseMessage respo = await clien.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + tikcket.description);
                        if (respo.IsSuccessStatusCode)
                        {
                            File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await respo.Content.ReadAsByteArrayAsync());
                            Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");
                            formulario.Close();
                        }
                        else MessageBox.Show("NOSE PUDO IMPRIMIR PDF" + respo.ToString());
                    }
                    else MessageBox.Show("NOSE PUDO ENVIAR ERROR DE CONEXION");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + e.StackTrace);
                }
            }
            else MessageBox.Show("ERROR DE ENVIO NO SE OBTUVO AUTENTIFICACION");
            boton.Visible = true;
         
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            double subtotal = 0;
            if (gridDetalle.SelectedRows.Count == 1)
            {
                subtotal = Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString());
                tablaN.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index); }
            total = total - subtotal;
            if(Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["IGV"].Value.ToString()) ==0)
             totalExonerado -= subtotal;
            lblTotal.Text = string.Format("{0:0.0000}",total);
        }

        private void lblTotal_TextChanged(object sender, EventArgs e)
        {
            double opGravadas = (total-totalExonerado) / (1 + igv.valorDouble);
            lblOpGravadas.Text = String.Format("{0:0.0000}", opGravadas);
            lblIGV.Text = String.Format("{0:0.0000}", total - opGravadas-totalExonerado);   
            lblExonerado.Text= String.Format("{0:0.0000}", totalExonerado);
        }
    }
}
