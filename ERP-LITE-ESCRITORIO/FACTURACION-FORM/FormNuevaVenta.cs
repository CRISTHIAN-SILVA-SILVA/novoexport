﻿using ERP_LITE_ESCRITORIO.PEDIDOS_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PEDIDO_SERVICE;
using SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
namespace ERP_LITE_DESKTOP.FACTURACION_FORM{
    public partial class FormNuevaVenta : MaterialForm{
        MedioPago medioPago=null;
        DataTable tabla = new DataTable();
        AlmacenService aService = new AlmacenImpl();
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        Almacen a; Double cantidadAnterior, total = 0,totalExonerado=0;
        PedidoService pService = new PedidoImpl();
        ProductoService productoService = new ProductoImpl();
        ClienteService csrvice = new ClienteImpl();
        DetallePedidoService dpservice = new DetallePedidoImpl(); int pedido;
        TipoPagoService tpService = new TipoPagoImpl();
        MedioPagoService mpService = new MedioPagoImpl();
        double precioAnterior = 0;

        DataTable tablaP = new DataTable();
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        Pedido mPedido = null; Cliente empresa = null;
        public delegate void pasar();  public event pasar pasado;
        Cotizacion cotizacion = null;
        public FormNuevaVenta(int idPedido)  {
            InitializeComponent();
            if (igv == null) { this.Close(); MessageBox.Show("PARAMETRO IGV NO EXISTE"); }
            pedido = idPedido;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
            tablaP.Columns.Add("ID");
            tablaP.Columns.Add("PRODUCTO");
            tablaP.Columns.Add("CANTIDAD");
            gridProducto.DataSource = tablaP;
            gridProducto.Columns[0].Visible = false;
            gridProducto.Columns[1].Width = 900;
            gridProducto.Columns[2].Width = 100;

            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("FACTOR");
            tabla.Columns.Add("OBSERVACION");
            tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 540;
            gridDetalle.Columns[4].Width = 100;
            gridDetalle.Columns[5].Width = 100;
            gridDetalle.Columns[6].Width = 100;
            gridDetalle.Columns[7].Width = 100;
            gridDetalle.Columns[8].Visible = false;
            gridDetalle.Columns[9].Width = 120;
            
            comboMedioPago.DataSource = mpService.listarNoAnulados();
            comboTipoPago.DataSource = tpService.listarNoAnulados();        }
        void llenarClientes(bool esEmpresa) {
            List<Cliente> clientes= csrvice.listarClientes(); 
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            if (esEmpresa)         
                clientes = csrvice.listarEmpresas();
            clientes.ForEach(c => coleccion.Add(c.razonSocial));
            comboCliente.AutoCompleteCustomSource = coleccion;
            comboCliente.DataSource = clientes; }
        void terminar() {
            if(pedido!=0)
                pasado(); this.Close();}
        private void FormNuevaVenta_Load(object sender, EventArgs e)  {
            rbBoleta.Checked = true;
             this.Activate();
            a = aService.buscarPrincipal();         
            if (pedido != 0)  {
                mPedido = pService.buscar(pedido);
                if (mPedido != null)
                {
                   
                 if(mPedido.esBoleta)   rbBoleta.Checked = true;else rbFactura.Checked = true;
                    comboCliente.SelectedValue = (int)mPedido.idCliente;
                    comboCliente.Enabled = false;
                    txtDNI.Enabled = false;
                    rbBoleta.Enabled = false;
                    rbFactura.Enabled = false;
                    this.Text = "Pedido N°: " + mPedido.numeroPedido.ToString();
                    tabla.Clear();
                    double igvItem = 0;
                    foreach (var i in dpservice.listarPorPedido(mPedido.id))
                    {
                        igvItem = i.igv;
                        Producto p = productoService.buscar(i.producto.idProducto);
                        if (p.esExonerado) { igvItem = 0; totalExonerado += i.total; }
                        tabla.Rows.Add(i.id, i.producto.idProducto, i.idProducto, i.producto.nombre, i.precio, i.cantidad, string.Format("{0:0.0000}", igvItem), string.Format("{0:0.0000}", i.total), i.factor,i.observacion);
                    }             
                    total = mPedido.total;
                    lblTotal.Text = string.Format("{0:0.0000}", total);
                    btnCotizaciones.Enabled = false;
                    txtCotizacion.Text = mPedido.idCotizacion.ToString();
                    txtCotizacion.Enabled = false;
                }
                else rbBoleta.Checked = true;  }
            else this.Text = "Nueva Venta"; }
        private void btnClose_Click(object sender, EventArgs e)  { this.Close();  }
        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e) {
            tablaP.Clear();
            if (Util.validaCadena(txtBuscarProducto.Text))
                productoService.listarPorCadena(txtBuscarProducto.Text.ToUpper()).ForEach(x => tablaP.Rows.Add(x.id, x.nombre, x.stock));
        }
        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            Producto p = productoService.buscar(Convert.ToInt32(gridProducto.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            FormPresentaciones form = new FormPresentaciones(p);
            form.pasado += new FormPresentaciones.pasar(agregarItem);
            form.ShowDialog();
        }
        void agregarItem(ProductoPresentacion pp)
        {
            if (tabla.Rows.Find(pp.id.ToString()) == null)
                tabla.Rows.Add("", pp.idProducto, pp.id, pp.nombre, pp.precio, "0", "0", "0", pp.factor);
        }

        private void gridDetalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex == 5)
            {
                gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].ReadOnly = false;
                gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Selected = true;
                gridDetalle.BeginEdit(true);
            }
            else if (e.ColumnIndex == 4)
            {
                gridDetalle.Rows[e.RowIndex].Cells[4].ReadOnly = false;//PRECIOOOO
                gridDetalle.Rows[e.RowIndex].Cells[4].Selected = true;
                gridDetalle.BeginEdit(true);
            }
        }
        private void gridDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
            if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString()))
            {
                if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString()))
                {
                    Producto p = productoService.buscar(Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["IDPRODUCTO"].Value.ToString()));
                    double cantidad = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
                    double precio = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
                    double subTotal = cantidad * precio;
                    double igvItem = subTotal - subTotal / (1 + igv.valorDouble);
                    if (p.esExonerado) { igvItem = 0; totalExonerado += subTotal - cantidadAnterior * precio; }
                    gridDetalle.Rows[e.RowIndex].Cells["IGV"].Value = string.Format("{0:0.0000}", igvItem);
                    gridDetalle.Rows[e.RowIndex].Cells["TOTAL"].Value = string.Format("{0:0.0000}", subTotal);
                    total += (subTotal - cantidadAnterior * precioAnterior);
                    lblTotal.Text = string.Format("{0:0.0000}", total);
                }
                else { MessageBox.Show("PRECIO INCORRECTO"); gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value = cantidadAnterior; }
            }
            else { MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value = cantidadAnterior; }
        }
        private void gridDetalle_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)  {
            cantidadAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
            precioAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
        }
        private void btnEliminar_Click(object sender, EventArgs e) {
            if (gridDetalle.SelectedRows.Count == 1)  {
                double subtotal = Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString());
              
                total = total - subtotal;
                if (Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["IGV"].Value.ToString()) == 0) 
                    totalExonerado -= subtotal;               
                lblTotal.Text = string.Format("{0:0.0000}", total); tabla.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
            } }
        void crearVenta(int cliente) {
            Venta venta = new Venta {  
                subTotal = total, montoTotal = total,
          //  orden=txtOrden.Text,
                igv = Convert.ToDouble(lblIGV.Text),
                idTipoPago = (int)comboTipoPago.SelectedValue,
                idCliente = cliente, idPedido = pedido,totalExonerado = totalExonerado, opGravadas = Convert.ToDouble(lblOpGravadas.Text),};
            if (cotizacion != null) venta.idCotizacion = cotizacion.id;
            if (mPedido != null) { venta.usuarioCreate = mPedido.usuarioCreate; } else venta.usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni;
            if (comboTipoPago.Text.Equals("PAGO A CREDITO")) {
                
                 venta.interes = Convert.ToDouble(txtInteres.Value.ToString()); venta.diasCredito = (int)txtDias.Value;
              
                venta.opGravadas += venta.interes / (1+igv.valorDouble);
                venta.igv = venta.montoTotal - venta.opGravadas - venta.totalExonerado;
            } 
            TipoComprobantePago tcp = null;
            TipoComprobantePagoService tcps = new TipoComprobanteImpl();
            if (rbBoleta.Checked)   tcp = tcps.buscarPorNombre("Boleta de Venta"); else   tcp = tcps.buscarPorNombre("Factura");
            if (tcp == null) { MessageBox.Show("NO EXISTE TIPO DE COMPROBANTE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);return; }
            ComprobantePago cp = new ComprobantePago { idCliente = cliente,
                idMedioPago = (int)comboMedioPago.SelectedValue,
                serie = tcp.serie, generaDetraccion = cehckDetraccion.Checked,
                    idTipoComprobante=tcp.id,
                    montoTotal = venta.montoTotal,
                    subTotal = venta.montoTotal,hash="",
                    usuarioCreate = venta.usuarioCreate,idTipoPago=(int)comboTipoPago.SelectedValue,  };
            if (comboTipoPago.Text.Equals("PAGO UNICO"))            {     
               cp.montoPagado = cp.montoTotal;
                cp.pagado = true; }
            List<DetalleVenta> detalles = new List<DetalleVenta>();
            DetalleVenta dv;
            Producto p;
            int tipoAfectacion = 1;
            double totalDetraccion = 0;
            for (int i = 0; i < gridDetalle.Rows.Count; i++)  {
                dv = new DetalleVenta
                {
                    idTipoAfectacionIgv = tipoAfectacion,
                    idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRODUCTO"].Value.ToString()),
                    idProductoPresentacion = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString()),
                    igv = Convert.ToDouble(gridDetalle.Rows[i].Cells["IGV"].Value.ToString()),
                    precio = Convert.ToDouble(gridDetalle.Rows[i].Cells["PRECIO"].Value.ToString()),
                    total = Convert.ToDouble(gridDetalle.Rows[i].Cells["TOTAL"].Value.ToString()),
                    observacion= gridDetalle.Rows[i].Cells["OBSERVACION"].Value.ToString(),
                };

                p =productoService.buscar(dv.idProducto);
                if (p.esExonerado)
                    tipoAfectacion = 9;
                else tipoAfectacion = 1;
                if (p.esDetraccion) {
                    if (dv.total >= p.montoDetraccion) {
                        totalDetraccion += dv.total*p.porcentajeDetraccion/100;
                    }
                  
                }
                dv.idTipoAfectacionIgv = tipoAfectacion;
                dv.cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString());
                dv.factor =  Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString());
                dv.precioN = dv.precio;dv.cantidadN = dv.cantidad;
                
                if (dv.cantidad <= 0) { MessageBox.Show("EL PRODUCTO: " + gridDetalle.Rows[i].Cells["PRODUCTO"].Value.ToString() + "; TIENE CANTIDAD 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                detalles.Add(dv); }
            cp.montoDetraccion = totalDetraccion;
            venta.detalles = detalles;
            FormDetalleVenta form = new FormDetalleVenta(venta,cp,tcp,comboTipoPago.Text,comboMedioPago.Text,a,false);
            form.pasado += new FormDetalleVenta.pasar(terminar);
            form.ShowDialog();
        }


        private void btnGuardar_Click(object sender, EventArgs e) {
            if (comboCliente.Text.Trim().Equals("")) { MessageBox.Show("CLIENTE INCORRECTO"); return; }
            if (comboTipoPago.SelectedValue!=null)
                if (comboMedioPago.SelectedValue != null)
                    if (gridDetalle.RowCount > 0) {
                        if (rbBoleta.Checked)   {
                            if (comboCliente.SelectedValue != null)
                                crearVenta((int)comboCliente.SelectedValue);
                            else {if (Util.validaCadena(comboCliente.Text)) {
                                      if (Util.esDNI(txtDNI.Text))  {
                                        Cliente cli = new Cliente { direccion = "", email = "", dniRepresentante =txtDNI.Text, ruc = "", telefono = "", razonSocial = comboCliente.Text.ToUpper() };
                                       Cliente existe = csrvice.buscarPorDNI(txtDNI.Text);
                                        if (cli.dniRepresentante != "00000000")
                                            if (existe != null)
                                            { MessageBox.Show("DNI YA ESTA REGISTRADA"); return; }
                                            else { csrvice.crear(cli); crearVenta(cli.id); }
                                        else { csrvice.crear(cli); crearVenta(cli.id); }
                                    }
                                else MessageBox.Show("DNI INCORRECTO"); }
                               else MessageBox.Show("CLIENTE INCORRECTO");
                            }
                        }
                        else if (comboCliente.SelectedValue != null)
                            crearVenta((int)comboCliente.SelectedValue);
                        else MessageBox.Show("NO EXISTE CLIENTE");                        
                   }else MessageBox.Show("LA VENTA ES VACIA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);          }
        private void btnCotizaciones_Click(object sender, EventArgs e)
        {

        }
        private void rbBoleta_CheckedChanged(object sender, EventArgs e) {
            if (rbBoleta.Checked)  {
                comboCliente.Focus();
                llenarClientes(false);
            
               
                if (mPedido == null) {
                    comboCliente.Text = " ";
                    comboCliente.Text = ""; txtDNI.Text = "00000000"; }
                     } }
        private void rbFactura_CheckedChanged(object sender, EventArgs e)  {           
            if (rbFactura.Checked) {
                comboCliente.Focus();
                llenarClientes(true);
                if (mPedido == null)
                {
                    comboCliente.Text = " ";
                    comboCliente.Text = ""; lblDocIdentidad.Text = ""; }
                  } }
        private void lblTotal_TextChanged(object sender, EventArgs e) {
            double opGravadas = (total -totalExonerado)/ (1 + igv.valorDouble);
            lblOpGravadas.Text = String.Format("{0:0.0000}", opGravadas);
            lblIGV.Text= String.Format("{0:0.0000}", total-totalExonerado -opGravadas); 
            lblExoneradas.Text = string.Format("{0:0.0000}",totalExonerado); }
        private void comboTipoPago_SelectedValueChanged(object sender, EventArgs e)  {
            if (comboTipoPago.Text.Equals("PAGO UNICO"))
            { comboMedioPago.Enabled = true; panelCredito.Visible = false;  }
            else  {
                comboMedioPago.Enabled = false;
                if (comboTipoPago.Text.Equals("PAGO A CREDITO"))
                { panelCredito.Visible = true; } } }
        private void comboCliente_SelectedValueChanged(object sender, EventArgs e){   
            Cliente c = csrvice.buscar((int)comboCliente.SelectedValue);
            if (c != null) if (rbBoleta.Checked) { txtDNI.Text = c.dniRepresentante; txtDNI.Enabled = false; } else lblDocIdentidad.Text = c.ruc; else this.Close();}

        private void comboTipoPago_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        void pasadoCotizacion(Cotizacion c) {
            txtCotizacion.Text=c.serie+"-"+Util.NormalizarCampo( c.numero.ToString(),8);
            cotizacion = c;
            if (c.cliente.esEmpresa) rbFactura.Checked = true;
            else rbBoleta.Checked = true;
            comboCliente.SelectedValue = c.idCliente;
            comboTipoPago.SelectedValue = c.idTipoPago;      
            if (comboTipoPago.Text.Equals("PAGO A CREDITO")) {
                txtDias.Value =(decimal) c.diasCredito;
                txtInteres.Value =(decimal) c.porcentajeInteres;  }
            ProductoPresentacion pp =null;
            double totaExonerado=0;
            double igvv = 0;
            //LLENAR COTIZACION DETALLE
            foreach (var d in c.detalles)
            {
                igvv = 0;
                total = 0;
                totalExonerado = 0;
                pp = ppService.buscar(d.idProducto);
                total += d.total;
                if (pp.producto.esExonerado)
                {
                    totalExonerado += d.total;
                    igvv = 0;
                }
                else igvv = d.igv;
                tabla.Rows.Add(d.id, pp.idProducto, pp.id, pp.producto.nombre, d.precio, d.cantidad, d.igv, d.total, d.factor);
            }
            lblTotal.Text = string.Format("{0:0.0000}", c.total);
        }
        private void btnCotizaciones_Click_1(object sender, EventArgs e)
        {
            FormAdminCotizaciones form = new FormAdminCotizaciones(true);
            form.pasado += new FormAdminCotizaciones.pasar(pasadoCotizacion);
            form.ShowDialog();
        }

        private void btnObservacion_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                gridDetalle.Rows[gridDetalle.SelectedRows[0].Index].Cells["OBSERVACION"].Value = txtObservacion.Text.ToUpper();
            }
        }

        private void cehckDetraccion_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void comboCliente_TextUpdate(object sender, EventArgs e){
            if (comboCliente.SelectedValue == null) { txtDNI.Enabled = true; txtDNI.Text = "00000000"; }}
        private void comboMedioPago_SelectedValueChanged(object sender, EventArgs e){ }
    }
}
