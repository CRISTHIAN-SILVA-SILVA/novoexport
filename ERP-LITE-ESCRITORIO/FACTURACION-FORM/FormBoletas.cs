﻿using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    public partial class FormBoletas : Form
    {

        TipoComprobantePagoService tcpservice = new TipoComprobanteImpl();
        ComprobantePagoImpl cpservice = new ComprobantePagoImpl();
        ClienteImpl cservice = new ClienteImpl();
        VentaService vservice = new VentaImpl();
        DataTable tabla = new DataTable();
        public FormBoletas()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("CLIENTE");
            tabla.Columns.Add("TIPO DE PAGO");
            tabla.Columns.Add("MEDIO DE PAGO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("ESTADO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 80;
            grid.Columns[2].Width = 70;
            grid.Columns[3].Width = 90;
            grid.Columns[4].Width = 320;
            grid.Columns[5].Width = 180;
            grid.Columns[6].Width = 160;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 100;

            List<Cliente> proveedores = cservice.listarNoAnulados();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            proveedores.ForEach(c => coleccion.Add(c.razonSocial));
            comboProveedor.AutoCompleteCustomSource = coleccion;
            comboProveedor.DataSource = proveedores;
        }

        private void FormBoletas_Load(object sender, EventArgs e)
        {
            comboProveedor.Visible = false;
           checkBoleta.Checked = true;
            checkFactura.Checked = true;
            txtMontoInicial.Visible = false;
            txtMontoFinal.Visible = false;
            txtNumero.Visible = false;
            txtInicio.Visible = false;
            txtFin.Visible = false;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            tabla.Clear();
            string estado = "";
            if (rbFecha.Checked)
            {
                foreach (var x in cpservice.listarPorFechas(txtInicio.Value.Date, txtFin.Value.Date.AddDays(1))) {
                    if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                    tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
          x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal,estado));
                }
                

            }
            else if (rbMonto.Checked)
            {
                if (Util.esDouble(txtMontoInicial.Text) && Util.esDouble(txtMontoFinal.Text))
                    foreach (var x in cpservice.listarPorMontos(Convert.ToDouble(txtMontoInicial.Text), Convert.ToDouble(txtMontoFinal.Text)))
                    {
                        if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                        tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
                x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal),estado);
                    }

                else MessageBox.Show("MONTOS INVALIDOS","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else if(rbNumero.Checked) {

                if (Util.esDouble(txtNumero.Text))
                    foreach (var x in cpservice.listarPorNumero(Convert.ToInt32(txtNumero.Text))) {
                        if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                        tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
          x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal),estado);
                    }

            }
            else 
            {
                if(comboProveedor.SelectedValue!=null)
                
                    foreach (var x in cpservice.listarPorCliente50((int)comboProveedor.SelectedValue))
                    {
                        if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                        tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
          x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal), estado);
                    }

            }
        }

        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFecha.Checked) {
                txtInicio.Select();
                txtMontoInicial.Visible = false;
                txtMontoFinal.Visible = false;
                txtNumero.Visible = false;
                txtInicio.Visible = true;
                txtFin.Visible = true;
                comboProveedor.Visible = false;
            }
        }

        private void rbMonto_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMonto.Checked) {
                txtMontoInicial.Focus();
                txtInicio.Visible = false;
                txtFin.Visible = false;
                txtNumero.Visible = false;
                txtMontoFinal.Visible = true;
                txtMontoInicial.Visible = true;
                comboProveedor.Visible = false;
            }
        }

        private void rbNumero_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNumero.Checked)
            {
                txtNumero.Focus();
                txtInicio.Visible = false;
                txtFin.Visible = false;
                txtNumero.Visible = true;
                txtMontoFinal.Visible = false;
                txtMontoInicial.Visible = false;
                comboProveedor.Visible = false;
            }

        }
        
        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ComprobantePago cp=null;
            if (grid.SelectedRows.Count == 1)
            { cp = cpservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                Venta venta = vservice.buscar(cp.idVenta);
                FormDetalleVenta form = new FormDetalleVenta(venta,cp,cp.tipoComprobante,cp.tipoPago.nombre,cp.medioPago.nombre,null,true);
                form.pasadoNota += new FormDetalleVenta.pasar(llenar);
                form.ShowDialog();
            }


                
        }

        private void checkBoleta_CheckedChanged(object sender, EventArgs e)
        {
            llenar();
        }

        private void checkFactura_CheckedChanged(object sender, EventArgs e)
        {
            llenar();
        }

        void llenar() {
            tabla.Clear();
            string estado = "";
            if (checkFactura.Checked)
            {
                if (checkBoleta.Checked)
                {
                    foreach (var x in cpservice.listarNoAnulados())
                    {
                        if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                        tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
                x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal), estado);

                    }
                }
                else
                {
                    TipoComprobantePago tcp = tcpservice.buscarPorNombre("Factura");
                    if (tcp != null)
                        foreach (var x in cpservice.listarPorTipoComprobante(tcp.id))
                        {
                            if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                            tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
                       x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal), estado);

                        }
                }
            }
            else {
                if (checkBoleta.Checked)
                {
                    TipoComprobantePago tcp = tcpservice.buscarPorNombre("Boleta de Venta");
                    if (tcp != null)
                        foreach (var x in cpservice.listarPorTipoComprobante(tcp.id)) {
                            if (x.enviado) estado = "ENVIADO"; else estado = "NO ENVIADO";
                            tabla.Rows.Add(x.id, x.fechaCreate.ToString("yyyy-MM-dd"),
                        x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoPago.nombre, x.medioPago.nombre, string.Format("{0:0.00}", x.montoTotal),estado);

                        } }
            }
        }
        
        private void btnLimpiar_Click_1(object sender, EventArgs e)
        {
            llenar();
        }

        private void materialRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCliente.Checked)
            {
                txtInicio.Select();
                txtMontoInicial.Visible = false;
                txtMontoFinal.Visible = false;
                txtNumero.Visible = false;
                txtInicio.Visible = false;
                txtFin.Visible = false;
                comboProveedor.Visible = true;
            }
        }
    }
}
