﻿namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{
    partial class FormAdminNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdminNotas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.checkDebito = new MaterialSkin.Controls.MaterialCheckBox();
            this.checkCredito = new MaterialSkin.Controls.MaterialCheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLimpiar = new MaterialSkin.Controls.MaterialFlatButton();
            this.checkComprobante = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtNumeroComprobante = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.rbNumero = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtFin = new System.Windows.Forms.DateTimePicker();
            this.btnBuscar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.rbMonto = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtMontoFinal = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.rbFecha = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtInicio = new System.Windows.Forms.DateTimePicker();
            this.txtMontoInicial = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExportar = new MaterialSkin.Controls.MaterialFlatButton();
            this.grid = new System.Windows.Forms.DataGridView();
            this.materialFlatButton1 = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // checkDebito
            // 
            this.checkDebito.AutoSize = true;
            this.checkDebito.Depth = 0;
            this.checkDebito.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkDebito.Location = new System.Drawing.Point(972, 19);
            this.checkDebito.Margin = new System.Windows.Forms.Padding(0);
            this.checkDebito.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkDebito.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkDebito.Name = "checkDebito";
            this.checkDebito.Ripple = true;
            this.checkDebito.Size = new System.Drawing.Size(137, 30);
            this.checkDebito.TabIndex = 130;
            this.checkDebito.Text = "NOTA DE DEBITO";
            this.checkDebito.UseVisualStyleBackColor = true;
            this.checkDebito.CheckedChanged += new System.EventHandler(this.checkDebito_CheckedChanged);
            // 
            // checkCredito
            // 
            this.checkCredito.AutoSize = true;
            this.checkCredito.Depth = 0;
            this.checkCredito.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkCredito.Location = new System.Drawing.Point(9, 19);
            this.checkCredito.Margin = new System.Windows.Forms.Padding(0);
            this.checkCredito.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkCredito.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkCredito.Name = "checkCredito";
            this.checkCredito.Ripple = true;
            this.checkCredito.Size = new System.Drawing.Size(146, 30);
            this.checkCredito.TabIndex = 129;
            this.checkCredito.Text = "NOTA DE CREDITO";
            this.checkCredito.UseVisualStyleBackColor = true;
            this.checkCredito.CheckedChanged += new System.EventHandler(this.checkCredito_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnLimpiar);
            this.panel1.Controls.Add(this.checkComprobante);
            this.panel1.Controls.Add(this.txtNumeroComprobante);
            this.panel1.Controls.Add(this.rbNumero);
            this.panel1.Controls.Add(this.txtNumero);
            this.panel1.Controls.Add(this.txtFin);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.rbMonto);
            this.panel1.Controls.Add(this.txtMontoFinal);
            this.panel1.Controls.Add(this.rbFecha);
            this.panel1.Controls.Add(this.txtInicio);
            this.panel1.Controls.Add(this.txtMontoInicial);
            this.panel1.Location = new System.Drawing.Point(12, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(927, 100);
            this.panel1.TabIndex = 128;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.AutoSize = true;
            this.btnLimpiar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnLimpiar.Depth = 0;
            this.btnLimpiar.Icon = null;
            this.btnLimpiar.Location = new System.Drawing.Point(774, 50);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnLimpiar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Primary = false;
            this.btnLimpiar.Size = new System.Drawing.Size(75, 36);
            this.btnLimpiar.TabIndex = 125;
            this.btnLimpiar.Text = "limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // checkComprobante
            // 
            this.checkComprobante.AutoSize = true;
            this.checkComprobante.Depth = 0;
            this.checkComprobante.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkComprobante.Location = new System.Drawing.Point(201, 11);
            this.checkComprobante.Margin = new System.Windows.Forms.Padding(0);
            this.checkComprobante.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkComprobante.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkComprobante.Name = "checkComprobante";
            this.checkComprobante.Ripple = true;
            this.checkComprobante.Size = new System.Drawing.Size(138, 30);
            this.checkComprobante.TabIndex = 120;
            this.checkComprobante.TabStop = true;
            this.checkComprobante.Text = "Por Comprobante";
            this.checkComprobante.UseVisualStyleBackColor = true;
            this.checkComprobante.CheckedChanged += new System.EventHandler(this.checkComprobante_CheckedChanged);
            // 
            // txtNumeroComprobante
            // 
            this.txtNumeroComprobante.Depth = 0;
            this.txtNumeroComprobante.Hint = "";
            this.txtNumeroComprobante.Location = new System.Drawing.Point(227, 38);
            this.txtNumeroComprobante.MaxLength = 32767;
            this.txtNumeroComprobante.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumeroComprobante.Name = "txtNumeroComprobante";
            this.txtNumeroComprobante.PasswordChar = '\0';
            this.txtNumeroComprobante.SelectedText = "";
            this.txtNumeroComprobante.SelectionLength = 0;
            this.txtNumeroComprobante.SelectionStart = 0;
            this.txtNumeroComprobante.Size = new System.Drawing.Size(125, 23);
            this.txtNumeroComprobante.TabIndex = 119;
            this.txtNumeroComprobante.TabStop = false;
            this.txtNumeroComprobante.UseSystemPasswordChar = false;
            // 
            // rbNumero
            // 
            this.rbNumero.AutoSize = true;
            this.rbNumero.Depth = 0;
            this.rbNumero.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbNumero.Location = new System.Drawing.Point(40, 11);
            this.rbNumero.Margin = new System.Windows.Forms.Padding(0);
            this.rbNumero.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbNumero.Name = "rbNumero";
            this.rbNumero.Ripple = true;
            this.rbNumero.Size = new System.Drawing.Size(103, 30);
            this.rbNumero.TabIndex = 112;
            this.rbNumero.TabStop = true;
            this.rbNumero.Text = "Por Número";
            this.rbNumero.UseVisualStyleBackColor = true;
            this.rbNumero.CheckedChanged += new System.EventHandler(this.rbNumero_CheckedChanged);
            // 
            // txtNumero
            // 
            this.txtNumero.Depth = 0;
            this.txtNumero.Hint = "";
            this.txtNumero.Location = new System.Drawing.Point(63, 38);
            this.txtNumero.MaxLength = 32767;
            this.txtNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.Size = new System.Drawing.Size(110, 23);
            this.txtNumero.TabIndex = 109;
            this.txtNumero.TabStop = false;
            this.txtNumero.UseSystemPasswordChar = false;
            // 
            // txtFin
            // 
            this.txtFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFin.Location = new System.Drawing.Point(529, 71);
            this.txtFin.Name = "txtFin";
            this.txtFin.Size = new System.Drawing.Size(224, 21);
            this.txtFin.TabIndex = 110;
            // 
            // btnBuscar
            // 
            this.btnBuscar.AutoSize = true;
            this.btnBuscar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnBuscar.Depth = 0;
            this.btnBuscar.Icon = null;
            this.btnBuscar.Location = new System.Drawing.Point(775, 5);
            this.btnBuscar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Primary = true;
            this.btnBuscar.Size = new System.Drawing.Size(74, 36);
            this.btnBuscar.TabIndex = 118;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // rbMonto
            // 
            this.rbMonto.AutoSize = true;
            this.rbMonto.Depth = 0;
            this.rbMonto.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbMonto.Location = new System.Drawing.Point(394, 11);
            this.rbMonto.Margin = new System.Windows.Forms.Padding(0);
            this.rbMonto.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbMonto.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbMonto.Name = "rbMonto";
            this.rbMonto.Ripple = true;
            this.rbMonto.Size = new System.Drawing.Size(94, 30);
            this.rbMonto.TabIndex = 113;
            this.rbMonto.TabStop = true;
            this.rbMonto.Text = "Por Monto";
            this.rbMonto.UseVisualStyleBackColor = true;
            this.rbMonto.CheckedChanged += new System.EventHandler(this.rbMonto_CheckedChanged);
            // 
            // txtMontoFinal
            // 
            this.txtMontoFinal.Depth = 0;
            this.txtMontoFinal.Hint = "";
            this.txtMontoFinal.Location = new System.Drawing.Point(404, 67);
            this.txtMontoFinal.MaxLength = 32767;
            this.txtMontoFinal.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMontoFinal.Name = "txtMontoFinal";
            this.txtMontoFinal.PasswordChar = '\0';
            this.txtMontoFinal.SelectedText = "";
            this.txtMontoFinal.SelectionLength = 0;
            this.txtMontoFinal.SelectionStart = 0;
            this.txtMontoFinal.Size = new System.Drawing.Size(84, 23);
            this.txtMontoFinal.TabIndex = 117;
            this.txtMontoFinal.TabStop = false;
            this.txtMontoFinal.UseSystemPasswordChar = false;
            // 
            // rbFecha
            // 
            this.rbFecha.AutoSize = true;
            this.rbFecha.Depth = 0;
            this.rbFecha.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbFecha.Location = new System.Drawing.Point(593, 11);
            this.rbFecha.Margin = new System.Windows.Forms.Padding(0);
            this.rbFecha.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbFecha.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbFecha.Name = "rbFecha";
            this.rbFecha.Ripple = true;
            this.rbFecha.Size = new System.Drawing.Size(91, 30);
            this.rbFecha.TabIndex = 114;
            this.rbFecha.TabStop = true;
            this.rbFecha.Text = "Por Fecha";
            this.rbFecha.UseVisualStyleBackColor = true;
            this.rbFecha.CheckedChanged += new System.EventHandler(this.rbFecha_CheckedChanged);
            // 
            // txtInicio
            // 
            this.txtInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicio.Location = new System.Drawing.Point(529, 44);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(224, 21);
            this.txtInicio.TabIndex = 116;
            // 
            // txtMontoInicial
            // 
            this.txtMontoInicial.Depth = 0;
            this.txtMontoInicial.Hint = "";
            this.txtMontoInicial.Location = new System.Drawing.Point(404, 38);
            this.txtMontoInicial.MaxLength = 32767;
            this.txtMontoInicial.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMontoInicial.Name = "txtMontoInicial";
            this.txtMontoInicial.PasswordChar = '\0';
            this.txtMontoInicial.SelectedText = "";
            this.txtMontoInicial.SelectionLength = 0;
            this.txtMontoInicial.SelectionStart = 0;
            this.txtMontoInicial.Size = new System.Drawing.Size(84, 23);
            this.txtMontoInicial.TabIndex = 115;
            this.txtMontoInicial.TabStop = false;
            this.txtMontoInicial.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label1.Location = new System.Drawing.Point(422, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(303, 24);
            this.label1.TabIndex = 126;
            this.label1.Text = "NOTA DE  CREDITO /  DEBITO";
            // 
            // btnExportar
            // 
            this.btnExportar.AutoSize = true;
            this.btnExportar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnExportar.Depth = 0;
            this.btnExportar.Icon = ((System.Drawing.Image)(resources.GetObject("btnExportar.Icon")));
            this.btnExportar.Location = new System.Drawing.Point(968, 106);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnExportar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Primary = false;
            this.btnExportar.Size = new System.Drawing.Size(118, 36);
            this.btnExportar.TabIndex = 125;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.BackgroundColor = System.Drawing.Color.White;
            this.grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.DefaultCellStyle = dataGridViewCellStyle3;
            this.grid.EnableHeadersVisualStyles = false;
            this.grid.GridColor = System.Drawing.Color.Black;
            this.grid.Location = new System.Drawing.Point(9, 154);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grid.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.grid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.grid.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.grid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.grid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.grid.RowTemplate.Height = 31;
            this.grid.RowTemplate.ReadOnly = true;
            this.grid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(1100, 458);
            this.grid.TabIndex = 124;
            this.grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellDoubleClick);
            // 
            // materialFlatButton1
            // 
            this.materialFlatButton1.AutoSize = true;
            this.materialFlatButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialFlatButton1.Depth = 0;
            this.materialFlatButton1.Icon = ((System.Drawing.Image)(resources.GetObject("materialFlatButton1.Icon")));
            this.materialFlatButton1.Location = new System.Drawing.Point(972, 59);
            this.materialFlatButton1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialFlatButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFlatButton1.Name = "materialFlatButton1";
            this.materialFlatButton1.Primary = false;
            this.materialFlatButton1.Size = new System.Drawing.Size(97, 36);
            this.materialFlatButton1.TabIndex = 172;
            this.materialFlatButton1.Text = "Enviar";
            this.materialFlatButton1.UseVisualStyleBackColor = true;
            this.materialFlatButton1.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // FormAdminNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1112, 564);
            this.Controls.Add(this.materialFlatButton1);
            this.Controls.Add(this.checkDebito);
            this.Controls.Add(this.checkCredito);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.grid);
            this.Name = "FormAdminNotas";
            this.Text = "FormAdminNotas";
            this.Load += new System.EventHandler(this.FormAdminNotas_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialCheckBox checkDebito;
        private MaterialSkin.Controls.MaterialCheckBox checkCredito;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialRadioButton rbNumero;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumero;
        private System.Windows.Forms.DateTimePicker txtFin;
        private MaterialSkin.Controls.MaterialRaisedButton btnBuscar;
        private MaterialSkin.Controls.MaterialRadioButton rbMonto;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMontoFinal;
        private MaterialSkin.Controls.MaterialRadioButton rbFecha;
        private System.Windows.Forms.DateTimePicker txtInicio;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMontoInicial;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialFlatButton btnExportar;
        private System.Windows.Forms.DataGridView grid;
        private MaterialSkin.Controls.MaterialRadioButton checkComprobante;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumeroComprobante;
        private MaterialSkin.Controls.MaterialFlatButton btnLimpiar;
        private MaterialSkin.Controls.MaterialFlatButton materialFlatButton1;
    }
}