﻿using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.PEDIDO_SERVICE;
using SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    public partial class FormAdminVentas : Form
    {
        DataTable tabla = new DataTable();
        PedidoService pservice = new PedidoImpl();
        UsuarioService uservice = new UsuarioImp();
        public FormAdminVentas()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NUMERO PEDIDO");
            tabla.Columns.Add("CLIENTE");
            tabla.Columns.Add("VENDEDOR");

            tabla.Columns.Add("COTIZACION");
            tabla.Columns.Add("TOTAL");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 410;
            grid.Columns[3].Width = 410;
            grid.Columns[4].Width = 100;
            grid.Columns[5].Width = 80;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaVenta form = new FormNuevaVenta(0);
            
            form.ShowDialog();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcel(grid);
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            FormNuevaVenta form = new FormNuevaVenta(Convert.ToInt32( grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            form.pasado+=new FormNuevaVenta.pasar(cargar);
            form.ShowDialog();
        }

        public void cargar() {
            tabla.Clear();
            pservice.listarNoAnulados().ForEach(x => tabla.Rows.Add(x.id, x.numeroPedido,x.cliente.razonSocial, x.usuarioCreate, x.idCotizacion, x.total));

        }
        private void FormAdminVentas_Load(object sender, EventArgs e)
        {
            cargar(); 
        }
    }
}
