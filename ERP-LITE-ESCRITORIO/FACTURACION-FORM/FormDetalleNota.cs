﻿using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{
    public partial class FormDetalleNota : MaterialForm
    {
        Nota nota;
        public FormDetalleNota(Nota n)
        {
            nota = n;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormDetalleNota_Load(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            FormReporteNota form = new FormReporteNota(nota);
            form.ShowDialog();
        }
    }
}
