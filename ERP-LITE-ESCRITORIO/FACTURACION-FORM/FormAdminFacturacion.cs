﻿using ERP_LITE_ESCRITORIO.FACTURACION_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    public partial class FormAdminFacturacion : MaterialForm
    {
        public FormAdminFacturacion()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
        }
        void abrirForm(Object formHijo)
        {
            if (this.contenedor.Controls.Count > 0)
                this.contenedor.Controls.RemoveAt(0);
            Form hijo = formHijo as Form;
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            this.contenedor.Controls.Add(hijo);
            this.contenedor.Tag = hijo;
            hijo.Show();
        }
        private void FormAdminFacturacion_Load(object sender, EventArgs e)
        {
            abrirForm(new FormAdminVentas());
        }

        private void btnPedidos_Click(object sender, EventArgs e)
        {
            abrirForm (new FormAdminVentas());
     
        }

        private void btnBoletas_Click(object sender, EventArgs e)
        {
            abrirForm(new FormBoletas());
        }

        private void contenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnEnvio_Click(object sender, EventArgs e)
        {
            abrirForm(new FormEnvioComprobante());
        }

        private void btnNotaCredito_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminNotas());
        }

        private void btnGuiaRemision_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminGuiasRemision());
        }

        private void btnLimpiarFacturador_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE LIMPIAR FACTURADOR?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                String[] filePaths = Directory.GetFiles(@"D:\SFS_v1.2\sunat_archivos\sfs\DATA");
                foreach (string filename in filePaths)
                    File.Delete(filename);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            abrirForm(new FormNoEnviados());
        }
    }
}
