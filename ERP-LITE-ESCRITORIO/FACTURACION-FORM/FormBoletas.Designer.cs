﻿namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    partial class FormBoletas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBoletas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExportar = new MaterialSkin.Controls.MaterialFlatButton();
            this.grid = new System.Windows.Forms.DataGridView();
            this.txtNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtFin = new System.Windows.Forms.DateTimePicker();
            this.rbNumero = new MaterialSkin.Controls.MaterialRadioButton();
            this.rbMonto = new MaterialSkin.Controls.MaterialRadioButton();
            this.rbFecha = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtMontoInicial = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtInicio = new System.Windows.Forms.DateTimePicker();
            this.txtMontoFinal = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnBuscar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLimpiar = new MaterialSkin.Controls.MaterialFlatButton();
            this.checkBoleta = new MaterialSkin.Controls.MaterialCheckBox();
            this.checkFactura = new MaterialSkin.Controls.MaterialCheckBox();
            this.rbCliente = new MaterialSkin.Controls.MaterialRadioButton();
            this.comboProveedor = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label1.Location = new System.Drawing.Point(453, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(231, 24);
            this.label1.TabIndex = 33;
            this.label1.Text = "BOLETAS / FACTURAS";
            // 
            // btnExportar
            // 
            this.btnExportar.AutoSize = true;
            this.btnExportar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnExportar.Depth = 0;
            this.btnExportar.Icon = ((System.Drawing.Image)(resources.GetObject("btnExportar.Icon")));
            this.btnExportar.Location = new System.Drawing.Point(971, 97);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnExportar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Primary = false;
            this.btnExportar.Size = new System.Drawing.Size(118, 36);
            this.btnExportar.TabIndex = 32;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.BackgroundColor = System.Drawing.Color.White;
            this.grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.DefaultCellStyle = dataGridViewCellStyle3;
            this.grid.EnableHeadersVisualStyles = false;
            this.grid.GridColor = System.Drawing.Color.Black;
            this.grid.Location = new System.Drawing.Point(12, 145);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grid.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.grid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.grid.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.grid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.grid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.grid.RowTemplate.Height = 31;
            this.grid.RowTemplate.ReadOnly = true;
            this.grid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(1100, 458);
            this.grid.TabIndex = 31;
            this.grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellDoubleClick);
            // 
            // txtNumero
            // 
            this.txtNumero.Depth = 0;
            this.txtNumero.Hint = "";
            this.txtNumero.Location = new System.Drawing.Point(37, 42);
            this.txtNumero.MaxLength = 32767;
            this.txtNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.Size = new System.Drawing.Size(151, 23);
            this.txtNumero.TabIndex = 109;
            this.txtNumero.TabStop = false;
            this.txtNumero.UseSystemPasswordChar = false;
            // 
            // txtFin
            // 
            this.txtFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFin.Location = new System.Drawing.Point(372, 75);
            this.txtFin.Name = "txtFin";
            this.txtFin.Size = new System.Drawing.Size(224, 21);
            this.txtFin.TabIndex = 110;
            // 
            // rbNumero
            // 
            this.rbNumero.AutoSize = true;
            this.rbNumero.Depth = 0;
            this.rbNumero.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbNumero.Location = new System.Drawing.Point(55, 15);
            this.rbNumero.Margin = new System.Windows.Forms.Padding(0);
            this.rbNumero.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbNumero.Name = "rbNumero";
            this.rbNumero.Ripple = true;
            this.rbNumero.Size = new System.Drawing.Size(103, 30);
            this.rbNumero.TabIndex = 112;
            this.rbNumero.TabStop = true;
            this.rbNumero.Text = "Por Número";
            this.rbNumero.UseVisualStyleBackColor = true;
            this.rbNumero.CheckedChanged += new System.EventHandler(this.rbNumero_CheckedChanged);
            // 
            // rbMonto
            // 
            this.rbMonto.AutoSize = true;
            this.rbMonto.Depth = 0;
            this.rbMonto.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbMonto.Location = new System.Drawing.Point(237, 15);
            this.rbMonto.Margin = new System.Windows.Forms.Padding(0);
            this.rbMonto.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbMonto.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbMonto.Name = "rbMonto";
            this.rbMonto.Ripple = true;
            this.rbMonto.Size = new System.Drawing.Size(94, 30);
            this.rbMonto.TabIndex = 113;
            this.rbMonto.TabStop = true;
            this.rbMonto.Text = "Por Monto";
            this.rbMonto.UseVisualStyleBackColor = true;
            this.rbMonto.CheckedChanged += new System.EventHandler(this.rbMonto_CheckedChanged);
            // 
            // rbFecha
            // 
            this.rbFecha.AutoSize = true;
            this.rbFecha.Depth = 0;
            this.rbFecha.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbFecha.Location = new System.Drawing.Point(436, 15);
            this.rbFecha.Margin = new System.Windows.Forms.Padding(0);
            this.rbFecha.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbFecha.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbFecha.Name = "rbFecha";
            this.rbFecha.Ripple = true;
            this.rbFecha.Size = new System.Drawing.Size(91, 30);
            this.rbFecha.TabIndex = 114;
            this.rbFecha.TabStop = true;
            this.rbFecha.Text = "Por Fecha";
            this.rbFecha.UseVisualStyleBackColor = true;
            this.rbFecha.CheckedChanged += new System.EventHandler(this.rbFecha_CheckedChanged);
            // 
            // txtMontoInicial
            // 
            this.txtMontoInicial.Depth = 0;
            this.txtMontoInicial.Hint = "";
            this.txtMontoInicial.Location = new System.Drawing.Point(247, 42);
            this.txtMontoInicial.MaxLength = 32767;
            this.txtMontoInicial.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMontoInicial.Name = "txtMontoInicial";
            this.txtMontoInicial.PasswordChar = '\0';
            this.txtMontoInicial.SelectedText = "";
            this.txtMontoInicial.SelectionLength = 0;
            this.txtMontoInicial.SelectionStart = 0;
            this.txtMontoInicial.Size = new System.Drawing.Size(84, 23);
            this.txtMontoInicial.TabIndex = 115;
            this.txtMontoInicial.TabStop = false;
            this.txtMontoInicial.UseSystemPasswordChar = false;
            // 
            // txtInicio
            // 
            this.txtInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicio.Location = new System.Drawing.Point(372, 48);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(224, 21);
            this.txtInicio.TabIndex = 116;
            // 
            // txtMontoFinal
            // 
            this.txtMontoFinal.Depth = 0;
            this.txtMontoFinal.Hint = "";
            this.txtMontoFinal.Location = new System.Drawing.Point(247, 71);
            this.txtMontoFinal.MaxLength = 32767;
            this.txtMontoFinal.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMontoFinal.Name = "txtMontoFinal";
            this.txtMontoFinal.PasswordChar = '\0';
            this.txtMontoFinal.SelectedText = "";
            this.txtMontoFinal.SelectionLength = 0;
            this.txtMontoFinal.SelectionStart = 0;
            this.txtMontoFinal.Size = new System.Drawing.Size(84, 23);
            this.txtMontoFinal.TabIndex = 117;
            this.txtMontoFinal.TabStop = false;
            this.txtMontoFinal.UseSystemPasswordChar = false;
            // 
            // btnBuscar
            // 
            this.btnBuscar.AutoSize = true;
            this.btnBuscar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnBuscar.Depth = 0;
            this.btnBuscar.Icon = null;
            this.btnBuscar.Location = new System.Drawing.Point(875, 15);
            this.btnBuscar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Primary = true;
            this.btnBuscar.Size = new System.Drawing.Size(74, 36);
            this.btnBuscar.TabIndex = 118;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboProveedor);
            this.panel1.Controls.Add(this.rbCliente);
            this.panel1.Controls.Add(this.btnLimpiar);
            this.panel1.Controls.Add(this.rbNumero);
            this.panel1.Controls.Add(this.txtNumero);
            this.panel1.Controls.Add(this.txtFin);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.rbMonto);
            this.panel1.Controls.Add(this.txtMontoFinal);
            this.panel1.Controls.Add(this.rbFecha);
            this.panel1.Controls.Add(this.txtInicio);
            this.panel1.Controls.Add(this.txtMontoInicial);
            this.panel1.Location = new System.Drawing.Point(12, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 100);
            this.panel1.TabIndex = 121;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.AutoSize = true;
            this.btnLimpiar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnLimpiar.Depth = 0;
            this.btnLimpiar.Icon = null;
            this.btnLimpiar.Location = new System.Drawing.Point(875, 58);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnLimpiar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Primary = false;
            this.btnLimpiar.Size = new System.Drawing.Size(75, 36);
            this.btnLimpiar.TabIndex = 124;
            this.btnLimpiar.Text = "limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click_1);
            // 
            // checkBoleta
            // 
            this.checkBoleta.AutoSize = true;
            this.checkBoleta.Depth = 0;
            this.checkBoleta.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkBoleta.Location = new System.Drawing.Point(12, 10);
            this.checkBoleta.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoleta.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkBoleta.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkBoleta.Name = "checkBoleta";
            this.checkBoleta.Ripple = true;
            this.checkBoleta.Size = new System.Drawing.Size(148, 30);
            this.checkBoleta.TabIndex = 122;
            this.checkBoleta.Text = "BOLETA DE VENTA";
            this.checkBoleta.UseVisualStyleBackColor = true;
            this.checkBoleta.CheckedChanged += new System.EventHandler(this.checkBoleta_CheckedChanged);
            // 
            // checkFactura
            // 
            this.checkFactura.AutoSize = true;
            this.checkFactura.Depth = 0;
            this.checkFactura.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkFactura.Location = new System.Drawing.Point(998, 10);
            this.checkFactura.Margin = new System.Windows.Forms.Padding(0);
            this.checkFactura.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkFactura.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkFactura.Name = "checkFactura";
            this.checkFactura.Ripple = true;
            this.checkFactura.Size = new System.Drawing.Size(91, 30);
            this.checkFactura.TabIndex = 123;
            this.checkFactura.Text = "FACTURA";
            this.checkFactura.UseVisualStyleBackColor = true;
            this.checkFactura.CheckedChanged += new System.EventHandler(this.checkFactura_CheckedChanged);
            // 
            // rbCliente
            // 
            this.rbCliente.AutoSize = true;
            this.rbCliente.Depth = 0;
            this.rbCliente.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbCliente.Location = new System.Drawing.Point(683, 15);
            this.rbCliente.Margin = new System.Windows.Forms.Padding(0);
            this.rbCliente.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbCliente.Name = "rbCliente";
            this.rbCliente.Ripple = true;
            this.rbCliente.Size = new System.Drawing.Size(97, 30);
            this.rbCliente.TabIndex = 125;
            this.rbCliente.TabStop = true;
            this.rbCliente.Text = "Por Cliente";
            this.rbCliente.UseVisualStyleBackColor = true;
            this.rbCliente.CheckedChanged += new System.EventHandler(this.materialRadioButton1_CheckedChanged);
            // 
            // comboProveedor
            // 
            this.comboProveedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboProveedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboProveedor.DisplayMember = "razonSocial";
            this.comboProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboProveedor.FormattingEnabled = true;
            this.comboProveedor.Location = new System.Drawing.Point(602, 57);
            this.comboProveedor.MaxLength = 200;
            this.comboProveedor.Name = "comboProveedor";
            this.comboProveedor.Size = new System.Drawing.Size(266, 26);
            this.comboProveedor.TabIndex = 181;
            this.comboProveedor.ValueMember = "id";
            // 
            // FormBoletas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1119, 597);
            this.Controls.Add(this.checkFactura);
            this.Controls.Add(this.checkBoleta);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.grid);
            this.Name = "FormBoletas";
            this.Text = "FormBoletas";
            this.Load += new System.EventHandler(this.FormBoletas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialFlatButton btnExportar;
        private System.Windows.Forms.DataGridView grid;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumero;
        private System.Windows.Forms.DateTimePicker txtFin;
        private MaterialSkin.Controls.MaterialRadioButton rbNumero;
        private MaterialSkin.Controls.MaterialRadioButton rbMonto;
        private MaterialSkin.Controls.MaterialRadioButton rbFecha;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMontoInicial;
        private System.Windows.Forms.DateTimePicker txtInicio;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMontoFinal;
        private MaterialSkin.Controls.MaterialRaisedButton btnBuscar;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialCheckBox checkBoleta;
        private MaterialSkin.Controls.MaterialCheckBox checkFactura;
        private MaterialSkin.Controls.MaterialFlatButton btnLimpiar;
        private MaterialSkin.Controls.MaterialRadioButton rbCliente;
        private System.Windows.Forms.ComboBox comboProveedor;
    }
}