﻿using ERP_LITE_ESCRITORIO.FACTURACION_FORM;
using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using LibPrintTicket;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.EFAC;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using Newtonsoft.Json;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    public partial class FormDetalleVenta : MaterialForm
    {
        DataTable tabla = new DataTable();
        Venta venta = null;
        TipoPagoImpl tpservice = new TipoPagoImpl();
        ClienteService cservice = new ClienteImpl();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        ComprobantePagoImpl cpservice = new ComprobantePagoImpl();
        DetalleVentaImpl dservice = new DetalleVentaImpl();
        Parametro igv;
        String tipoPagoR, medioPagoR;
        VentaService vservice = new VentaImpl();
        ComprobantePago cp = null;
        CajaDiariaService cdservice = new CajaDiariaImpl();
        Almacen almacenPrincipal;
        TipoComprobantePago tcp;
        bool esDetalleVendido = false;
        public delegate void pasar();
        public event pasar pasado;
        TipoAfecIGVService afectacionIGVService = new TipoAfecIGVService();
        double opGratuitas = 0;
        GuiaRemisionImpl grservice = new GuiaRemisionImpl();
        GuiaRemision g=null;
        public delegate void pasarNota();
        public event pasar pasadoNota;
        public FormDetalleVenta(Venta v,ComprobantePago comprobante,TipoComprobantePago t,string tipoPago,string medioPago,Almacen a,bool esVentaa)
        {
           
            esDetalleVendido = esVentaa;
            InitializeComponent();
            tcp = t;
          
            venta = v;
           txtVencimiento.Value=DateTime.Now.AddDays(venta.diasCredito);
            almacenPrincipal = a;
            cp = comprobante;
            tipoPagoR = tipoPago;medioPagoR = medioPago;
            igv = ParametroImpl.getInstancia().FirstOrDefault(x=>x.nombre=="IGV");
            if (igv == null) this.Close();
         
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            lblEmpresa.Text = LocalImpl.getInstancia().razonSocial;
            lblDireccionSede.Text = LocalImpl.getInstancia().direccion;
            lblRucEmpresa.Text = "RUC: " + LocalImpl.getInstancia().ruc;
            lblTipoComprobante.Text =t.descripcion.ToUpper(); lblSerie.Text = "SERIE: "+t.serie;
            lblMedioPago.Text = medioPago;
            txtEmision.Value = DateTime.Now;
            lblDetraccion.Text =string.Format("{0:0.00}", cp.montoDetraccion);
            if (esVentaa)
            {
                materialFlatButton2.Visible = true;
                txtOrden.Enabled = false;
                txtOrden.Text = venta.orden;
                txtGuia.Enabled = false;
                txtGuia.Text = comprobante.guia;
                if (comprobante.pagado) { materialFlatButton2.Text = "CAMBIAR A NO PAGADO"; }
                else materialFlatButton2.Text = "CAMBIAR A PAGADO";
                if (comprobante.enviado) { btnNoEnviado.Text = "CAMBIAR A NO ENVIADO"; }
                else btnNoEnviado.Text = "CAMBIAR A ENVIADO";
                txtOrden.Enabled=true;
                txtGuia.Enabled = true;
                if (!comprobante.enviado) materialFlatButton1.Visible = true;
                btnImprimir.Visible = true;
                txtGuia.Enabled = true;
                cp.tipoComprobante = t;
                 g = grservice.buscarPorComprobante(cp.id);
                if (g != null) {
                    txtGuia.Text = g.numero.ToString(); }
                panelBotones.Visible = true;
                btnGuardar.Text ="IMPRIMIR";
                txtEmision.Value = comprobante.fechaCreate;
                txtVencimiento.Value = comprobante.fechaVencimiento;
                txtEmision.Enabled = false;
                txtVencimiento.Enabled = false;
                lblSerie.Text = comprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(), 8);
                lblEnviado.Visible = true;
                if (comprobante.enviado)
                { lblEnviado.Text = "COMPROBANTE ENVIADO"; lblEnviado.ForeColor =Color.FromArgb(129, 199, 132); btnCrearTxt.Visible = false; }
                else { lblEnviado.Text = "COMPROBANTE NO ENVIANDO"; lblEnviado.ForeColor = System.Drawing.ColorTranslator.FromHtml("#D32F2F"); btnCrearTxt.Visible = true; }
                if (!comprobante.enviado) btnNotaCredito.Visible = false;
                btnGuardar.Visible = false;
            }
            Cliente cliente = cservice.buscar(venta.idCliente);
            lblCliente.Text= cliente.razonSocial;
            lblTipoPago.Text = tipoPago;
            if(t.descripcion.Equals("Factura"))
                lblDocumentoCliente.Text = cliente.ruc;
            else lblDocumentoCliente.Text = cliente.dniRepresentante;
          
            txtVencimiento.Value = DateTime.Now.AddDays(venta.diasCredito);
            lblTotalLetras.Text = Util.Convertir(string.Format("{0:0.00}", venta.montoTotal),true);
         //   if (esVentaa) btnGratuito.Visible = false;
           
           
            if (venta.interes > 0&&!esVentaa) {
                double total = 0, igvVenta = 0, totalAnterior = 0;
                for (int i =0;i<venta.detalles.Count;i++) {
                   
                    totalAnterior = venta.detalles[i].total;
                    venta.detalles[i].total = venta.detalles[i].total+ venta.detalles[i].total*venta.interes/100;
                    venta.detalles[i].precio = venta.detalles[i].total/ venta.detalles[i].cantidad;
                    venta.detalles[i].precioN = venta.detalles[i].precio;
                    if (venta.detalles[i].igv != 0)
                        venta.detalles[i].igv = (venta.detalles[i].total / (1 + igv.valorDouble)) * igv.valorDouble;
                    else venta.totalExonerado += venta.detalles[i].total-totalAnterior;
                   total += venta.detalles[i].total;
                    igvVenta += venta.detalles[i].igv;
                }
                venta.igv = igvVenta;
                venta.montoTotal = total;
                venta.opGravadas = total - venta.totalExonerado - igvVenta;
                cp.montoN = total;
                cp.montoTotal = total;
                txtEmision.Value = venta.fechaCreate;
                txtVencimiento.Value = cp.fechaVencimiento;

            }
          


            lblIntereses.Text = string.Format("{0:0.0000}", venta.interes / 100 * venta.subTotal);
            lblOpExoneradas.Text = string.Format("{0:0.0000}", venta.totalExonerado);
            lblOpGravadas.Text = string.Format("{0:0.0000}", venta.montoTotal - venta.igv);
            lblIGV.Text = string.Format("{0:0.0000}", venta.igv);
            lblTotal.Text = string.Format("{0:0.0000}", venta.montoTotal);

            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("DESCUENTO");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("AFEC. IGV");
            tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 490;
            gridDetalle.Columns[4].Width = 80;
            gridDetalle.Columns[5].Width = 75;
            gridDetalle.Columns[6].Width = 80;
            gridDetalle.Columns[7].Width = 85;     gridDetalle.Columns[8].Width = 85; gridDetalle.Columns[9].Width = 250;

        }
        void terminar() {
        
                pasadoNota();
            this.Close();
          
        }
        void llenar() {
            ProductoPresentacion p;
            tabla.Clear();
            TipoAfectacionIGV tipoAfectacionIGV;
            foreach (var i in venta. detalles)
            {
                tipoAfectacionIGV = afectacionIGVService.buscar(i.idTipoAfectacionIgv);  
                p = ppservice.buscar(i.idProductoPresentacion);
                tabla.Rows.Add(i.id, p.idProducto, i.idProductoPresentacion, p.nombre+" "+i.observacion, String.Format("{0:0.0000}", i.precio), String.Format("{0:0.00}", i.cantidad), i.descuentoItem, String.Format("{0:0.0000}", i.igv), String.Format("{0:0.0000}", i.total - i.descuentoItem),tipoAfectacionIGV.descripcion);
            }
        }
        private void FormDetalleVenta_Load(object sender, EventArgs e)
        {
            llenar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNotaCredito_Click(object sender, EventArgs e)
        {
            FormEligeNota form = new FormEligeNota(cp,venta,lblCliente.Text,tcp);
            form.pasado += new FormEligeNota.pasar(terminar);
            form.ShowDialog();
        }

        private void btnGuia_Click(object sender, EventArgs e)
        {
            formGuia form = new formGuia(cp,venta);
            form.ShowDialog();
        }

        private void btnGratuito_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                gridDetalle.Rows[gridDetalle.SelectedRows[0].Index].DefaultCellStyle.BackColor = Color.Tomato;
                int idpresentacion = Convert.ToInt32(gridDetalle.Rows[gridDetalle.SelectedRows[0].Index].Cells["IDPRESENTACION"].Value.ToString());
                for (int i = 0; i < venta.detalles.Count; i++)
                {
                    if (venta.detalles[i].idProductoPresentacion == idpresentacion)
                     { venta.opGratuitas+= venta.detalles[i].total; if (venta.detalles[i].igv == 0)venta.totalExonerado -= venta.detalles[i].total;
                   
                        venta.detalles[i].total = 0;lblGratuitas.Text =string.Format("{0:0.00}", venta.opGratuitas);
                        venta.montoTotal -= venta.opGratuitas;
                        venta.opGravadas = venta.montoTotal / (1 +igv.valorDouble);
                        venta.igv = venta.opGravadas * igv.valorDouble;
                        lblIGV.Text= string.Format("{0:0.0000}", venta.igv);
                        lblOpExoneradas.Text = string.Format("{0:0.0000}", venta.totalExonerado);
                        lblOpGravadas.Text= string.Format("{0:0.0000}", venta.opGravadas);
                        lblTotal.Text= string.Format("{0:0.0000}", venta.montoTotal);
                        cp.montoTotal = venta.montoTotal;
                        cp.montoN = venta.montoTotal;
                       
                    }
                }
                llenar();
            }
        }
        void pasadoTarjeta() {

            pasado();
            this.Close();
        }

        private void btnCrearTxt_Click(object sender, EventArgs e)
        {
            TxtSUNAT.ConstruirComprobante(cp);
            MessageBox.Show("ARCHIVOS CREADOS");
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if(g!=null)
            if (Util.esDouble(txtGuia.Text)) {

                g.numero = Convert.ToInt32(txtGuia.Text);
                TxtSUNAT.construirGuia(g);
                g.motivoTranslado = null;
                g.transportista = null;
                    MessageBox.Show("GUIA CAMBIADA");
                    grservice.editar(g);
                txtGuia.Enabled = false;
            }
        
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;
            btnGuardar.Visible = false;
            if (MessageBox.Show("SEGURO DE GUARDAR", "CONFIRMAR?", MessageBoxButtons.YesNo
                , MessageBoxIcon.Question) == DialogResult.Yes)
            {           
                try
                {
                    if (!esDetalleVendido)
                    {
                        int idCaja = cdservice.getCaja();
                        cp.fechaVencimiento = txtVencimiento.Value;
                        venta.fechaCreate = txtEmision.Value;
                        cp.fechaCreate = txtEmision.Value;
                        cp.guia = txtGuia.Text;
                        venta.orden = txtOrden.Text;

                        if (idCaja == 0) { MessageBox.Show("ERROR INESPERADO AL ABRIR CAJA INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); btnGuardar.Enabled = true; btnGuardar.Visible = true; return; }
                        if (tipoPagoR.Equals("PAGO UNICO"))
                        {
                            cp.tipoPago = new TipoPago { nombre = "PAGO UNICO" };
                            cp.pagado = true;
                            if (!lblMedioPago.Text.Equals("EFECTIVO"))
                            {
                                FormPagoTarjeta form = new FormPagoTarjeta(cp, venta, almacenPrincipal.id, 0, 0, DateTime.Now, cp.montoN, txtGuia.Text);
                                form.pasado += new FormPagoTarjeta.pasar(pasadoTarjeta);
                                form.ShowDialog();
                            }
                            else
                            {
                                string r = cpservice.guardarComprobanteCompleto(cp, venta, idCaja, almacenPrincipal.id, true, null, txtGuia.Text);

                                if (r.Equals("CORRECTO"))
                                {
                               
                                    cp.venta = venta;
                                    List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
                                    ProductoPresentacion ppp = null;
                                    foreach (var d in venta.detalles)
                                    {
                                        ppp = ppservice.buscar(d.idProductoPresentacion);
                                        presentaciones.Add(ppp);
                                    }
                                    string codDoc = CodigosSUNAT.RUC;
                                    FacturaJsonEfac fac = new FacturaJsonEfac();
                                    EfacGuia facG = new EfacGuia();
                                    EfacGuiaOrden facGO = new EfacGuiaOrden();
                                    EfacOrden facO = new EfacOrden();
                                    string json = "";
                                    if (cp.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

                                    if (String.IsNullOrEmpty(cp.venta.orden) || string.IsNullOrWhiteSpace(cp.venta.orden))
                                    {
                                        if (String.IsNullOrEmpty(txtGuia.Text) || string.IsNullOrWhiteSpace(txtGuia.Text))
                                        {
                                            Invoice inv = new Invoice(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                             Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble);
                                            fac.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(fac, Formatting.Indented);
                                        }
                                        else
                                        {
                                            InvoceGuia inv = new InvoceGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                            Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, txtGuia.Text);
                                            facG.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(facG, Formatting.Indented);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(txtGuia.Text) || string.IsNullOrWhiteSpace(txtGuia.Text))
                                        {
                                            InvoiceNota inv = new InvoiceNota(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                             Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden);
                                            facO.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(facO, Formatting.Indented);

                                        }
                                        else
                                        {
                                            InvoiceNotaGuia inv = new InvoiceNotaGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                           Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden, txtGuia.Text);
                                            facGO.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(facGO, Formatting.Indented);


                                        }
                                    }
                                    getToken(json, cp,this);
                                }
                                else if (r.Equals("ERROR")) MessageBox.Show("ERROR INESPERADO INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                else MessageBox.Show(r, "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }else
                        if (tipoPagoR.Equals("PAGO A CREDITO"))
                        {
                            string r = cpservice.guardarComprobanteCompleto(cp, venta, 0, almacenPrincipal.id, false, null, txtGuia.Text);
                            if (r.Equals("CORRECTO"))
                            {
                                cp.venta = venta;
                                List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
                                ProductoPresentacion ppp = null;
                                foreach (var d in venta.detalles)
                                {
                                    ppp = ppservice.buscar(d.idProductoPresentacion);
                                    presentaciones.Add(ppp);
                                }
                                string codDoc = CodigosSUNAT.RUC;
                                FacturaJsonEfac fac = new FacturaJsonEfac();
                                EfacGuia facG = new EfacGuia();
                                EfacGuiaOrden facGO = new EfacGuiaOrden();
                                EfacOrden facO = new EfacOrden();
                                string json = "";
                                cp.tipoPago = new TipoPago { nombre="PAGO A CREDITO" };
                                if (cp.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

                                if (String.IsNullOrEmpty(cp.venta.orden) || string.IsNullOrWhiteSpace(cp.venta.orden))
                                {
                                    if (String.IsNullOrEmpty(txtGuia.Text) || string.IsNullOrWhiteSpace(txtGuia.Text))
                                    {
                                        Invoice inv = new Invoice(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                         Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble);
                                        fac.Invoice.Add(inv);
                                        json = JsonConvert.SerializeObject(fac, Formatting.Indented);
                                    }
                                    else
                                    {
                                        InvoceGuia inv = new InvoceGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                        Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, txtGuia.Text);
                                        facG.Invoice.Add(inv);
                                        json = JsonConvert.SerializeObject(facG, Formatting.Indented);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(txtGuia.Text) || string.IsNullOrWhiteSpace(txtGuia.Text))
                                    {
                                        InvoiceNota inv = new InvoiceNota(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                         Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden);
                                        facO.Invoice.Add(inv);
                                        json = JsonConvert.SerializeObject(facO, Formatting.Indented);

                                    }
                                    else
                                    {
                                        InvoiceNotaGuia inv = new InvoiceNotaGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                       Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden, txtGuia.Text);
                                        facGO.Invoice.Add(inv);
                                        json = JsonConvert.SerializeObject(facGO, Formatting.Indented);


                                    }
                                }
                                getToken(json, cp,this);
                            }
                            else if (r.Equals("ERROR")) MessageBox.Show("ERROR INESPERADO INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            else MessageBox.Show(r, "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        /*   if (MessageBox.Show("SEGURO DE IMPRIMIR", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                           {
                               // TicketService.imprimirTicket(cp, venta,cp.hash);
                               FormFactura form = new FormFactura(cp, venta, cp.hash);
                               form.ShowDialog();
                           }*/
                    }
                }
                catch (Exception k)
                {
                    MessageBox.Show(k.Message + "        " + k.ToString());
                }
              
            }
          //  btnGuardar.Visible = true;
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            getPDF(cp.hash);
        }

        //https://ose-gw1.efact.pe:443/api-efact-ose/oauth/token
        //https://ose-gw1.efact.pe:443/api-efact-ose/v1/document
        //https://ose-gw1.efact.pe:443/api-efact-ose/v1/cdr/{ticket}
        //https://ose-gw1.efact.pe:443/api-efact-ose/v1/xml/{ticket}
        //https://ose-gw1.efact.pe:443/api-efact-ose/v1/pdf/{ticket}
        //PRODUCFCCION
        //	https://ose.efact.pe/api-efact-ose/oauth/token
        //https://ose.efact.pe/api-efact-ose/v1/document   
        //https://ose.efact.pe/api-efact-ose/v1/cdr/{ticket}
        //https://ose.efact.pe/api-efact-ose/v1/xml/{ticket}
        //	https://ose.efact.pe/api-efact-ose/v1/pdf/{ticket}
        //9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b
        public async Task getPDF(string ticket) {
           

            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);

            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token = JsonConvert.DeserializeObject<AccesToken>(r);


                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Clear();
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                HttpResponseMessage resp= await client.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + ticket);
                if (resp.IsSuccessStatusCode)
                {
                    File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await resp.Content.ReadAsByteArrayAsync());
                    Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");
                }
                else MessageBox.Show("NOSE PUDO IMPRIMIR PDF"+resp.ToString());

            }
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            materialFlatButton1.Visible = false;
            cp.venta = venta;
            List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
            ProductoPresentacion ppp = null;
            foreach (var d in venta.detalles)
            {
                ppp = ppservice.buscar(d.idProductoPresentacion);
                presentaciones.Add(ppp);
            }
            string codDoc = CodigosSUNAT.RUC;
            FacturaJsonEfac fac = new FacturaJsonEfac();
            EfacGuia facG = new EfacGuia();
            EfacGuiaOrden facGO = new EfacGuiaOrden();
            EfacOrden facO = new EfacOrden();
            string json = "";
            cp.tipoPago = new TipoPago { nombre = "PAGO A CREDITO" };
            if (cp.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

            if (String.IsNullOrEmpty(cp.venta.orden) || string.IsNullOrWhiteSpace(cp.venta.orden))
            {
                if (String.IsNullOrEmpty(txtGuia.Text) || string.IsNullOrWhiteSpace(txtGuia.Text))
                {
                    Invoice inv = new Invoice(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                     Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble);
                    fac.Invoice.Add(inv);
                    json = JsonConvert.SerializeObject(fac, Formatting.Indented);
                }
                else
                {
                    InvoceGuia inv = new InvoceGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                    Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, txtGuia.Text);
                    facG.Invoice.Add(inv);
                    json = JsonConvert.SerializeObject(facG, Formatting.Indented);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(txtGuia.Text) || string.IsNullOrWhiteSpace(txtGuia.Text))
                {
                    InvoiceNota inv = new InvoiceNota(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                     Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden);
                    facO.Invoice.Add(inv);
                    json = JsonConvert.SerializeObject(facO, Formatting.Indented);

                }
                else
                {
                    InvoiceNotaGuia inv = new InvoiceNotaGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                   Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden, txtGuia.Text);
                    facGO.Invoice.Add(inv);
                    json = JsonConvert.SerializeObject(facGO, Formatting.Indented);


                }
            }
            getToken(json, cp, this);
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro de cambiar estado de Pago", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                cpservice.cambiarAPagado(cp);
                if(cp.pagado)
                materialFlatButton2.Text = "Cambiar a Pagado";
                else materialFlatButton2.Text = "Cambiar a No Pagado";
                MessageBox.Show("EL CAMBIO HA SIDO REALIZADO");
            }
        }

        private void btnNoEnviado_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Seguro de cambiar estado de Pago", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                cpservice.cambiarAEnviado(cp);
                if (cp.pagado)
                    btnNoEnviado.Text = "Cambiar a Enviado";
                else btnNoEnviado.Text = "Cambiar a No Enviado";
                MessageBox.Show("EL CAMBIO HA SIDO REALIZADO");
            }
        }

        public async Task getToken(string json,ComprobantePago cp,MaterialForm formulario)
        {//"20102536593-03-BF04-00000020" + ".json";
            string file = LocalImpl.getInstancia().ruc + "-" + cp.tipoComprobante.codigo_sunat + "-" + cp.serie + "-" +Util.NormalizarCampo( cp.numero .ToString(),8)+ ".json";
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");

            byte[] bytes = await (sc.ReadAsByteArrayAsync());
            File.WriteAllBytes(@"D:\jsonEfac\" + file, bytes);

            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {  
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);
            
            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token= JsonConvert.DeserializeObject<AccesToken>(r);
                try
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Clear();
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;                    
                    MultipartFormDataContent  contenido = new MultipartFormDataContent(); HttpContent contnt;
                    HttpContent fileContent = new StreamContent(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)));
                    var stream = new FileStream(@"D:\jsonEfac\" + file, FileMode.Open);
                    contnt = new StreamContent(stream);
                    contnt.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        Name = "file",
                        FileName = file
                    };
                   contenido.Add(contnt);            
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                    client.DefaultRequestHeaders.Add("Accept", "*/*" );
                   // MessageBox.Show(token.access_token);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                    var resp = await client.PostAsync("https://ose.efact.pe/api-efact-ose/v1/document", contenido);
              
                if (resp.IsSuccessStatusCode)
                {
                    var re =await  resp.Content.ReadAsStringAsync();
                        TicketEfac tikcket=JsonConvert.DeserializeObject<TicketEfac>(re);
                        ComprobantePago cp2 = cpservice.buscarSolo(cp.id);
                        cp.hash = tikcket.description;
                        cp2.hash = tikcket.description;
                        cp2.enviado = true;
                        cpservice.editar(cp2);
                        btnImprimir.Visible = true;
                        btnGuardar.Visible = false;
                        


                        HttpClient clien = new HttpClient();
                        clien.DefaultRequestHeaders.Accept.Clear();
                        clien.DefaultRequestHeaders.Clear();
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                        //  contenido.Headers.Remove("Content_type");
                        //  contenido.Add(fileContent,"file", "20102536593-01-FF04-00000020.json");
                        //  contenido.Add(fileContent,"file", "20102536593-01-FF04-00000020.json");
                        clien.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                        clien.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                        HttpResponseMessage respo = await clien.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + tikcket.description);
                        if (respo.IsSuccessStatusCode)
                        {
                            File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await respo.Content.ReadAsByteArrayAsync());
                            Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");

                            this.Close();
                        }
                        else MessageBox.Show("NOSE PUDO IMPRIMIR PDF"+respo.ToString());
                    }
                else MessageBox.Show("NOSE PUDO ENVIAR ERROR DE CONEXION"+resp.Content.ToString());
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + e.StackTrace);
                }
            }
            else MessageBox.Show("ERROR DE ENVIO NO SE OBTUVO AUTENTIFICACION");
           
        }
           
    }

   


}
