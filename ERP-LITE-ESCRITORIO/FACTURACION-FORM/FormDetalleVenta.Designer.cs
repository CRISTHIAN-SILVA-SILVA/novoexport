﻿namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    partial class FormDetalleVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDetalleVenta));
            this.txtEmision = new System.Windows.Forms.DateTimePicker();
            this.btnGuardar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCancelar = new MaterialSkin.Controls.MaterialFlatButton();
            this.lblTipoPago = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.txtVencimiento = new System.Windows.Forms.DateTimePicker();
            this.lblMedioPago = new MaterialSkin.Controls.MaterialLabel();
            this.lblTotalLetras = new MaterialSkin.Controls.MaterialLabel();
            this.txtEfectivo = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblIGV = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.lblGratuitas = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.lblOpGravadas = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.lblTotal = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.gridDetalle = new System.Windows.Forms.DataGridView();
            this.lblSerie = new MaterialSkin.Controls.MaterialLabel();
            this.lblMoneda = new MaterialSkin.Controls.MaterialLabel();
            this.dh = new MaterialSkin.Controls.MaterialLabel();
            this.lblDocumentoCliente = new MaterialSkin.Controls.MaterialLabel();
            this.lblIntereses = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel28 = new MaterialSkin.Controls.MaterialLabel();
            this.lblCliente = new MaterialSkin.Controls.MaterialLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTipoComprobante = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtOrden = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGuia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblDireccionSede = new System.Windows.Forms.Label();
            this.lblRucEmpresa = new System.Windows.Forms.Label();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.lblOpExoneradas = new MaterialSkin.Controls.MaterialLabel();
            this.panelVuelto = new System.Windows.Forms.Panel();
            this.btnCrearTxt = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnGuia = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnNotaCredito = new MaterialSkin.Controls.MaterialFlatButton();
            this.panelBotones = new System.Windows.Forms.Panel();
            this.lblEnviado = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDetraccion = new System.Windows.Forms.Label();
            this.btnImprimir = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialFlatButton1 = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialFlatButton2 = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnNoEnviado = new MaterialSkin.Controls.MaterialFlatButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelVuelto.SuspendLayout();
            this.panelBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtEmision
            // 
            this.txtEmision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmision.Location = new System.Drawing.Point(139, 38);
            this.txtEmision.Name = "txtEmision";
            this.txtEmision.Size = new System.Drawing.Size(242, 23);
            this.txtEmision.TabIndex = 0;
            // 
            // btnGuardar
            // 
            this.btnGuardar.AutoSize = true;
            this.btnGuardar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGuardar.Depth = 0;
            this.btnGuardar.Icon = null;
            this.btnGuardar.Location = new System.Drawing.Point(1076, 510);
            this.btnGuardar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Primary = true;
            this.btnGuardar.Size = new System.Drawing.Size(89, 36);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Terminar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.AutoSize = true;
            this.btnCancelar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancelar.Depth = 0;
            this.btnCancelar.Icon = null;
            this.btnCancelar.Location = new System.Drawing.Point(1076, 622);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCancelar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Primary = false;
            this.btnCancelar.Size = new System.Drawing.Size(91, 36);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblTipoPago
            // 
            this.lblTipoPago.Depth = 0;
            this.lblTipoPago.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblTipoPago.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTipoPago.Location = new System.Drawing.Point(782, 42);
            this.lblTipoPago.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblTipoPago.Name = "lblTipoPago";
            this.lblTipoPago.Size = new System.Drawing.Size(210, 19);
            this.lblTipoPago.TabIndex = 6;
            this.lblTipoPago.Text = "FECHA VCTO";
            this.lblTipoPago.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(26, 291);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(105, 19);
            this.materialLabel6.TabIndex = 9;
            this.materialLabel6.Text = "DOCUMENTO:";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(153, 291);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(104, 19);
            this.materialLabel7.TabIndex = 10;
            this.materialLabel7.Text = "FECHA VCTO:";
            // 
            // txtVencimiento
            // 
            this.txtVencimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVencimiento.Location = new System.Drawing.Point(139, 66);
            this.txtVencimiento.Name = "txtVencimiento";
            this.txtVencimiento.Size = new System.Drawing.Size(242, 23);
            this.txtVencimiento.TabIndex = 12;
            // 
            // lblMedioPago
            // 
            this.lblMedioPago.Depth = 0;
            this.lblMedioPago.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblMedioPago.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMedioPago.Location = new System.Drawing.Point(782, 64);
            this.lblMedioPago.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblMedioPago.Name = "lblMedioPago";
            this.lblMedioPago.Size = new System.Drawing.Size(215, 19);
            this.lblMedioPago.TabIndex = 13;
            this.lblMedioPago.Text = "FECHA VCTO";
            this.lblMedioPago.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotalLetras
            // 
            this.lblTotalLetras.AutoSize = true;
            this.lblTotalLetras.Depth = 0;
            this.lblTotalLetras.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblTotalLetras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTotalLetras.Location = new System.Drawing.Point(15, 510);
            this.lblTotalLetras.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblTotalLetras.Name = "lblTotalLetras";
            this.lblTotalLetras.Size = new System.Drawing.Size(47, 19);
            this.lblTotalLetras.TabIndex = 14;
            this.lblTotalLetras.Text = "letras";
            // 
            // txtEfectivo
            // 
            this.txtEfectivo.Depth = 0;
            this.txtEfectivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEfectivo.Hint = "";
            this.txtEfectivo.Location = new System.Drawing.Point(19, 34);
            this.txtEfectivo.MaxLength = 32767;
            this.txtEfectivo.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtEfectivo.Name = "txtEfectivo";
            this.txtEfectivo.PasswordChar = '\0';
            this.txtEfectivo.SelectedText = "";
            this.txtEfectivo.SelectionLength = 0;
            this.txtEfectivo.SelectionStart = 0;
            this.txtEfectivo.Size = new System.Drawing.Size(78, 23);
            this.txtEfectivo.TabIndex = 15;
            this.txtEfectivo.TabStop = false;
            this.txtEfectivo.Text = "0";
            this.txtEfectivo.UseSystemPasswordChar = false;
            // 
            // lblIGV
            // 
            this.lblIGV.Depth = 0;
            this.lblIGV.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblIGV.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblIGV.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblIGV.Location = new System.Drawing.Point(112, 95);
            this.lblIGV.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblIGV.Name = "lblIGV";
            this.lblIGV.Size = new System.Drawing.Size(99, 22);
            this.lblIGV.TabIndex = 139;
            this.lblIGV.Text = "0.00";
            this.lblIGV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialLabel15
            // 
            this.materialLabel15.Depth = 0;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel15.Location = new System.Drawing.Point(12, 95);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(97, 22);
            this.materialLabel15.TabIndex = 138;
            this.materialLabel15.Text = "IGV:";
            // 
            // lblGratuitas
            // 
            this.lblGratuitas.Depth = 0;
            this.lblGratuitas.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblGratuitas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblGratuitas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGratuitas.Location = new System.Drawing.Point(112, 75);
            this.lblGratuitas.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblGratuitas.Name = "lblGratuitas";
            this.lblGratuitas.Size = new System.Drawing.Size(99, 22);
            this.lblGratuitas.TabIndex = 137;
            this.lblGratuitas.Text = "0.0000";
            this.lblGratuitas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialLabel13
            // 
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel13.Location = new System.Drawing.Point(8, 75);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(101, 22);
            this.materialLabel13.TabIndex = 136;
            this.materialLabel13.Text = "Op Gratuitas:";
            // 
            // lblOpGravadas
            // 
            this.lblOpGravadas.Depth = 0;
            this.lblOpGravadas.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblOpGravadas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOpGravadas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOpGravadas.Location = new System.Drawing.Point(112, 53);
            this.lblOpGravadas.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblOpGravadas.Name = "lblOpGravadas";
            this.lblOpGravadas.Size = new System.Drawing.Size(99, 22);
            this.lblOpGravadas.TabIndex = 135;
            this.lblOpGravadas.Text = "0.00";
            this.lblOpGravadas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialLabel12
            // 
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel12.Location = new System.Drawing.Point(8, 53);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(117, 22);
            this.materialLabel12.TabIndex = 134;
            this.materialLabel12.Text = "Base Imponible:";
            // 
            // lblTotal
            // 
            this.lblTotal.Depth = 0;
            this.lblTotal.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTotal.Location = new System.Drawing.Point(112, 117);
            this.lblTotal.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(99, 22);
            this.lblTotal.TabIndex = 141;
            this.lblTotal.Text = "0.00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialLabel14
            // 
            this.materialLabel14.Depth = 0;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel14.Location = new System.Drawing.Point(12, 117);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(60, 22);
            this.materialLabel14.TabIndex = 140;
            this.materialLabel14.Text = "Total:";
            // 
            // gridDetalle
            // 
            this.gridDetalle.AllowUserToAddRows = false;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.gridDetalle.BackgroundColor = System.Drawing.Color.White;
            this.gridDetalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridDetalle.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.gridDetalle.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.gridDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.DefaultCellStyle = dataGridViewCellStyle13;
            this.gridDetalle.EnableHeadersVisualStyles = false;
            this.gridDetalle.GridColor = System.Drawing.Color.Black;
            this.gridDetalle.Location = new System.Drawing.Point(9, 256);
            this.gridDetalle.MultiSelect = false;
            this.gridDetalle.Name = "gridDetalle";
            this.gridDetalle.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.gridDetalle.RowHeadersVisible = false;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.gridDetalle.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.Height = 31;
            this.gridDetalle.RowTemplate.ReadOnly = true;
            this.gridDetalle.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDetalle.Size = new System.Drawing.Size(1159, 248);
            this.gridDetalle.TabIndex = 142;
            // 
            // lblSerie
            // 
            this.lblSerie.Depth = 0;
            this.lblSerie.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblSerie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblSerie.Location = new System.Drawing.Point(6, 36);
            this.lblSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(247, 19);
            this.lblSerie.TabIndex = 147;
            this.lblSerie.Text = "SERIE: FE01";
            this.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMoneda
            // 
            this.lblMoneda.Depth = 0;
            this.lblMoneda.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblMoneda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMoneda.Location = new System.Drawing.Point(500, 67);
            this.lblMoneda.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(165, 19);
            this.lblMoneda.TabIndex = 148;
            this.lblMoneda.Text = "SOLES";
            this.lblMoneda.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dh
            // 
            this.dh.AutoSize = true;
            this.dh.Depth = 0;
            this.dh.Font = new System.Drawing.Font("Roboto", 11F);
            this.dh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dh.Location = new System.Drawing.Point(15, 9);
            this.dh.MouseState = MaterialSkin.MouseState.HOVER;
            this.dh.Name = "dh";
            this.dh.Size = new System.Drawing.Size(82, 19);
            this.dh.TabIndex = 150;
            this.dh.Text = "EFECTIVO:";
            // 
            // lblDocumentoCliente
            // 
            this.lblDocumentoCliente.Depth = 0;
            this.lblDocumentoCliente.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblDocumentoCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDocumentoCliente.Location = new System.Drawing.Point(496, 43);
            this.lblDocumentoCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblDocumentoCliente.Name = "lblDocumentoCliente";
            this.lblDocumentoCliente.Size = new System.Drawing.Size(169, 19);
            this.lblDocumentoCliente.TabIndex = 153;
            this.lblDocumentoCliente.Text = "FECHA VCTO";
            this.lblDocumentoCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIntereses
            // 
            this.lblIntereses.Depth = 0;
            this.lblIntereses.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblIntereses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblIntereses.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblIntereses.Location = new System.Drawing.Point(112, 8);
            this.lblIntereses.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblIntereses.Name = "lblIntereses";
            this.lblIntereses.Size = new System.Drawing.Size(99, 22);
            this.lblIntereses.TabIndex = 155;
            this.lblIntereses.Text = "0.00";
            this.lblIntereses.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // materialLabel28
            // 
            this.materialLabel28.Depth = 0;
            this.materialLabel28.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel28.Location = new System.Drawing.Point(12, 8);
            this.materialLabel28.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel28.Name = "materialLabel28";
            this.materialLabel28.Size = new System.Drawing.Size(97, 22);
            this.materialLabel28.TabIndex = 154;
            this.materialLabel28.Text = "Interés:";
            // 
            // lblCliente
            // 
            this.lblCliente.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblCliente.Depth = 0;
            this.lblCliente.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCliente.Location = new System.Drawing.Point(144, 11);
            this.lblCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(999, 19);
            this.lblCliente.TabIndex = 156;
            this.lblCliente.Text = "DIRECCION";
            this.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.lblTipoComprobante);
            this.panel1.Controls.Add(this.lblSerie);
            this.panel1.Location = new System.Drawing.Point(915, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(255, 68);
            this.panel1.TabIndex = 157;
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoComprobante.Location = new System.Drawing.Point(3, 13);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(250, 23);
            this.lblTipoComprobante.TabIndex = 157;
            this.lblTipoComprobante.Text = "COMPROBANTE ELECTRONICO";
            this.lblTipoComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.txtOrden);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtGuia);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblTipoPago);
            this.panel2.Controls.Add(this.lblCliente);
            this.panel2.Controls.Add(this.lblMedioPago);
            this.panel2.Controls.Add(this.lblDocumentoCliente);
            this.panel2.Controls.Add(this.lblMoneda);
            this.panel2.Controls.Add(this.txtEmision);
            this.panel2.Controls.Add(this.txtVencimiento);
            this.panel2.Location = new System.Drawing.Point(12, 150);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1158, 100);
            this.panel2.TabIndex = 158;
            // 
            // txtOrden
            // 
            this.txtOrden.Depth = 0;
            this.txtOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrden.Hint = "";
            this.txtOrden.Location = new System.Drawing.Point(1077, 61);
            this.txtOrden.MaxLength = 13;
            this.txtOrden.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtOrden.Name = "txtOrden";
            this.txtOrden.PasswordChar = '\0';
            this.txtOrden.SelectedText = "";
            this.txtOrden.SelectionLength = 0;
            this.txtOrden.SelectionStart = 0;
            this.txtOrden.Size = new System.Drawing.Size(63, 23);
            this.txtOrden.TabIndex = 178;
            this.txtOrden.TabStop = false;
            this.txtOrden.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(995, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 23);
            this.label10.TabIndex = 177;
            this.label10.Text = "ORDEN:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGuia
            // 
            this.txtGuia.Depth = 0;
            this.txtGuia.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGuia.Hint = "";
            this.txtGuia.Location = new System.Drawing.Point(1080, 33);
            this.txtGuia.MaxLength = 13;
            this.txtGuia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtGuia.Name = "txtGuia";
            this.txtGuia.PasswordChar = '\0';
            this.txtGuia.SelectedText = "";
            this.txtGuia.SelectionLength = 0;
            this.txtGuia.SelectionStart = 0;
            this.txtGuia.Size = new System.Drawing.Size(63, 23);
            this.txtGuia.TabIndex = 176;
            this.txtGuia.TabStop = false;
            this.txtGuia.UseSystemPasswordChar = false;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(998, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 23);
            this.label9.TabIndex = 175;
            this.label9.Text = "N° GUIA:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(671, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 23);
            this.label6.TabIndex = 173;
            this.label6.Text = "MEDIO PAGO:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(408, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 23);
            this.label5.TabIndex = 172;
            this.label5.Text = "RUC / DNI:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(671, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 23);
            this.label7.TabIndex = 174;
            this.label7.Text = "TIPO PAGO:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(407, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 23);
            this.label4.TabIndex = 171;
            this.label4.Text = "MONEDA:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 23);
            this.label3.TabIndex = 170;
            this.label3.Text = "FECHA VCTO:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 23);
            this.label2.TabIndex = 169;
            this.label2.Text = "FECHA EMISIÓN:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 23);
            this.label1.TabIndex = 168;
            this.label1.Text = "CLIENTE:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Controls.Add(this.lblDireccionSede);
            this.panel3.Controls.Add(this.lblRucEmpresa);
            this.panel3.Controls.Add(this.lblEmpresa);
            this.panel3.Location = new System.Drawing.Point(12, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(897, 68);
            this.panel3.TabIndex = 159;
            // 
            // lblDireccionSede
            // 
            this.lblDireccionSede.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccionSede.Location = new System.Drawing.Point(4, 45);
            this.lblDireccionSede.Name = "lblDireccionSede";
            this.lblDireccionSede.Size = new System.Drawing.Size(890, 23);
            this.lblDireccionSede.TabIndex = 170;
            this.lblDireccionSede.Text = "COMPROBANTE ELECTRONICO";
            this.lblDireccionSede.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRucEmpresa
            // 
            this.lblRucEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRucEmpresa.Location = new System.Drawing.Point(4, 23);
            this.lblRucEmpresa.Name = "lblRucEmpresa";
            this.lblRucEmpresa.Size = new System.Drawing.Size(893, 23);
            this.lblRucEmpresa.TabIndex = 169;
            this.lblRucEmpresa.Text = "COMPROBANTE ELECTRONICO";
            this.lblRucEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.Location = new System.Drawing.Point(8, 0);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(886, 25);
            this.lblEmpresa.TabIndex = 168;
            this.lblEmpresa.Text = "lblEmpresa";
            this.lblEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel4.Controls.Add(this.materialLabel1);
            this.panel4.Controls.Add(this.lblOpExoneradas);
            this.panel4.Controls.Add(this.materialLabel28);
            this.panel4.Controls.Add(this.materialLabel12);
            this.panel4.Controls.Add(this.lblOpGravadas);
            this.panel4.Controls.Add(this.materialLabel13);
            this.panel4.Controls.Add(this.lblIntereses);
            this.panel4.Controls.Add(this.lblGratuitas);
            this.panel4.Controls.Add(this.materialLabel15);
            this.panel4.Controls.Add(this.lblIGV);
            this.panel4.Controls.Add(this.materialLabel14);
            this.panel4.Controls.Add(this.lblTotal);
            this.panel4.Location = new System.Drawing.Point(845, 511);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(228, 147);
            this.panel4.TabIndex = 160;
            // 
            // materialLabel1
            // 
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel1.Location = new System.Drawing.Point(8, 33);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(134, 22);
            this.materialLabel1.TabIndex = 156;
            this.materialLabel1.Text = "Op Exoneradas:";
            // 
            // lblOpExoneradas
            // 
            this.lblOpExoneradas.Depth = 0;
            this.lblOpExoneradas.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblOpExoneradas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOpExoneradas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOpExoneradas.Location = new System.Drawing.Point(148, 31);
            this.lblOpExoneradas.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblOpExoneradas.Name = "lblOpExoneradas";
            this.lblOpExoneradas.Size = new System.Drawing.Size(63, 22);
            this.lblOpExoneradas.TabIndex = 157;
            this.lblOpExoneradas.Text = "0.00";
            this.lblOpExoneradas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelVuelto
            // 
            this.panelVuelto.Controls.Add(this.dh);
            this.panelVuelto.Controls.Add(this.txtEfectivo);
            this.panelVuelto.Location = new System.Drawing.Point(726, 510);
            this.panelVuelto.Name = "panelVuelto";
            this.panelVuelto.Size = new System.Drawing.Size(113, 66);
            this.panelVuelto.TabIndex = 161;
            this.panelVuelto.Visible = false;
            // 
            // btnCrearTxt
            // 
            this.btnCrearTxt.AutoSize = true;
            this.btnCrearTxt.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCrearTxt.Depth = 0;
            this.btnCrearTxt.Icon = ((System.Drawing.Image)(resources.GetObject("btnCrearTxt.Icon")));
            this.btnCrearTxt.Location = new System.Drawing.Point(194, 7);
            this.btnCrearTxt.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCrearTxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCrearTxt.Name = "btnCrearTxt";
            this.btnCrearTxt.Primary = false;
            this.btnCrearTxt.Size = new System.Drawing.Size(121, 36);
            this.btnCrearTxt.TabIndex = 163;
            this.btnCrearTxt.Text = "CREAR TXT";
            this.btnCrearTxt.UseVisualStyleBackColor = true;
            this.btnCrearTxt.Click += new System.EventHandler(this.btnCrearTxt_Click);
            // 
            // btnGuia
            // 
            this.btnGuia.AutoSize = true;
            this.btnGuia.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGuia.Depth = 0;
            this.btnGuia.Icon = ((System.Drawing.Image)(resources.GetObject("btnGuia.Icon")));
            this.btnGuia.Location = new System.Drawing.Point(4, 6);
            this.btnGuia.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnGuia.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnGuia.Name = "btnGuia";
            this.btnGuia.Primary = false;
            this.btnGuia.Size = new System.Drawing.Size(80, 36);
            this.btnGuia.TabIndex = 162;
            this.btnGuia.Text = "Guia";
            this.btnGuia.UseVisualStyleBackColor = true;
            this.btnGuia.Click += new System.EventHandler(this.btnGuia_Click);
            // 
            // btnNotaCredito
            // 
            this.btnNotaCredito.AutoSize = true;
            this.btnNotaCredito.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNotaCredito.Depth = 0;
            this.btnNotaCredito.Icon = ((System.Drawing.Image)(resources.GetObject("btnNotaCredito.Icon")));
            this.btnNotaCredito.Location = new System.Drawing.Point(92, 6);
            this.btnNotaCredito.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnNotaCredito.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNotaCredito.Name = "btnNotaCredito";
            this.btnNotaCredito.Primary = false;
            this.btnNotaCredito.Size = new System.Drawing.Size(94, 36);
            this.btnNotaCredito.TabIndex = 164;
            this.btnNotaCredito.Text = "Notas";
            this.btnNotaCredito.UseVisualStyleBackColor = true;
            this.btnNotaCredito.Click += new System.EventHandler(this.btnNotaCredito_Click);
            // 
            // panelBotones
            // 
            this.panelBotones.Controls.Add(this.btnGuia);
            this.panelBotones.Controls.Add(this.btnCrearTxt);
            this.panelBotones.Controls.Add(this.btnNotaCredito);
            this.panelBotones.Location = new System.Drawing.Point(521, 607);
            this.panelBotones.Name = "panelBotones";
            this.panelBotones.Size = new System.Drawing.Size(318, 47);
            this.panelBotones.TabIndex = 166;
            this.panelBotones.Visible = false;
            // 
            // lblEnviado
            // 
            this.lblEnviado.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnviado.Location = new System.Drawing.Point(12, 613);
            this.lblEnviado.Name = "lblEnviado";
            this.lblEnviado.Size = new System.Drawing.Size(395, 32);
            this.lblEnviado.TabIndex = 167;
            this.lblEnviado.Text = "COMPROBANTE NO ENVIADO";
            this.lblEnviado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEnviado.Visible = false;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 554);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(158, 32);
            this.label8.TabIndex = 168;
            this.label8.Text = "DETRACCION:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDetraccion
            // 
            this.lblDetraccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetraccion.Location = new System.Drawing.Point(179, 554);
            this.lblDetraccion.Name = "lblDetraccion";
            this.lblDetraccion.Size = new System.Drawing.Size(131, 32);
            this.lblDetraccion.TabIndex = 169;
            this.lblDetraccion.Text = "0.00";
            this.lblDetraccion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnImprimir
            // 
            this.btnImprimir.AutoSize = true;
            this.btnImprimir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnImprimir.Depth = 0;
            this.btnImprimir.Icon = null;
            this.btnImprimir.Location = new System.Drawing.Point(1079, 564);
            this.btnImprimir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Primary = true;
            this.btnImprimir.Size = new System.Drawing.Size(83, 36);
            this.btnImprimir.TabIndex = 170;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Visible = false;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // materialFlatButton1
            // 
            this.materialFlatButton1.AutoSize = true;
            this.materialFlatButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialFlatButton1.Depth = 0;
            this.materialFlatButton1.Icon = ((System.Drawing.Image)(resources.GetObject("materialFlatButton1.Icon")));
            this.materialFlatButton1.Location = new System.Drawing.Point(566, 528);
            this.materialFlatButton1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialFlatButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFlatButton1.Name = "materialFlatButton1";
            this.materialFlatButton1.Primary = false;
            this.materialFlatButton1.Size = new System.Drawing.Size(97, 36);
            this.materialFlatButton1.TabIndex = 171;
            this.materialFlatButton1.Text = "Enviar";
            this.materialFlatButton1.UseVisualStyleBackColor = true;
            this.materialFlatButton1.Visible = false;
            this.materialFlatButton1.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // materialFlatButton2
            // 
            this.materialFlatButton2.AutoSize = true;
            this.materialFlatButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialFlatButton2.Depth = 0;
            this.materialFlatButton2.Icon = ((System.Drawing.Image)(resources.GetObject("materialFlatButton2.Icon")));
            this.materialFlatButton2.Location = new System.Drawing.Point(311, 612);
            this.materialFlatButton2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialFlatButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFlatButton2.Name = "materialFlatButton2";
            this.materialFlatButton2.Primary = false;
            this.materialFlatButton2.Size = new System.Drawing.Size(203, 36);
            this.materialFlatButton2.TabIndex = 172;
            this.materialFlatButton2.Text = "Cambiar A No Pagado";
            this.materialFlatButton2.UseVisualStyleBackColor = true;
            this.materialFlatButton2.Visible = false;
            this.materialFlatButton2.Click += new System.EventHandler(this.materialFlatButton2_Click);
            // 
            // btnNoEnviado
            // 
            this.btnNoEnviado.AutoSize = true;
            this.btnNoEnviado.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNoEnviado.Depth = 0;
            this.btnNoEnviado.Icon = ((System.Drawing.Image)(resources.GetObject("btnNoEnviado.Icon")));
            this.btnNoEnviado.Location = new System.Drawing.Point(311, 556);
            this.btnNoEnviado.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnNoEnviado.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNoEnviado.Name = "btnNoEnviado";
            this.btnNoEnviado.Primary = false;
            this.btnNoEnviado.Size = new System.Drawing.Size(206, 36);
            this.btnNoEnviado.TabIndex = 173;
            this.btnNoEnviado.Text = "Cambiar A No Enviado";
            this.btnNoEnviado.UseVisualStyleBackColor = true;
            this.btnNoEnviado.Click += new System.EventHandler(this.btnNoEnviado_Click);
            // 
            // FormDetalleVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 666);
            this.Controls.Add(this.btnNoEnviado);
            this.Controls.Add(this.materialFlatButton2);
            this.Controls.Add(this.materialFlatButton1);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.lblDetraccion);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblEnviado);
            this.Controls.Add(this.panelBotones);
            this.Controls.Add(this.panelVuelto);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gridDetalle);
            this.Controls.Add(this.lblTotalLetras);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.MaximizeBox = false;
            this.Name = "FormDetalleVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalle Venta";
            this.Load += new System.EventHandler(this.FormDetalleVenta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panelVuelto.ResumeLayout(false);
            this.panelVuelto.PerformLayout();
            this.panelBotones.ResumeLayout(false);
            this.panelBotones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker txtEmision;
        private MaterialSkin.Controls.MaterialRaisedButton btnGuardar;
        private MaterialSkin.Controls.MaterialFlatButton btnCancelar;
        private MaterialSkin.Controls.MaterialLabel lblTipoPago;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private System.Windows.Forms.DateTimePicker txtVencimiento;
        private MaterialSkin.Controls.MaterialLabel lblMedioPago;
        private MaterialSkin.Controls.MaterialLabel lblTotalLetras;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtEfectivo;
        private MaterialSkin.Controls.MaterialLabel lblIGV;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialLabel lblGratuitas;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialLabel lblOpGravadas;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialLabel lblTotal;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private System.Windows.Forms.DataGridView gridDetalle;
        private MaterialSkin.Controls.MaterialLabel lblSerie;
        private MaterialSkin.Controls.MaterialLabel lblMoneda;
        private MaterialSkin.Controls.MaterialLabel dh;
        private MaterialSkin.Controls.MaterialLabel lblDocumentoCliente;
        private MaterialSkin.Controls.MaterialLabel lblIntereses;
        private MaterialSkin.Controls.MaterialLabel materialLabel28;
        private MaterialSkin.Controls.MaterialLabel lblCliente;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panelVuelto;
        private MaterialSkin.Controls.MaterialFlatButton btnCrearTxt;
        private MaterialSkin.Controls.MaterialFlatButton btnGuia;
        private MaterialSkin.Controls.MaterialFlatButton btnNotaCredito;
        private System.Windows.Forms.Panel panelBotones;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.Label lblEnviado;
        private System.Windows.Forms.Label lblTipoComprobante;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDireccionSede;
        private System.Windows.Forms.Label lblRucEmpresa;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel lblOpExoneradas;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDetraccion;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtGuia;
        private System.Windows.Forms.Label label9;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtOrden;
        private System.Windows.Forms.Label label10;
        private MaterialSkin.Controls.MaterialRaisedButton btnImprimir;
        private MaterialSkin.Controls.MaterialFlatButton materialFlatButton1;
        private MaterialSkin.Controls.MaterialFlatButton materialFlatButton2;
        private MaterialSkin.Controls.MaterialFlatButton btnNoEnviado;
    }
}