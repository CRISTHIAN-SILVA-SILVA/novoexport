﻿using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.EFAC;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using Newtonsoft.Json;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{
    public partial class FormAdminNotas : Form
    {
        TipoComprobantePagoService tcpservice = new TipoComprobanteImpl();
        ComprobantePagoImpl cpservice = new ComprobantePagoImpl();
        ProductoPresentacionImpl ppservice = new ProductoPresentacionImpl();
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        NotaImpl nservice = new NotaImpl();
        DataTable tabla = new DataTable();
        ComprobantePagoService cservice = new ComprobantePagoImpl();
        public FormAdminNotas()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("CLIENTE");
            tabla.Columns.Add("MOTIVO NOTA");
            tabla.Columns.Add("MONTO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 100;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 380;
            grid.Columns[5].Width = 300;
            grid.Columns[6].Width = 100;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            tabla.Clear();
            if (rbFecha.Checked)
                nservice.listarPorFechas(txtInicio.Value.Date, txtFin.Value.Date.AddDays(1)).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                             x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
            else if (rbMonto.Checked)
            {
                if (Util.esDouble(txtMontoInicial.Text) && Util.esDouble(txtMontoFinal.Text))
                    nservice.listarPorMontos(Convert.ToDouble(txtMontoInicial.Text), Convert.ToDouble(txtMontoFinal.Text)).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                  x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
            }
            else if (rbNumero.Checked)
            {
                if (Util.esDouble(txtNumero.Text))
                    nservice.listarPorNumero(Convert.ToInt32(txtNumero.Text)).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                  x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));

            }
            else
            {
                if (checkComprobante.Checked)
                    if (txtNumeroComprobante.Text.Trim().Length > 5)
                    {
                        if (Util.esDouble(txtNumeroComprobante.Text.Substring(5)))
                        {
                            ComprobantePago c = cservice.listarPorSerieNumero(txtNumeroComprobante.Text.Substring(0, 4).ToUpper(), Convert.ToInt32(txtNumeroComprobante.Text.Substring(5)));
                            if (c == null) { MessageBox.Show("COMPROBANTE NO EXISTE"); return; }
                            nservice.listarPorComprobante(c.id).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                           x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
                        }
                    }
            }
        }

        private void rbNumero_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNumero.Checked)
            {
                habilitar(false, false, false, true);
            }
        }

        private void rbMonto_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMonto.Checked)
            {
                habilitar(false, false, true, false);
            }
        }

        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFecha.Checked)
            {
                habilitar(true, false, false, false);
            }
        }

        private void checkDebito_CheckedChanged(object sender, EventArgs e)
        {
            llenar();
        }

        private void checkCredito_CheckedChanged(object sender, EventArgs e)
        {
            llenar();
        }
        void habilitar(bool fecha, bool comprobante, bool monto, bool numero)
        {
            txtNumero.Visible = numero;

            txtInicio.Visible = fecha;
            txtFin.Visible = fecha;

            txtMontoFinal.Visible = monto;
            txtMontoInicial.Visible = monto;

            txtNumeroComprobante.Visible = comprobante;

        }
        private void checkComprobante_CheckedChanged(object sender, EventArgs e)
        {
            if (checkComprobante.Checked)
            {
                habilitar(false, true, false, false);
            }

        }

        private void FormAdminNotas_Load(object sender, EventArgs e)
        {
            rbNumero.Checked = true;

            checkDebito.Checked = true;
            checkCredito.Checked = true;

        }
        void llenar()
        {
            tabla.Clear();
            if (checkCredito.Checked)
            {
                if (checkDebito.Checked)
                {
                    nservice.listarNoAnulados().ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                        x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));

                }
                else
                {
                    TipoComprobantePago tcp = tcpservice.buscarPorNombre("Nota de credito");
                    if (tcp != null)
                        nservice.listarPorTipoComprobante(tcp.id).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                        x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
                }
            }
            else
            {
                if (checkDebito.Checked)
                {
                    TipoComprobantePago tcp = tcpservice.buscarPorNombre("Nota de debito");
                    if (tcp != null)
                        nservice.listarPorTipoComprobante(tcp.id).ForEach(x => tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString(),
                        x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.cliente.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
                }
            }
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            {
                Nota n = nservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (n != null)
                {
                    getPDF(n.hash, n);
                }
            }

            /*
            if (grid.SelectedRows.Count == 1)
            {
                Nota n = nservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (n != null)
                {
                    FormReporteNota form = new FormReporteNota(n);
                    form.ShowDialog();
                }
            }*/
        }
        public async Task getPDF(string ticket, Nota cp)
        {


            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);

            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token = JsonConvert.DeserializeObject<AccesToken>(r);


                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Clear();
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                HttpResponseMessage resp = await client.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + ticket);
                if (resp.IsSuccessStatusCode)
                {
                    File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await resp.Content.ReadAsByteArrayAsync());
                    Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");
                }
                else MessageBox.Show("NOSE PUDO IMPRIMIR PDF" + resp.ToString());

            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            llenar();
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {

            if (grid.SelectedRows.Count == 1)
            {
                if (MessageBox.Show("SEGURO DE ENVIAR COMPROBANTE", "CONFIMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Nota nota = nservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    if (nota != null)
                    {

                        ComprobantePago comprobante = cpservice.buscar(nota.idComprobanteVenta);

                        List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
                        ProductoPresentacion ppp = null;
                        foreach (var d in nota.detalles)
                        {
                            ppp = ppservice.buscar(d.idPresentacion);
                            presentaciones.Add(ppp);
                        }
                        string codDoc = CodigosSUNAT.RUC;
                        NotaCreditoEfac fac = new NotaCreditoEfac();
                        string json = "";
                        if (comprobante.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

                        nota.comprobante = comprobante;
                        CreditNote inv = new CreditNote(nota, LocalImpl.local, presentaciones, codDoc,
                          Util.Convertir(string.Format("{0:0.00}", nota.total), true), igv.valorDouble, comprobante.tipoComprobante.codigo_sunat);
                        fac.CreditNote.Add(inv);
                        json = JsonConvert.SerializeObject(fac, Formatting.Indented);

                        getToken(json, nota);

                    }
                }
            }
        }
        public async Task getToken(string json, Nota cp)
        {//"20102536593-03-BF04-00000020" + ".json";
            string file = LocalImpl.getInstancia().ruc + "-" + cp.tipoComprobante.codigo_sunat + "-" + cp.serie + "-" + Util.NormalizarCampo(cp.numero.ToString(), 8) + ".json";
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");

            byte[] bytes = await (sc.ReadAsByteArrayAsync());
            File.WriteAllBytes(@"D:\jsonEfac\" + file, bytes);

            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);

            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token = JsonConvert.DeserializeObject<AccesToken>(r);
                try
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Clear();
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                    MultipartFormDataContent contenido = new MultipartFormDataContent();

                    // MultipartFormDataContent form = new MultipartFormDataContent();
                    HttpContent contnt;//= new StringContent("fileToUpload");
                                       //  form.Add(contnt, "fileToUpload");
                    HttpContent fileContent = new StreamContent(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)));
                    var stream = new FileStream(@"D:\jsonEfac\" + file, FileMode.Open);
                    contnt = new StreamContent(stream);
                    contnt.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        Name = "file",
                        FileName = file
                    };
                    contenido.Add(contnt);
                    contenido.Headers.Remove("Content_type");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);

                    client.DefaultRequestHeaders.Add("Accept", "*/*");
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                    var resp = await client.PostAsync("https://ose.efact.pe/api-efact-ose/v1/document", contenido);

                    if (resp.IsSuccessStatusCode)
                    {
                        var re = await resp.Content.ReadAsStringAsync();
                        TicketEfac tikcket = JsonConvert.DeserializeObject<TicketEfac>(re);
                        Nota cp2 = nservice.buscarSolo(cp.id);
                        cp.hash = tikcket.description;
                        cp2.hash = tikcket.description;
                        //  cp2.env= true;
                        nservice.editar(cp2);
                        HttpClient clien = new HttpClient();
                        clien.DefaultRequestHeaders.Accept.Clear();
                        clien.DefaultRequestHeaders.Clear();
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                        clien.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                        clien.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                        HttpResponseMessage respo = await clien.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + tikcket.description);
                        if (respo.IsSuccessStatusCode)
                        {
                            File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await respo.Content.ReadAsByteArrayAsync());
                            Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");
                         
                        }
                        else MessageBox.Show("NOSE PUDO IMPRIMIR PDF" + respo.ToString());
                    }
                    else MessageBox.Show("NOSE PUDO ENVIAR ERROR DE CONEXION");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + e.StackTrace);
                }
            }
            else MessageBox.Show("ERROR DE ENVIO NO SE OBTUVO AUTENTIFICACION");
            
        }

    }
}
