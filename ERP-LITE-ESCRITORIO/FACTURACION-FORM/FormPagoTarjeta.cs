﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.EFAC;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using Newtonsoft.Json;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{
    public partial class FormPagoTarjeta : MaterialForm
    {
        CajaDiariaService cdservice = new CajaDiariaImpl();
        ComprobantePagoImpl cpservice = new ComprobantePagoImpl();
        ProductoPresentacionImpl ppservice = new ProductoPresentacionImpl();
        ComprobantePago cp;Venta venta;int almacenPrincipal = 0;
        public delegate void pasar();
        public event pasar pasado;
        CuentaBancoService cbservice = new CuentaBancoService();
        BancoService bancoService = new BancoService();
        int comprobantePago, medioPago;
        DateTime fechaPagoo;
        double monto;
        string guia = "";
        Parametro igv;
        public FormPagoTarjeta(ComprobantePago c ,Venta vta ,int idA,int pago,int medioPago,DateTime fechaPago,double mont,string g)
        {
            guia = g;
            monto = mont;
            fechaPagoo = fechaPago;
            comprobantePago = pago;this.medioPago = medioPago;
            cp = c;venta = vta;
            almacenPrincipal = idA;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
            igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        }

        private void FormPagoTarjeta_Load(object sender, EventArgs e)
        {
            comboCuentas.DataSource = cbservice.listar();
        }

        private void btnSeguir_Click(object sender, EventArgs e)
        {
            btnSeguir.Enabled = false;
        
                if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (comboCuentas.SelectedValue != null)
                        if (Util.esDouble(txtPorcentaje.Text))
                        {
                           // ComprobantePago c = cpservice.buscar(comprobantePago);
                            PagoTarjeta pt = new PagoTarjeta
                            {
                                idCuenta = (int)comboCuentas.SelectedValue, 
                                esVenta = true, 
                                fecha = DateTime.Now,
                                porcentaje = Convert.ToDouble(txtPorcentaje.Text),
                                post = txtCodigo.Text,
                            };
                            if (Convert.ToDouble(txtPorcentaje.Text) < 0)
                            { MessageBox.Show("PORCENTJE INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); btnSeguir.Enabled = true; return; }
                            if (cp != null)
                            {
                                int idCaja = cdservice.getCaja();
                                string r = cpservice.guardarComprobanteCompleto(cp, venta, idCaja, almacenPrincipal, true, pt, guia);
                                if (r.Equals("CORRECTO"))
                                {
                                    cp.venta = venta;
                                    List<ProductoPresentacion> presentaciones = new List<ProductoPresentacion>();
                                    ProductoPresentacion ppp = null;
                                    foreach (var d in venta.detalles)
                                    {
                                        ppp = ppservice.buscar(d.idProductoPresentacion);
                                        presentaciones.Add(ppp);
                                    }
                                    string codDoc = CodigosSUNAT.RUC;
                                    FacturaJsonEfac fac = new FacturaJsonEfac();
                                    EfacGuia facG = new EfacGuia();
                                    EfacGuiaOrden facGO = new EfacGuiaOrden();
                                    EfacOrden facO = new EfacOrden();
                                    string json = "";
                                    if (cp.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA)) codDoc = CodigosSUNAT.DNI;

                                    if (String.IsNullOrEmpty(cp.venta.orden) || string.IsNullOrWhiteSpace(cp.venta.orden))
                                    {
                                        if (String.IsNullOrEmpty(guia) || string.IsNullOrWhiteSpace(guia))
                                        {
                                            Invoice inv = new Invoice(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                             Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble);
                                            fac.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(fac, Formatting.Indented);
                                        }
                                        else
                                        {
                                            InvoceGuia inv = new InvoceGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                            Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, guia);
                                            facG.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(facG, Formatting.Indented);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(guia) || string.IsNullOrWhiteSpace(guia))
                                        {
                                            InvoiceNota inv = new InvoiceNota(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                             Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden);
                                            facO.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(facO, Formatting.Indented);

                                        }
                                        else
                                        {
                                            InvoiceNotaGuia inv = new InvoiceNotaGuia(cp, venta, venta.detalles, LocalImpl.local, presentaciones, codDoc,
                                           Util.Convertir(string.Format("{0:0.00}", cp.montoTotal), true), igv.valorDouble, cp.venta.orden, guia);
                                            facGO.Invoice.Add(inv);
                                            json = JsonConvert.SerializeObject(facGO, Formatting.Indented);


                                        }
                                    }
                                    getToken(json, cp, this);
                                }
                                else if (r.Equals("ERROR")) MessageBox.Show("ERROR INESPERADO INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                if (cpservice.pagar(Convert.ToInt32(comprobantePago), medioPago, pt, fechaPagoo, monto))
                                {
                                    pasado();
                                    this.Close();
                                }
                                else { MessageBox.Show("ERROR INESPERADO INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                            }

                        }
                        else MessageBox.Show("PORCENTJE INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    else MessageBox.Show("NO EXISTE NINGUNA CUENTA");
                

            }
            btnSeguir.Enabled = true;
        }
        private void txtSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboCuentas_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        public async Task getToken(string json, ComprobantePago cp,MaterialForm formulario)
        {//"20102536593-03-BF04-00000020" + ".json";
            string file = LocalImpl.getInstancia().ruc + "-" + cp.tipoComprobante.codigo_sunat + "-" + cp.serie + "-" + Util.NormalizarCampo(cp.numero.ToString(), 8) + ".json";
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json"); 

            byte[] bytes = await (sc.ReadAsByteArrayAsync());
            File.WriteAllBytes(@"D:\jsonEfac\" + file, bytes);

            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Accept.Clear();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<String, String>("username", "20102536593"),
                new KeyValuePair<String, String>("password", "9c9b1a46fc251c622f688cc7f3141cf6439cefdbc84b6c2eab13dd8af7f45f9b"),
                    new KeyValuePair<String, String>("grant_type", "password"),  });
            cliente.DefaultRequestHeaders.Add("Content_type", "application/x-www-form-urlencoded");
            cliente.DefaultRequestHeaders.Add("Authorization", "Basic Y2xpZW50OnNlY3JldA==");
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            cliente.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes("client:secret")));
            var response = await cliente.PostAsync("https://ose.efact.pe/api-efact-ose/oauth/token", content);

            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                AccesToken token = JsonConvert.DeserializeObject<AccesToken>(r);
                try
                {
                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Clear();
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                    MultipartFormDataContent contenido = new MultipartFormDataContent();

                    // MultipartFormDataContent form = new MultipartFormDataContent();
                    HttpContent contnt;//= new StringContent("fileToUpload");
                                       //  form.Add(contnt, "fileToUpload");
                    HttpContent fileContent = new StreamContent(new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)));
                    var stream = new FileStream(@"D:\jsonEfac\" + file, FileMode.Open);
                    contnt = new StreamContent(stream);
                    contnt.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        Name = "file",
                        FileName = file
                    };
                    contenido.Add(contnt);



                    //    StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");
                    //  HttpContent fileContent = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes( json));
                    //  

                    /*     fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data") //<- 'form-data' instead of 'attachment'
                            {
                        Name = "file", // <- included line...
                                FileName = "20102536593-01-FF04-00000020.json",
                    };
                    */
                    contenido.Headers.Remove("Content_type");
                    //  contenido.Add(fileContent,"file", "20102536593-01-FF04-00000020.json");
                    //  contenido.Add(fileContent,"file", "20102536593-01-FF04-00000020.json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);

                    client.DefaultRequestHeaders.Add("Accept", "*/*");

                    //    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    //  client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("multipart/form-data"));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                    var resp = await client.PostAsync("https://ose.efact.pe/api-efact-ose/v1/document", contenido);

                    if (resp.IsSuccessStatusCode)
                    {
                        var re = await resp.Content.ReadAsStringAsync();
                        TicketEfac tikcket = JsonConvert.DeserializeObject<TicketEfac>(re);
                        ComprobantePago cp2 = cpservice.buscarSolo(cp.id);
                        cp.hash = tikcket.description;
                        cp2.hash = tikcket.description;
                        cp2.enviado = true;
                        cpservice.editar(cp2);
                   //     pasado();



                        HttpClient clien = new HttpClient();
                        clien.DefaultRequestHeaders.Accept.Clear();
                        clien.DefaultRequestHeaders.Clear();
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11;

                        //  contenido.Headers.Remove("Content_type");
                        //  contenido.Add(fileContent,"file", "20102536593-01-FF04-00000020.json");
                        //  contenido.Add(fileContent,"file", "20102536593-01-FF04-00000020.json");
                        clien.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.access_token);
                        clien.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.access_token);
                        HttpResponseMessage respo = await clien.GetAsync("https://ose.efact.pe/api-efact-ose/v1/pdf/" + tikcket.description);
                        if (respo.IsSuccessStatusCode)
                        {
                            File.WriteAllBytes(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf", await respo.Content.ReadAsByteArrayAsync());
                            Process.Start(@"D:\COMPROBANTES PDF\" + cp.serie + "-" + cp.numero + ".pdf");
                            formulario.Close();
                        }
                        else MessageBox.Show("NOSE PUDO IMPRIMIR PDF" + respo.ToString());
                    }
                    else MessageBox.Show("NOSE PUDO ENVIAR ERROR DE CONEXION");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + e.StackTrace);
                }
            }
            else MessageBox.Show("ERROR DE ENVIO NO SE OBTUVO AUTENTIFICACION");

        }

    }
}
