﻿namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    partial class FormAdminFacturacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdminFacturacion));
            this.contenedor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGuiaRemision = new System.Windows.Forms.Button();
            this.btnNotaCredito = new System.Windows.Forms.Button();
            this.btnBoletas = new System.Windows.Forms.Button();
            this.btnPedidos = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contenedor
            // 
            this.contenedor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.contenedor.Location = new System.Drawing.Point(224, 64);
            this.contenedor.Name = "contenedor";
            this.contenedor.Size = new System.Drawing.Size(1124, 634);
            this.contenedor.TabIndex = 6;
            this.contenedor.Paint += new System.Windows.Forms.PaintEventHandler(this.contenedor_Paint);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnGuiaRemision);
            this.panel1.Controls.Add(this.btnNotaCredito);
            this.panel1.Controls.Add(this.btnBoletas);
            this.panel1.Controls.Add(this.btnPedidos);
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 634);
            this.panel1.TabIndex = 5;
            // 
            // btnGuiaRemision
            // 
            this.btnGuiaRemision.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGuiaRemision.FlatAppearance.BorderSize = 0;
            this.btnGuiaRemision.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnGuiaRemision.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnGuiaRemision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuiaRemision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuiaRemision.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnGuiaRemision.Image = ((System.Drawing.Image)(resources.GetObject("btnGuiaRemision.Image")));
            this.btnGuiaRemision.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuiaRemision.Location = new System.Drawing.Point(0, 168);
            this.btnGuiaRemision.Name = "btnGuiaRemision";
            this.btnGuiaRemision.Size = new System.Drawing.Size(228, 49);
            this.btnGuiaRemision.TabIndex = 6;
            this.btnGuiaRemision.Text = "          Guias de Remisión";
            this.btnGuiaRemision.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuiaRemision.UseVisualStyleBackColor = true;
            this.btnGuiaRemision.Click += new System.EventHandler(this.btnGuiaRemision_Click);
            // 
            // btnNotaCredito
            // 
            this.btnNotaCredito.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNotaCredito.FlatAppearance.BorderSize = 0;
            this.btnNotaCredito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnNotaCredito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnNotaCredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNotaCredito.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnNotaCredito.Image = ((System.Drawing.Image)(resources.GetObject("btnNotaCredito.Image")));
            this.btnNotaCredito.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNotaCredito.Location = new System.Drawing.Point(0, 113);
            this.btnNotaCredito.Name = "btnNotaCredito";
            this.btnNotaCredito.Size = new System.Drawing.Size(228, 49);
            this.btnNotaCredito.TabIndex = 5;
            this.btnNotaCredito.Text = "          Notas de C / D";
            this.btnNotaCredito.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNotaCredito.UseVisualStyleBackColor = true;
            this.btnNotaCredito.Click += new System.EventHandler(this.btnNotaCredito_Click);
            // 
            // btnBoletas
            // 
            this.btnBoletas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBoletas.FlatAppearance.BorderSize = 0;
            this.btnBoletas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnBoletas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnBoletas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBoletas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBoletas.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBoletas.Image = ((System.Drawing.Image)(resources.GetObject("btnBoletas.Image")));
            this.btnBoletas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBoletas.Location = new System.Drawing.Point(0, 58);
            this.btnBoletas.Name = "btnBoletas";
            this.btnBoletas.Size = new System.Drawing.Size(228, 49);
            this.btnBoletas.TabIndex = 5;
            this.btnBoletas.Text = "          Boletas / Facturas";
            this.btnBoletas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBoletas.UseVisualStyleBackColor = true;
            this.btnBoletas.Click += new System.EventHandler(this.btnBoletas_Click);
            // 
            // btnPedidos
            // 
            this.btnPedidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPedidos.FlatAppearance.BorderSize = 0;
            this.btnPedidos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnPedidos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnPedidos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPedidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPedidos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPedidos.Image = ((System.Drawing.Image)(resources.GetObject("btnPedidos.Image")));
            this.btnPedidos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPedidos.Location = new System.Drawing.Point(0, 3);
            this.btnPedidos.Name = "btnPedidos";
            this.btnPedidos.Size = new System.Drawing.Size(228, 49);
            this.btnPedidos.TabIndex = 1;
            this.btnPedidos.Text = "          Pedidos";
            this.btnPedidos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPedidos.UseVisualStyleBackColor = true;
            this.btnPedidos.Click += new System.EventHandler(this.btnPedidos_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(3, 223);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 49);
            this.button1.TabIndex = 7;
            this.button1.Text = "          No Enviados";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormAdminFacturacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 692);
            this.Controls.Add(this.contenedor);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "FormAdminFacturacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Módulo Facturación";
            this.Load += new System.EventHandler(this.FormAdminFacturacion_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel contenedor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGuiaRemision;
        private System.Windows.Forms.Button btnNotaCredito;
        private System.Windows.Forms.Button btnBoletas;
        private System.Windows.Forms.Button btnPedidos;
        private System.Windows.Forms.Button button1;
    }
}