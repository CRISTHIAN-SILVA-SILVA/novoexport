﻿namespace ERP_LITE_DESKTOP.FACTURACION_FORM
{
    partial class FormEligeNota
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboNota = new System.Windows.Forms.ComboBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.comboTipoNota = new System.Windows.Forms.ComboBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtMotivo = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.btnSeguir = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSerie = new MaterialSkin.Controls.MaterialLabel();
            this.lblTipoComprobante = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCliente = new MaterialSkin.Controls.MaterialLabel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboNota
            // 
            this.comboNota.DisplayMember = "descripcion";
            this.comboNota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboNota.FormattingEnabled = true;
            this.comboNota.Items.AddRange(new object[] {
            "NOTA DE CREDITO",
            "NOTA DE DEBITO"});
            this.comboNota.Location = new System.Drawing.Point(98, 168);
            this.comboNota.Name = "comboNota";
            this.comboNota.Size = new System.Drawing.Size(539, 24);
            this.comboNota.TabIndex = 170;
            this.comboNota.ValueMember = "id";
            this.comboNota.SelectedIndexChanged += new System.EventHandler(this.comboNota_SelectedIndexChanged);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(12, 203);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(80, 19);
            this.materialLabel2.TabIndex = 169;
            this.materialLabel2.Text = "Tipo Nota:";
            // 
            // comboTipoNota
            // 
            this.comboTipoNota.DisplayMember = "descripcion";
            this.comboTipoNota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipoNota.FormattingEnabled = true;
            this.comboTipoNota.Location = new System.Drawing.Point(98, 198);
            this.comboTipoNota.Name = "comboTipoNota";
            this.comboTipoNota.Size = new System.Drawing.Size(539, 24);
            this.comboTipoNota.TabIndex = 168;
            this.comboTipoNota.ValueMember = "id";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(12, 173);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(46, 19);
            this.materialLabel1.TabIndex = 167;
            this.materialLabel1.Text = "Nota:";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Depth = 0;
            this.txtMotivo.Hint = "";
            this.txtMotivo.Location = new System.Drawing.Point(98, 228);
            this.txtMotivo.MaxLength = 32767;
            this.txtMotivo.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.PasswordChar = '\0';
            this.txtMotivo.SelectedText = "";
            this.txtMotivo.SelectionLength = 0;
            this.txtMotivo.SelectionStart = 0;
            this.txtMotivo.Size = new System.Drawing.Size(539, 23);
            this.txtMotivo.TabIndex = 171;
            this.txtMotivo.TabStop = false;
            this.txtMotivo.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(12, 232);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(60, 19);
            this.materialLabel3.TabIndex = 172;
            this.materialLabel3.Text = "Motivo:";
            // 
            // btnSeguir
            // 
            this.btnSeguir.AutoSize = true;
            this.btnSeguir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSeguir.Depth = 0;
            this.btnSeguir.Icon = null;
            this.btnSeguir.Location = new System.Drawing.Point(572, 260);
            this.btnSeguir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSeguir.Name = "btnSeguir";
            this.btnSeguir.Primary = true;
            this.btnSeguir.Size = new System.Drawing.Size(65, 36);
            this.btnSeguir.TabIndex = 173;
            this.btnSeguir.Text = "nueva";
            this.btnSeguir.UseVisualStyleBackColor = true;
            this.btnSeguir.Click += new System.EventHandler(this.btnSeguir_Click);
            // 
            // txtSalir
            // 
            this.txtSalir.AutoSize = true;
            this.txtSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.txtSalir.Depth = 0;
            this.txtSalir.Icon = null;
            this.txtSalir.Location = new System.Drawing.Point(507, 260);
            this.txtSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSalir.Name = "txtSalir";
            this.txtSalir.Primary = false;
            this.txtSalir.Size = new System.Drawing.Size(58, 36);
            this.txtSalir.TabIndex = 174;
            this.txtSalir.Text = "salir";
            this.txtSalir.UseVisualStyleBackColor = true;
            this.txtSalir.Click += new System.EventHandler(this.txtSalir_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.lblCliente);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblTipoComprobante);
            this.panel1.Controls.Add(this.lblSerie);
            this.panel1.Location = new System.Drawing.Point(12, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(625, 88);
            this.panel1.TabIndex = 175;
            // 
            // lblSerie
            // 
            this.lblSerie.Depth = 0;
            this.lblSerie.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblSerie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblSerie.Location = new System.Drawing.Point(4, 23);
            this.lblSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(618, 19);
            this.lblSerie.TabIndex = 147;
            this.lblSerie.Text = "SERIE: FE01";
            this.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoComprobante.Location = new System.Drawing.Point(4, 0);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(618, 23);
            this.lblTipoComprobante.TabIndex = 157;
            this.lblTipoComprobante.Text = "COMPROBANTE ELECTRONICO";
            this.lblTipoComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 23);
            this.label3.TabIndex = 160;
            this.label3.Text = "CLIENTE:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCliente
            // 
            this.lblCliente.Depth = 0;
            this.lblCliente.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCliente.Location = new System.Drawing.Point(86, 57);
            this.lblCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(536, 19);
            this.lblCliente.TabIndex = 161;
            this.lblCliente.Text = "SERIE: FE01";
            this.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormEligeNota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 305);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtSalir);
            this.Controls.Add(this.btnSeguir);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.txtMotivo);
            this.Controls.Add(this.comboNota);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.comboTipoNota);
            this.Controls.Add(this.materialLabel1);
            this.MaximizeBox = false;
            this.Name = "FormEligeNota";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Elige Tipo De Nota";
            this.Load += new System.EventHandler(this.FormEligeNota_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboNota;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.ComboBox comboTipoNota;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMotivo;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialRaisedButton btnSeguir;
        private MaterialSkin.Controls.MaterialFlatButton txtSalir;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTipoComprobante;
        private MaterialSkin.Controls.MaterialLabel lblSerie;
        private MaterialSkin.Controls.MaterialLabel lblCliente;
        private System.Windows.Forms.Label label3;
    }
}