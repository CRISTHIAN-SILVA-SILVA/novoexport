﻿namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{
    partial class formGuia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtPartida = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtLlegada = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.lblCliente = new MaterialSkin.Controls.MaterialLabel();
            this.lblDocCliente = new MaterialSkin.Controls.MaterialLabel();
            this.gridDetalle = new System.Windows.Forms.DataGridView();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.lblDocTransporte = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.txtPlaca = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtLicencia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.dfdfsfds = new MaterialSkin.Controls.MaterialLabel();
            this.btnCancelar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtConductor = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.txtConductor1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.comboTransporte = new System.Windows.Forms.ComboBox();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.txtNumeroGuia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPartida
            // 
            this.txtPartida.Depth = 0;
            this.txtPartida.Hint = "";
            this.txtPartida.Location = new System.Drawing.Point(101, 146);
            this.txtPartida.MaxLength = 32767;
            this.txtPartida.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPartida.Name = "txtPartida";
            this.txtPartida.PasswordChar = '\0';
            this.txtPartida.SelectedText = "";
            this.txtPartida.SelectionLength = 0;
            this.txtPartida.SelectionStart = 0;
            this.txtPartida.Size = new System.Drawing.Size(386, 23);
            this.txtPartida.TabIndex = 146;
            this.txtPartida.TabStop = false;
            this.txtPartida.UseSystemPasswordChar = false;
            // 
            // materialLabel2
            // 
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(12, 146);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(79, 23);
            this.materialLabel2.TabIndex = 145;
            this.materialLabel2.Text = "Partida:";
            // 
            // txtLlegada
            // 
            this.txtLlegada.Depth = 0;
            this.txtLlegada.Hint = "";
            this.txtLlegada.Location = new System.Drawing.Point(578, 146);
            this.txtLlegada.MaxLength = 32767;
            this.txtLlegada.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtLlegada.Name = "txtLlegada";
            this.txtLlegada.PasswordChar = '\0';
            this.txtLlegada.SelectedText = "";
            this.txtLlegada.SelectionLength = 0;
            this.txtLlegada.SelectionStart = 0;
            this.txtLlegada.Size = new System.Drawing.Size(389, 23);
            this.txtLlegada.TabIndex = 148;
            this.txtLlegada.TabStop = false;
            this.txtLlegada.UseSystemPasswordChar = false;
            // 
            // materialLabel1
            // 
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(493, 146);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(79, 23);
            this.materialLabel1.TabIndex = 147;
            this.materialLabel1.Text = "Llegada:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(12, 105);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(79, 23);
            this.materialLabel3.TabIndex = 149;
            this.materialLabel3.Text = "Cliente:";
            // 
            // materialLabel4
            // 
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(682, 105);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(67, 23);
            this.materialLabel4.TabIndex = 151;
            this.materialLabel4.Text = "N° Doc:";
            // 
            // lblCliente
            // 
            this.lblCliente.Depth = 0;
            this.lblCliente.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCliente.Location = new System.Drawing.Point(97, 105);
            this.lblCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(557, 23);
            this.lblCliente.TabIndex = 152;
            this.lblCliente.Text = "N° Doc:";
            this.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDocCliente
            // 
            this.lblDocCliente.Depth = 0;
            this.lblDocCliente.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblDocCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDocCliente.Location = new System.Drawing.Point(767, 105);
            this.lblDocCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblDocCliente.Name = "lblDocCliente";
            this.lblDocCliente.Size = new System.Drawing.Size(200, 23);
            this.lblDocCliente.TabIndex = 153;
            this.lblDocCliente.Text = "N° Doc:";
            this.lblDocCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gridDetalle
            // 
            this.gridDetalle.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDetalle.BackgroundColor = System.Drawing.Color.White;
            this.gridDetalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridDetalle.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.gridDetalle.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridDetalle.EnableHeadersVisualStyles = false;
            this.gridDetalle.GridColor = System.Drawing.Color.Black;
            this.gridDetalle.Location = new System.Drawing.Point(12, 204);
            this.gridDetalle.MultiSelect = false;
            this.gridDetalle.Name = "gridDetalle";
            this.gridDetalle.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridDetalle.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.gridDetalle.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.Height = 31;
            this.gridDetalle.RowTemplate.ReadOnly = true;
            this.gridDetalle.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDetalle.Size = new System.Drawing.Size(957, 161);
            this.gridDetalle.TabIndex = 180;
            // 
            // materialLabel8
            // 
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel8.Location = new System.Drawing.Point(12, 385);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(84, 22);
            this.materialLabel8.TabIndex = 181;
            this.materialLabel8.Text = "Transporte";
            // 
            // lblDocTransporte
            // 
            this.lblDocTransporte.Depth = 0;
            this.lblDocTransporte.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblDocTransporte.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDocTransporte.Location = new System.Drawing.Point(101, 418);
            this.lblDocTransporte.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblDocTransporte.Name = "lblDocTransporte";
            this.lblDocTransporte.Size = new System.Drawing.Size(443, 23);
            this.lblDocTransporte.TabIndex = 184;
            this.lblDocTransporte.Text = "N° Doc:";
            this.lblDocTransporte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // materialLabel10
            // 
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(12, 418);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(67, 23);
            this.materialLabel10.TabIndex = 183;
            this.materialLabel10.Text = "N° Doc:";
            // 
            // materialLabel11
            // 
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(610, 420);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(79, 23);
            this.materialLabel11.TabIndex = 185;
            this.materialLabel11.Text = "Placa:";
            // 
            // txtPlaca
            // 
            this.txtPlaca.Depth = 0;
            this.txtPlaca.Hint = "";
            this.txtPlaca.Location = new System.Drawing.Point(695, 418);
            this.txtPlaca.MaxLength = 32767;
            this.txtPlaca.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.PasswordChar = '\0';
            this.txtPlaca.SelectedText = "";
            this.txtPlaca.SelectionLength = 0;
            this.txtPlaca.SelectionStart = 0;
            this.txtPlaca.Size = new System.Drawing.Size(97, 23);
            this.txtPlaca.TabIndex = 188;
            this.txtPlaca.TabStop = false;
            this.txtPlaca.UseSystemPasswordChar = false;
            // 
            // txtLicencia
            // 
            this.txtLicencia.Depth = 0;
            this.txtLicencia.Hint = "";
            this.txtLicencia.Location = new System.Drawing.Point(883, 420);
            this.txtLicencia.MaxLength = 32767;
            this.txtLicencia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtLicencia.Name = "txtLicencia";
            this.txtLicencia.PasswordChar = '\0';
            this.txtLicencia.SelectedText = "";
            this.txtLicencia.SelectionLength = 0;
            this.txtLicencia.SelectionStart = 0;
            this.txtLicencia.Size = new System.Drawing.Size(80, 23);
            this.txtLicencia.TabIndex = 190;
            this.txtLicencia.TabStop = false;
            this.txtLicencia.UseSystemPasswordChar = false;
            // 
            // dfdfsfds
            // 
            this.dfdfsfds.Depth = 0;
            this.dfdfsfds.Font = new System.Drawing.Font("Roboto", 11F);
            this.dfdfsfds.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dfdfsfds.Location = new System.Drawing.Point(798, 418);
            this.dfdfsfds.MouseState = MaterialSkin.MouseState.HOVER;
            this.dfdfsfds.Name = "dfdfsfds";
            this.dfdfsfds.Size = new System.Drawing.Size(79, 23);
            this.dfdfsfds.TabIndex = 189;
            this.dfdfsfds.Text = "Licencia:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.AutoSize = true;
            this.btnCancelar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancelar.Depth = 0;
            this.btnCancelar.Icon = null;
            this.btnCancelar.Location = new System.Drawing.Point(776, 456);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCancelar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Primary = false;
            this.btnCancelar.Size = new System.Drawing.Size(91, 36);
            this.btnCancelar.TabIndex = 192;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtConductor
            // 
            this.txtConductor.AutoSize = true;
            this.txtConductor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.txtConductor.Depth = 0;
            this.txtConductor.Icon = null;
            this.txtConductor.Location = new System.Drawing.Point(874, 456);
            this.txtConductor.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtConductor.Name = "txtConductor";
            this.txtConductor.Primary = true;
            this.txtConductor.Size = new System.Drawing.Size(89, 36);
            this.txtConductor.TabIndex = 191;
            this.txtConductor.Text = "Terminar";
            this.txtConductor.UseVisualStyleBackColor = true;
            this.txtConductor.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // materialLabel12
            // 
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(610, 381);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(79, 23);
            this.materialLabel12.TabIndex = 187;
            this.materialLabel12.Text = "Conductor:";
            // 
            // txtConductor1
            // 
            this.txtConductor1.Depth = 0;
            this.txtConductor1.Hint = "";
            this.txtConductor1.Location = new System.Drawing.Point(695, 381);
            this.txtConductor1.MaxLength = 32767;
            this.txtConductor1.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtConductor1.Name = "txtConductor1";
            this.txtConductor1.PasswordChar = '\0';
            this.txtConductor1.SelectedText = "";
            this.txtConductor1.SelectionLength = 0;
            this.txtConductor1.SelectionStart = 0;
            this.txtConductor1.Size = new System.Drawing.Size(268, 23);
            this.txtConductor1.TabIndex = 186;
            this.txtConductor1.TabStop = false;
            this.txtConductor1.UseSystemPasswordChar = false;
            // 
            // comboTransporte
            // 
            this.comboTransporte.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboTransporte.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboTransporte.DisplayMember = "razonSocial";
            this.comboTransporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTransporte.FormattingEnabled = true;
            this.comboTransporte.Location = new System.Drawing.Point(107, 381);
            this.comboTransporte.MaxLength = 200;
            this.comboTransporte.Name = "comboTransporte";
            this.comboTransporte.Size = new System.Drawing.Size(437, 26);
            this.comboTransporte.TabIndex = 193;
            this.comboTransporte.ValueMember = "id";
            this.comboTransporte.SelectedIndexChanged += new System.EventHandler(this.comboTransporte_SelectedIndexChanged);
            // 
            // materialLabel5
            // 
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(17, 178);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(79, 23);
            this.materialLabel5.TabIndex = 194;
            this.materialLabel5.Text = "Detalle:";
            // 
            // txtNumeroGuia
            // 
            this.txtNumeroGuia.Depth = 0;
            this.txtNumeroGuia.Hint = "";
            this.txtNumeroGuia.Location = new System.Drawing.Point(476, 69);
            this.txtNumeroGuia.MaxLength = 32767;
            this.txtNumeroGuia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumeroGuia.Name = "txtNumeroGuia";
            this.txtNumeroGuia.PasswordChar = '\0';
            this.txtNumeroGuia.SelectedText = "";
            this.txtNumeroGuia.SelectionLength = 0;
            this.txtNumeroGuia.SelectionStart = 0;
            this.txtNumeroGuia.Size = new System.Drawing.Size(146, 23);
            this.txtNumeroGuia.TabIndex = 196;
            this.txtNumeroGuia.TabStop = false;
            this.txtNumeroGuia.UseSystemPasswordChar = false;
            // 
            // materialLabel6
            // 
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(391, 69);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(79, 23);
            this.materialLabel6.TabIndex = 195;
            this.materialLabel6.Text = "N°:";
            // 
            // formGuia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 507);
            this.Controls.Add(this.txtNumeroGuia);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.comboTransporte);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.txtConductor);
            this.Controls.Add(this.txtLicencia);
            this.Controls.Add(this.dfdfsfds);
            this.Controls.Add(this.txtPlaca);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.txtConductor1);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.lblDocTransporte);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.gridDetalle);
            this.Controls.Add(this.lblDocCliente);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.txtLlegada);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.txtPartida);
            this.Controls.Add(this.materialLabel2);
            this.MaximizeBox = false;
            this.Name = "formGuia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Guia Remision";
            this.Load += new System.EventHandler(this.FormGuiaRemision_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField txtPartida;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtLlegada;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel lblCliente;
        private MaterialSkin.Controls.MaterialLabel lblDocCliente;
        private System.Windows.Forms.DataGridView gridDetalle;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel lblDocTransporte;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPlaca;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtLicencia;
        private MaterialSkin.Controls.MaterialLabel dfdfsfds;
        private MaterialSkin.Controls.MaterialFlatButton btnCancelar;
        private MaterialSkin.Controls.MaterialRaisedButton txtConductor;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtConductor1;
        private System.Windows.Forms.ComboBox comboTransporte;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumeroGuia;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
    }
}