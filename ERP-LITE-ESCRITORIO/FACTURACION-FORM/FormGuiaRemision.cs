﻿using ERP_LITE_DESKTOP;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.FACTURACION_FORM
{

    public partial class formGuia : MaterialForm
    {
        TransportistaService tservice = new TransportistaImpl();
        ClienteService cservice = new ClienteImpl();
        GuiaRemisonService gservice = new GuiaRemisionImpl();
        ComprobantePago comprobante = null;
        DataTable tabla = new DataTable();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        public formGuia(ComprobantePago cp,Venta v)
        {
            comprobante = cp;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            lblCliente.Text = cp.cliente.razonSocial;

            txtPartida.Text = LocalImpl.getInstancia().direccion ;
            txtLlegada.Text= cp.cliente.direccion;
            List<Transportista> transportistas = tservice.listarNoAnulados();
            AutoCompleteStringCollection colec = new AutoCompleteStringCollection();
            transportistas.ForEach(c => colec.Add(c.razonSocial));
            comboTransporte.AutoCompleteCustomSource = colec;
            comboTransporte.DataSource = transportistas;

            tabla.Columns.Add("ID");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("CANTIDAD");
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Width = 80;
            gridDetalle.Columns[1].Width = 730;
            gridDetalle.Columns[2].Width = 100;
            foreach (var x in v.detalles) {
                if (!x.anulado)
                {
                    ProductoPresentacion pp = ppservice.buscar(x.idProductoPresentacion);
                    tabla.Rows.Add(x.id, pp.nombre, String.Format("{0:0.00}", x.cantidad));
                }
            }

            lblCliente.Text = cp.cliente.razonSocial;
            lblDocCliente.Text = cp.cliente.ruc + " / " + cp.cliente.dniRepresentante;

        }

        private void FormGuiaRemision_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtPartida.Focus();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(Util.esDouble(txtNumeroGuia.Text))
                if (Util.validaCadena(txtPartida.Text))
                    if (Util.validaCadena(txtLlegada.Text))
                        if (comboTransporte.SelectedValue != null)
                        {
                            GuiaRemision guia = new GuiaRemision {
                               
                                direccionLlegada = txtLlegada.Text,
                                direccionPartida = txtPartida.Text,
                                idComprobante = comprobante.id,
                                licencia = txtLicencia.Text,
                                idTransportista = (int)comboTransporte.SelectedValue,
                                idMotivo = MotivoTransladoImpl.getInstancia().FirstOrDefault(x => x.descripcion == "VENTA").id,
                                placa = txtPlaca.Text,
                                conductor = txtConductor1.Text
                              //  serieNumeroComprobante = comprobante.serie + Util.NormalizarCampo(comprobante.numero.ToString(), 8)
                                , usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni, numero = (int)Convert.ToDouble(txtNumeroGuia.Text),
                            };
                       
                            if (gservice.crear(guia))
                            {
                                TxtSUNAT.construirGuia(guia);
                                this.Close();
                            }
                            else MessageBox.Show("ERRROR INESPERADO INTENTALO DE NUEVO");
                        }
                        else MessageBox.Show("NO HAY TRANPORTE","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    else MessageBox.Show("FALTA DIRECCION LLEGADA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("FALTA DIRECCION DE PARTIDA", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else MessageBox.Show("NUMERO DE GUIA INVALIDO", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void comboTransporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboTransporte.SelectedValue != null) {
               lblDocTransporte.Text=  tservice.buscar((int)comboTransporte.SelectedValue).ruc;
            }

        }
    }
}
