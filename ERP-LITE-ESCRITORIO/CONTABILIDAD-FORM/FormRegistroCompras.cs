﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONTABILIDAD_FORM
{
    public partial class FormRegistroCompras : MaterialForm
    {
        DataTable tabla = new DataTable();
        DateTime inicio, fin;
        RegistroCompraImpl rcservice = new RegistroCompraImpl();
        string periodo = "";
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        public FormRegistroCompras(DateTime i, DateTime f)
        {
            inicio = i; fin = f;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            lblPeriodo.Text = i.Month + " - " + i.Year;
            lblRazonSocial.Text = LocalImpl.getInstancia().razonSocial;
            lblRuc.Text = LocalImpl.getInstancia().ruc;

            periodo = i.Year + Util.NormalizarCampo(i.Month.ToString(), 2) + "00";





            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("TIPO C.");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("TIPO D.");
            tabla.Columns.Add("NRO DOC");
            tabla.Columns.Add("RAZON SOCIAL");
            tabla.Columns.Add("C/K");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("MONEDA");
            tabla.Columns.Add("BASE IMPONIBLE");
            tabla.Columns.Add("EXONERADO/INAFECTO");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("DESCUENTO");
            tabla.Columns.Add("FECHA DETRACCION");
            tabla.Columns.Add("DETRACCION");
            tabla.Columns.Add("NRO CUENTA");
            tabla.Columns.Add("IMPORTE TOTAL");
            tabla.Columns.Add("TIPO CAMBIO");
            tabla.Columns.Add("FECHA R");
            tabla.Columns.Add("TIPO R");
            tabla.Columns.Add("SERIE R");
            tabla.Columns.Add("NUMERO R");
            grid.DataSource = tabla;

            /*
                        tabla.Columns.Add("COMPRAGASTO");
                        tabla.Columns.Add("PERIDO");
                        tabla.Columns.Add("TDOC");
                        tabla.Columns.Add("CODCUENTA");
                        tabla.Columns.Add("FORMA PAG");

                        tabla.Columns.Add("CK");
                        tabla.Columns.Add("FECHA");

                        tabla.Columns.Add("NOMBRE");
                        tabla.Columns.Add("RUC");
                        tabla.Columns.Add("SERIE");
                        tabla.Columns.Add("NRO");
                        tabla.Columns.Add("MONEDA");
                        tabla.Columns.Add("TOTAL");

                        //  tabla.Columns.Add("8");  //debe ir 0 preguntar??

                        tabla.Columns.Add("PERCEP/EXONERA");

                        tabla.Columns.Add("DETRACCION");
                        tabla.Columns.Add("TIPO CAMBIO");
                        tabla.Columns.Add("TC1");

                        //    tabla.Columns.Add("VALOR EXPORTACION.");


                        tabla.Columns.Add("BASE IM.");



                        tabla.Columns.Add("IGV");
                        tabla.Columns.Add("TOTALS");
                        tabla.Columns.Add("TIP");
                        tabla.Columns.Add("FORMA PAGO");
                        grid.DataSource = tabla;
                        //      tabla.Columns.Add("ESTADO");  // DEBE SER 1*/

        }

        private void FormRegistroCompras_Load(object sender, EventArgs e)
        {
            int signo = 1;
            string numeroNota = "";
            foreach (RegistroCompra x in rcservice.listarPorFechas(inicio, fin))
            {
                if (x.tipoComprobante.Equals("07")) signo = -1; else signo = 1;
                if (x.numeroNota != 0) { numeroNota = x.numeroNota.ToString(); } else { numeroNota = ""; }
                if (x.montoDetraccion == 0) { } else { }
                tabla.Rows.Add(x.fechaEmision.ToShortDateString(),
                   x.tipoComprobante, x.serie, x.numeroComprobante,
                   x.tipoDocIdentidad,
                   x.docIdentidad,   //forma pago
                   x.proveedor.razonSocial, "",                                  //03
                 String.Format("{0:0.00}", x.total),          //04
                                                              //    x.fechaVencimiento.ToString("dd/MM/yyyy")                     //05
               x.moneda.codigo_sunat, String.Format("{0:0.00}", signo * x.opGravadas), String.Format("{0:0.00}", signo * x.opExoneradas), String.Format("{0:0.00}", signo * x.igv),
               x.descuento,

               x.fechaDetraccion, String.Format("{0:0.00}", signo * x.montoDetraccion), x.numeroDetraccion, String.Format("{0:0.00}", signo * x.total), x.tipoCambio,

               x.fechaNota, x.tipoNota, x.serieNota, numeroNota

                 );
            }
                tabla.Rows.Add();
                tabla.Rows.Add("GASTOS");
                tabla.Rows.Add();

                foreach (RegistroCompra x in rcservice.listarPorFechasGastos(inicio, fin))
                {
                    if (x.tipoComprobante.Equals("07")) signo = -1; else signo = 1;
                    if (x.numeroNota != 0) { numeroNota = x.numeroNota.ToString(); } else { numeroNota = ""; }
                    if (x.montoDetraccion == 0) { } else { }
                    tabla.Rows.Add(x.fechaEmision.ToShortDateString(),
                       x.tipoComprobante, x.serie, x.numeroComprobante,
                       x.tipoDocIdentidad,
                       x.docIdentidad,   //forma pago
                       x.proveedor.razonSocial, "",                                  //03
                     String.Format("{0:0.00}", x.total),          //04
                                                                  //    x.fechaVencimiento.ToString("dd/MM/yyyy")                     //05
                   x.moneda.codigo_sunat, String.Format("{0:0.00}", signo * x.opGravadas), String.Format("{0:0.00}", signo * x.valorNoGravadas), String.Format("{0:0.00}", signo * x.igv),
                   x.descuento,
                   x.fechaDetraccion, String.Format("{0:0.00}", signo * x.montoDetraccion), x.numeroDetraccion, String.Format("{0:0.00}", signo * x.total), x.tipoCambio,

                   x.fechaNota, x.tipoNota, x.serieNota, numeroNota

                     );


                    /*       if (x.esGasto) { tipo = "1"; cod_cuenta = ""; } else { tipo = "0"; cod_cuenta = "60"; }
                           if (x.numeroComprobante == 0) numero = ""; else numero = x.numeroComprobante.ToString();
                           if (x.numeroNota == 0) nr = ""; else nr = x.numeroNota.ToString();
                           if (x.anulado) estado = "2"; else estado = "1";
                           index++;
                           tabla.Rows.Add(tipo, periodo, x.tipoComprobante, cod_cuenta, "",   //forma pago
                               "C",                                       //03
                               x.fechaEmision.ToString("dd/MM/yyyy"),          //04
                                                                               //    x.fechaVencimiento.ToString("dd/MM/yyyy")                     //05
                                              x.proveedor.razonSocial,                                 //  , x.tipoComprobante                          //06
                             x.docIdentidad
                               , x.serie,                                       //07
                                 numero,                                             //08 año dua
                              x.moneda.codigo_sunat,                                                  //10 consoliddo no credto fiscal
                            string.Format("{0:0.00}", x.total), ""
                             , string.Format("{0:0.00}", x.montoDetraccion),
                             "",           //TPO DE CAMBIO1
                         string.Format("{0:0.00}", x.tipoCambio),

                         string.Format("{0:0.00}", x.opGravadas),
                       string.Format("{0:0.00}", x.igv),
                         string.Format("{0:0.00}", x.total),
                   x.tipoComprobante,
                           x.medioPago.nombre

                             );*/

                }
        }

        private void btnNuevoIngreso_Click(object sender, EventArgs e)
        {
            string filename = "LE" + LocalImpl.getInstancia().ruc + periodo + "00" + "820200" + "1" + "1" + "1" + "1" + ".TXT";
            Exportar.exportarATXTAsync(grid, filename);
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
    }
}
