﻿using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONTABILIDAD_FORM
{
    public partial class FormPagosVentas : Form
    {
        MovimientoCajaImpl mcserrvice = new MovimientoCajaImpl();
       DataTable tabla = new DataTable();
        DateTime inicio, fin;
        RegistroVentaImpl rvservice = new RegistroVentaImpl();
        ComprobantePagoImpl cpservice = new ComprobantePagoImpl();
        PagosBancoImpl ptservice = new PagosBancoImpl();
        public FormPagosVentas(DateTime i,DateTime f)
        {
            inicio = i;fin = f.Date;
            InitializeComponent();

            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("CODIGO");
            tabla.Columns.Add("TDOC");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("N°");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("MONEDA");
            tabla.Columns.Add("SUBTOTAL");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("SALDO");
            tabla.Columns.Add("COBRANZA.FECHA");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("CUADRO");
            tabla.Columns.Add("LETRA");
            tabla.Columns.Add("TIPO DE PAGO");
            tabla.Columns.Add("FORMA PAGO");


            grid.DataSource = tabla;
            grid.Columns[0].Width = 70; grid.Columns[1].Width = 70;
            grid.Columns[2].Width = 80; grid.Columns[3].Width = 70; grid.Columns[4].Width = 80;
            grid.Columns[5].Width = 70; grid.Columns[6].Width = 40;
            grid.Columns[7].Width = 80; grid.Columns[8].Width = 100; grid.Columns[9].Width = 400;
            grid.Columns[10].Width = 80; grid.Columns[11].Width = 80;
            grid.Columns[12].Width = 80; grid.Columns[13].Width = 80; grid.Columns[14].Width = 80; grid.Columns[15].Width = 80; grid.Columns[16].Width = 80;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void FormPagosVentas_Load(object sender, EventArgs e)
        {
            ComprobantePago c;
            string formaPago = "";
            List<MovimientoCaja> movimientos;
            int signo = 1;
            PagoTarjeta pt;
            foreach (var x in rvservice.listarPorFechas(inicio, fin)) {
                if (x.codTipoComprobante == "07" || x.codTipoComprobante == "08")
                {
                    if (x.codTipoComprobante == "07") signo = -1; else signo = 1;
                    tabla.Rows.Add(x.fechaEmision.ToString("yyyy-MM-dd"), x.codTipoComprobante, "", x.serie, x.numero, 
                        x.cliente.razonSocial, "S/", signo * (x.baseImponible + x.totalExonerado),
                    signo * (x.total + x.totalExonerado - x.baseImponible), signo * x.total, 
                    "","",signo* x.total,"","","",
                    "AFECTA A " + x.serieReferencia + "-" + x.numeroReferencia);
                }
                else
                {
                    signo = 1;
                    c = cpservice.buscarPorSerieNumero(x.serie, x.numero);
                    if (c.idTipoPago==1)
                    {
                        if (c.idMedioPago==1)
                            formaPago = c.medioPago.nombre;
                        else
                        {
                            pt = ptservice.buscarPorVenta(c.serie + Util.NormalizarCampo(c.numero.ToString(), 8));
                            if (pt == null) pt = ptservice.buscarPorVenta(c.serie +"-"+ Util.NormalizarCampo(c.numero.ToString(), 8));
                            formaPago = c.medioPago.nombre + " " + pt.post + " " + pt.cuentaBanco.cuenta;
                        }
                        tabla.Rows.Add(x.fechaEmision.ToString("yyyy-MM-dd"), x.codTipoComprobante, "", x.serie, x.numero,
                     x.cliente.razonSocial, "S/", signo * (x.baseImponible + x.totalExonerado),
                 signo * (x.total + x.totalExonerado - x.baseImponible), signo * x.total, "",x.fechaEmision.ToString("yyyy-MM-dd"),x.total , "", "", c.tipoPago.nombre, formaPago);//FORMA PAGO
                    }
                    else
                    {
                       // if (!c.pagado)
                       // {
                            formaPago = "PENDIENTE";
                            string saldo = "";
                            movimientos = mcserrvice.listarVenta(c.id, fin);
                            if (movimientos.Count == 0) {
                                saldo =x.total.ToString() ;
                            }
                            tabla.Rows.Add(x.fechaEmision.ToString("yyyy-MM-dd"), x.codTipoComprobante, "", x.serie, x.numero,
                                x.cliente.razonSocial, "S/", signo * (x.baseImponible + x.totalExonerado),
                            signo * (x.total + x.totalExonerado - x.baseImponible), signo * x.total,
                            saldo, "",  "", "", "", c.tipoPago.nombre, formaPago);
                          
                            double pagado = 0;
                            int index = 0;
                            saldo = "";
                      
                            foreach (var J in movimientos)
                            {
                                index++;
                                pagado += J.monto;
                                if (index == movimientos.Count) saldo =( c.montoN - pagado).ToString(); else saldo = "";
                                if (c.idMedioPago!=1)
                                {
                                    pt = ptservice.buscarPorVenta(c.serie + Util.NormalizarCampo(c.numero.ToString(), 8)); formaPago = J.medioPago.nombre + " " + pt.post + " " + pt.cuentaBanco.cuenta;
                                }
                                else formaPago = J.medioPago.nombre;
                                tabla.Rows.Add(x.fechaEmision.ToString("yyyy-MM-dd"), x.codTipoComprobante, "", x.serie, x.numero,
                                      x.cliente.razonSocial, "S/", "",
                                "", "", saldo, J.fecha.ToShortDateString(), J.monto,  "", "","", formaPago);//FORMA PAGO
                            }
                    //    }
               /*         else
                        {
                            movimientos = mcserrvice.listarVenta(c.id, fin);
                            string saldo = "PENDIENTE";
                            if (movimientos.Count == 0)
                            {
                                saldo = x.total.ToString();

                            }
                                formaPago = "";
                            tabla.Rows.Add(x.fechaEmision.ToString("yyyy-MM-dd"), x.codTipoComprobante, "", x.serie, x.numero,
                                x.cliente.razonSocial, "S/", signo * (x.baseImponible + x.totalExonerado),
                            signo * (x.total + x.totalExonerado - x.baseImponible), signo * x.total,
                            "", "","", "", "", c.tipoPago.nombre, formaPago);

                            saldo = "";
                            double pagado = 0;
                            int index = 0;
                            foreach (var J in movimientos)
                            {
                                index++;
                                pagado += J.monto;
                                if (c.idMedioPago!=1)
                                {
                                    pt = ptservice.buscarPorVenta(c.serie + Util.NormalizarCampo(c.numero.ToString(), 8)); formaPago = J.medioPago.nombre + " " + pt.post + " " + pt.cuentaBanco.cuenta;
                                }
                                else formaPago = J.medioPago.nombre;
                                tabla.Rows.Add(x.fechaEmision.ToString("yyyy-MM-dd"), x.codTipoComprobante, "", x.serie, x.numero,
                                      x.cliente.razonSocial, "S/", "",
                                "", "",
                                  "",//SALDO
                                  J.fecha.ToShortDateString(),//FECHA PAGO
                                  J.monto,//MONTO PAGO
                                  "", "", "", formaPago
                                );//FORMA PAGO
                            }
                        }*/
                    }
               
                }
            }
        }
    }
}
