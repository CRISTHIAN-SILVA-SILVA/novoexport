﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONTABILIDAD_FORM
{
    public partial class FormAdminContabilidad : MaterialForm
    {
        public FormAdminContabilidad()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

        }

        private void FormRegistroVentaCompra_Load(object sender, EventArgs e)
        {
            comboMes.SelectedIndex = 0;
            comboAño.SelectedIndex = 0;
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            int diasMes = DateTime.DaysInMonth(Convert.ToInt32(comboAño.Text),comboMes.SelectedIndex+1);
            DateTime inicio = DateTime.Parse("01/"+(comboMes.SelectedIndex+1)+"/"+comboAño.Text);
            DateTime fin = inicio.AddDays(diasMes) ;
            FormRegistroVentas form = new FormRegistroVentas(inicio,fin);
            form.ShowDialog();
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            int diasMes = DateTime.DaysInMonth(Convert.ToInt32(comboAño.Text), comboMes.SelectedIndex + 1);
            DateTime inicio = DateTime.Parse("01/" + (comboMes.SelectedIndex + 1) + "/" + comboAño.Text);
            DateTime fin = inicio.AddDays(diasMes);
            FormRegistroCompras form = new FormRegistroCompras(inicio, fin);
            form.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int diasMes = DateTime.DaysInMonth(Convert.ToInt32(comboAño.Text), comboMes.SelectedIndex + 1);
            DateTime inicio = DateTime.Parse("01/" + (comboMes.SelectedIndex + 1) + "/" + comboAño.Text);
            DateTime fin = inicio.AddDays(diasMes);
            FormPagosVentas form = new FormPagosVentas(inicio,fin);
            form.ShowDialog();
        }
    }
}
