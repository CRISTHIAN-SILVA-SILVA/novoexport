﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONTABILIDAD_FORM
{
    public partial class FormRegistroVentas : MaterialForm
    {
        DataTable tabla = new DataTable();
        RegistroVentaImpl rvservice = new RegistroVentaImpl();
        NotaService nService = new NotaImpl();
        string periodo = "";
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        ComprobantePagoService cpservice = new ComprobantePagoImpl();
        public FormRegistroVentas(DateTime i,DateTime f)
        {

            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            lblPeriodo.Text = i.Month+ " - " +i.Year;
            lblRazonSocial.Text = LocalImpl.getInstancia().razonSocial;
            lblRuc.Text = LocalImpl.getInstancia().ruc;

            periodo = i.Year + Util.NormalizarCampo(i.Month.ToString(), 2) + "00";


            /*      tabla.Columns.Add("FECHA");
                  tabla.Columns.Add("CODIGO");
                  tabla.Columns.Add("TDOC");
                  tabla.Columns.Add("SERIE");
                  tabla.Columns.Add("N°");
                  tabla.Columns.Add("NOMBRE");
                  tabla.Columns.Add("MONEDA");
                  tabla.Columns.Add("SUBTOTAL");
                  tabla.Columns.Add("IGV");
                  tabla.Columns.Add("TOTAL");
                  tabla.Columns.Add("SALDO");
                  tabla.Columns.Add("COBRANZA.FECHA");
                  tabla.Columns.Add("MONTO");
                  tabla.Columns.Add("CUADRO");
                  tabla.Columns.Add("LETRA");
                  tabla.Columns.Add("CUADRO C2");
                  tabla.Columns.Add("FORMA PAGO");

                */
            tabla.Columns.Add("PERIODO");

                   tabla.Columns.Add("N°");
                   tabla.Columns.Add("F. EMISION");
                   tabla.Columns.Add("F. VCTO");
                   tabla.Columns.Add("TIPO");
                   tabla.Columns.Add("SERIE");
                   tabla.Columns.Add("NUMERO");

                 //  tabla.Columns.Add("8");  //debe ir 0 preguntar??

                   tabla.Columns.Add("TIPO DOC");
                   tabla.Columns.Add("N° DOC");
                   tabla.Columns.Add("RAZON SOCIAL O DENOMINACION");
        
               //    tabla.Columns.Add("VALOR EXPORTACION.");
                   tabla.Columns.Add("BASE IM.");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("EXONERADO");
                   tabla.Columns.Add("IMPORTE");


                   tabla.Columns.Add("F. DOC R");
                   tabla.Columns.Add("TIPO R");
                   tabla.Columns.Add("SERIE R");
                   tabla.Columns.Add("NUMERO R");

             //      tabla.Columns.Add("ESTADO");  // DEBE SER 1*/
            grid.DataSource = tabla;
            grid.Columns[0].Width = 70;            grid.Columns[1].Width = 70;
            grid.Columns[2].Width =80;           grid.Columns[3].Width = 70;            grid.Columns[4].Width = 80;
            grid.Columns[5].Width = 70;      grid.Columns[6].Width = 40;
            grid.Columns[7].Width = 80;          grid.Columns[8].Width = 100; grid.Columns[9].Width = 400;
            grid.Columns[10].Width = 80;         grid.Columns[11].Width = 80;
            grid.Columns[12].Width = 80;     grid.Columns[13].Width = 80;      grid.Columns[14].Width = 80; grid.Columns[15].Width = 80; grid.Columns[16].Width = 80;
            /*int index = 0;
            string nr = "-";
            string estado = "1";
            string forma_pago = "";
            string fechaPago="";
            String montoDeuda = "";
            string montoPagado = "";
            int signo = 1;
            ComprobantePago c;
            foreach (var x in rvservice.listarPorFechas(i, f)) {
                if (x.codTipoComprobante.Equals(CodigosSUNAT.FACTURA) || x.codTipoComprobante.Equals(CodigosSUNAT.BOLETA))
                {
                    c = cpservice.listarPorSerieNumero(x.serie, x.numero);
                    if (c.pagado)
                    {
                        forma_pago = c.medioPago.nombre;
                        fechaPago = c.fechaPago.ToString("dd/MM/yyyy"); montoDeuda = "0.00";montoPagado= string.Format("{0:0.00}", c.montoTotal);

                    }
                    else { forma_pago = "PENDIENTE"; fechaPago = ""; montoDeuda = string.Format("{0:0.00}", c.montoTotal);montoPagado = "0.00"; }
                }
                else { forma_pago = "";fechaPago = ""; montoDeuda = ""; montoPagado = ""; signo = -1; }
                               
                if (x.anulado)
                {
                    forma_pago = "ANULADO CON NOTA: ";
                    List<Nota> n = nService.listarPorComprobante(x.id);
                    foreach(var nc in n)if(nc.tipoNota.Equals("ANULACION_EN_LA_OPERACION"))   forma_pago = forma_pago+nc.serie+"-"+Util.NormalizarCampo(x.numero.ToString(),8);
                   }
                tabla.Rows.Add(
                    x.fechaEmision.ToString("dd/MM/yyyy"),
                    x.codTipoComprobante,
                    "",
                    x.serie,
                    Util.NormalizarCampo(x.numero.ToString(),8),
                    x.cliente.razonSocial,
                    "S/.",
                  String.Format("{0:0.00}",  x.baseImponible*signo),
                 String.Format("{0:0.00}", x.baseImponible*igv.valorDouble*signo),
                     String.Format("{0:0.00}", x.total*signo),
                       String.Format("{0:0.00}",montoDeuda),
                     fechaPago,
                         String.Format("{0:0.00}",montoPagado),
                         "","","",
                         forma_pago



                    );
            }
            */
            int index = 0;
            string nr = "";
            int signo = 1;
            foreach (var x in rvservice.listarPorFechas(i, f)) {
                if (x.numeroReferencia == 0) { nr = ""; signo = 1; } else { nr = x.numeroReferencia.ToString(); signo = -1; }
        
                    index++;
                    tabla.Rows.Add(periodo,                             //01
                        Util.NormalizarCampo(index.ToString(),4),       //02
                     //   "M1",                                           //03
                        x.fechaEmision.ToString("dd/MM/yyyy"),          //04
                        x.fechaVencimiento                              //05
                        , x.codTipoComprobante                            //06
                        ,x.serie,                                       //07
                        Util.NormalizarCampo(x.numero.ToString(),8),    //08
               //         "",                                             //09 TICKETS
                        x.codTipoDocCliente,                            //10
                        x.docCliente,                                   //11
                        x.cliente.razonSocial,                          //12
                   //     "",                                             //13 VALOR EXPORTACION
                      string.Format("{0:0.00}", x.baseImponible*signo),       //14  BASE IMPONIBLE
                    //  "",                                               //15 DESCUENTO BASE IMPL
                      string.Format("{0:0.00}", x.baseImponible*igv.valorDouble*signo),   //16 IGV
                  //    "",                                                           //17 DESCUENTO IGV
                          string.Format("{0:0.00}", x.totalExonerado*signo),              //18 IMPORTE EXONERADO
                    //      "",                                                       //19 IMPORTE INEFACTAS
                    //      "",                                                       //20 ISC
                    //      "",                                                       //21 BASE IMPL ARROZ
                     //     "",                                                       //22 IMPUESTO VENTAS ARROZ PILADO
                     //     "",                                                       //23 OTROS TRIBUTOS QUE NO FORMAS PARTE DE BASE IMPL
                      string.Format("{0:0.00}", signo*(x.baseImponible+ x.baseImponible * igv.valorDouble + x.totalExonerado)),  //24 IMPORTE TOTAL 
                     // "",                                                           //25 CODIGO TIPO MONEDA      
                    //  "",                                                           //26 CAMBIO
                      x.fechaReferencia                                             //27
                      ,x.tipoR                                                      //28
                      ,x.serieReferencia                                            //29
                      ,nr                                                          //30
                   //   "",                                                           //31             
                   //   "",                                                           //32 ERROR TIPO 1 TIPO CAMBIO
                   //   "1"                                                          //33 1 SI FUE CANCELADO POR MEDIO PAGO TABLA 1
                                                                               //34 TIPO DE ESTADO ->PREGUNTAR?
                      
                      );
                
             /*   index++;
                tabla.Rows.Add( x.fechaEmision.ToString("dd/MM/yyyy"), x.fechaVencimiento
                    , x.codTipoComprobante,""   , x.serie, Util.NormalizarCampo(x.numero.ToString(), 8), x.codTipoDocCliente, x.cliente.razonSocial, x.docCliente, 
                    "S/.", string.Format("{0:0.00}", x.baseImponible + x.totalExonerado), string.Format("{0:0.00}", x.baseImponible*igv.valorDouble),
                    string.Format("{0:0.00}", x.total), string.Format("{0:0.00}", )
                    x.fechaReferencia, x.tipoR, x.serieReferencia, nr);*/

               }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void FormRegistroVentas_Load(object sender, EventArgs e)
        {

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void btnNuevoIngreso_Click(object sender, EventArgs e)
        {
            string filename = "LE" + LocalImpl.getInstancia().ruc + periodo + "00" + "140100" + "1" + "1" + "1" + "1" + ".TXT";
            Exportar.exportarATXTAsync(grid,filename);
        }
    }
}
