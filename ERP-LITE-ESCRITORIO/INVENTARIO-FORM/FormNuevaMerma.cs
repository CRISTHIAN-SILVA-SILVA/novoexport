﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormNuevaMerma : MaterialForm
    {
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        AlmacenService aService = new AlmacenImpl();
        MermaService mservice = new MermaImpl();
        DataTable tabla = new DataTable();
        DataTable tablaP = new DataTable();
        double total = 0;
        double subTotalAnt = 0;
        TransferenciaProductoService tpService = new TransferenciaProductoImpl();
        double cantidadAnterior = 0;


        public delegate void pasar(Merma merma);
        public event pasar pasado;
        public FormNuevaMerma()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            comboOrigen.DataSource = aService.listarNoAnulados();

        
            tablaP.Columns.Add("IDPRESENTACION");
            tablaP.Columns.Add("IDPRODUCTO");
            tablaP.Columns.Add("PRODUCTO");
            tablaP.Columns.Add("PRECIO");
            tablaP.Columns.Add("FACTOR");
            gridProducto.DataSource = tablaP;
            gridProducto.Columns[0].Visible = false;
            gridProducto.Columns[1].Visible = false;
            gridProducto.Columns[2].Width = 750;
            gridProducto.Columns[3].Width = 200;
            gridProducto.Columns[4].Visible = false;

            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("CANTIDAD");
            
            tabla.Columns.Add("FACTOR");
            tabla.Columns.Add("IDALMACEN");
            tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Width = 550;
            gridDetalle.Columns[3].Width = 200;
            gridDetalle.Columns[5].Width = 200;
            gridDetalle.Columns[4].Visible = false;
        }

        private void FormNuevaMerma_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtMotivo.Focus();
        }

        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e)
        {
            tablaP.Clear();
            if (Util.validaCadena(txtBuscarProducto.Text))
                if (comboOrigen.SelectedValue != null)
                   ppService.listarPorNombre( txtBuscarProducto.Text.ToUpper()).ForEach(x=>tablaP.Rows.Add(x.id,x.idProducto,x.nombre,x.precio,x.factor));
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;
               if (Util.validaCadena(txtMotivo.Text)) {
                    if (gridDetalle.Rows.Count > 0) {
                    if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {

                        Merma merma = new Merma
                        {
                            usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                            motivo = txtMotivo.Text,
                        };
                        List<DetalleMerma> detalles = new List<DetalleMerma>();
                        DetalleMerma detalle;
                        for (int i = 0; i < gridDetalle.RowCount; i++)
                        {
                            detalle = new DetalleMerma
                            {
                                cantidad = Convert.ToDouble(gridDetalle.Rows[0].Cells["CANTIDAD"].Value.ToString()),
                                factor = Convert.ToDouble(gridDetalle.Rows[0].Cells["FACTOR"].Value.ToString()),
                                idPresentacion = Convert.ToInt32(gridDetalle.Rows[0].Cells["IDPRESENTACION"].Value.ToString()),
                                idAlmacen = Convert.ToInt32(gridDetalle.Rows[0].Cells["IDALMACEN"].Value.ToString()),
                                idProducto = Convert.ToInt32(gridDetalle.Rows[0].Cells["IDPRODUCTO"].Value.ToString()),
                            };
                            detalles.Add(detalle);
                        }
                        merma.detalles = detalles;
                        if (mservice.crear(merma)) { pasado(merma); this.Close(); } else { MessageBox.Show("ERROR INESPERADO PUDE QUE SE AYA EXCEDIDO EL STOCK, O INTENTALO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); }
                    }
                    }
                    else MessageBox.Show("DETALLE VACIO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else MessageBox.Show("FALTA MOTIVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnGuardar.Enabled = true;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (comboOrigen.SelectedValue != null) {
                if (gridProducto.SelectedRows.Count == 1)
                {
                    if (tabla.Rows.Find(gridProducto.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString()) == null)
                        tabla.Rows.Add(gridProducto.SelectedRows[0].Cells["IDPRODUCTO"].Value.ToString(),
                        gridProducto.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString(),
                        gridProducto.SelectedRows[0].Cells["PRODUCTO"].Value.ToString(),
                        "0",
                        gridProducto.SelectedRows[0].Cells["FACTOR"].Value.ToString(),
                      (int)  comboOrigen.SelectedValue
                        );
                }
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
                tabla.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
        }


        private void gridDetalle_CellEndEdit_1(object sender, DataGridViewCellEventArgs e)
        {
            if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString()))
            {
                double cantidad = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
                if (cantidad <= 0) { MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value = cantidadAnterior; }
            }
            else { MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value = cantidadAnterior; }

        }

        private void gridDetalle_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].ReadOnly = false;
            gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Selected = true;
            gridDetalle.BeginEdit(true);
        }

        private void gridDetalle_CellBeginEdit_1(object sender, DataGridViewCellCancelEventArgs e)
        {
            cantidadAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
        }
    }
}
