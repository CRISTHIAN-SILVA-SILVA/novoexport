﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormNuevaPresentacion : MaterialForm
    {
        Producto producto = null;
        ProductoPresentacion presentacion = null;
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        public delegate void pasar(int filaModifica, int id, string unidad, double factor, double precio, string modificado);
        public event pasar pasado;
        int filaMoodifica = 0;
        public FormNuevaPresentacion(Producto p, ProductoPresentacion pp, int fila)
        {
            InitializeComponent();
            combo.DataSource = UnidadMedidaImpl.getInstancia();
            filaMoodifica = fila;
            producto = p;
            presentacion = pp;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            if (p != null)
            {
                txtProducto.Text = p.nombre;
            }
            if (pp != null)
            {
                combo.Enabled = false;
                txtFactor.Enabled = false;
                txtFactor.Text = pp.factor.ToString();
                txtPrecio.Text = pp.precio.ToString();
            }
        }

        private void FormNuevaPresentacion_Load(object sender, EventArgs e)
        {
            combo.DataSource = UnidadMedidaImpl.getInstancia();

            AutoCompleteStringCollection coleccionUnidades = new AutoCompleteStringCollection();
            foreach (UnidadMedida c in UnidadMedidaImpl.getInstancia())
                coleccionUnidades.Add(c.nombre);
            combo.AutoCompleteCustomSource = coleccionUnidades;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            if (presentacion == null)
            {
                if (combo.SelectedValue != null)
                    if (Util.esDouble(txtFactor.Text))
                        if (Convert.ToDouble(txtFactor.Text) > 1)
                            if (Util.esDouble(txtPrecio.Text))
                            {
                                if (MessageBox.Show("SEGURO DE AGREGAR PRESENTACION?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    ProductoPresentacion pp = new ProductoPresentacion
                                    {
                                        idUnidadMedida = (int)combo.SelectedValue,
                                        nombre = producto.nombre + " " + combo.Text + " X " + Convert.ToDouble(txtFactor.Text) ,
                                        usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                                        fechaCreate = DateTime.Now,
                                        factor = Convert.ToDouble(txtFactor.Text),
                                        precio = Convert.ToDouble(txtPrecio.Text),
                                        idProducto = producto.id,
                                    };
                                    ProductoPresentacion existe = ppservice.buscarPorNombre(pp.nombre);
                                    if (existe == null)
                                        if (ppservice.crear(pp)) { pasado(-1, pp.id, combo.Text, pp.factor, pp.precio, ""); this.Close(); }
                                        else MessageBox.Show("ERROR INESPERADO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    else if (existe.anulado)
                                    {
                                        existe.anulado = true;
                                        ppservice.editar(existe);
                                    }
                                    else MessageBox.Show("ESTA PRESENTACION YA EXISTE", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            else MessageBox.Show("PRECIO NO VALIDO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else MessageBox.Show("FACTOR TIENE QUE SER MAYOR A 1", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else MessageBox.Show("FACTOR NO VALIDO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else MessageBox.Show("NO EXISTE UNIDAD DE MEDIDA", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (Util.esDouble(txtPrecio.Text))
                {
                    if (MessageBox.Show("SEGURO DE MODIFICAR PRESENTACION?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        presentacion.precio = Convert.ToDouble(txtPrecio.Text);
                        presentacion.fechaUpdate = DateTime.Now;
                        presentacion.usuarioUpdate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                        if (ppservice.editar(presentacion))
                        {
                            pasado(filaMoodifica, 0, "", presentacion.factor, presentacion.precio, presentacion.usuarioUpdate);
                            this.Close();
                        }
                        else MessageBox.Show("ERROR INESPERADO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                }
            }
            btnAgregar.Enabled = true;
        }

        private void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo.Text.Trim().Equals("DOCENA")) txtFactor.Text = "12";
         else   if (combo.Text.Trim().Equals("GRUESA")) txtFactor.Text = "144";
            else if (combo.Text.Trim().Equals("CIENTO DE UNIDADES")) txtFactor.Text = "100";
            else if (combo.Text.Trim().Equals("MILLARES")) txtFactor.Text = "1000";
        }
    }
}
