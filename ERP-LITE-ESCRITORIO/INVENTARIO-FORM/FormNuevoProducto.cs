﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormNuevoProducto : MaterialForm
    {
        DataTable tabla = new DataTable();
        MarcaService mservice = new MarcaImpl();
        Producto p = null;
        CategoriaService cservice = new CategoriaImpl();
        ProductoService pservice = new ProductoImpl();
        ProductoPresentacionService PPSERVICE = new ProductoPresentacionImpl();
        public delegate void pasar(int filaModifica, int id, string nombre, double stock, double precio, string unidad, string categoria, string modificado);
        public event pasar pasado;
        int filaModifica = 0;
        string marca = "";
        public FormNuevoProducto(int fila, Producto p)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
            tabla.Columns.Add("ID");
            tabla.Columns.Add("UNIDAD DE MEDIDA");
            tabla.Columns.Add("FACTOR");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("MODIFICADO POR");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 320;
            grid.Columns[2].Width = 100;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 500;
            filaModifica = fila;
     
            this.p = p;
        }
        public void inabilitar()
        {
            txtNombre.Enabled = false;
            txtPrecio.Enabled = false;
            txtCosto.Enabled = false;
            comboCategoria.Enabled = false;
            comboMarca.Enabled = false;
            comboUnidad.Enabled = false;
            btnAgregar.Visible = false;
            
        }

        private void FormNuevoProducto_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtNombre.Focus();
            comboUnidad.DataSource = UnidadMedidaImpl.getInstancia();
            comboUnidad.SelectedValue = 58;

            AutoCompleteStringCollection coleccionUnidades = new AutoCompleteStringCollection();
            foreach (UnidadMedida c in UnidadMedidaImpl.getInstancia())
                coleccionUnidades.Add(c.nombre);
            comboUnidad.AutoCompleteCustomSource = coleccionUnidades;

            List<Marca> marcas = mservice.listarNoAnulados();
            comboMarca.DataSource = marcas;
            AutoCompleteStringCollection coleccionMarcas = new AutoCompleteStringCollection();
            foreach (Marca c in marcas)
                coleccionMarcas.Add(c.nombre);
            comboMarca.AutoCompleteCustomSource = coleccionMarcas;

            List<Categoria> categorias = cservice.listarNoAnulados();
            comboCategoria.DataSource = categorias;
            AutoCompleteStringCollection coleccionCategorias = new AutoCompleteStringCollection();
            foreach (Categoria c in categorias)
                coleccionCategorias.Add(c.nombre);
            comboCategoria.AutoCompleteCustomSource = coleccionCategorias;

            if (p != null)
            {
                checkExonerado.Enabled = false;
                checkExonerado.Checked = p.esExonerado;
                txtNombre.Text = p.nombre;
                txtPrecio.Text = String.Format("{0:0.00}", p.precio);
                txtCosto.Text = String.Format("{0:0.00}", p.costo);
                comboUnidad.Text = p.unidad.nombre;
                comboMarca.Text = p.marca.nombre;
                comboCategoria.Text = p.categoria.nombre;
                inabilitar();
                PPSERVICE.listarPorProductoNoAnuladas(p.id).ForEach(pp => tabla.Rows.Add(pp.id, pp.unidad.nombre, String.Format("{0:0.00}", pp.factor), String.Format("{0:0.00}", pp.precio), pp.usuarioUpdate));
            }
        }
        void agregar(Producto pr) {
            tabla.Rows.Add(pr.presentaciones[0].id, comboUnidad.Text, String.Format("{0:0.00}", 1), String.Format("{0:0.00}", pr.presentaciones[0].precio), "");
            pasado(-1, pr.id, pr.nombre, pr.stock, pr.precio, comboUnidad.Text, comboCategoria.Text, "");
            inabilitar();
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            if (Util.validaCadena(txtNombre.Text))
                if (comboCategoria.SelectedValue != null)
                    if (comboMarca.SelectedValue != null)
                        if (comboUnidad.SelectedValue != null)
                            if (Util.esDouble(txtCosto.Text))
                                if (Util.esDouble(txtPrecio.Text))
                                {
                                    p = new Producto
                                    {
                                        nombre = txtNombre.Text.Trim().ToUpper()+" "+comboMarca.Text,
                                        idCategoria = (int)comboCategoria.SelectedValue,
                                        idMarca = (int)comboMarca.SelectedValue,
                                        idUnidadMedida = (int)comboUnidad.SelectedValue,
                                        costo = Convert.ToDouble(txtCosto.Text),
                                        precio = Convert.ToDouble(txtPrecio.Text),
                                        usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                                        fechaCreate = DateTime.Now,
                                         esExonerado=checkExonerado.Checked,
                                    };
                                    ProductoPresentacion pp = new ProductoPresentacion
                                    {
                                        factor = 1,
                                        idUnidadMedida = p.idUnidadMedida,
                                        fechaCreate = p.fechaCreate,
                                        usuarioCreate = p.usuarioCreate,
                                        precio = p.precio,
                                        nombre = p.nombre + " " + comboUnidad.Text 
                                    };
                                    List<ProductoPresentacion> ps = new List<ProductoPresentacion>(); ps.Add(pp);
                                    p.presentaciones = ps;

                                    Producto existe = pservice.buscarPorNombre(p.nombre);
                                    if (existe == null) {
                                        FormCantidadAlmacen form = new FormCantidadAlmacen(p);
                                        form.pasado +=new FormCantidadAlmacen.pasar(agregar);
                                        form.ShowDialog();
                                        checkExonerado.Enabled = false;
                                        checkExonerado.Checked = p.esExonerado;
                                    }
                                    else if (existe.anulado)
                                    {
                                        if (MessageBox.Show("EL PRODUCTO HA SIDO ELIMINADO; DESEA RESTAURARlO?; LA RESTAURACION SE REALIZARA CON STOCK CERO", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                        {
                                            if (Util.esDouble(txtPrecio.Text))
                                            {
                                                existe.usuarioUpdate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                                                existe.fechaUpdate = DateTime.Now;
                                                existe.costo = Convert.ToDouble(txtCosto.Text);
                                                existe.precio = Convert.ToDouble(txtPrecio.Text);
                                                txtCosto.Text = existe.costo.ToString();
                                                txtPrecio.Text = existe.precio.ToString();
                                                if (pservice.restaurar(existe))
                                                {
                                                    PPSERVICE.listarPorProductoNoAnuladas(existe.id).ForEach(pr => tabla.Rows.Add(pr.id, pr.unidad.nombre, String.Format("{0:0.00}", pr.factor), String.Format("{0:0.00}", pr.precio), pr.usuarioUpdate));
                                                    pasado(-1, existe.id, existe.nombre, 0, existe.precio, existe.unidad.nombre, existe.categoria.nombre, existe.usuarioUpdate);
                                                    inabilitar();
                                                    btnNuevo.Focus();
                                                }
                                                else MessageBox.Show("ERROR INESPERADO INTENTALO DE NUEVO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            }
                                            else MessageBox.Show("FORMATO PRECIO INCORRECTO");
                                        }
                                        }
                                    else MessageBox.Show("PRODUCTO YA EXISTE", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                else MessageBox.Show("PRECIO INCORRECTO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            else MessageBox.Show("COSTO INCORRECTO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else MessageBox.Show("FALTA UNIDAD DE MEDIDA", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else MessageBox.Show("FALTA MARCA", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else MessageBox.Show("FALTA CATEGORIA", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else MessageBox.Show("FALTA NOMBRE", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            btnAgregar.Enabled = true;
        }
        void agregar(int fila, int id, string unidad, double factor, double precio, string modificado)
        {
            if (fila == -1)
                tabla.Rows.Add(id, unidad, string.Format("{0:0.00}", factor), string.Format("{0:0.00}", precio), modificado);
            else
            {
                grid.Rows[fila].Cells["PRECIO"].Value = string.Format("{0:0.00}", precio);
                grid.Rows[fila].Cells["MODIFICADO POR"].Value = modificado;
                if (factor == 1)
                {
                    txtPrecio.Text = string.Format("{0:0.00}", precio);
                    pasado(filaModifica, 0, "", 0, precio, "", "", modificado);
                }
            }
          
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            if (p != null)
            {
              p.marca=  mservice.buscar(p.idMarca);
                FormNuevaPresentacion form = new FormNuevaPresentacion(p, null, -1);
                form.pasado += new FormNuevaPresentacion.pasar(agregar);
                form.ShowDialog();
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ELIMINAR PRESENTACION?", "PREGUNTA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (PPSERVICE.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString())))
                        tabla.Rows.RemoveAt(grid.SelectedRows[0].Index);
                    else MessageBox.Show("ERROR INESPERADO VUELVE A INTENTALO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            btnEliminar.Enabled = true;
        }

        private void grid_DoubleClick(object sender, EventArgs e)
        {

        
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (p != null)
            {
                ProductoPresentacion pre = PPSERVICE.buscar(Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
                if (pre != null)
                {
                    FormNuevaPresentacion form = new FormNuevaPresentacion(p, pre, e.RowIndex);
                    form.pasado += new FormNuevaPresentacion.pasar(agregar);
                    form.ShowDialog();
                }
            }
        }
    }
}
