﻿using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.INVENTARIO_FORM
{
    public partial class FormCostosProducto : Form
    {
        DataTable tabla = new DataTable();
        int producto;
        DateTime i, f;
        DetalleCompraImpl dmservice = new DetalleCompraImpl();
        Parametro igv;
        double igvC = 0;
        public FormCostosProducto(int id,DateTime inicio,DateTime fin)
        {
            igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
            if (igv == null) this.Close();
            igvC = 1 + igv.valorDouble;
            InitializeComponent();  
            label1.Text=label1.Text+"   "+inicio.ToShortDateString()+" - "+fin.ToShortDateString() ;
            i = inicio;
            f = fin;
            producto = id;
            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDMOVIMIENTO");
            tabla.Columns.Add("IDCOMPRA");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("PROOVEDOR");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("COSTO");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("CAMBIO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Visible = false;
            grid.Columns[2].Visible = false;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 400;
            grid.Columns[5].Width = 100;
            grid.Columns[6].Width = 80;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 80;
            grid.Columns[9].Width = 80;
            grid.Columns[10].Width = 80;
        }

        private void FormCostosProducto_Load(object sender, EventArgs e)
        {
            foreach (var x in dmservice.listarPorProdcutoFecha(producto,i,f)) {
                tabla.Rows.Add(x.id,x.idMovimiento,x.idCompra,x.fecha.ToString("yyyy-MM-dd"),x.proveedor,x.serie,x.numero,x.cantidad,x.costo,x.total,string.Format("{0:0.000}", x.totalMov/(x.total/igvC)));
            }
        }
    }
}
