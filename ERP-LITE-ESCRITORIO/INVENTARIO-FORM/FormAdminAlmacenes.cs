﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormAdminAlmacenes : Form
    {
        DataTable tabla = new DataTable();
        AlmacenService aservice = new AlmacenImpl();
        public FormAdminAlmacenes()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("DIRECCION");
            tabla.Columns.Add("TELEFONO");
            tabla.Columns.Add("ENCARGADO");
            tabla.Columns.Add("PRINCIPAL");
            tabla.Columns.Add("ANULADO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 300;
            grid.Columns[2].Width = 350;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 220;
            grid.Columns[5].Width =100;
            grid.Columns[6].Visible = false;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevoAlmacen form = new FormNuevoAlmacen(null, 0);
            form.pasado += new FormNuevoAlmacen.pasar(agregar);
            form.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("CONFIRMAR ELIMINACION", "¿SEGURO DE ELIMINAR ALMACEN?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    aservice.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
        }
        void agregar(int filaModifica, Almacen a)
        {
            /*  if (filaModifica == -1)
                  tabla.Rows.Add(a.id, a.nombre, a.direccion, a.telefono, a.encargado, a.esPrincipal, a.anulado);
              else
              {
                  grid.Rows[filaModifica].Cells["NOMBRE"].Value = a.nombre;
                  grid.Rows[filaModifica].Cells["TELEFONO"].Value = a.telefono;
                  grid.Rows[filaModifica].Cells["ENCARGADO"].Value = a.encargado;
                  grid.Rows[filaModifica].Cells["ES PRINCIPAL"].Value = a.esPrincipal;
                  grid.Rows[filaModifica].Cells["ANULADO"].Value = a.anulado;
              }*/
            cargar();

        }
        private void btnExportar_Click(object sender, EventArgs e)
        {
            btnExportar.Enabled = false;
            Exportar.exportarAExcelAsync(grid);
            btnExportar.Enabled = true;
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Almacen modifica = aservice.buscar(Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            if (modifica != null)
            {
                FormNuevoAlmacen form = new FormNuevoAlmacen(modifica, e.RowIndex);
                form.pasado += new FormNuevoAlmacen.pasar(agregar);
                form.ShowDialog();
            }
        }
        void cargar() {
            tabla.Clear();
            foreach (var i in aservice.listarNoAnulados())
                tabla.Rows.Add(i.id, i.nombre, i.direccion, i.telefono, i.encargado, i.esPrincipal, i.anulado);
        }
        private void FormAdminAlmacenes_Load(object sender, EventArgs e)
        {
            cargar();
        }
    }
}
