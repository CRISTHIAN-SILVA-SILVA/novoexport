﻿using ERP_LITE_DESKTOP.PEDIDOS_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormNuevaTransferencia : MaterialForm
    {
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        AlmacenService aService = new AlmacenImpl();
        DataTable tabla = new DataTable();
        double total = 0;
        double subTotalAnt = 0;
        TransferenciaProductoService tpService = new TransferenciaProductoImpl();

        public delegate void pasar(TransferenciaProducto tp);
        public event pasar pasado;
        public FormNuevaTransferencia()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            List<Almacen> almacenesOrigen = aService.listarNoAnulados();
            List<Almacen> almacenesDestino = aService.listarNoAnulados();
            comboOrigen.DataSource = almacenesOrigen;
            comboDestino.DataSource = almacenesDestino;
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");           
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("FACTOR");
            tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };        
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Width= 550;
            gridDetalle.Columns[3].Width = 100;
            gridDetalle.Columns[4].Width = 100;
            gridDetalle.Columns[5].Width = 100;
            gridDetalle.Columns[6].Width = 100;
            gridDetalle.Columns[7].Visible =false;
        }
        private void FormNuevaTransferencia_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtBuscarProducto.Focus();
        }
        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e)
        {
            if (Util.validaCadena(txtBuscarProducto.Text))
                if (comboOrigen.SelectedValue != null)
                { gridProducto.DataSource = ppService.buscarPresentacionesPorAlmacen((int)comboOrigen.SelectedValue, txtBuscarProducto.Text.ToUpper());
                    gridProducto.Columns[0].Visible = false;
                    gridProducto.Columns[1].Visible = false;
                    gridProducto.Columns[2].Width = 700;
                    gridProducto.Columns[3].Width   = 150;
                    gridProducto.Columns[4].Width = 100;
                    gridProducto.Columns[5].Visible = false;
                    gridProducto.Columns[6].Visible = false;
                }          
        }
        void agregar(double precio,double cantidad,int fila,bool modifica) {
            if (modifica) {
                double subtotal = precio * cantidad;
       
                gridDetalle.Rows[fila].Cells["CANTIDAD"].Value = cantidad;
                gridDetalle.Rows[fila].Cells["TOTAL"].Value = subtotal;
                total = total + (subtotal-subTotalAnt);
                lblTotal.Text = string.Format("{0:0.00}", total);
            }
            else
                if (cantidad <= Convert.ToDouble(gridProducto.Rows[fila].Cells["CANTIDAD_ALMACEN"].Value.ToString()))
                {
                    double subtotal = cantidad * precio;
                    tabla.Rows.Add(gridProducto.Rows[fila].Cells["IDPRODUCTO"].Value.ToString(),
                                   gridProducto.Rows[fila].Cells["IDPRESENTACION"].Value.ToString(),
                                   gridProducto.Rows[fila].Cells["PRESENTACION"].Value.ToString(),
                                   precio,
                                   cantidad,
                                   subtotal* 0.18,
                                   subtotal, gridProducto.Rows[fila].Cells["FACTOR"].Value.ToString());
                    total = total + subtotal;
                lblTotal.Text = string.Format("{0:0.00}", total); 
                }
                else MessageBox.Show("CANTIDAD INSUFICIENTE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (tabla.Rows.Find(gridProducto.Rows[e.RowIndex].Cells[1].Value.ToString()) == null)
            {
                double cantidad = Convert.ToDouble(gridProducto.Rows[e.RowIndex].Cells["CANTIDAD_ALMACEN"].Value.ToString());
                double precio = Convert.ToDouble(gridProducto.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
                FormCantidadProducto form = new FormCantidadProducto(gridProducto.Rows[e.RowIndex].Cells["PRESENTACION"].Value.ToString(),precio,e.RowIndex,false);
                form.pasado += new FormCantidadProducto.pasar(agregar);
                form.ShowDialog();
         
            }
        }
        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                double subtotal = Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString());
                tabla.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
                total = total - subtotal;
                lblTotal.Text = string.Format("{0:0.00}", total);
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;
            if(!comboOrigen.SelectedValue.Equals(comboDestino.SelectedValue))
            if (gridDetalle.RowCount > 0)
            {
                if (MessageBox.Show("SEGURO DE GUARDAR TRANSFERENCIA?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    TransferenciaProducto tp = new TransferenciaProducto();
                    tp.conductor = txtConductor.Text;
                    tp.placaVehiculo = txtPlaca.Text;

                    tp.usuarioCreate = FormLogin.user.nombres + "-" + FormLogin.user.dni;
                    List<DetalleTransferenciaProducto> listaDetalles = new List<DetalleTransferenciaProducto>();
                    List<TransferenciaAlmacen> listaAlmacenes = new List<TransferenciaAlmacen>();

                    TransferenciaAlmacen taO = new TransferenciaAlmacen();
                    taO.esOrigen = true;
                    taO.idAlmacen = (int)comboOrigen.SelectedValue;
                    TransferenciaAlmacen taD = new TransferenciaAlmacen();
                    taD.esDestino = true;
                    taD.idAlmacen = (int)comboDestino.SelectedValue;
                    listaAlmacenes.Add(taD);
                    listaAlmacenes.Add(taO);
                    DetalleTransferenciaProducto dtp;
                    for (int i = 0; i < gridDetalle.Rows.Count; i++)
                    {
                        dtp = new DetalleTransferenciaProducto();
                        dtp.cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString());
                        dtp.idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRODUCTO"].Value.ToString());
                        dtp.precio = Convert.ToDouble(gridDetalle.Rows[i].Cells["PRECIO"].Value.ToString());
                        dtp.total = Convert.ToDouble(gridDetalle.Rows[i].Cells["TOTAL"].Value.ToString());
                        dtp.factor = Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString());
                        dtp.idPresentacion = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString());
                        listaDetalles.Add(dtp);
                    }
                    tp.total = Convert.ToDouble(lblTotal.Text);
                    tp.detalles = listaDetalles;
                    tp.almacenesTransferencia = listaAlmacenes;

                    if (tpService.crear(tp)) { pasado(tp); this.Close(); } else { MessageBox.Show("ERROR INESPERADO INTENTALO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); }
                }
            }
            else MessageBox.Show("EL DETALLE ESTA VACIO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else MessageBox.Show("ORIGEN Y DESTINO INVALIDOS", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnGuardar.Enabled = true;
        }

        private void gridDetalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {          
                double cantidad = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
                double precio = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
            subTotalAnt = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["TOTAL"].Value.ToString());
            FormCantidadProducto form = new FormCantidadProducto(gridDetalle.Rows[e.RowIndex].Cells["PRODUCTO"].Value.ToString(), precio, e.RowIndex,true);
                form.pasado += new FormCantidadProducto.pasar(agregar);
                form.ShowDialog();
      
        }

        private void gridDetalle_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

        }

        private void gridDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboOrigen_SelectedValueChanged(object sender, EventArgs e)
        {
            tabla.Clear();
            gridProducto.DataSource = null;
        }
    }
}
