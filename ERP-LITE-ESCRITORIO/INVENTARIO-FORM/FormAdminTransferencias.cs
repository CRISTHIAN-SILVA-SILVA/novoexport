﻿using ERP_LITE_ESCRITORIO.INVENTARIO_FORM;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormAdminTransferencias : Form
    {
        DataTable tabla = new DataTable();
        TransferenciaProductoService tpService = new TransferenciaProductoImpl();
        public FormAdminTransferencias()
        {
            InitializeComponent();
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("USUARIO");
            tabla.Columns.Add("TOTAL($/)");           
            tabla.Columns.Add("CONDUCTOR");
            tabla.Columns.Add("VEHICULO");
            grid.DataSource = tabla;
            grid.Columns[0].Width = 100;
            grid.Columns[1].Width = 80;
            grid.Columns[2].Width = 320;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 300;
            grid.Columns[5].Width = 200;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaTransferencia form = new FormNuevaTransferencia();
            form.pasado += new FormNuevaTransferencia.pasar(agregar);
            form.ShowDialog();
        }

        private void FormAdminTransferencias_Load(object sender, EventArgs e)
        {
            tpService.listarNoAnulados().ForEach(x=>tabla.Rows.Add(x.id,x.fechaCreate.ToShortDateString(),x.usuarioCreate,String.Format("{0:0.00}", x.total),x.conductor,x.placaVehiculo));
        }
        void agregar(TransferenciaProducto x) {
            tabla.Rows.Add(x.id, x.fechaCreate.ToShortDateString() , x.usuarioCreate, String.Format("{0:0.00}", x.total), x.conductor, x.placaVehiculo);
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            { TransferenciaProducto tp = tpService.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["NUMERO"].Value.ToString()));
                FormDetalleTransferencia form = new FormDetalleTransferencia(tp);
                form.ShowDialog();
            }
        }
    }
}
