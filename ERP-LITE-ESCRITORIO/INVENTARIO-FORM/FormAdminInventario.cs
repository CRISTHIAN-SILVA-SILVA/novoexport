﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormAdminInventario : MaterialForm
    {
        public FormAdminInventario()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }
        void abrirForm(Object formHijo)
        {
            if (this.contenedor.Controls.Count > 0)
                this.contenedor.Controls.RemoveAt(0);
            Form hijo = formHijo as Form;
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            this.contenedor.Controls.Add(hijo);
            this.contenedor.Tag = hijo;
            hijo.Show();
        }
        private void FormAdminInventario_Load(object sender, EventArgs e)
        {
            abrirForm(new FormAdminProductos());
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminProductos());
        }

        private void btnMarcas_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminMarcas());
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminCategorias());
        }

        private void btnAlmacenes_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminAlmacenes());
        }

        private void btnTransferencia_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminTransferencias());
        }

        private void btnMermas_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminMermas());
        }
    }
}
