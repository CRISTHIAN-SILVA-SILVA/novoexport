﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormAdminMarcas : Form
    {
        string nombreAnterior = "";
        DataTable tabla = new DataTable();
        MarcaService mservice = new MarcaImpl();
        public FormAdminMarcas()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NOMBRE");
            grid.DataSource = tabla;
            grid.Columns[0].Width = 225;
            grid.Columns[1].Width = 857;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Util.validaCadena(txtNombre.Text))
            {
                if (MessageBox.Show("SEGURO DE AGREGAR MARCA?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (mservice.buscarPorNombre(txtNombre.Text) == null)
                    {
                        Marca marca = new Marca
                        {
                            nombre = txtNombre.Text.Trim().ToUpper()
                        };
                        mservice.crear(marca);
                        tabla.Rows.Add(marca.id, marca.nombre);
                    }
                    else MessageBox.Show("YA EXISTE UNA MARCA CON ESTE NOMBRE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else MessageBox.Show("FALTA NOMBRE");
        }

        private void FormAdminMarcas_Load(object sender, EventArgs e)
        {
            tabla.Clear();
            mservice.listarNoAnulados().ForEach(x => tabla.Rows.Add(x.id, x.nombre));
        }

        private void btnExportar_Click_1(object sender, EventArgs e)
        {
            btnExportar.Enabled = false;
            Exportar.exportarAExcelAsync(grid);
            btnExportar.Enabled = true;
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            grid.Rows[e.RowIndex].Cells["NOMBRE"].ReadOnly = false;
            grid.Rows[e.RowIndex].Cells["NOMBRE"].Selected = true;
            grid.BeginEdit(true);
        }

        private void grid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            nombreAnterior = grid.Rows[e.RowIndex].Cells["NOMBRE"].Value.ToString();
        }

        private void grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (MessageBox.Show("SEGURO DE MODIFICAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Marca modifica = new Marca
                {
                    id = Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()),
                    nombre = grid.Rows[e.RowIndex].Cells["NOMBRE"].Value.ToString().Trim().ToUpper()
                };
                mservice.editar(modifica);
                grid.Rows[e.RowIndex].Cells["NOMBRE"].Value =modifica.nombre;
            }
            else grid.Rows[e.RowIndex].Cells["NOMBRE"].Value = nombreAnterior;

        }
    }
}
