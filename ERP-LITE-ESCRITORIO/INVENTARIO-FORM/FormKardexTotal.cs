﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.INVENTARIO_FORM
{
    public partial class FormKardexTotal : Form
    {
        DataTable tabla = new DataTable();
        ProductoService pservice = new ProductoImpl();
        DetalleMovimientoProductoImpl dservice = new DetalleMovimientoProductoImpl();
        DateTime fecha;
        public FormKardexTotal(DateTime f)
        {
            fecha = f;
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("UNIDAD DE MEDIDA");
            tabla.Columns.Add("STOCK");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("COSTO");
            tabla.Columns.Add("TOTAL");
            label1.Text = "Kardex Total a la fecha: " + fecha.ToLongDateString() ;
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 432;
            grid.Columns[2].Width = 150;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 100;
            grid.Columns[5].Width = 100;
            grid.Columns[6].Width = 200;
        }

        private void FormKardexTotal_Load(object sender, EventArgs e)
        {
            foreach (var p in pservice.listarNoAnulados()) {

             DetalleMovimientoProducto mov=   dservice.listarKardexProducto(p.id,fecha);
                if (mov == null)
                    tabla.Rows.Add(p.id, p.nombre, p.unidad.nombre, string.Format("{0:0.00}", p.stockInicial), string.Format("{0:0.00}", p.precio),
                        p.costoInicial, string.Format("{0:0.00}", p.stockInicial * p.costoInicial));
                else {
                    tabla.Rows.Add(p.id, p.nombre, p.unidad.nombre, string.Format("{0:0.00}", mov.nuevoStock), string.Format("{0:0.00}", p.precio),
                      mov.nuevoCostoUnitario, string.Format("{0:0.00}", mov.nuevoStock* mov.nuevoCostoUnitario));
                }
            }
     

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
    }
}
