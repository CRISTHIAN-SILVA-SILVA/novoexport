﻿using ERP_LITE_ESCRITORIO.INVENTARIO_FORM;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormAdminProductos : Form
    {
        ProductoService pservice = new ProductoImpl();
        DetalleMovimientoProductoImpl dmpservice = new DetalleMovimientoProductoImpl();

        DataTable tabla = new DataTable();
        public FormAdminProductos()
        {
          //  dmpservice.ordenarKardex();
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("STOCK");
           
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("UNIDAD DE MEDIDA");
            tabla.Columns.Add("CATEGORIA");
            tabla.Columns.Add("COSTO");
            tabla.Columns.Add("TOTAL");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 325;
            grid.Columns[2].Width = 70;
            grid.Columns[3].Width = 70;
            grid.Columns[4].Width = 150;
            grid.Columns[5].Width = 165;
            grid.Columns[6].Width = 150;
            grid.Columns[7].Width = 150;
        }

        void llenar() {
            pservice.listarNoAnulados().ForEach(p => tabla.Rows.Add(p.id, p.nombre, string.Format("{0:0.00}", p.stock), string.Format("{0:0.00}", p.precio), p.unidad.nombre, p.categoria.nombre,string.Format("{0:0.00}",p.costo), string.Format("{0:0.00}", p.costo*p.stock)));

        }
        //ANTES DE UTILIZAR ESTE METODO TENDRIA QUE CARGAR EL COSTO UNITARIO INICAL Y STOCK INICIAL DE TODOS LOS PRODUCTOS

            //metodo sin sobrantes ni notas de credito  y debito compras
            //esta si gratuitoa


   
        
        private void FormAdminProductos_Load(object sender, EventArgs e)
        {
            llenar();
            this.Activate();
            txtBuscarProducto.Focus();
        }
        void agregar(int filaModifica, int id, string nombre, double stock, double precio, string unidad, string categoria, string modificado)
        {
            if (filaModifica == -1)
                tabla.Rows.Add(id, nombre, string.Format("{0:0.00}", stock), string.Format("{0:0.00}", precio), unidad, categoria, "","");
            else
            {
                grid.Rows[filaModifica].Cells["PRECIO"].Value = string.Format("{0:0.00}", precio);
                grid.Rows[filaModifica].Cells["MODIFICADO POR"].Value = modificado;
            }

        }
    
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevoProducto form = new FormNuevoProducto(0, null);
            form.pasado += new FormNuevoProducto.pasar(agregar);
            form.ShowDialog();
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Producto p = pservice.buscar(Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            if (p != null)
            {
                FormNuevoProducto form = new FormNuevoProducto(e.RowIndex, p);
                form.pasado += new FormNuevoProducto.pasar(agregar);
                form.ShowDialog();
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            btnExportar.Enabled = false;
            Exportar.exportarAExcelAsync(grid);
            btnExportar.Enabled = true;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ELIMINAR PRODUCTO?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    if (pservice.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString())))
                        tabla.Rows.RemoveAt(grid.SelectedRows[0].Index);
                    else MessageBox.Show("ERROR INESPERADO", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            btnEliminar.Enabled = true;
        }

        private void txtKardex_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            {
                Producto p = pservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (p != null)
                {
                    FormKardexValorizado form = new FormKardexValorizado(p);
                    form.ShowDialog();
                }
            }
        }

        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e)
        {
            tabla.Clear();
            pservice.listarPorNombre(txtBuscarProducto.Text.ToUpper()).ForEach(p => tabla.Rows.Add(p.id, p.nombre, string.Format("{0:0.00}", p.stock), string.Format("{0:0.00}", p.precio), p.unidad.nombre, p.categoria.nombre, string.Format("{0:0.00}", p.costo), string.Format("{0:0.00}", p.costo * p.stock)));

        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            FormElijeFecha form = new FormElijeFecha();
            form.ShowDialog();
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            DetalleMovimientoProductoImpl dmps = new DetalleMovimientoProductoImpl();
            dmps.multiplicarTipoCambio();
            MessageBox.Show("Normalizado");
        }
    }
}
