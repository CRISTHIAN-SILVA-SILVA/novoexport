﻿using MaterialSkin;
using MaterialSkin.Controls;
using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.INVENTARIO_FORM
{
    public partial class FormKardexValorizado : MaterialForm
    {
        Producto producto;
        DetalleMovimientoProductoImpl dmpservice = new DetalleMovimientoProductoImpl();
        double totalSalidas = 0, totalEntradas = 0, totalSaldo = 0, totalMermas = 0 ;
        List<DetalleKardexPermanente> lista = new List<DetalleKardexPermanente>();
        private void reportViewer1_Load(object sender, EventArgs e){}
        public FormKardexValorizado(Producto p)
        {
            producto = p;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            FormCostosProducto form = new FormCostosProducto(producto.id,txtInicio.Value.Date,txtFin.Value.Date.AddDays(1));
            form.ShowDialog();
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e   )
        {

        }

        private void materialRaisedButton1_Click_1(object sender, EventArgs e)
        {
            FormNormalizarKardex form = new INVENTARIO_FORM.FormNormalizarKardex(producto);
            form.ShowDialog();
        }

        private void FormKardexValorizado_Load(object sender, EventArgs e)
        {
            this.Text = producto.nombre.ToUpper();
        }

        private void gridDetalle_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private void btnGenerar_Click(object sender, EventArgs e)
        {
            lista.Clear();
            totalEntradas = 0;
            totalMermas = 0;
            totalSalidas = 0;
            DetalleKardexPermanente d;
         //   chartEntradas.Series.Clear();
            int entradas = 1, salidas = 1;
            chartEntradas.Series["ENTRADAS"].Points.Clear();
            chartEntradas.Series["SALIDAS"].Points.Clear();





            DetalleMovimientoProducto dinicial;
            double stockI = 0,costoi=0;

            foreach (var x in dmpservice.listarKardexProducto(producto.id, txtInicio.Value.Date, txtFin.Value.Date.AddDays(1)))
            {

                if (entradas == 1 && salidas == 1)
                {
                    dinicial = dmpservice.buscarPorIndex(x.idAnterior-1,x.idProducto);
                    if (dinicial == null) {
                        stockI = producto.stockInicial;
                        costoi = producto.costoInicial;
                    } else {
                        stockI = dinicial.nuevoStock;
                        costoi = dinicial.nuevoCostoUnitario;
                    }
                 
                    d = new DetalleKardexPermanente
                    {
                        ID = "0",
                        fecha = txtInicio.Value.ToShortDateString(),
                        tipoComprobante = "-",
                        serie = "-",
                        numero = "-",
                        tipOperacion = "16",
                        entradas = string.Format("{0:0.00}", stockI),
                        salidas = "-",
                        saldo = string.Format("{0:0.00}", stockI),
                        COSTOE = string.Format("{0:0.00}", costoi),
                        COSTON = string.Format("{0:0.00}", costoi),
                        TOTALE = string.Format("{0:0.00}", stockI * costoi),
                        TOTALS = "",
                        COSTOS = "",
                        TOTALN = string.Format("{0:0.00}",stockI*costoi)

                    };
                    totalEntradas += producto.stockInicial;
                    lista.Add(d);
                }


                if (x.esIngreso)
                {
                    d = new DetalleKardexPermanente
                    { ID=x.idAnterior.ToString(),
                        fecha = x.fecha.ToShortDateString(),
                        tipoComprobante = x.tbl10,
                        serie = x.serie,
                        numero = Util.NormalizarCampo(x.numero.ToString(), 8),
                        tipOperacion = x.tbl12,
                        entradas = x.cantidad.ToString(),
                         COSTOE= string.Format("{0:0.00}", x.costoUnitario), TOTALE= string.Format("{0:0.00}", x.total),
                          COSTOS="", TOTALS="",
                           COSTON= string.Format("{0:0.00}",x.nuevoCostoUnitario), TOTALN= string.Format("{0:0.00}", x.nuevoCostoUnitario*x.nuevoStock),
                        salidas = "-",
                        saldo = x.nuevoStock.ToString(),
                       
                    };
                    totalEntradas +=x.cantidad;
                    chartEntradas.Series["ENTRADAS"].Points.AddXY(entradas,x.cantidad);
                    entradas++;
                }
                else {
                    d = new DetalleKardexPermanente
                    {
                        ID = x.idAnterior.ToString(),
                        fecha = x.fecha.ToShortDateString(),
                        tipoComprobante = x.tbl10,
                        serie = x.serie,
                        numero = Util.NormalizarCampo(x.numero.ToString(), 8),
                        tipOperacion = x.tbl12,
                        entradas = "-",
                        COSTOE = "",
                        TOTALE = "",
                        COSTOS = string.Format("{0:0.00}", x.costoUnitario),
                        TOTALS = string.Format("{0:0.00}", x.total),
                        COSTON = string.Format("{0:0.00}", x.nuevoCostoUnitario),
                        TOTALN = string.Format("{0:0.00}", x.nuevoCostoUnitario * x.nuevoStock),
                        salidas = x.cantidad.ToString(),
                        saldo = x.nuevoStock.ToString(),
                    };
                    totalSalidas += x.cantidad;
                    chartEntradas.Series["SALIDAS"].Points.AddXY(salidas, x.cantidad);
                    salidas++;
                }
                lista.Add(d);
            }

            chart1.Series["entradas"].Points.Clear();

           chart1.Series["entradas"].Points.AddXY("ENTRADAS",totalEntradas);
            chart1.Series["entradas"].Points.AddXY("SALIDAS", totalSalidas);
            
            ReportParameter[] parametros = new ReportParameter[7];
            parametros[0] = new ReportParameter("EMPRESA", LocalImpl.getInstancia().razonSocial);
            parametros[1] = new ReportParameter("DIRECCION", LocalImpl.getInstancia().direccion);
            parametros[2] = new ReportParameter("RUC", LocalImpl.getInstancia().ruc);
            parametros[3] = new ReportParameter("PERIODO", "");
            parametros[4] = new ReportParameter("TIPO", "01");
            parametros[5] = new ReportParameter("PRODUCTO", producto.nombre);
            parametros[6] = new ReportParameter("UNIDAD_MEDIDA", producto.unidad.codigo);
            reportViewer1.LocalReport.SetParameters(parametros);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();

         
        }
    }
}
