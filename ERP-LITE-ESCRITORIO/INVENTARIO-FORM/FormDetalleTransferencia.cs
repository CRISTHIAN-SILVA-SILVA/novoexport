﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.INVENTARIO_FORM
{
    public partial class FormDetalleTransferencia : MaterialForm
    {
        DataTable tabla = new DataTable();
        AlmacenService aservice = new AlmacenImpl();
        DetalleTransferenciaProductoService dtpservice=new DetalleTransferenciaProductoImpl();
        TransferenciaProducto tpp;
        public FormDetalleTransferencia(TransferenciaProducto tp)
        {
            tpp = tp;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            int idOrigen=0 ;
            int idDestino =0;
            foreach (var i in tp.almacenesTransferencia) {
                if (i.esDestino) idDestino = i.idAlmacen;
                if (i.esOrigen) idOrigen = i.idAlmacen;
            }
            lblDestino.Text = aservice.buscar(idDestino).nombre;
            lblOrigen.Text = aservice.buscar(idOrigen).nombre;
            txtEmision.Value = tp.fechaCreate;
            lblUsuario.Text = tp.usuarioCreate;
            txtEmision.Enabled = false;
            lblTotal.Text = string.Format("{0:0.00}",tp.total);
            tabla.Columns.Add("ID");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("TOTAL");
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Width = 600;
            gridDetalle.Columns[2].Width = 100;
            gridDetalle.Columns[3].Width = 120;
            gridDetalle.Columns[4].Width = 120;
       }

        private void FormDetalleTransferencia_Load(object sender, EventArgs e)
        {
            dtpservice.listarPorTranferencia(tpp.id).ForEach(x => tabla.Rows.Add(x.id, x.presentacion.nombre, string.Format("{0:0.00}", x.cantidad), string.Format("{0:0.00}", x.precio), string.Format("{0:0.00}", x.total)));

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
