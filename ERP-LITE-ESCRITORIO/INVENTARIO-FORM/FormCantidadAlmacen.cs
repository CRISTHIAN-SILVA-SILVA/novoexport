﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormCantidadAlmacen : MaterialForm
    {
        AlmacenService aService = new AlmacenImpl();
        DataTable tabla = new DataTable();
        double cantidadTtotal = 0;
        double cantidadAnteriror = 0;
        public delegate void pasar(Producto p);
        public event pasar pasado;
        Producto producto = null;
        ProductoService PPSERVICE = new ProductoImpl();
        public FormCantidadAlmacen(Producto p)
        {
            InitializeComponent();
            producto = p;
            txtProducto.Text = producto.nombre;
            tabla.Columns.Add("ID");
            tabla.Columns.Add("ALMACEN");
            tabla.Columns.Add("CANTIDAD");
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width=515;
            grid.Columns[2].Width= 100;
        }

        private void FormCantidadAlmacen_Load(object sender, EventArgs e)
        {
            this.Activate();
            btnAgregar.Focus();
            aService.listarNoAnulados().ForEach(x => tabla.Rows.Add(x.id,x.nombre,"0"));
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            if (MessageBox.Show("SEGURO DE AGREGAR PRODUCTO?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AlmacenProducto ap;
                List<AlmacenProducto> detalles = new List<AlmacenProducto>();
                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    ap = new AlmacenProducto
                    {
                        cantidadFisica = Convert.ToDouble(grid.Rows[i].Cells["CANTIDAD"].Value.ToString()),
                        cantidadLogica = Convert.ToDouble(grid.Rows[i].Cells["CANTIDAD"].Value.ToString()),
                        idAlmacen = Convert.ToInt32(grid.Rows[i].Cells["ID"].Value.ToString()),

                    };
                    detalles.Add(ap);
                }
                producto.stock =Convert.ToDouble(lblTotal.Text);
                producto.almacenesProducto = detalles;
                PPSERVICE.crear(producto);

                pasado(producto);
                this.Close();
              
            }
            btnAgregar.Enabled = true;
              
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            grid.Rows[e.RowIndex].Cells["CANTIDAD"].ReadOnly = false;
            grid.Rows[e.RowIndex].Cells["CANTIDAD"].Selected = true;
            grid.BeginEdit(true);
        }

        private void grid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            cantidadAnteriror =Convert.ToDouble( grid.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());     }

        private void grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (Util.esDouble(grid.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString()))
            {
                double cantidad = Convert.ToDouble(grid.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
                cantidadTtotal = cantidadTtotal + (cantidad - cantidadAnteriror);
                lblTotal.Text = string.Format("{0:0.00}", cantidadTtotal);

            }
            else { MessageBox.Show("CANTIDAD INCORRECTA","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information); grid.Rows[e.RowIndex].Cells["CANTIDAD"].Value = cantidadAnteriror; }
        }
    }
}
