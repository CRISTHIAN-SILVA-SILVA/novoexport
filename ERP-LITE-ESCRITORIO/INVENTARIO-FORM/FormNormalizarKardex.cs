﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.INVENTARIO_FORM
{
    public partial class FormNormalizarKardex : Form
    {
        DetalleMovimientoProductoImpl dmservice = new DetalleMovimientoProductoImpl();
        Producto producto;
        public FormNormalizarKardex(Producto p)
        {
            InitializeComponent();
            producto = p;
            materialLabel2.Text = p.nombre;
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if (Util.esDouble(txtStock.Text) && Util.esDouble(txtCosto.Text))
            {
                if (Convert.ToDouble(txtStock.Text) >= 0 && Convert.ToDouble(txtCosto.Text) >= 0)
                {
                    dmservice.ordenarMovsProducto(producto.id,Convert.ToDouble(txtStock.Text),Convert.ToDouble(txtCosto.Text));
                    MessageBox.Show("MOVIMIEnTOS NORMALIZADOS");
                    this.Close();
                }
                else MessageBox.Show("PARAMETROS INCORRECTOS");
        }
            else MessageBox.Show("PARAMETROS INCORRECTOS");
           
        }
    }
}
