﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormAdminMermas : Form
    {
        DataTable tabla = new DataTable();
        MermaService mservice = new MermaImpl();
        public FormAdminMermas()
        {
            InitializeComponent();
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("USUARIO");
            tabla.Columns.Add("MOTIVO");
            tabla.Columns.Add("TOTAL");
            grid.DataSource = tabla;
            grid.Columns[0].Width = 150;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 300;
            grid.Columns[3].Width = 400;
            grid.Columns[4].Width = 140;
        }

        private void FormAdminMermas_Load(object sender, EventArgs e)
        {
            mservice.listarNoAnulados().ForEach(m => tabla.Rows.Add(m.id, m.fechaCreate.ToShortDateString() , m.usuarioCreate, m.motivo, String.Format("{0:0.00}", m.total)));
        }
        void agregar(Merma m)
        {
            tabla.Rows.Add(m.id, m.fechaCreate.ToShortDateString() , m.usuarioCreate,m.motivo,String.Format("{0:0.00}",m.total));
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaMerma form = new FormNuevaMerma();
            form.pasado += new FormNuevaMerma.pasar(agregar);
            form.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ELIMINAR PERDIDA","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes) {
                     
                }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
    }
}
