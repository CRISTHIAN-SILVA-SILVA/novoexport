﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.INVENTARIO_FORM
{
    public partial class FormNuevoAlmacen : MaterialForm
    {
        AlmacenService aservice = new AlmacenImpl();
        Almacen modifica = null;
        int filaModifica = -1;
        public delegate void pasar(int filaModifica, Almacen mod);
        public event pasar pasado;
        public FormNuevoAlmacen(Almacen mod, int filaModifi)
        {
            InitializeComponent();
            modifica = mod;
            filaModifica = filaModifi;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            if (modifica != null)
            {
                txtNombre.Text = modifica.nombre;
                txtTelefono.Text = modifica.telefono;
                txtDireccion.Text = modifica.direccion;
                txtEncargado.Text = modifica.encargado;
                txtDireccion.Enabled = false;
                checkEsPrincipal.Checked = modifica.esPrincipal;
            }
        }

        private void FormNuevoAlmacen_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtNombre.Focus();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (modifica == null)
                if (Util.validaCadena(txtNombre.Text))
                {
                    if (Util.validaCadena(txtDireccion.Text))
                    {
                        Almacen almacen = new Almacen();
                        almacen.nombre = txtNombre.Text.Trim().ToUpper();
                        almacen.direccion = txtDireccion.Text.ToUpper();
                        almacen.telefono = txtTelefono.Text;
                        almacen.encargado = txtEncargado.Text;
                        almacen.esPrincipal = checkEsPrincipal.Checked;
                    
                        aservice.crear(almacen);
                        pasado(-1, almacen);
                        this.Close();
                    }
                  else  MessageBox.Show("FALTA DIRECCION", "FALTA DIRECCION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else MessageBox.Show("FALTA NOMBRE", "FALTA NOMBRE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                if (Util.validaCadena(txtNombre.Text))
                    if (txtTelefono.Text.Count() < 10)
                    {
                        modifica.nombre = txtNombre.Text.Trim();
                        modifica.telefono = txtTelefono.Text;
                        modifica.esPrincipal = checkEsPrincipal.Checked;
                        aservice.editar(modifica);
                        pasado(filaModifica, modifica);
                        this.Close();
                    }
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
