﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
   public class TipoOperacionImpl
    {
        private static List<TipoOperacion> todas = null;
        private static string esquema = "public";
        public static List<TipoOperacion> getInstancia()
        {
            if (todas == null)
            {
                using (var db = new BaseDeDatos(esquema))
                {
                    todas = db.TiposOperacion.ToList();
                }
            }
            return todas;
        }
    }
}
