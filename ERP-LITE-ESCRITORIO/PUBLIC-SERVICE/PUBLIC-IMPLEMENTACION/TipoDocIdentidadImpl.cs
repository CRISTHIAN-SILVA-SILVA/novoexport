﻿using MODEL_ERP_LITE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
   public class TipoDocIdentidadImpl
    {
        private static string codigoRUC="";
        private static string codigoDNI= "";
        public static string getCodigoDNI() {
            if (codigoDNI.Equals(""))
            {
                using (var db = new BaseDeDatos(""))
                {
                    return db.TipoDocumentosIdentidad.FirstOrDefault(x => x.descripcion_abreviada == "DNI").codigo_sunat;
                }
            }
            else return codigoDNI;
        }
        public static string getCodigoRUC() {
            if (codigoRUC.Equals("")) {
                using (var db = new BaseDeDatos(""))
                {
                    return db.TipoDocumentosIdentidad.FirstOrDefault(x => x.descripcion_abreviada == "RUC").codigo_sunat;
                }
            }
            else return codigoRUC;
        }
    }

}
