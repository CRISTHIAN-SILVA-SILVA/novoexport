﻿namespace ERP_LITE_DESKTOP.PEDIDOS_FORM
{
    partial class FormNuevoPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNuevoPedido));
            this.gridDetalle = new System.Windows.Forms.DataGridView();
            this.btnCotizaciones = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtCotizacion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnGuardar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnClose = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtBuscarProducto = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.gridProducto = new System.Windows.Forms.DataGridView();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtPedido = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnPedidos = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.lblTotal = new MaterialSkin.Controls.MaterialLabel();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.txtDNI = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblDocIdentidad = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.comboCliente = new System.Windows.Forms.ComboBox();
            this.rbBoleta = new MaterialSkin.Controls.MaterialRadioButton();
            this.rbFactura = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.btnAgregarCliente = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnObservacion = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtObservacion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProducto)).BeginInit();
            this.SuspendLayout();
            // 
            // gridDetalle
            // 
            this.gridDetalle.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDetalle.BackgroundColor = System.Drawing.Color.White;
            this.gridDetalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridDetalle.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.gridDetalle.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridDetalle.EnableHeadersVisualStyles = false;
            this.gridDetalle.GridColor = System.Drawing.Color.Black;
            this.gridDetalle.Location = new System.Drawing.Point(12, 361);
            this.gridDetalle.MultiSelect = false;
            this.gridDetalle.Name = "gridDetalle";
            this.gridDetalle.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridDetalle.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.gridDetalle.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.Height = 31;
            this.gridDetalle.RowTemplate.ReadOnly = true;
            this.gridDetalle.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.gridDetalle.Size = new System.Drawing.Size(1078, 153);
            this.gridDetalle.TabIndex = 1;
            this.gridDetalle.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.gridDetalle_CellBeginEdit);
            this.gridDetalle.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDetalle_CellDoubleClick);
            this.gridDetalle.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDetalle_CellEndEdit);
            this.gridDetalle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridDetalle_KeyDown);
            // 
            // btnCotizaciones
            // 
            this.btnCotizaciones.AutoSize = true;
            this.btnCotizaciones.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCotizaciones.Depth = 0;
            this.btnCotizaciones.Icon = null;
            this.btnCotizaciones.Location = new System.Drawing.Point(954, 145);
            this.btnCotizaciones.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCotizaciones.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCotizaciones.Name = "btnCotizaciones";
            this.btnCotizaciones.Primary = false;
            this.btnCotizaciones.Size = new System.Drawing.Size(28, 36);
            this.btnCotizaciones.TabIndex = 2;
            this.btnCotizaciones.Text = ">";
            this.btnCotizaciones.UseVisualStyleBackColor = true;
            this.btnCotizaciones.Click += new System.EventHandler(this.btnCotizaciones_Click);
            // 
            // txtCotizacion
            // 
            this.txtCotizacion.Depth = 0;
            this.txtCotizacion.Enabled = false;
            this.txtCotizacion.Hint = "";
            this.txtCotizacion.Location = new System.Drawing.Point(989, 158);
            this.txtCotizacion.MaxLength = 32767;
            this.txtCotizacion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCotizacion.Name = "txtCotizacion";
            this.txtCotizacion.PasswordChar = '\0';
            this.txtCotizacion.SelectedText = "";
            this.txtCotizacion.SelectionLength = 0;
            this.txtCotizacion.SelectionStart = 0;
            this.txtCotizacion.Size = new System.Drawing.Size(95, 23);
            this.txtCotizacion.TabIndex = 4;
            this.txtCotizacion.TabStop = false;
            this.txtCotizacion.UseSystemPasswordChar = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.AutoSize = true;
            this.btnGuardar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGuardar.Depth = 0;
            this.btnGuardar.Icon = null;
            this.btnGuardar.Location = new System.Drawing.Point(1000, 545);
            this.btnGuardar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Primary = true;
            this.btnGuardar.Size = new System.Drawing.Size(84, 36);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnClose.Depth = 0;
            this.btnClose.Icon = null;
            this.btnClose.Location = new System.Drawing.Point(918, 545);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnClose.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnClose.Name = "btnClose";
            this.btnClose.Primary = false;
            this.btnClose.Size = new System.Drawing.Size(75, 36);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "LIMPIAR";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // materialLabel2
            // 
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(13, 159);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(79, 23);
            this.materialLabel2.TabIndex = 7;
            this.materialLabel2.Text = "Producto:";
            // 
            // txtBuscarProducto
            // 
            this.txtBuscarProducto.Depth = 0;
            this.txtBuscarProducto.Hint = "";
            this.txtBuscarProducto.Location = new System.Drawing.Point(97, 158);
            this.txtBuscarProducto.MaxLength = 32767;
            this.txtBuscarProducto.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtBuscarProducto.Name = "txtBuscarProducto";
            this.txtBuscarProducto.PasswordChar = '\0';
            this.txtBuscarProducto.SelectedText = "";
            this.txtBuscarProducto.SelectionLength = 0;
            this.txtBuscarProducto.SelectionStart = 0;
            this.txtBuscarProducto.Size = new System.Drawing.Size(551, 23);
            this.txtBuscarProducto.TabIndex = 190;
            this.txtBuscarProducto.TabStop = false;
            this.txtBuscarProducto.UseSystemPasswordChar = false;
            this.txtBuscarProducto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBuscarProducto_KeyDown);
            this.txtBuscarProducto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBuscarProducto_KeyUp);
            // 
            // gridProducto
            // 
            this.gridProducto.AllowUserToAddRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.gridProducto.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.gridProducto.BackgroundColor = System.Drawing.Color.White;
            this.gridProducto.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridProducto.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.gridProducto.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridProducto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridProducto.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridProducto.EnableHeadersVisualStyles = false;
            this.gridProducto.GridColor = System.Drawing.Color.Black;
            this.gridProducto.Location = new System.Drawing.Point(12, 184);
            this.gridProducto.MultiSelect = false;
            this.gridProducto.Name = "gridProducto";
            this.gridProducto.ReadOnly = true;
            this.gridProducto.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridProducto.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridProducto.RowHeadersVisible = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            this.gridProducto.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.gridProducto.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.gridProducto.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridProducto.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.gridProducto.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.gridProducto.RowTemplate.Height = 31;
            this.gridProducto.RowTemplate.ReadOnly = true;
            this.gridProducto.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridProducto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridProducto.Size = new System.Drawing.Size(1078, 148);
            this.gridProducto.TabIndex = 9;
            this.gridProducto.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridProducto_CellDoubleClick);
            this.gridProducto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridProducto_KeyDown);
            // 
            // materialLabel1
            // 
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel1.Location = new System.Drawing.Point(869, 159);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(81, 22);
            this.materialLabel1.TabIndex = 10;
            this.materialLabel1.Text = "Cotización";
            // 
            // materialLabel3
            // 
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(12, 335);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(139, 23);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Detalle de Pedido:";
            // 
            // materialLabel4
            // 
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel4.Location = new System.Drawing.Point(666, 159);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(60, 22);
            this.materialLabel4.TabIndex = 14;
            this.materialLabel4.Text = "Pedido:";
            // 
            // txtPedido
            // 
            this.txtPedido.Depth = 0;
            this.txtPedido.Enabled = false;
            this.txtPedido.Hint = "";
            this.txtPedido.Location = new System.Drawing.Point(765, 158);
            this.txtPedido.MaxLength = 32767;
            this.txtPedido.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.PasswordChar = '\0';
            this.txtPedido.SelectedText = "";
            this.txtPedido.SelectionLength = 0;
            this.txtPedido.SelectionStart = 0;
            this.txtPedido.Size = new System.Drawing.Size(95, 23);
            this.txtPedido.TabIndex = 13;
            this.txtPedido.TabStop = false;
            this.txtPedido.UseSystemPasswordChar = false;
            // 
            // btnPedidos
            // 
            this.btnPedidos.AutoSize = true;
            this.btnPedidos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnPedidos.Depth = 0;
            this.btnPedidos.Icon = null;
            this.btnPedidos.Location = new System.Drawing.Point(733, 145);
            this.btnPedidos.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnPedidos.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnPedidos.Name = "btnPedidos";
            this.btnPedidos.Primary = false;
            this.btnPedidos.Size = new System.Drawing.Size(28, 36);
            this.btnPedidos.TabIndex = 12;
            this.btnPedidos.Text = ">";
            this.btnPedidos.UseVisualStyleBackColor = true;
            this.btnPedidos.Click += new System.EventHandler(this.btnPedidos_Click);
            // 
            // materialLabel5
            // 
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel5.Location = new System.Drawing.Point(922, 517);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(60, 22);
            this.materialLabel5.TabIndex = 15;
            this.materialLabel5.Text = "Total:";
            // 
            // lblTotal
            // 
            this.lblTotal.Depth = 0;
            this.lblTotal.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTotal.Location = new System.Drawing.Point(985, 517);
            this.lblTotal.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(99, 22);
            this.lblTotal.TabIndex = 16;
            this.lblTotal.Text = "0.00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Icon = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Icon")));
            this.btnEliminar.Location = new System.Drawing.Point(12, 523);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(97, 36);
            this.btnEliminar.TabIndex = 100;
            this.btnEliminar.Text = "QUITAR";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // materialLabel6
            // 
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(754, 116);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(43, 23);
            this.materialLabel6.TabIndex = 193;
            this.materialLabel6.Text = "DNI:";
            this.materialLabel6.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // txtDNI
            // 
            this.txtDNI.Depth = 0;
            this.txtDNI.Hint = "";
            this.txtDNI.Location = new System.Drawing.Point(808, 116);
            this.txtDNI.MaxLength = 32767;
            this.txtDNI.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDNI.Name = "txtDNI";
            this.txtDNI.PasswordChar = '\0';
            this.txtDNI.SelectedText = "";
            this.txtDNI.SelectionLength = 0;
            this.txtDNI.SelectionStart = 0;
            this.txtDNI.Size = new System.Drawing.Size(100, 23);
            this.txtDNI.TabIndex = 192;
            this.txtDNI.TabStop = false;
            this.txtDNI.UseSystemPasswordChar = false;
            // 
            // lblDocIdentidad
            // 
            this.lblDocIdentidad.Depth = 0;
            this.lblDocIdentidad.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblDocIdentidad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDocIdentidad.Location = new System.Drawing.Point(975, 116);
            this.lblDocIdentidad.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblDocIdentidad.Name = "lblDocIdentidad";
            this.lblDocIdentidad.Size = new System.Drawing.Size(107, 23);
            this.lblDocIdentidad.TabIndex = 191;
            this.lblDocIdentidad.Text = "12345678901";
            this.lblDocIdentidad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // materialLabel14
            // 
            this.materialLabel14.Depth = 0;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.Location = new System.Drawing.Point(930, 116);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(39, 23);
            this.materialLabel14.TabIndex = 190;
            this.materialLabel14.Text = "Ruc:";
            this.materialLabel14.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // comboCliente
            // 
            this.comboCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboCliente.DisplayMember = "razonSocial";
            this.comboCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboCliente.FormattingEnabled = true;
            this.comboCliente.Location = new System.Drawing.Point(97, 113);
            this.comboCliente.MaxLength = 100;
            this.comboCliente.Name = "comboCliente";
            this.comboCliente.Size = new System.Drawing.Size(651, 26);
            this.comboCliente.TabIndex = 189;
            this.comboCliente.ValueMember = "id";
            this.comboCliente.TextUpdate += new System.EventHandler(this.comboCliente_TextUpdate);
            this.comboCliente.SelectedValueChanged += new System.EventHandler(this.comboCliente_SelectedValueChanged);
            // 
            // rbBoleta
            // 
            this.rbBoleta.AutoSize = true;
            this.rbBoleta.Depth = 0;
            this.rbBoleta.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbBoleta.Location = new System.Drawing.Point(16, 74);
            this.rbBoleta.Margin = new System.Windows.Forms.Padding(0);
            this.rbBoleta.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbBoleta.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbBoleta.Name = "rbBoleta";
            this.rbBoleta.Ripple = true;
            this.rbBoleta.Size = new System.Drawing.Size(80, 30);
            this.rbBoleta.TabIndex = 188;
            this.rbBoleta.TabStop = true;
            this.rbBoleta.Text = "BOLETA";
            this.rbBoleta.UseVisualStyleBackColor = true;
            this.rbBoleta.CheckedChanged += new System.EventHandler(this.rbBoleta_CheckedChanged);
            // 
            // rbFactura
            // 
            this.rbFactura.AutoSize = true;
            this.rbFactura.Depth = 0;
            this.rbFactura.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbFactura.Location = new System.Drawing.Point(147, 74);
            this.rbFactura.Margin = new System.Windows.Forms.Padding(0);
            this.rbFactura.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbFactura.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbFactura.Name = "rbFactura";
            this.rbFactura.Ripple = true;
            this.rbFactura.Size = new System.Drawing.Size(90, 30);
            this.rbFactura.TabIndex = 187;
            this.rbFactura.TabStop = true;
            this.rbFactura.Text = "FACTURA";
            this.rbFactura.UseVisualStyleBackColor = true;
            this.rbFactura.CheckedChanged += new System.EventHandler(this.rbFactura_CheckedChanged);
            // 
            // materialLabel8
            // 
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(12, 116);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(80, 23);
            this.materialLabel8.TabIndex = 186;
            this.materialLabel8.Text = "Cliente:";
            this.materialLabel8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // btnAgregarCliente
            // 
            this.btnAgregarCliente.AutoSize = true;
            this.btnAgregarCliente.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregarCliente.Depth = 0;
            this.btnAgregarCliente.Icon = null;
            this.btnAgregarCliente.Location = new System.Drawing.Point(264, 68);
            this.btnAgregarCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregarCliente.Name = "btnAgregarCliente";
            this.btnAgregarCliente.Primary = true;
            this.btnAgregarCliente.Size = new System.Drawing.Size(149, 36);
            this.btnAgregarCliente.TabIndex = 194;
            this.btnAgregarCliente.Text = "Agregar Empresa";
            this.btnAgregarCliente.UseVisualStyleBackColor = true;
            this.btnAgregarCliente.Click += new System.EventHandler(this.btnAgregarCliente_Click);
            // 
            // btnObservacion
            // 
            this.btnObservacion.AutoSize = true;
            this.btnObservacion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnObservacion.Depth = 0;
            this.btnObservacion.Icon = null;
            this.btnObservacion.Location = new System.Drawing.Point(612, 517);
            this.btnObservacion.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnObservacion.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnObservacion.Name = "btnObservacion";
            this.btnObservacion.Primary = false;
            this.btnObservacion.Size = new System.Drawing.Size(114, 36);
            this.btnObservacion.TabIndex = 195;
            this.btnObservacion.Text = "observacion";
            this.btnObservacion.UseVisualStyleBackColor = true;
            this.btnObservacion.Click += new System.EventHandler(this.btnObservacion_Click);
            // 
            // txtObservacion
            // 
            this.txtObservacion.Depth = 0;
            this.txtObservacion.Hint = "";
            this.txtObservacion.Location = new System.Drawing.Point(379, 530);
            this.txtObservacion.MaxLength = 30;
            this.txtObservacion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtObservacion.Name = "txtObservacion";
            this.txtObservacion.PasswordChar = '\0';
            this.txtObservacion.SelectedText = "";
            this.txtObservacion.SelectionLength = 0;
            this.txtObservacion.SelectionStart = 0;
            this.txtObservacion.Size = new System.Drawing.Size(220, 23);
            this.txtObservacion.TabIndex = 196;
            this.txtObservacion.TabStop = false;
            this.txtObservacion.UseSystemPasswordChar = false;
            // 
            // FormNuevoPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 593);
            this.Controls.Add(this.txtObservacion);
            this.Controls.Add(this.btnObservacion);
            this.Controls.Add(this.btnAgregarCliente);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.txtDNI);
            this.Controls.Add(this.lblDocIdentidad);
            this.Controls.Add(this.materialLabel14);
            this.Controls.Add(this.comboCliente);
            this.Controls.Add(this.rbBoleta);
            this.Controls.Add(this.rbFactura);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.txtPedido);
            this.Controls.Add(this.btnPedidos);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.gridProducto);
            this.Controls.Add(this.txtBuscarProducto);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtCotizacion);
            this.Controls.Add(this.btnCotizaciones);
            this.Controls.Add(this.gridDetalle);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FormNuevoPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FormPedidos_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormNuevoPedido_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProducto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridDetalle;
        private MaterialSkin.Controls.MaterialFlatButton btnCotizaciones;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCotizacion;
        private MaterialSkin.Controls.MaterialRaisedButton btnGuardar;
        private MaterialSkin.Controls.MaterialFlatButton btnClose;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtBuscarProducto;
        private System.Windows.Forms.DataGridView gridProducto;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPedido;
        private MaterialSkin.Controls.MaterialFlatButton btnPedidos;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel lblTotal;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDNI;
        private MaterialSkin.Controls.MaterialLabel lblDocIdentidad;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private System.Windows.Forms.ComboBox comboCliente;
        private MaterialSkin.Controls.MaterialRadioButton rbBoleta;
        private MaterialSkin.Controls.MaterialRadioButton rbFactura;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialRaisedButton btnAgregarCliente;
        private MaterialSkin.Controls.MaterialFlatButton btnObservacion;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtObservacion;
    }
}