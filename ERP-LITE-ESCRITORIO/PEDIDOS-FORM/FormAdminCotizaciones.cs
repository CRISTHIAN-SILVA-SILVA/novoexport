﻿using ERP_LITE_DESKTOP.PEDIDOS_FORM;
using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.PEDIDO;
using SERVICE_ERP_LITE.PEDIDO_SERVICE;
using SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.PEDIDOS_FORM
{
    public partial class FormAdminCotizaciones : MaterialForm
    {
        DataTable tabla = new DataTable();
        CotizacionService cService = new CotizacionImpl();
        public delegate void pasar(Cotizacion c);
        public event pasar pasado;
        bool esVentanaVenta = false;
        public FormAdminCotizaciones(bool esVenta)
        {
            esVentanaVenta = esVenta;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("SERIE-NUMERO");
            tabla.Columns.Add("CLIENTE");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("TIPO DE PAGO");
     
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 150;
            grid.Columns[3].Width = 550;
            grid.Columns[4].Width = 100;
            grid.Columns[5].Width = 170;
    
        }

        private void FormAdminCotizaciones_Load(object sender, EventArgs e)
        {
            agregar(null);
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
        void agregar(Cotizacion c) {
            tabla.Clear();
            foreach (var x in cService.listarNoAnulados()) {
                tabla.Rows.Add(x.id,x.fechaCreate.ToShortDateString(),x.serie+"-"+Util.NormalizarCampo(x.numero.ToString(),8),x.cliente.razonSocial,x.total,x.tipoPago.nombre);
            }
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaCotizacion form = new FormNuevaCotizacion(null);
            form.pasado += new FormNuevaCotizacion.pasar(agregar);
            form.ShowDialog();
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            {
                Cotizacion c = cService.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null)
                {
                    if (!esVentanaVenta)
                    {
                        FormNuevaCotizacion form = new FormNuevaCotizacion(c);
                        form.pasado += new FormNuevaCotizacion.pasar(agregar);
                        form.ShowDialog();
                    }
                    else { pasado(c);this.Close(); }
                }
            }
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1) {
                Cotizacion c = cService.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null) {
                    FormReporteCotizacion form = new FormReporteCotizacion(c);
                    form.ShowDialog();
                }
            }
        }
    }
}
