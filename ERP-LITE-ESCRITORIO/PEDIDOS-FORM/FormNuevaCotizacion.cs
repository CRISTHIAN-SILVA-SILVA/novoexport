﻿using ERP_LITE_ESCRITORIO.PEDIDOS_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PEDIDO_SERVICE;
using SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.PEDIDOS_FORM
{
    public partial class FormNuevaCotizacion : MaterialForm
    {
        TipoPagoService tpService = new TipoPagoImpl();
        public delegate void pasar(Cotizacion c);
        public event pasar pasado;
        Cotizacion cotizacionMod = null;
        DetalleCotizacionService dpservice = new DetalleCotizacionImpl();
        CotizacionService pService = new CotizacionImpl();
        ProductoService proservice = new ProductoImpl();
        ClienteService csrvice = new ClienteImpl();
        DataTable tabla = new DataTable();
        AlmacenService aService = new AlmacenImpl();
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        Almacen a;
        Double cantidadAnterior;
        double total = 0;
        Parametro igv;
        double precioAnterior = 0;

        DataTable tablaP = new DataTable();
        public FormNuevaCotizacion(Cotizacion c)
        {
            cotizacionMod = c;
            InitializeComponent();
            igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            tablaP.Columns.Add("ID");
            tablaP.Columns.Add("PRODUCTO");
            tablaP.Columns.Add("CANTIDAD");
            gridProducto.DataSource = tablaP;
            gridProducto.Columns[0].Visible = false;
            gridProducto.Columns[1].Width = 900;
            gridProducto.Columns[2].Width = 100;


            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("FACTOR");
            tabla.Columns.Add("OBSERVACION");
            tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 550;
            gridDetalle.Columns[4].Width = 100;
            gridDetalle.Columns[5].Width = 100;
            gridDetalle.Columns[6].Width = 100;
            gridDetalle.Columns[7].Width = 100;
            gridDetalle.Columns[8].Visible = false;
            gridDetalle.Columns[9].Width = 120;
            comboTipoPago.DataSource = tpService.listarNoAnulados();

            List<Cliente> clientes = csrvice.listarClientes();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            clientes = csrvice.listarEmpresas();
            clientes.AddRange(csrvice.listarClientes());
            clientes.ForEach(r => coleccion.Add(r.razonSocial));
            comboCliente.AutoCompleteCustomSource = coleccion;
            comboCliente.DataSource = clientes;

            comboCliente.Text = "";




            if (c != null) {
                comboCliente.Text = c.cliente.razonSocial;
                comboCliente.Enabled = false;
                txtDNI.Text = c.cliente.dniRepresentante;
                txtDNI.Enabled = false;
                lblDocIdentidad.Text = c.cliente.ruc;
                if (c.porcentajeInteres > 0)
                {
                    panelCredito.Visible = true;
                    txtDias.Value = Convert.ToInt32(c.diasCredito);
                    txtInteres.Value = Convert.ToInt32(c.porcentajeInteres);
                }
                else panelCredito.Visible = false;
                txtDias.Enabled = false;
                txtInteres.Enabled = false;
                comboTipoPago.Text = c.tipoPago.nombre;
                comboTipoPago.Enabled = false;
                lblTotal.Text = string.Format("{0:0.00}",c.total);
            } 
        }

        private void FormNuevaCotizacion_Load(object sender, EventArgs e)
        {
            this.Activate();
            if (cotizacionMod != null) {
                foreach (var i in cotizacionMod.detalles)
                {
                    ProductoPresentacion pp = ppService.buscar(i.idProducto);
                    tabla.Rows.Add(i.id,pp.idProducto,pp.id,pp.nombre,i.precio,i.cantidad,i.igv,i.total,i.factor);
                }
            }
            a = aService.buscarPrincipal();
            if (a == null)
            {
                MessageBox.Show("NO EXISTE ALMACEN PRINCIPAL", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }

         
            if (igv == null) { MessageBox.Show("PARAMETRO IGV NO EXISTE"); this.Close(); }
        }

        private void comboTipoPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboTipoPago.Text.Equals("PAGO UNICO"))
            {  panelCredito.Visible = false; }
            else
            {
               
                if (comboTipoPago.Text.Equals("PAGO A CREDITO"))
                { panelCredito.Visible = true; }
            }
        }

        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e)
        {
            tablaP.Clear();
            if (Util.validaCadena(txtBuscarProducto.Text))
                proservice.listarPorCadena(txtBuscarProducto.Text.ToUpper()).ForEach(x => tablaP.Rows.Add(x.id, x.nombre, x.stock));
            
        }

        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            Producto p = proservice.buscar(Convert.ToInt32(gridProducto.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            FormPresentaciones form = new FormPresentaciones(p);
            form.pasado += new FormPresentaciones.pasar(agregarItem);
            form.ShowDialog();
        }
        void agregarItem(ProductoPresentacion pp)
        {
            if (tabla.Rows.Find(pp.id.ToString()) == null)
                tabla.Rows.Add("", pp.idProducto, pp.id, pp.nombre, pp.precio, "0", "0", "0", pp.factor);
        }

        private void gridDetalle_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            cantidadAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
            precioAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
        }

        private void gridDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
           
             
            if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString()))
            {
                if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString()))
                {
                    double cantidad = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
                    double precio = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
                    double subTotal = cantidad * precio;
                    double totalAnterior = cantidadAnterior * precio;

                    Producto pr = proservice.buscar(Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["IDPRODUCTO"].Value.ToString()));
                    double igvItem = subTotal - subTotal / (1 + igv.valorDouble);
                    if (pr.esExonerado)
                        igvItem = 0;

                    gridDetalle.Rows[e.RowIndex].Cells["IGV"].Value = String.Format("{0:0.0000}", igvItem);
                    gridDetalle.Rows[e.RowIndex].Cells["TOTAL"].Value = subTotal;

                    total = total + (subTotal - cantidadAnterior * precio);
                    lblTotal.Text = string.Format("{0:0.00}", total);
                    if (cotizacionMod != null)
                    {
                        cotizacionMod.fechaUpdate = DateTime.Now;
                        cotizacionMod.usuarioUpdate = FormLogin.user.nombres + "-" + FormLogin.user.dni;
                        if (gridDetalle.Rows[e.RowIndex].Cells["ID"].Value.ToString().Equals(""))
                        {
                            DetalleCotizacion dpedido = new DetalleCotizacion
                            {
                                cantidad = cantidad,
                                idCotizacion = cotizacionMod.id,
                                precio = precio,
                                total = subTotal,
                                igv = igvItem,
                                idProducto = Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["IDPRESENTACION"].Value.ToString()),
                                factor = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["FACTOR"].Value.ToString()),
                            };

                            cotizacionMod.total = total;
                            dpservice.crear(dpedido);

                            pService.editar(cotizacionMod);
                            gridDetalle.Rows[e.RowIndex].Cells["ID"].Value = dpedido.id;

                        }
                        else
                        {
                            DetalleCotizacion detalleMod = dpservice.buscar(Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
                            detalleMod.cantidad = cantidad;
                            detalleMod.igv = igvItem;
                            detalleMod.total = subTotal; cotizacionMod.fechaUpdate = DateTime.Now;
                            cotizacionMod.total = total;
                            dpservice.editar(detalleMod);

                            pService.editar(cotizacionMod);
                        }
                    }
                }
                else { MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value = precioAnterior; }
            }
            else { MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value = cantidadAnterior; }
        }

        private void gridDetalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex == 5)
            {
                gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].ReadOnly = false;
                gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Selected = true;
                gridDetalle.BeginEdit(true);
            }
            else if (e.ColumnIndex == 4)
            {
                gridDetalle.Rows[e.RowIndex].Cells[4].ReadOnly = false;//PRECIOOOO
                gridDetalle.Rows[e.RowIndex].Cells[4].Selected = true;
                gridDetalle.BeginEdit(true);
            }
        }

        private void comboCliente_SelectedValueChanged(object sender, EventArgs e)
        {
            Cliente c = csrvice.buscar((int)comboCliente.SelectedValue);
            if (c != null) { txtDNI.Text = c.dniRepresentante; txtDNI.Enabled = false; lblDocIdentidad.Text = c.ruc; }

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (comboTipoPago.SelectedValue != null) { 
                    int idCliente = 0;
                btnGuardar.Enabled = false;
                if (comboCliente.SelectedValue == null)
                {
                    if (Util.validaCadena(comboCliente.Text))
                    {
                        if (Util.esDNI(txtDNI.Text))
                        {
                            Cliente cli = new Cliente { direccion = "", email = "", dniRepresentante = "00000000", ruc = "", telefono = "", razonSocial = comboCliente.Text.ToUpper() };
                            Cliente existe = csrvice.buscarPorDNI(txtDNI.Text);
                            if (cli.dniRepresentante != "00000000")
                                if (existe != null)
                                { MessageBox.Show("DNI YA ESTA REGISTRADA"); btnGuardar.Enabled = true; return; }
                                else csrvice.crear(cli);
                            else { csrvice.crear(cli); }
                            idCliente = cli.id;
                        }
                        else { MessageBox.Show("DNI INCORRECTO"); btnGuardar.Enabled = true; return; }
                    }
                    else { MessageBox.Show("NOMBRE CLIENTE INCORRECTO"); btnGuardar.Enabled = true; return; }
                }
                else { idCliente = (int)comboCliente.SelectedValue; }



                if (cotizacionMod== null)
                {
                    Cotizacion p = new Cotizacion();
                    p.usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                    p.idCliente = idCliente;
                    p.total = Convert.ToDouble(lblTotal.Text);
                    p.igv = p.total - p.total / (1+igv.valorDouble);
                    if (panelCredito.Visible)
                    {
                        p.diasCredito = (int)(txtDias.Value);
                        p.porcentajeInteres = Convert.ToDouble(txtInteres.Value);
                    }
                    p.idTipoPago = (int)comboTipoPago.SelectedValue;
                    List<DetalleCotizacion> detalles = new List<DetalleCotizacion>();
                    DetalleCotizacion dp;
                    for (int i = 0; i < gridDetalle.Rows.Count; i++) 
                    {
                        dp = new DetalleCotizacion();
                        dp.cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString());
                        if (dp.cantidad <= 0)
                        {
                            MessageBox.Show("EL PRODUCTO: " + gridDetalle.Rows[i].Cells["PRODUCTO"].Value.ToString() + " NO ADMITE CANTIDAD 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon
        .Information); btnGuardar.Enabled = true; return;
                        }
                        dp.idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString());
                        dp.igv = Convert.ToDouble(gridDetalle.Rows[i].Cells["IGV"].Value.ToString());
                        dp.precio = Convert.ToDouble(gridDetalle.Rows[i].Cells["PRECIO"].Value.ToString());
                        dp.total = Convert.ToDouble(gridDetalle.Rows[i].Cells["TOTAL"].Value.ToString());
                        dp.factor = Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString());
                        detalles.Add(dp);
                    }
                    p.detalles = detalles;
                    if (pService.crear(p))
                    {
                            pasado(null);
                            this.Close();
                        btnGuardar.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("INTENTALO DE NUEVO", "ERROR INESPERADO", MessageBoxButtons.OK, MessageBoxIcon.Warning); btnGuardar.Enabled = true;
                    }
                }
            }

                else MessageBox.Show("NO EXISTE TIPO DE PAGO");
            }
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboCliente_TextChanged(object sender, EventArgs e)
        {
            if (comboCliente.SelectedValue == null)
            {
                txtDNI.Enabled = true; txtDNI.Text = "00000000";
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                double subtotal = Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString());
                tabla.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
                total = total - subtotal;
                lblTotal.Text = string.Format("{0:0.00}", total);
            }
        }

        private void btnObservacion_Click(object sender, EventArgs e)
        {
            gridDetalle.Rows[gridDetalle.SelectedRows[0].Index].Cells["OBSERVACION"].Value = txtObservacion.Text.ToUpper();
        }
    }
}
