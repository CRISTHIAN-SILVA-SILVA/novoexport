﻿using ERP_LITE_DESKTOP.RELACIONES;
using ERP_LITE_ESCRITORIO.PEDIDOS_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.INVENTARIO.JOIN;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PEDIDO_SERVICE;
using SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.PEDIDOS_FORM
{
    public partial class FormNuevoPedido : MaterialForm
    {
        Pedido mPedido = null;
        Producto prod;
        ClienteService csrvice = new ClienteImpl();
        DataTable tabla = new DataTable();
        DataTable tablaP = new DataTable();
        AlmacenService aService = new AlmacenImpl();
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        ProductoService proservice = new ProductoImpl();
        Almacen a;bool agrego = false;
        Double cantidadAnterior;
        double total = 0;
        PedidoService pService = new PedidoImpl();
        Parametro igv;
        DetallePedidoService dpservice = new DetallePedidoImpl();
        bool noSalir = false;
        Pedido pedidoModifica = null;
        double precioAnterior = 0;
        public FormNuevoPedido()
        {
            igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
            if (igv == null) this.Close();
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            tablaP.Columns.Add("ID");
            tablaP.Columns.Add("PRODUCTO");
            tablaP.Columns.Add("CANTIDAD");
            gridProducto.DataSource = tablaP;
            gridProducto.Columns[0].Visible = false;
            gridProducto.Columns[1].Width = 900;
            gridProducto.Columns[2].Width = 100;
            tabla.Columns.Add("ID");                    //00
            tabla.Columns.Add("IDPRODUCTO");            //01
            tabla.Columns.Add("IDPRESENTACION");        //02
            tabla.Columns.Add("PRODUCTO");              //03
            tabla.Columns.Add("PRECIO");                //04
            tabla.Columns.Add("CANTIDAD");              //05
            tabla.Columns.Add("IGV");                   //06
            tabla.Columns.Add("TOTAL");                 //07
            tabla.Columns.Add("FACTOR");                //08
            tabla.Columns.Add("OBSERVACION");           //09
            tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 540;
            gridDetalle.Columns[4].Width = 100;
            gridDetalle.Columns[5].Width = 100;
            gridDetalle.Columns[6].Width = 100;
            gridDetalle.Columns[7].Width = 100;

            gridDetalle.Columns[8].Visible = false;
            gridDetalle.Columns[9].Width = 120;
        }

        void llenarClientes(bool esEmpresa)
        {
            List<Cliente> clientes = csrvice.listarClientes();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            if (esEmpresa)
                clientes = csrvice.listarEmpresas();
            clientes.ForEach(c => coleccion.Add(c.razonSocial));
            comboCliente.AutoCompleteCustomSource = coleccion;
            comboCliente.DataSource = clientes;
        }
        private void FormPedidos_Load(object sender, EventArgs e)
        {
            this.Activate();
            comboCliente.Text = " ";
            comboCliente.Text = "";
            comboCliente.Focus();
            rbFactura.Checked = true;
            rbBoleta.Checked = true;
            a = aService.buscarPrincipal();
            if (a == null)
            {
                MessageBox.Show("NO EXISTE ALMACEN PRINCIPAL", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }

        }
        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e)
        {
            tablaP.Clear();
            if (Util.validaCadena(txtBuscarProducto.Text))
            {
                proservice.listarPorCadena(txtBuscarProducto.Text.ToUpper()).ForEach(x => tablaP.Rows.Add(x.id, x.nombre, x.stock));
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            txtBuscarProducto.Clear();
            total = 0;
            tabla.Clear();
            gridProducto.DataSource = null;
            mPedido = null;
            lblTotal.Text = "0.00";
            this.Text = "Pedido";
            txtPedido.Clear();
            txtCotizacion.Clear();
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (comboCliente.Text.Trim().Equals("")) { MessageBox.Show("CLIENTE INCORRECTO"); return; }
            if (gridDetalle.RowCount <= 0) { MessageBox.Show("FALTA DETALLE"); return; }
            int idCliente = 0;
            btnGuardar.Enabled = false;
            if (comboCliente.SelectedValue == null)
                if (rbBoleta.Checked)
                    if (Util.validaCadena(comboCliente.Text))
                        if (Util.esDNI(txtDNI.Text))
                        {
                            Cliente cli = new Cliente { direccion = "", email = "", dniRepresentante = "00000000", ruc = "", telefono = "", razonSocial = comboCliente.Text.ToUpper() };
                            Cliente existe = csrvice.buscarPorDNI(txtDNI.Text);
                            if (cli.dniRepresentante != "00000000")
                                if (existe != null)
                                { MessageBox.Show("DNI YA ESTA REGISTRADA"); btnGuardar.Enabled = true; return; }
                                else csrvice.crear(cli);
                            else { csrvice.crear(cli); }
                            idCliente = cli.id;
                        }
                        else { MessageBox.Show("DNI INCORRECTO"); btnGuardar.Enabled = true; return; }
                    else { MessageBox.Show("NOMBRE CLIENTE INCORRECTO"); btnGuardar.Enabled = true; return; }
                else { MessageBox.Show("CLIENTE INCORRECTO"); btnGuardar.Enabled = true; return; }
            else { idCliente = (int)comboCliente.SelectedValue; }

            if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                {
                    if (pedidoModifica == null)
                    {
                        Pedido p = new Pedido();
                        p.idVendedor = FormLogin.user.id;
                        p.usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                        p.idCliente = idCliente;
                        p.total = Convert.ToDouble(lblTotal.Text);
                        p.igv = p.total - p.total / (1 + igv.valorDouble);
                        p.esBoleta = rbBoleta.Checked;
                        List<DetallePedido> detalles = new List<DetallePedido>();
                        DetallePedido dp;
                        for (int i = 0; i < gridDetalle.Rows.Count; i++)
                        {
                            dp = new DetallePedido();
                            dp.cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString());
                            if (dp.cantidad <= 0)
                            {
                                MessageBox.Show("EL PRODUCTO: " + gridDetalle.Rows[i].Cells["PRODUCTO"].Value.ToString() + " NO ADMITE CANTIDAD 0", "AVISO", MessageBoxButtons.OK, MessageBoxIcon
                                .Information); btnGuardar.Enabled = true; return;
                            }
                            dp.idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString());
                            dp.igv = Convert.ToDouble(gridDetalle.Rows[i].Cells["IGV"].Value.ToString());
                            dp.precio = Convert.ToDouble(gridDetalle.Rows[i].Cells["PRECIO"].Value.ToString());
                            dp.total = Convert.ToDouble(gridDetalle.Rows[i].Cells["TOTAL"].Value.ToString());
                            dp.factor = Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString());
                            dp.observacion = gridDetalle.Rows[i].Cells["OBSERVACION"].Value.ToString();
                            detalles.Add(dp);
                        }
                        p.detalles = detalles;
                        if (pService.crear(p))
                        {
                            tabla.Clear();
                            txtBuscarProducto.Clear();
                            gridProducto.DataSource = null;
                            txtPedido.Clear();
                            txtCotizacion.Clear();
                            comboCliente.Text = "";
                            lblTotal.Text = "0.00";
                            MessageBox.Show("EL NUMERO DE PEDIDO ES:  " + p.numeroPedido, "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnGuardar.Enabled = true;
                        }
                        else { MessageBox.Show("INTENTALO DE NUEVO", "ERROR INESPERADO", MessageBoxButtons.OK, MessageBoxIcon.Warning); btnGuardar.Enabled = true; }
                    }
                    else
                    {
                        pedidoModifica.cliente = null;
                        pedidoModifica.total = Convert.ToDouble(lblTotal.Text);
                        pedidoModifica.idCliente = idCliente;
                        pedidoModifica.esBoleta = rbBoleta.Checked;
                        pService.editar(pedidoModifica);
                        tabla.Clear();
                        txtBuscarProducto.Clear();
                        gridProducto.DataSource = null;
                        txtPedido.Clear();
                        txtCotizacion.Clear();
                        lblTotal.Text = "0.0000";
                        btnGuardar.Enabled = true;
                        comboCliente.Text = "";
                        MessageBox.Show("PEDIDO MODIFICADO");
                    }
                }
            }
        }
        void llenarPedido(int id)
        {
            mPedido = pService.buscar(id);
            if (mPedido != null)
            {
                if (mPedido.esBoleta) { rbBoleta.Checked = true; } else rbFactura.Checked = true;
                comboCliente.SelectedValue = mPedido.idCliente;
                txtPedido.Text = mPedido.numeroPedido.ToString();
                tabla.Clear();
                ProductoPresentacion pp;
                foreach (var i in dpservice.listarPorPedido(mPedido.id))
                {
                    pp = ppService.buscar(i.idProducto);
                    tabla.Rows.Add(i.id, pp.idProducto, pp.id, pp.nombre, i.precio, i.cantidad, string.Format("{0:0.0000}", i.igv), string.Format("{0:0.0000}", i.total), i.factor, i.observacion);
                }
                total = mPedido.total;
                lblTotal.Text = string.Format("{0:0.0000}", total);
                pedidoModifica = mPedido;
            }
            txtBuscarProducto.Clear();
        }
        private void btnPedidos_Click(object sender, EventArgs e)
        {
            FormAdminPedidosDiarios form = new FormAdminPedidosDiarios();
            form.pasado += new FormAdminPedidosDiarios.pasar(llenarPedido);
            form.ShowDialog();
        }
        private void btnCotizaciones_Click(object sender, EventArgs e)
        {
        }
        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            Producto p = proservice.buscar(Convert.ToInt32(gridProducto.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            FormPresentaciones form = new FormPresentaciones(p);
            form.pasado += new FormPresentaciones.pasar(agregarItem);
            form.ShowDialog();

        }
        void agregarItem(ProductoPresentacion pp)
        {
           
            if (tabla.Rows.Find(pp.id.ToString()) == null)
                tabla.Rows.Add("", pp.idProducto, pp.id, pp.nombre, pp.precio, "0", "0", "0", pp.factor);

            agrego = true;

        }

        private void gridDetalle_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            cantidadAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
            precioAnterior = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
        }
        private void gridDetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString()))
            {
                if (Util.esDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString()))
                {
                    double cantidad = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value.ToString());
                    double precio = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["PRECIO"].Value.ToString());
                    double subTotal = cantidad * precio;
                    double totalAnterior = cantidadAnterior * precio;
                    Producto pr = proservice.buscar(Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["IDPRODUCTO"].Value.ToString()));
                    double igvItem = subTotal - subTotal / (1 + igv.valorDouble);
                    if (pr.esExonerado)
                        igvItem = 0;
                    gridDetalle.Rows[e.RowIndex].Cells["IGV"].Value = String.Format("{0:0.0000}", igvItem);
                    gridDetalle.Rows[e.RowIndex].Cells["TOTAL"].Value = String.Format("{0:0.0000}", subTotal);
                    total = total + (subTotal - cantidadAnterior * precioAnterior);
                    lblTotal.Text = string.Format("{0:0.0000}", total);
                    if (mPedido != null)
                    {
                        if (gridDetalle.Rows[e.RowIndex].Cells["ID"].Value.ToString().Equals(""))
                        {
                            DetallePedido dpedido = new DetallePedido
                            {
                                cantidad = cantidad,
                                idPedido = mPedido.id,
                                precio = precio,
                                total = subTotal,
                                igv = igvItem,
                                idProducto = Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["IDPRESENTACION"].Value.ToString()),
                                factor = Convert.ToDouble(gridDetalle.Rows[e.RowIndex].Cells["FACTOR"].Value.ToString()),
                            };
                            mPedido.fechaUpdate = DateTime.Now;
                            mPedido.total = total;
                            dpservice.crear(dpedido);
                            pService.editar(mPedido);
                            gridDetalle.Rows[e.RowIndex].Cells["ID"].Value = dpedido.id;
                        }
                        else
                        {
                            DetallePedido detalleMod = dpservice.buscar(Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
                            detalleMod.cantidad = cantidad;
                            detalleMod.igv = igvItem;
                            detalleMod.total = subTotal; mPedido.fechaUpdate = DateTime.Now;
                            mPedido.total = total;
                            dpservice.editar(detalleMod);
                            pService.editar(mPedido);
                        }
                    }
                }
                else { MessageBox.Show("FORMATO DE PRECIO INCORRECTO"); gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value = precioAnterior; }
            }
            else { MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Value = cantidadAnterior; }

        }
        private void gridDetalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (e.ColumnIndex == 5)
            {
                gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].ReadOnly = false;
                gridDetalle.Rows[e.RowIndex].Cells["CANTIDAD"].Selected = true;
                gridDetalle.BeginEdit(true);
            }
            else if (e.ColumnIndex == 4)
            {
                gridDetalle.Rows[e.RowIndex].Cells[4].ReadOnly = false;//PRECIOOOO
                gridDetalle.Rows[e.RowIndex].Cells[4].Selected = true;
                gridDetalle.BeginEdit(true);
            }
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                double subtotal = Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString());
                tabla.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
                total = total - subtotal;
                lblTotal.Text = string.Format("{0:0.00}", total);
            }
        }
        private void rbFactura_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFactura.Checked)
            {
                comboCliente.Focus();
                llenarClientes(true);
                comboCliente.Text = "";
                lblDocIdentidad.Text = "";
                btnAgregarCliente.Visible = true;
            }
        }
        private void rbBoleta_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBoleta.Checked)
            {
                btnAgregarCliente.Visible = false;
                comboCliente.Focus();
                llenarClientes(false);
                txtDNI.Text = "00000000";
                comboCliente.Text = "";
            }
        }
        private void comboCliente_SelectedValueChanged(object sender, EventArgs e)
        {
            Cliente c = csrvice.buscar((int)comboCliente.SelectedValue);
            if (c != null) if (rbBoleta.Checked) { txtDNI.Text = c.dniRepresentante; txtDNI.Enabled = false; } else lblDocIdentidad.Text = c.ruc; else this.Close();
        }
        private void comboCliente_TextUpdate(object sender, EventArgs e)
        {
            if (comboCliente.SelectedValue == null) { txtDNI.Enabled = true; txtDNI.Text = "00000000"; }
        }
        void agregarEmpresa(Cliente Empresa)
        {
            llenarClientes(true);
            comboCliente.SelectedValue = Empresa.id;
        }
        private void btnAgregarCliente_Click(object sender, EventArgs e)
        {
            FormNuevoCliente form = new FormNuevoCliente(null, 0, false);
            form.pasadoVenta += new FormNuevoCliente.pasarVenta(agregarEmpresa);
            form.ShowDialog();
        }

        private void btnObservacion_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                if (pedidoModifica != null)
                {
                    DetallePedido detalleMod = dpservice.buscar(Convert.ToInt32(gridDetalle.SelectedRows[0].Cells["ID"].Value.ToString()));
                    detalleMod.observacion = txtObservacion.Text;

                    dpservice.editar(detalleMod);
                }
                gridDetalle.Rows[gridDetalle.SelectedRows[0].Index].Cells["OBSERVACION"].Value = txtObservacion.Text.ToUpper();
            }
        }

        private void FormNuevoPedido_KeyUp(object sender, KeyEventArgs e)
        {
            //COMBINACIONES DE TECLAS
            if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.M))) btnPedidos_Click(null, null);
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.Z))) btnCotizaciones_Click(null, null);
         //   else if (e.KeyData == (Keys.Escape)&&!noSalir) this.Close();

            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.C))) comboCliente.Focus();
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.B))) rbBoleta.Checked = true;
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.F))) rbFactura.Checked = true;
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.O))) txtObservacion.Focus();
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.D)))
            { gridDetalle.Focus(); }
            // else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.D))) txtDNI.Focus();
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Convert.ToInt32(Keys.Insert))) txtBuscarProducto.Focus();
            //   else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Convert.ToInt32(Keys))) txtBuscarProducto.Focus();
            //    else if (e.KeyData == Keys.Enter) gridProducto.Focus();
            else if (Convert.ToInt32(e.KeyData) == Convert.ToInt32(Keys.Control + Convert.ToInt32(Keys.E)))
            {
                if(rbFactura.Checked)
                btnAgregarCliente_Click(null, null);
            }
        }
        
        private void gridProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (gridProducto.SelectedRows.Count == 1)
                if (e.KeyData == Keys.Enter)
                {
                    agrego = false;
                    e.SuppressKeyPress = true;
                    prod = proservice.buscar(Convert.ToInt32(gridProducto.Rows[gridProducto.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                    noSalir = true;
                    FormPresentaciones form = new FormPresentaciones(prod);
                    form.pasado += new FormPresentaciones.pasar(agregarItem);
                    form.ShowDialog();
                    gridDetalle.Focus();
                    if (gridDetalle.RowCount > 0 && agrego)
                    {
                        gridDetalle.CurrentCell = gridDetalle[5, gridDetalle.RowCount - 1];
                        gridDetalle_KeyDown(null, new KeyEventArgs(Keys.Enter));

                    }
                    else txtBuscarProducto.Focus();
                //    noSalir = false;
                }
              //  else if (e.KeyData == Keys.Back) txtBuscarProducto.Focus();
           
        }

        private void gridDetalle_KeyDown(object sender, KeyEventArgs e)
        {
            if (gridDetalle.RowCount> 0)
                if (e.KeyData == Keys.Enter)
                {
                  
                    e.SuppressKeyPress = true;
                    if(gridDetalle.CurrentCell!=null)

                        if (gridDetalle.CurrentCell.ColumnIndex == 5)
                        {
                            gridDetalle.Rows[gridDetalle.CurrentCell.RowIndex].Cells["CANTIDAD"].ReadOnly = false;
                            gridDetalle.BeginEdit(true);
                        }
                        else if (gridDetalle.CurrentCell.ColumnIndex == 4)
                        {
                            gridDetalle.Rows[gridDetalle.CurrentCell.RowIndex].Cells[4].ReadOnly = false;//PRECIOOOO
                            gridDetalle.BeginEdit(true);
                        }
                }
        }

        private void txtBuscarProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyData) == (Keys.Enter)) gridProducto.Focus(); 
        }
    }
}
