﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.PEDIDOS_FORM
{
    public partial class FormPresentaciones : MaterialForm
    {
        Producto producto;
        DataTable tabla = new DataTable();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        public delegate void pasar(ProductoPresentacion pp);
        public event pasar pasado;
        public FormPresentaciones(Producto p)
        {
            producto = p;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            tabla.Columns.Add("ID");
            tabla.Columns.Add("UNIDAD");
            tabla.Columns.Add("PRECIO");
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Width = 100;
            gridDetalle.Columns[1].Width = 210;
            gridDetalle.Columns[2].Width = 200;
            this.Text = producto.nombre;

        }

        private void FormPresentaciones_Load(object sender, EventArgs e)
        {  
            foreach (var x in ppservice.buscarPorProdcuto(producto.id)) {
                tabla.Rows.Add(x.id,x.unidad.nombre,x.precio);
            }
        }

        private void gridDetalle_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            ProductoPresentacion pp = ppservice.buscar(Convert.ToInt32(gridDetalle.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            this.Close(); pasado(pp);
        }
        private void gridDetalle_KeyDown(object sender, KeyEventArgs e)
        {
            if(gridDetalle.SelectedRows.Count == 1)
            {
                if (e.KeyData == Keys.Enter)
                {
                    ProductoPresentacion pp = ppservice.buscar(Convert.ToInt32(gridDetalle.Rows[gridDetalle.SelectedRows[0].Index].Cells["ID"].Value.ToString()));
                    pasado(pp);
                    this.Close();
                }
                if (e.KeyData == Keys.Escape) this.Close();
                
            }
        }

        private void FormPresentaciones_KeyDown(object sender, KeyEventArgs e)
        {
               }
    }
}
