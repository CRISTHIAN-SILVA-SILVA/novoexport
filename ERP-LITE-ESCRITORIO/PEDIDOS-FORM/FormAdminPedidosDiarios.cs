﻿using MaterialSkin;
using MaterialSkin.Controls;
using SERVICE_ERP_LITE.PEDIDO_SERVICE;
using SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.PEDIDOS_FORM
{
    public partial class FormAdminPedidosDiarios : MaterialForm
    {
        DataTable tabla = new DataTable();
        PedidoService pService = new PedidoImpl();
        public delegate void pasar(int id);
        public event pasar pasado;
        public FormAdminPedidosDiarios()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            tabla.Columns.Add("ID");
            tabla.Columns.Add("HORA");
            tabla.Columns.Add("CLIENTE");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("COTIZACION");
            tabla.Columns.Add("NUMERO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width =100;
            grid.Columns[2].Width = 500;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 100;
            grid.Columns[5].Width = 100;
        }

        private void FormAdminPedidosDiarios_Load(object sender, EventArgs e)
        {
            pService.listarDiarioPorVendedor(FormLogin.user.id).ForEach(x => tabla.Rows.Add(x.id,x.fechaCreate.ToShortTimeString(),x.cliente.razonSocial,String.Format("{0:0.00}",x.total),x.idCotizacion,x.numeroPedido));
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            pasado(Convert.ToInt32( grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            this.Close();
        }
    }
}
