﻿using MaterialSkin;
using MaterialSkin.Controls;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.PEDIDOS_FORM
{
    public partial class FormCantidadProducto : MaterialForm
    {
        public delegate void pasar(double precio, double cantidad,int fila,bool modifica);
        public event pasar pasado;
        int fila = 0;
        double precio = 0;
        string producto;
        bool modifica = false;
        public FormCantidadProducto(String p,double precio, int f,bool mod)
        {
         
            InitializeComponent();
            fila = f;
            producto = p;
            precio = precio;
            modifica = mod;
            txtPrecio.Text = String.Format("{0:0.00}", precio);
            txtProducto.Text = producto;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormCantidadProducto_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtCantidad.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            if (Util.esDouble(txtCantidad.Text))
                if (Convert.ToDouble(txtCantidad.Text) > 0) { 
                        if (Util.esDouble(txtPrecio.Text))
                            { pasado(Convert.ToDouble(txtPrecio.Text), Convert.ToDouble(txtCantidad.Text), fila,modifica); this.Close(); }
                        else MessageBox.Show("PRECIO INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else MessageBox.Show("CANTIDAD INCORRECTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            btnAgregar.Enabled = true;
        }
    }
}
