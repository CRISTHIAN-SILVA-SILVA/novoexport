﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormAgregarDetraccion : MaterialForm
    {
        public delegate void pasar(Compra c);
        public event pasar pasado;
        CompraService cservice = new CompraImpl();
        Compra compra = null;
        public FormAgregarDetraccion(Compra c)
        {
            compra = c;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

        }

        private void FormAgregarDetraccion_Load(object sender, EventArgs e)
        {

        }

        private void txtSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSeguir_Click(object sender, EventArgs e)
        {
            if (Util.validaCadena(txtSerie.Text))
                if (Util.esDouble(txtMonto.Text)) {
                    
                    compra.detraccion = Convert.ToDouble(txtMonto.Text);
                    cservice.modificaConRegistro(compra,txtSerie.Text,txtFecha.Value);
                    //MODIFICAR;
                    pasado(null);
                    this.Close();
                }
        }
    }
}
