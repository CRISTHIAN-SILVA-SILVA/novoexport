﻿namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    partial class FormEligenNotaCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTipoComprobante = new System.Windows.Forms.Label();
            this.lblCliente = new MaterialSkin.Controls.MaterialLabel();
            this.lblSerie = new MaterialSkin.Controls.MaterialLabel();
            this.txtSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnSeguir = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.txtMotivo = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.comboNota = new System.Windows.Forms.ComboBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.comboTipoNota = new System.Windows.Forms.ComboBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSerie = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblDebito = new MaterialSkin.Controls.MaterialLabel();
            this.txtDescuentoGlobal = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtEmision = new System.Windows.Forms.DateTimePicker();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblTipoComprobante);
            this.panel1.Controls.Add(this.lblCliente);
            this.panel1.Controls.Add(this.lblSerie);
            this.panel1.Location = new System.Drawing.Point(27, 88);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(660, 88);
            this.panel1.TabIndex = 184;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 23);
            this.label3.TabIndex = 160;
            this.label3.Text = "PROVEEDOR:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoComprobante.Location = new System.Drawing.Point(4, 0);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(653, 23);
            this.lblTipoComprobante.TabIndex = 157;
            this.lblTipoComprobante.Text = "COMPROBANTE ELECTRONICO";
            this.lblTipoComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCliente
            // 
            this.lblCliente.Depth = 0;
            this.lblCliente.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCliente.Location = new System.Drawing.Point(131, 59);
            this.lblCliente.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(529, 19);
            this.lblCliente.TabIndex = 161;
            this.lblCliente.Text = "SERIE: FE01";
            this.lblCliente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSerie
            // 
            this.lblSerie.Depth = 0;
            this.lblSerie.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblSerie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblSerie.Location = new System.Drawing.Point(4, 23);
            this.lblSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblSerie.Name = "lblSerie";
            this.lblSerie.Size = new System.Drawing.Size(653, 19);
            this.lblSerie.TabIndex = 147;
            this.lblSerie.Text = "SERIE: FE01";
            this.lblSerie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSalir
            // 
            this.txtSalir.AutoSize = true;
            this.txtSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.txtSalir.Depth = 0;
            this.txtSalir.Icon = null;
            this.txtSalir.Location = new System.Drawing.Point(558, 347);
            this.txtSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSalir.Name = "txtSalir";
            this.txtSalir.Primary = false;
            this.txtSalir.Size = new System.Drawing.Size(58, 36);
            this.txtSalir.TabIndex = 183;
            this.txtSalir.Text = "salir";
            this.txtSalir.UseVisualStyleBackColor = true;
            this.txtSalir.Click += new System.EventHandler(this.txtSalir_Click);
            // 
            // btnSeguir
            // 
            this.btnSeguir.AutoSize = true;
            this.btnSeguir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSeguir.Depth = 0;
            this.btnSeguir.Icon = null;
            this.btnSeguir.Location = new System.Drawing.Point(623, 347);
            this.btnSeguir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSeguir.Name = "btnSeguir";
            this.btnSeguir.Primary = true;
            this.btnSeguir.Size = new System.Drawing.Size(65, 36);
            this.btnSeguir.TabIndex = 182;
            this.btnSeguir.Text = "nueva";
            this.btnSeguir.UseVisualStyleBackColor = true;
            this.btnSeguir.Click += new System.EventHandler(this.btnSeguir_Click);
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(31, 322);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(60, 19);
            this.materialLabel3.TabIndex = 181;
            this.materialLabel3.Text = "Motivo:";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Depth = 0;
            this.txtMotivo.Hint = "";
            this.txtMotivo.Location = new System.Drawing.Point(117, 318);
            this.txtMotivo.MaxLength = 32767;
            this.txtMotivo.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.PasswordChar = '\0';
            this.txtMotivo.SelectedText = "";
            this.txtMotivo.SelectionLength = 0;
            this.txtMotivo.SelectionStart = 0;
            this.txtMotivo.Size = new System.Drawing.Size(571, 23);
            this.txtMotivo.TabIndex = 180;
            this.txtMotivo.TabStop = false;
            this.txtMotivo.UseSystemPasswordChar = false;
            // 
            // comboNota
            // 
            this.comboNota.DisplayMember = "descripcion";
            this.comboNota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboNota.FormattingEnabled = true;
            this.comboNota.Items.AddRange(new object[] {
            "NOTA DE CREDITO",
            "NOTA DE DEBITO"});
            this.comboNota.Location = new System.Drawing.Point(117, 258);
            this.comboNota.Name = "comboNota";
            this.comboNota.Size = new System.Drawing.Size(571, 24);
            this.comboNota.TabIndex = 179;
            this.comboNota.ValueMember = "id";
            this.comboNota.SelectedIndexChanged += new System.EventHandler(this.comboNota_SelectedIndexChanged);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(31, 293);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(80, 19);
            this.materialLabel2.TabIndex = 178;
            this.materialLabel2.Text = "Tipo Nota:";
            // 
            // comboTipoNota
            // 
            this.comboTipoNota.DisplayMember = "descripcion";
            this.comboTipoNota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipoNota.FormattingEnabled = true;
            this.comboTipoNota.Location = new System.Drawing.Point(117, 288);
            this.comboTipoNota.Name = "comboTipoNota";
            this.comboTipoNota.Size = new System.Drawing.Size(571, 24);
            this.comboTipoNota.TabIndex = 177;
            this.comboTipoNota.ValueMember = "id";
            this.comboTipoNota.SelectedIndexChanged += new System.EventHandler(this.comboTipoNota_SelectedIndexChanged);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(31, 263);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(46, 19);
            this.materialLabel1.TabIndex = 176;
            this.materialLabel1.Text = "Nota:";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(31, 233);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(47, 19);
            this.materialLabel5.TabIndex = 192;
            this.materialLabel5.Text = "Serie:";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(376, 229);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(67, 19);
            this.materialLabel4.TabIndex = 191;
            this.materialLabel4.Text = "Numero:";
            // 
            // txtNumero
            // 
            this.txtNumero.Depth = 0;
            this.txtNumero.Hint = "";
            this.txtNumero.Location = new System.Drawing.Point(449, 229);
            this.txtNumero.MaxLength = 8;
            this.txtNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.Size = new System.Drawing.Size(239, 23);
            this.txtNumero.TabIndex = 190;
            this.txtNumero.TabStop = false;
            this.txtNumero.UseSystemPasswordChar = false;
            // 
            // txtSerie
            // 
            this.txtSerie.Depth = 0;
            this.txtSerie.Hint = "";
            this.txtSerie.Location = new System.Drawing.Point(117, 229);
            this.txtSerie.MaxLength = 4;
            this.txtSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.PasswordChar = '\0';
            this.txtSerie.SelectedText = "";
            this.txtSerie.SelectionLength = 0;
            this.txtSerie.SelectionStart = 0;
            this.txtSerie.Size = new System.Drawing.Size(202, 23);
            this.txtSerie.TabIndex = 189;
            this.txtSerie.TabStop = false;
            this.txtSerie.UseSystemPasswordChar = false;
            // 
            // lblDebito
            // 
            this.lblDebito.AutoSize = true;
            this.lblDebito.Depth = 0;
            this.lblDebito.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblDebito.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDebito.Location = new System.Drawing.Point(159, 359);
            this.lblDebito.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblDebito.Name = "lblDebito";
            this.lblDebito.Size = new System.Drawing.Size(159, 19);
            this.lblDebito.TabIndex = 193;
            this.lblDebito.Text = "DESCUENTO GLOBAL:";
            this.lblDebito.Visible = false;
            // 
            // txtDescuentoGlobal
            // 
            this.txtDescuentoGlobal.Depth = 0;
            this.txtDescuentoGlobal.Hint = "";
            this.txtDescuentoGlobal.Location = new System.Drawing.Point(331, 355);
            this.txtDescuentoGlobal.MaxLength = 8;
            this.txtDescuentoGlobal.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDescuentoGlobal.Name = "txtDescuentoGlobal";
            this.txtDescuentoGlobal.PasswordChar = '\0';
            this.txtDescuentoGlobal.SelectedText = "";
            this.txtDescuentoGlobal.SelectionLength = 0;
            this.txtDescuentoGlobal.SelectionStart = 0;
            this.txtDescuentoGlobal.Size = new System.Drawing.Size(157, 23);
            this.txtDescuentoGlobal.TabIndex = 194;
            this.txtDescuentoGlobal.TabStop = false;
            this.txtDescuentoGlobal.UseSystemPasswordChar = false;
            this.txtDescuentoGlobal.Visible = false;
            // 
            // txtEmision
            // 
            this.txtEmision.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmision.Location = new System.Drawing.Point(287, 187);
            this.txtEmision.Name = "txtEmision";
            this.txtEmision.Size = new System.Drawing.Size(263, 24);
            this.txtEmision.TabIndex = 195;
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(156, 192);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(125, 19);
            this.materialLabel6.TabIndex = 196;
            this.materialLabel6.Text = "FECHA EMISION:";
            this.materialLabel6.Visible = false;
            // 
            // FormEligenNotaCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 395);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.txtEmision);
            this.Controls.Add(this.txtDescuentoGlobal);
            this.Controls.Add(this.lblDebito);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.txtSalir);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.btnSeguir);
            this.Controls.Add(this.txtSerie);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.txtMotivo);
            this.Controls.Add(this.comboNota);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.comboTipoNota);
            this.Controls.Add(this.materialLabel1);
            this.MaximizeBox = false;
            this.Name = "FormEligenNotaCompra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Eligen Nota Compra";
            this.Load += new System.EventHandler(this.FormEligenNotaCompra_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTipoComprobante;
        private MaterialSkin.Controls.MaterialFlatButton txtSalir;
        private MaterialSkin.Controls.MaterialRaisedButton btnSeguir;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMotivo;
        private System.Windows.Forms.ComboBox comboNota;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.ComboBox comboTipoNota;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel lblSerie;
        private MaterialSkin.Controls.MaterialLabel lblCliente;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumero;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSerie;
        private MaterialSkin.Controls.MaterialLabel lblDebito;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDescuentoGlobal;
        private System.Windows.Forms.DateTimePicker txtEmision;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
    }
}