﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormNuevaNotaCItem : MaterialForm
    {
        public delegate void pasar();
        public event pasar pasado;
        Compra compra = null;
        NotaCompraService nservice = new NotaCompraImpl();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        TipoComprobantePago tcpNota;
        DataTable tablaN = new DataTable();
        ProductoService pservice = new ProductoImpl();
        DataTable tabla = new DataTable();
        double totalExonerado = 0;
        TipoNotaImpl tnservice = new TipoNotaImpl();
        TipoNota tipoNotaO=null;
        bool esDevolucion = false, esDisminucion = false, esAumento = false, esDescuento = false;
        NotaCompra nota;
        Parametro igvP = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        double totalN = 0;
        public FormNuevaNotaCItem(NotaCompra n,Compra c,TipoComprobantePago tcNota,int idTipoNota,bool esDetalle)
        {
            InitializeComponent();
            nota = n;
            compra = c;
            tcpNota = tcNota;
            tipoNotaO = tnservice.buscar(idTipoNota);
            if (tipoNotaO == null) this.Close();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("PRECIO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("DESCUENTO");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("FACTOR");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Visible = false;
            grid.Columns[2].Visible = false;
            grid.Columns[3].Width = 550;
            grid.Columns[4].Width = 80;
            grid.Columns[5].Width = 80;
            grid.Columns[6].Width = 80;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 80;
            grid.Columns[9].Visible = false;
            tablaN.Columns.Add("ID");
            tablaN.Columns.Add("IDPRODUCTO");
            tablaN.Columns.Add("IDPRESENTACION");
            tablaN.Columns.Add("PRODUCTO");
            tablaN.Columns.Add("VALOR U.");
            tablaN.Columns.Add("CANTIDAD");
            tablaN.Columns.Add("IGV");
            tablaN.Columns.Add("TOTAL");
            tablaN.Columns.Add("FACTOR");
            tablaN.PrimaryKey = new DataColumn[] { tablaN.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tablaN;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 550;
            gridDetalle.Columns[4].Width = 100;
            gridDetalle.Columns[5].Width = 100;
            gridDetalle.Columns[6].Width = 100;
            gridDetalle.Columns[7].Width = 100; 
            gridDetalle.Columns[8].Visible = false;

            if (esDetalle) {
                btnAplicar.Visible = false;
                txtCambio.Visible = false;
                lblCambio.Visible = false;
                grid.Visible = false;
                lblSerieNota.Text = nota.serie + "-" + nota.numero;
                lblIGV.Text = nota.igv.ToString();
                btnGuardar.Visible = false;
                btnEliminar.Visible = false;
                lblTotal.Text = nota.total.ToString();
                lblExonerado.Text = nota.totalExonerado.ToString();
                Producto p;
                foreach (var i in nota.detalle) {
                    p = pservice.buscar(i.idProducto);
                    tablaN.Rows.Add
                        (i.id,i.idProducto,i.idPresentacion,p.nombre,i.valorUnitario,i.cantidad,i.igv,i.total,i.factor  );
                }
            }
        }
        private void btnAplicar_Click(object sender, EventArgs e){
            if (grid.SelectedRows.Count == 1) {
                if (Util.esDouble(txtCambio.Text))
                {
                    double cambio = Convert.ToDouble(txtCambio.Text);
                    double valorU = Convert.ToDouble(grid.SelectedRows[0].Cells["PRECIO"].Value.ToString())/(1+igvP.valorDouble),
                        total = 0, 
                        igv = Convert.ToDouble(grid.SelectedRows[0].Cells["IGV"].Value.ToString()),
                        cantidad= Convert.ToDouble(grid.SelectedRows[0].Cells["CANTIDAD"].Value.ToString());
                    double precio = Convert.ToDouble(grid.SelectedRows[0].Cells["PRECIO"].Value.ToString());


                    if (esDevolucion) { cantidad = cambio; total = cantidad * precio; if(igv!=0) igv =total - total / (1 + igvP.valorDouble); }

                    if (esDisminucion) {valorU = cambio;if (igv != 0) igv = valorU * cantidad * igvP.valorDouble;  total = valorU * cantidad+igv;  }
                    if (esAumento) { valorU = cambio; if (igv != 0) igv = valorU * cantidad * igvP.valorDouble; total = valorU * cantidad + igv; }  
                    if (esDescuento) { total = cambio;cantidad = 1; if (igv != 0) { igv = total - total / (1 + igvP.valorDouble); valorU = total/(1+igvP.valorDouble); }else valorU = cambio; }

                    tablaN.Rows.Add(grid.SelectedRows[0].Cells["ID"].Value.ToString(),
                        grid.SelectedRows[0].Cells["IDPRODUCTO"].Value.ToString()
                      , grid.SelectedRows[0].Cells["IDPRESENTACION"].Value.ToString(),
                        grid.SelectedRows[0].Cells["PRODUCTO"].Value.ToString()
                      , valorU
                      , cantidad
                      , igv
                      , total
                      , grid.SelectedRows[0].Cells["FACTOR"].Value.ToString());
                    totalN += total;
                    if (igv == 0) { totalExonerado += total; }
                    lblTotal.Text = string.Format("{0:0.0000}",totalN);
                }
                else MessageBox.Show("MONTO INCORRECTO");
            }
            
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1) {
              
                double resta = Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString());
            
                totalN -= resta;
                if (Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["TOTAL"].Value.ToString()) == 0) 
                    totalExonerado -= resta;
                tablaN.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
                lblTotal.Text = string.Format("{0:0.0000}", totalN);

            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;
            if (MessageBox.Show("SEGURO DE GUARDAR?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes) {
                if (grid.Rows.Count > 0)
                {
                    nota.igv = Convert.ToDouble(lblIGV.Text);
                    nota.opGravadas = Convert.ToDouble(lblOpGravadas.Text);
                    nota.total = Convert.ToDouble(lblTotal.Text);
                    nota.totalExonerado = Convert.ToDouble(lblExonerado.Text);
                    DetalleNotaCompra d;
                    List<DetalleNotaCompra> detalle = new List<DetalleNotaCompra>();
                    for (int i = 0; i < gridDetalle.Rows.Count; i++)
                    {
                        d = new DetalleNotaCompra
                        {
                            cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString()),
                            factor = Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString()),
                            igv = Convert.ToDouble(gridDetalle.Rows[i].Cells["IGV"].Value.ToString()),
                            total = Convert.ToDouble(gridDetalle.Rows[i].Cells["TOTAL"].Value.ToString()),
                            valorUnitario = Convert.ToDouble(gridDetalle.Rows[i].Cells["VALOR U."].Value.ToString()),
                            idPresentacion = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString()),
                            idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRODUCTO"].Value.ToString()),
                        };
                        detalle.Add(d);
                        nota.detalle = detalle;
                        if (nservice.guardarNotaCompleta(nota, false, esDisminucion, esDescuento, esAumento, esDevolucion, false, false))
                        {
                            this.Close();pasado();
                        }
                        else MessageBox.Show("ERRROR INESPERADO VUELVA HA INTENTARLO");
                    }
                }
                else MessageBox.Show("DETALLE VACIO");

            }
            btnGuardar.Enabled = true;
        }

        private void lblTotal_TextChanged(object sender, EventArgs e)
        {
            lblExonerado.Text = string.Format("{0:0.0000}", totalExonerado);
            double opGrabadas = (totalN-totalExonerado)/(1+igvP.valorDouble);
            lblIGV.Text = string.Format("{0:0.0000}",( totalN-totalExonerado)-opGrabadas);
            lblOpGravadas.Text = string.Format("{0:0.0000}", opGrabadas);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();  
        }

        void llenar(){
            tabla.Clear();
            ProductoPresentacion pp;
            foreach (var i in compra.detallesCompra) {
                pp = ppservice.buscar(i.idPresentacion);
                tabla.Rows.Add(i.id,i.idProducto,i.idPresentacion,pp.nombre,i.costo,i.cantidad,i.descuento,string.Format("{0:0.00}",i.igv), string.Format("{0:0.00}", i.total),i.factor);

            }
        }
        void habilitar(bool habilitar) {
            btnAplicar.Visible = habilitar;
            lblCambio.Visible = habilitar;
            txtCambio.Visible = habilitar;
        }
        private void FormNuevaNotaCItem_Load(object sender, EventArgs e)
        {
            llenar();
            lblEmpresa.Text = LocalImpl.getInstancia().razonSocial;
            lblDireccionSede.Text = LocalImpl.getInstancia().direccion;
            lblRucEmpresa.Text = LocalImpl.getInstancia().ruc;

            lblCliente.Text = compra.proveedor.razonSocial;
            lblSerieNumero.Text = compra.serie+ "-" + Util.NormalizarCampo(compra.numero.ToString(),8);
            lblTipoComprobante.Text = tcpNota.descripcion.ToUpper() ;
            lblTipoNota.Text = tipoNotaO.descripcion;
            lblSerieNota.Text = nota.serie + "-" +Util.NormalizarCampo( nota.numero.ToString(),8);

            if (tipoNotaO.descripcion.Equals("DESCUENTO_POR_ITEM")) {
                lblCambio.Text = "DESCUENTO ITEM:";
                esDescuento = true;
            }
            else if (tipoNotaO.descripcion.Equals("DEVOLUCION_POR_ITEM")) {
                lblCambio.Text = "CANTIDAD DEVUELTA:";
                esDevolucion = true;

            }
            else if (tipoNotaO.descripcion.Equals("DISMINUCION_EN_EL_VALOR")) {
                lblCambio.Text = "DISMINUCION VALOR:";
                esDisminucion = true;
            }
            else if (tipoNotaO.descripcion.Equals("AUMENTO_DE_VALOR")) {
                lblCambio.Text = "AUMENTO VALOR:";
                esAumento = true;
                nota.esCredito = false;
                
            }
        }
    }
}
