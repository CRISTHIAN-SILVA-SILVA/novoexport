﻿namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    partial class FormAgregarDetraccion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.txtSerie = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtMonto = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtSalir = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnSeguir = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtFecha = new System.Windows.Forms.DateTimePicker();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(32, 92);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(138, 19);
            this.materialLabel5.TabIndex = 194;
            this.materialLabel5.Text = "NUMERO CUENTA:";
            // 
            // txtSerie
            // 
            this.txtSerie.Depth = 0;
            this.txtSerie.Hint = "";
            this.txtSerie.Location = new System.Drawing.Point(176, 88);
            this.txtSerie.MaxLength = 15;
            this.txtSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.PasswordChar = '\0';
            this.txtSerie.SelectedText = "";
            this.txtSerie.SelectionLength = 0;
            this.txtSerie.SelectionStart = 0;
            this.txtSerie.Size = new System.Drawing.Size(257, 23);
            this.txtSerie.TabIndex = 193;
            this.txtSerie.TabStop = false;
            this.txtSerie.UseSystemPasswordChar = false;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(104, 132);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(66, 19);
            this.materialLabel1.TabIndex = 196;
            this.materialLabel1.Text = "MONTO:";
            // 
            // txtMonto
            // 
            this.txtMonto.Depth = 0;
            this.txtMonto.Hint = "";
            this.txtMonto.Location = new System.Drawing.Point(176, 132);
            this.txtMonto.MaxLength = 4;
            this.txtMonto.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.PasswordChar = '\0';
            this.txtMonto.SelectedText = "";
            this.txtMonto.SelectionLength = 0;
            this.txtMonto.SelectionStart = 0;
            this.txtMonto.Size = new System.Drawing.Size(257, 23);
            this.txtMonto.TabIndex = 195;
            this.txtMonto.TabStop = false;
            this.txtMonto.UseSystemPasswordChar = false;
            // 
            // txtSalir
            // 
            this.txtSalir.AutoSize = true;
            this.txtSalir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.txtSalir.Depth = 0;
            this.txtSalir.Icon = null;
            this.txtSalir.Location = new System.Drawing.Point(284, 211);
            this.txtSalir.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtSalir.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSalir.Name = "txtSalir";
            this.txtSalir.Primary = false;
            this.txtSalir.Size = new System.Drawing.Size(58, 36);
            this.txtSalir.TabIndex = 198;
            this.txtSalir.Text = "salir";
            this.txtSalir.UseVisualStyleBackColor = true;
            this.txtSalir.Click += new System.EventHandler(this.txtSalir_Click);
            // 
            // btnSeguir
            // 
            this.btnSeguir.AutoSize = true;
            this.btnSeguir.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSeguir.Depth = 0;
            this.btnSeguir.Icon = null;
            this.btnSeguir.Location = new System.Drawing.Point(349, 211);
            this.btnSeguir.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSeguir.Name = "btnSeguir";
            this.btnSeguir.Primary = true;
            this.btnSeguir.Size = new System.Drawing.Size(84, 36);
            this.btnSeguir.TabIndex = 197;
            this.btnSeguir.Text = "Guardar";
            this.btnSeguir.UseVisualStyleBackColor = true;
            this.btnSeguir.Click += new System.EventHandler(this.btnSeguir_Click);
            // 
            // txtFecha
            // 
            this.txtFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.Location = new System.Drawing.Point(176, 170);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(257, 24);
            this.txtFecha.TabIndex = 199;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(66, 170);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(104, 19);
            this.materialLabel2.TabIndex = 200;
            this.materialLabel2.Text = "FECHA PAGO:";
            // 
            // FormAgregarDetraccion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 262);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.txtFecha);
            this.Controls.Add(this.txtSalir);
            this.Controls.Add(this.btnSeguir);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.txtMonto);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.txtSerie);
            this.MaximizeBox = false;
            this.Name = "FormAgregarDetraccion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAgregarDetraccion";
            this.Load += new System.EventHandler(this.FormAgregarDetraccion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSerie;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMonto;
        private MaterialSkin.Controls.MaterialFlatButton txtSalir;
        private MaterialSkin.Controls.MaterialRaisedButton btnSeguir;
        private System.Windows.Forms.DateTimePicker txtFecha;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
    }
}