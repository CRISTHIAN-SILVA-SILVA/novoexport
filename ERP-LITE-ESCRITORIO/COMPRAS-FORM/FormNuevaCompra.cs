﻿using ERP_LITE_DESKTOP;  
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM{
    public partial class FormNuevaCompra : MaterialForm {
        ProveedorService pvservice = new ProveedorImpl();
        CompraService cservice = new CompraImpl();
        DataTable tabla = new DataTable(); 
        TipoPagoService tpService = new TipoPagoImpl();
        DetalleCompraImpl dservice = new DetalleCompraImpl();
        int indexCambioProducto = 0;
        MedioPagoService mpService = new MedioPagoImpl();
        DetalleCompraImpl dcservice = new DetalleCompraImpl();
        public delegate void pasar(Compra compra);
        public event pasar pasado;
        RegistroCompraImpl rcservice = new RegistroCompraImpl();
        TransportistaService tservice = new TransportistaImpl();
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        Almacen a;
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        AlmacenService aService = new AlmacenImpl();
        double total = 0,exonerado=0;
        GuiaCompraImpl gcservice = new GuiaCompraImpl();
        Compra compraModifica = null;
        bool primerIndexMoneda = true;
        public FormNuevaCompra(Compra mod){
            compraModifica = mod;
            InitializeComponent();
            if (igv == null) { this.Close(); MessageBox.Show("PARAMETRO IGV NO EXISTE"); }
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
            List<Proveedor> proveedores = pvservice.listarNoAnulados();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            proveedores.ForEach(c => coleccion.Add(c.razonSocial));
            comboProveedor.AutoCompleteCustomSource = coleccion;
            comboProveedor.DataSource = proveedores;
            List<Transportista> transportistas = tservice.listarNoAnulados();
            AutoCompleteStringCollection colec = new AutoCompleteStringCollection();
            transportistas.ForEach(c => colec.Add(c.razonSocial));
            comboTransporte.AutoCompleteCustomSource = colec;
            comboTransporte.DataSource = transportistas;
            comboMedioPago.DataSource = mpService.listarNoAnulados();
            comboTipoPago.DataSource = tpService.listarNoAnulados();
            a = aService.buscarPrincipal();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("IDPRODUCTO");
            tabla.Columns.Add("IDPRESENTACION");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("COSTO");
            tabla.Columns.Add("CANTIDAD");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("DESCUENTO");
            tabla.Columns.Add("DESC(%)");
            tabla.Columns.Add("VALOR TOTAL");
            tabla.Columns.Add("IMPORTE");
            tabla.Columns.Add("FACTOR");
            tabla.Columns.Add("ES_EXONERADO");
      //      tabla.PrimaryKey = new DataColumn[] { tabla.Columns["IDPRESENTACION"] };
            gridDetalle.DataSource = tabla;
            gridDetalle.Columns[0].Visible = false;
            gridDetalle.Columns[1].Visible = false;
            gridDetalle.Columns[2].Visible = false;
            gridDetalle.Columns[3].Width = 365;
            gridDetalle.Columns[4].Width = 80;
            gridDetalle.Columns[5].Width = 80;
            gridDetalle.Columns[6].Width = 80;
            gridDetalle.Columns[7].Width = 90;
            gridDetalle.Columns[8].Width = 80;
            gridDetalle.Columns[9].Width = 80;
            gridDetalle.Columns[10].Width = 80;
            gridDetalle.Columns[11].Visible = false;
            gridDetalle.Columns[12].Visible = false;       
            ThreadStart delegarProceso = new ThreadStart(() => { CambioMoneda.getTablaDolar(); });
            Thread subproceso = new Thread(delegarProceso);
            subproceso.Start();  }
        void llenarDetalle() {
            double desP = 0;
            if(compraModifica!=null)

            foreach (var x in compraModifica.detallesCompra) {
                    if (x.descuento == 0) { desP = 0; } else desP = (x.descuento / (x.total + x.descuento)) * 100;
                    ProductoPresentacion pp = ppService.buscar(x.idPresentacion) ;
                    tabla.Rows.Add(x.id,x.idProducto,x.idPresentacion,pp.nombre,x.costo,x.cantidad,x.igv,x.descuento,desP,
                       x.total / (1 + igv.valorDouble), x.total,x.factor,pp.producto.esExonerado);
                    total += x.total;
                    if (x.igv == 0) {
                        exonerado += total; } }
            lblTotal.Text = string.Format("{0:0.0000}",total);
            lblOpExoneradas.Text = string.Format("{0:0.0000}",exonerado);  }
        private void FormNuevaCompra_Load(object sender, EventArgs e) {
            if (compraModifica != null)
            {
                RegistroCompra rc = rcservice.buscarPorCompra(compraModifica.id);
                if (rc == null) checkNoEsFactura.Checked=true; else checkNoEsFactura.Checked = false;
                checkNoEsFactura.Enabled = false;
                btnSobrante.Visible = true;
                comboMedioPago.Enabled = false;
                comboTipoPago.Enabled = false;
                btnEliminar.Visible = false;

                txtBuscarProducto.Enabled = false;
                GuiaCompra g = gcservice.buscarPorCompra(compraModifica.id);
                if (g != null)
                {
                    comboTransporte.SelectedValue = g.idTranporte;
                    txtGuia.Text = g.numero;
                    checkGuia.Checked = true;
                }
                else { checkGuia.Checked = false; }
                checkGuia.Enabled = false;
                llenarDetalle();
                txtSerieNumero.Text = compraModifica.serie;
                txtNumero.Text = compraModifica.numero.ToString();
                txtInicio.Value = compraModifica.fechaEmision;
                txtFin.Value = compraModifica.fechaVencimiento;
                comboTipoPago.SelectedValue = compraModifica.tipoPago.id;
                comboProveedor.SelectedValue = compraModifica.proveedor.id;
                comboMoneda.SelectedValue = compraModifica.tipoMoneda.id;
                comboMedioPago.SelectedValue = compraModifica.medioPago.id;
                lblCambio.Text = string.Format("{0:0.000}", compraModifica.cambio);
                if (compraModifica.tipoMoneda.descripcion.Equals("DOLARES"))
                {
                    comboMoneda.SelectedIndex = 1;
                    primerIndexMoneda = false;
                }
                else comboMoneda.SelectedIndex = 0;
            
            }
            else { comboMoneda.SelectedIndex = 0; primerIndexMoneda = false; }
            
            this.Activate(); }
        private void btnClose_Click(object sender, EventArgs e) {this.Close();}
        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e){
            try
            {
                gridProducto.DataSource = null;
                if (Util.validaCadena(txtBuscarProducto.Text))
                {
                    gridProducto.DataSource = ppService.buscarPresentacionesPorAlmacen(a.id, txtBuscarProducto.Text.ToUpper());
                    gridProducto.Columns[0].Visible = false;
                    gridProducto.Columns[1].Visible = false;
                    gridProducto.Columns[2].Width = 690;
                    gridProducto.Columns[3].Width = 140;
                    gridProducto.Columns[4].Width = 100; gridProducto.Columns[5].Visible = false; gridProducto.Columns[6].Visible = false;
                }
            }
            catch (Exception k) { MessageBox.Show(k.Message + "  " + k.StackTrace + "    " + e.ToString()); }
        }
        private void comboTipoPago_SelectedValueChanged(object sender, EventArgs e)  {
            if (comboTipoPago.Text == "PAGO UNICO") { comboMedioPago.Enabled = true;  } else { comboMedioPago.Enabled = false; }}
        private void btnEliminar_Click(object sender, EventArgs e) {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                if (compraModifica != null)
                {
                    MessageBox.Show("OPERACION NO DISPONIBLE");
                }
                else
                {
                    total -= Convert.ToInt32(gridDetalle.SelectedRows[0].Cells["IMPORTE"].Value.ToString());
                    lblTotal.Text = string.Format("{0:0.0000}", total);
                    if (Convert.ToDouble(gridDetalle.SelectedRows[0].Cells["IGV"].Value.ToString()) == 0)
                    {
                        exonerado -= Convert.ToInt32(gridDetalle.SelectedRows[0].Cells["IMPORTE"].Value.ToString());
                        lblOpExoneradas.Text = string.Format("{0:0.0000}", exonerado);
                    }
                    tabla.Rows.RemoveAt(gridDetalle.SelectedRows[0].Index);
                }
            } }
        void cambio(double cambio) { lblCambio.Text = string.Format("{0:0.0000}",cambio); }
        private void comboMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!primerIndexMoneda)
            {
                if (comboMoneda.SelectedIndex == 1)
                {
                    Form2 form = new Form2();
                    form.pasado += new Form2.pasar(cambio);
                    form.ShowDialog();
                }
                else lblCambio.Text = "1.0000";
            }
        }
    private void btnCompleto_Click(object sender, EventArgs e) {
            if (MessageBox.Show("SEGURO DE GUARDAR?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                btnCompleto.Enabled = false;
                if (gridDetalle.RowCount > 0)
                    if (comboProveedor.SelectedValue != null && Util.validaCadena(comboProveedor.Text))
                        if (comboTipoPago.SelectedValue != null)
                            if (comboMedioPago.SelectedValue != null)
                            {
                                Compra compra = new Compra
                                {
                                    cambio = Convert.ToDouble(lblCambio.Text),
                                    descuentoGlobal = Convert.ToDouble(txtDedGlobal.Text),
                                    fechaEmision = txtInicio.Value,
                                    fechaVencimiento = txtFin.Value,
                                    idMoneda = 180 + comboMoneda.SelectedIndex,
                                    idMedioPago = (int)comboMedioPago.SelectedValue,
                                    idProveedor = (int)comboProveedor.SelectedValue,
                                    igv = Convert.ToDouble(lblIGV.Text),
                                    idTipoPago = (int)comboTipoPago.SelectedValue,
                                    montoTotal = Convert.ToDouble(lblTotal.Text) - Convert.ToDouble(txtDedGlobal.Text),
                                    opExoneradas = Convert.ToDouble(lblOpExoneradas.Text),
                                    opGravadas = Convert.ToDouble(lblOpGravadas.Text),
                                    serie = txtSerieNumero.Text.ToUpper(),
                                    usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                                    guia = txtGuia.Text,
                                    idTipoComprobante = 2,
                                };
                                try { compra.numero = Convert.ToInt32(txtNumero.Text); } catch { compra.numero = 0; }
                                List<DetallCompra> detalles = new List<DetallCompra>();
                                DetallCompra detalle;
                                for (int i = 0; i < gridDetalle.RowCount; i++)
                                {
                                    detalle = new DetallCompra
                                    {
                                        cantidad = Convert.ToDouble(gridDetalle.Rows[i].Cells["CANTIDAD"].Value.ToString()),
                                        costo = Convert.ToDouble(gridDetalle.Rows[i].Cells["COSTO"].Value.ToString()),
                                        descuento = Convert.ToDouble(gridDetalle.Rows[i].Cells["DESCUENTO"].Value.ToString()),
                                        factor = Convert.ToDouble(gridDetalle.Rows[i].Cells["FACTOR"].Value.ToString()),
                                        idPresentacion = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRESENTACION"].Value.ToString()),
                                        idProducto = Convert.ToInt32(gridDetalle.Rows[i].Cells["IDPRODUCTO"].Value.ToString()),
                                        igv = Convert.ToDouble(gridDetalle.Rows[i].Cells["IGV"].Value.ToString()),
                                        total = Convert.ToDouble(gridDetalle.Rows[i].Cells["IMPORTE"].Value.ToString()),
                                    };
                                    detalles.Add(detalle);
                                }
                                compra.detallesCompra = detalles;
                                GuiaCompra guia = null;
                                if (checkGuia.Checked)
                                {
                                    if (comboTransporte.SelectedValue != null)
                                        if (Util.validaCadena(txtGuia.Text))
                                            guia = new GuiaCompra
                                            {
                                                numero = txtGuia.Text,
                                                idTranporte = (int)comboTransporte.SelectedValue,
                                            };
                                        else { MessageBox.Show("FALTA NUMERO DE GUIA"); btnCompleto.Enabled = true; return; }
                                    else MessageBox.Show("NO EXISTE TRANSPORTISTA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                bool esPagoUnico = false;
                                if (comboTipoPago.Text == "PAGO UNICO") esPagoUnico = true;
                                if (compraModifica != null)
                                {
                                    compraModifica.cambio = Convert.ToDouble(lblCambio.Text);
                                    compraModifica.descuentoGlobal = Convert.ToDouble(txtDedGlobal.Text);
                                    compraModifica.fechaEmision = txtInicio.Value;
                                    compraModifica.fechaVencimiento = txtFin.Value;
                                    compraModifica.idMoneda = 180 + comboMoneda.SelectedIndex;
                                    compraModifica.idMedioPago = (int)comboMedioPago.SelectedValue;
                                    compraModifica.idProveedor = (int)comboProveedor.SelectedValue;
                                    compraModifica.igv = Convert.ToDouble(lblIGV.Text);
                                    compraModifica.idTipoPago = (int)comboTipoPago.SelectedValue;
                                    compraModifica.montoTotal = Convert.ToDouble(lblTotal.Text) - compraModifica.descuentoGlobal;
                                    compraModifica.montoN = compraModifica.montoTotal;
                                    compraModifica.opExoneradas = Convert.ToDouble(lblOpExoneradas.Text);
                                    compraModifica.opGravadas = Convert.ToDouble(lblOpGravadas.Text);
                                    compraModifica.serie = txtSerieNumero.Text;
                                    try { compraModifica.numero = Convert.ToInt32(txtNumero.Text); } catch { MessageBox.Show("NUMERO DE COMPROBANTE INVALIDO"); btnCompleto.Enabled = true; return; }
                                    compraModifica.usuarioUpdate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                                    if (comboTipoPago.Text.Equals("PAGO UNICO"))
                                    {
                                        compraModifica.pagado = true; compraModifica.montoPagado = compraModifica.montoN;
                                    }
                                    else { compraModifica.pagado = false; compraModifica.montoPagado = 0; }
                                    cservice.modificaConRegistro(compraModifica, "", DateTime.Now); pasado(null); this.Close();
                                }
                                else if (cservice.guardarCompleto(compra, guia, esPagoUnico,checkNoEsFactura.Checked)) { pasado(compra); this.Close(); } else MessageBox.Show("ERROR");
                            }
                            else MessageBox.Show("NO EXISTE MEDIO DE PAGO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else MessageBox.Show("NO EXISTE TIPO DE PAGO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else MessageBox.Show("SELECCIONE PROVEEDOR", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("DETALLE VACIO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); btnCompleto.Enabled = true;
            }
        }
        private void checkGuia_CheckedChanged(object sender, EventArgs e){
            if (checkGuia.Checked)
                panelGuia.Visible = true;
            else panelGuia.Visible = false;}
        void agregar(double cantidad, double precio, double descuento, bool porcentaje, DataGridViewRow fila,bool mod) {
            double desc = descuento;
            double descP = descuento;
            double valorTotal = 0, igvD = 0;
            valorTotal = precio * cantidad;
            if (porcentaje) desc = valorTotal *( descuento/100);
            else descP = (descuento /  valorTotal)*100;
            if (!Boolean.Parse(fila.Cells["ES_EXONERADO"].Value.ToString()))
                igvD = (valorTotal-desc) / (1 + igv.valorDouble) * igv.valorDouble;
            if (mod) {
                //agregar sobrante
                DetallCompra dc = new DetallCompra {
                     cantidad=cantidad,
                      costo=0,
                       descuento=0,
                        factor= Convert.ToDouble(fila.Cells["FACTOR"].Value.ToString()),
                    idCompra = compraModifica.id,
                           idPresentacion= Convert.ToInt32(fila.Cells["IDPRESENTACION"].Value.ToString()),
                            idProducto= Convert.ToInt32(fila.Cells["IDPRODUCTO"].Value.ToString()),
                            igv=0,
                             total=0,             
                };
                dcservice.agregarSobrante(dc, Convert.ToInt32(fila.Cells["ID"].Value.ToString()));
                tabla.Rows.Add("", fila.Cells["IDPRODUCTO"].Value.ToString(), fila.Cells["IDPRESENTACION"].Value.ToString(),
                    fila.Cells["PRODUCTO"].Value.ToString(), "0.00","0.00","0.00","0.00","0.00","0.00","0.00",
                    fila.Cells["FACTOR"].Value.ToString(), fila.Cells["ES_EXONERADO"].Value );
            }
            else
            { // if (tabla.Rows.Find(fila.Cells["IDPRESENTACION"].Value.ToString()) == null){                  
                     tabla.Rows.Add("", fila.Cells["IDPRODUCTO"].Value.ToString(),
                             fila.Cells["IDPRESENTACION"].Value.ToString(),
                              fila.Cells["PRESENTACION"].Value.ToString(),
                         precio, cantidad, igvD, desc, descP, (valorTotal - desc) / (1 + igv.valorDouble), valorTotal - desc
                              , fila.Cells["FACTOR"].Value.ToString(),
                             fila.Cells["ES_EXONERADO"].Value);
                        total += valorTotal - desc;
                        if (igvD == 0) { 
                            exonerado+= valorTotal - desc;
                            lblOpExoneradas.Text = string.Format("{0:0.0000}", exonerado);}
                        lblTotal.Text = string.Format("{0:0.0000}",total); } }
        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {    
            FormAgregaCompra form = new FormAgregaCompra(gridProducto.Rows[e.RowIndex],null,false);
            form.pasado +=new FormAgregaCompra.pasar(agregar);
            form.ShowDialog(); }
        private void lblTotal_TextChanged(object sender, EventArgs e){
            lblIGV.Text = string.Format("{0:0.0000}",(total-exonerado)- (total - exonerado) / (1 + igv.valorDouble));
            lblOpGravadas.Text= string.Format("{0:0.0000}", (total-Convert.ToDouble(lblIGV.Text))); }
        private void gridDetalle_CellDoubleClick(object sender,    DataGridViewCellEventArgs e) {
          /*  if (compraModifica != null)
            {
                indexCambioProducto = e.RowIndex;
                FormBuscraProducto form = new FormBuscraProducto();
                form.pasado += new FormBuscraProducto.pasar(pasarProducto);
                form.ShowDialog();
            }*/
        }

        private void btnSobrante_Click(object sender, EventArgs e)
        {
            if (gridDetalle.SelectedRows.Count == 1)
            {
                FormAgregarSobrante form = new FormAgregarSobrante(dservice.buscar(Convert.ToInt32(gridDetalle.SelectedRows[0].Cells["ID"].Value.ToString())), gridDetalle.SelectedRows[0].Cells["PRODUCTO"].Value.ToString());
                form.ShowDialog();
                this.Close();
            }
           

        }

        void pasarProducto(ProductoPresentacion p) {
            gridDetalle.Rows[indexCambioProducto].Cells["IDPRODUCTO"].Value = p.idProducto;
            gridDetalle.Rows[indexCambioProducto].Cells["IDPRESENTACION"].Value =p.id;
            gridDetalle.Rows[indexCambioProducto].Cells["PRODUCTO"].Value = p.producto.nombre;
            //TRAE EL MOVIMIENTO DEL COMPRA DETALLE ID Y CAMBIALO EN LOS 2
            dcservice.modificar(Convert.ToInt32(gridDetalle.Rows[indexCambioProducto].Cells["ID"].Value.ToString()),p.idProducto,p.id);
        }
    }
}
 