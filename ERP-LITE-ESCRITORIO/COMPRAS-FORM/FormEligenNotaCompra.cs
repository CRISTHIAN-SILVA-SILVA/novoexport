﻿using ERP_LITE_DESKTOP;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormEligenNotaCompra : MaterialForm
    {
        Compra compra = null;
        NotaCompraService nservice = new NotaCompraImpl();
        public FormEligenNotaCompra(Compra nta)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
            compra = nta;
           lblCliente.Text = nta.proveedor.razonSocial;
            lblTipoComprobante.Text = nta.tipoComprobante.descripcion.ToUpper();
            lblSerie.Text =nta.serie + "-" + Util.NormalizarCampo(nta.numero.ToString(), 8);
        }

        private void FormEligenNotaCompra_Load(object sender, EventArgs e)
        {
            comboNota.SelectedIndex = 0;

        }

        private void txtSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void pasado() {
            this.Close();
        }
        private void btnSeguir_Click(object sender, EventArgs e)
        {
            if (Util.validaCadena(txtSerie.Text) && txtSerie.Text.Length == 4){
                if (Util.esDouble(txtNumero.Text)){
                    TipoComprobantePagoService tcpservice = new TipoComprobanteImpl();
                    TipoComprobantePago tipoCredito = tcpservice.buscarPorNombre("Nota de credito");
                    TipoComprobantePago tipoDebito = tcpservice.buscarPorNombre("Nota de debito");
                    if (tipoCredito == null || tipoDebito == null) return;
                    NotaCompra nota = new NotaCompra{
                        esCredito = true,
                        idProveedor = compra.idProveedor,
                        idCompra = compra.id,
                        idTipoComprobante = tipoCredito.id,
                        idTipoNota = (int)comboTipoNota.SelectedValue,
                        igv = compra.igv,
                        usuarioCreate = FormLogin.user.nombres + " " + FormLogin.user.dni,
                        serie = txtSerie.Text,
                        numero = Convert.ToInt32(txtNumero.Text),
                        total = compra.montoTotal,
                        totalExonerado = compra.opExoneradas,
                        opGravadas = compra.opGravadas ,fechaEmision=txtEmision.Value};
                    DetalleNotaCompra detalle;
                    List<DetalleNotaCompra> detalles = new List<DetalleNotaCompra>();
                    foreach (var i in compra.detallesCompra)
                        if (!i.anulado) {
                            detalle = new DetalleNotaCompra {
                                cantidad = i.cantidad,
                                factor = i.factor,
                                idPresentacion = i.idPresentacion,
                                idProducto = i.idProducto,
                                igv = i.igv,
                                total = i.total,
                                valorUnitario = i.costo, };
                            detalles.Add(detalle); }
                    nota.detalle = detalles;

                    /*   if (comboTipoNota.Text.Equals("ANULACION_DE_LA_OPERACION") || comboTipoNota.Text.Equals("ANULACION_POR_ERROR_EN_EL_RUC"))
                           if (MessageBox.Show("SEGURO DE ANULAR COMPROBANTE?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                               if (nservice.guardarNotaCompleta(nota, true, false, false, false, false,false,false)){this.Close(); }
                       else */
                    if (comboTipoNota.Text.Equals("DESCUENTO_GLOBAL") || comboTipoNota.Text.Equals("INTERES_POR_MORA") || comboTipoNota.Text.Equals("PENALIDADES_OTROS_CONCEPTOS"))
                    {
                        if (MessageBox.Show("SEGURO DE GUARDAR?", "COMFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                        if (comboTipoNota.Text.Equals("DESCUENTO_GLOBAL"))
                            if (Util.esDouble(txtDescuentoGlobal.Text))
                            {
                                nota.total = Convert.ToDouble(txtDescuentoGlobal.Text);
                                if (nservice.guardarNotaCompleta(nota, false, false, false, false, false, false, true)) { this.Close(); }
                            }
                        }
                    }
                    else if (comboTipoNota.Text.Equals("DEVOLUCION_TOTAL"))
                    {
                        
                    //    compra.montoN = 0;
                     //   if (nservice.guardarNotaCompleta(nota, false, false, false, false, false, true, false)) { this.Close(); }
                    }
                    else if (comboTipoNota.Text.Equals("DESCUENTO_POR_ITEM") || comboTipoNota.Text.Equals("DEVOLUCION_POR_ITEM")
                       || comboTipoNota.Text.Equals("DISMINUCION_EN_EL_VALOR") || comboTipoNota.Text.Equals("AUMENTO_DE_VALOR"))
                    {
                        TipoComprobantePago tc = tipoCredito;
                        if (comboTipoNota.Text.Equals("AUMENTO_DE_VALOR")) tc = tipoDebito;
                        FormNuevaNotaCItem form = new FormNuevaNotaCItem(nota, compra, tc, (int)comboTipoNota.SelectedValue,false);
                        form.pasado += new FormNuevaNotaCItem.pasar(pasado);
                        form.ShowDialog();
                    }
                }
                else MessageBox.Show("NUMERO DE NOTA INCORRECTO");
            }
            else MessageBox.Show("SERIE INCORRECTA");
        }

        private void comboNota_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboNota.SelectedIndex == 0)
                comboTipoNota.DataSource = TipoNotaImpl.getInstancia().Where(x => x.esCredito).ToList();
            else comboTipoNota.DataSource = TipoNotaImpl.getInstancia().Where(x => x.esDebito).ToList();
        }

        private void comboTipoNota_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboTipoNota.Text.Equals("DESCUENTO_GLOBAL"))
            {
                txtDescuentoGlobal.Visible = true;
                lblDebito.Visible = true;
            }
            else
            {
                txtDescuentoGlobal.Visible = false;
                lblDebito.Visible =false;
            }
        }
    }
}
