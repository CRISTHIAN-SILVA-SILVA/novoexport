﻿namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    partial class FormCompras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCompras));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExportar = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.grid = new System.Windows.Forms.DataGridView();
            this.btnNuevo = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialFlatButton1 = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnNuevaNota = new MaterialSkin.Controls.MaterialFlatButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbFecha = new MaterialSkin.Controls.MaterialRadioButton();
            this.lblGuionSerie = new System.Windows.Forms.Label();
            this.txtSerie = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.comboTipoPago = new System.Windows.Forms.ComboBox();
            this.comboMoneda = new System.Windows.Forms.ComboBox();
            this.comboProveedor = new System.Windows.Forms.ComboBox();
            this.rbMoneda = new MaterialSkin.Controls.MaterialRadioButton();
            this.rbTipoPago = new MaterialSkin.Controls.MaterialRadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLimpiar = new MaterialSkin.Controls.MaterialFlatButton();
            this.rbSerieNumero = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtFin = new System.Windows.Forms.DateTimePicker();
            this.btnBuscar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.rbProveedor = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtInicio = new System.Windows.Forms.DateTimePicker();
            this.btnAnular = new MaterialSkin.Controls.MaterialFlatButton();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label1.Location = new System.Drawing.Point(402, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 24);
            this.label1.TabIndex = 21;
            this.label1.Text = "ADMINISTRACION DE COMPRAS";
            // 
            // btnExportar
            // 
            this.btnExportar.AutoSize = true;
            this.btnExportar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnExportar.Depth = 0;
            this.btnExportar.Icon = ((System.Drawing.Image)(resources.GetObject("btnExportar.Icon")));
            this.btnExportar.Location = new System.Drawing.Point(975, 146);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnExportar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Primary = false;
            this.btnExportar.Size = new System.Drawing.Size(118, 36);
            this.btnExportar.TabIndex = 20;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Icon = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Icon")));
            this.btnEliminar.Location = new System.Drawing.Point(115, 146);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(189, 36);
            this.btnEliminar.TabIndex = 19;
            this.btnEliminar.Text = "Convertir a letras";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grid.BackgroundColor = System.Drawing.Color.White;
            this.grid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.grid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.DefaultCellStyle = dataGridViewCellStyle3;
            this.grid.EnableHeadersVisualStyles = false;
            this.grid.GridColor = System.Drawing.Color.Black;
            this.grid.Location = new System.Drawing.Point(13, 192);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grid.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.grid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.grid.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.grid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.grid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.grid.RowTemplate.Height = 31;
            this.grid.RowTemplate.ReadOnly = true;
            this.grid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(1107, 412);
            this.grid.TabIndex = 18;
            this.grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grid_CellDoubleClick);
            // 
            // btnNuevo
            // 
            this.btnNuevo.AutoSize = true;
            this.btnNuevo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNuevo.Depth = 0;
            this.btnNuevo.Icon = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Icon")));
            this.btnNuevo.Location = new System.Drawing.Point(13, 146);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnNuevo.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Primary = false;
            this.btnNuevo.Size = new System.Drawing.Size(94, 36);
            this.btnNuevo.TabIndex = 17;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // materialFlatButton1
            // 
            this.materialFlatButton1.AutoSize = true;
            this.materialFlatButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialFlatButton1.Depth = 0;
            this.materialFlatButton1.Icon = ((System.Drawing.Image)(resources.GetObject("materialFlatButton1.Icon")));
            this.materialFlatButton1.Location = new System.Drawing.Point(312, 146);
            this.materialFlatButton1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialFlatButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFlatButton1.Name = "materialFlatButton1";
            this.materialFlatButton1.Primary = false;
            this.materialFlatButton1.Size = new System.Drawing.Size(198, 36);
            this.materialFlatButton1.TabIndex = 22;
            this.materialFlatButton1.Text = "Agregar Detraccion";
            this.materialFlatButton1.UseVisualStyleBackColor = true;
            this.materialFlatButton1.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // btnNuevaNota
            // 
            this.btnNuevaNota.AutoSize = true;
            this.btnNuevaNota.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNuevaNota.Depth = 0;
            this.btnNuevaNota.Icon = ((System.Drawing.Image)(resources.GetObject("btnNuevaNota.Icon")));
            this.btnNuevaNota.Location = new System.Drawing.Point(518, 146);
            this.btnNuevaNota.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnNuevaNota.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNuevaNota.Name = "btnNuevaNota";
            this.btnNuevaNota.Primary = false;
            this.btnNuevaNota.Size = new System.Drawing.Size(86, 36);
            this.btnNuevaNota.TabIndex = 23;
            this.btnNuevaNota.Text = "Nota";
            this.btnNuevaNota.UseVisualStyleBackColor = true;
            this.btnNuevaNota.Click += new System.EventHandler(this.btnNuevaNota_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbFecha);
            this.panel1.Controls.Add(this.lblGuionSerie);
            this.panel1.Controls.Add(this.txtSerie);
            this.panel1.Controls.Add(this.comboTipoPago);
            this.panel1.Controls.Add(this.comboMoneda);
            this.panel1.Controls.Add(this.comboProveedor);
            this.panel1.Controls.Add(this.rbMoneda);
            this.panel1.Controls.Add(this.rbTipoPago);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnLimpiar);
            this.panel1.Controls.Add(this.rbSerieNumero);
            this.panel1.Controls.Add(this.txtNumero);
            this.panel1.Controls.Add(this.txtFin);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.rbProveedor);
            this.panel1.Controls.Add(this.txtInicio);
            this.panel1.Location = new System.Drawing.Point(13, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1107, 100);
            this.panel1.TabIndex = 122;
            // 
            // rbFecha
            // 
            this.rbFecha.AutoSize = true;
            this.rbFecha.Depth = 0;
            this.rbFecha.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbFecha.Location = new System.Drawing.Point(487, 2);
            this.rbFecha.Margin = new System.Windows.Forms.Padding(0);
            this.rbFecha.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbFecha.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbFecha.Name = "rbFecha";
            this.rbFecha.Ripple = true;
            this.rbFecha.Size = new System.Drawing.Size(97, 30);
            this.rbFecha.TabIndex = 185;
            this.rbFecha.TabStop = true;
            this.rbFecha.Text = "Solo Fecha";
            this.rbFecha.UseVisualStyleBackColor = true;
            this.rbFecha.CheckedChanged += new System.EventHandler(this.rbFecha_CheckedChanged);
            // 
            // lblGuionSerie
            // 
            this.lblGuionSerie.AutoSize = true;
            this.lblGuionSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGuionSerie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.lblGuionSerie.Location = new System.Drawing.Point(67, 61);
            this.lblGuionSerie.Name = "lblGuionSerie";
            this.lblGuionSerie.Size = new System.Drawing.Size(17, 24);
            this.lblGuionSerie.TabIndex = 184;
            this.lblGuionSerie.Text = "-";
            // 
            // txtSerie
            // 
            this.txtSerie.Depth = 0;
            this.txtSerie.Hint = "";
            this.txtSerie.Location = new System.Drawing.Point(16, 62);
            this.txtSerie.MaxLength = 32767;
            this.txtSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.PasswordChar = '\0';
            this.txtSerie.SelectedText = "";
            this.txtSerie.SelectionLength = 0;
            this.txtSerie.SelectionStart = 0;
            this.txtSerie.Size = new System.Drawing.Size(45, 23);
            this.txtSerie.TabIndex = 183;
            this.txtSerie.TabStop = false;
            this.txtSerie.UseSystemPasswordChar = false;
            // 
            // comboTipoPago
            // 
            this.comboTipoPago.DisplayMember = "nombre";
            this.comboTipoPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipoPago.FormattingEnabled = true;
            this.comboTipoPago.Location = new System.Drawing.Point(201, 60);
            this.comboTipoPago.Name = "comboTipoPago";
            this.comboTipoPago.Size = new System.Drawing.Size(240, 26);
            this.comboTipoPago.TabIndex = 182;
            this.comboTipoPago.ValueMember = "id";
            // 
            // comboMoneda
            // 
            this.comboMoneda.DisplayMember = "nombre";
            this.comboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboMoneda.FormattingEnabled = true;
            this.comboMoneda.Items.AddRange(new object[] {
            "SOLES",
            "DOLARES"});
            this.comboMoneda.Location = new System.Drawing.Point(456, 59);
            this.comboMoneda.Name = "comboMoneda";
            this.comboMoneda.Size = new System.Drawing.Size(160, 26);
            this.comboMoneda.TabIndex = 181;
            this.comboMoneda.ValueMember = "id";
            // 
            // comboProveedor
            // 
            this.comboProveedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboProveedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboProveedor.DisplayMember = "razonSocial";
            this.comboProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboProveedor.FormattingEnabled = true;
            this.comboProveedor.Location = new System.Drawing.Point(635, 60);
            this.comboProveedor.MaxLength = 200;
            this.comboProveedor.Name = "comboProveedor";
            this.comboProveedor.Size = new System.Drawing.Size(359, 26);
            this.comboProveedor.TabIndex = 180;
            this.comboProveedor.ValueMember = "id";
            // 
            // rbMoneda
            // 
            this.rbMoneda.AutoSize = true;
            this.rbMoneda.Depth = 0;
            this.rbMoneda.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbMoneda.Location = new System.Drawing.Point(487, 29);
            this.rbMoneda.Margin = new System.Windows.Forms.Padding(0);
            this.rbMoneda.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbMoneda.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbMoneda.Name = "rbMoneda";
            this.rbMoneda.Ripple = true;
            this.rbMoneda.Size = new System.Drawing.Size(104, 30);
            this.rbMoneda.TabIndex = 128;
            this.rbMoneda.TabStop = true;
            this.rbMoneda.Text = "Por Moneda";
            this.rbMoneda.UseVisualStyleBackColor = true;
            this.rbMoneda.CheckedChanged += new System.EventHandler(this.rbMoneda_CheckedChanged);
            // 
            // rbTipoPago
            // 
            this.rbTipoPago.AutoSize = true;
            this.rbTipoPago.Depth = 0;
            this.rbTipoPago.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbTipoPago.Location = new System.Drawing.Point(260, 28);
            this.rbTipoPago.Margin = new System.Windows.Forms.Padding(0);
            this.rbTipoPago.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbTipoPago.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbTipoPago.Name = "rbTipoPago";
            this.rbTipoPago.Ripple = true;
            this.rbTipoPago.Size = new System.Drawing.Size(116, 30);
            this.rbTipoPago.TabIndex = 127;
            this.rbTipoPago.TabStop = true;
            this.rbTipoPago.Text = "Por Tipo Pago";
            this.rbTipoPago.UseVisualStyleBackColor = true;
            this.rbTipoPago.CheckedChanged += new System.EventHandler(this.rbTipoPago_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label3.Location = new System.Drawing.Point(649, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 24);
            this.label3.TabIndex = 126;
            this.label3.Text = "Fin:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label2.Location = new System.Drawing.Point(125, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 24);
            this.label2.TabIndex = 125;
            this.label2.Text = "Inicio:";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.AutoSize = true;
            this.btnLimpiar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnLimpiar.Depth = 0;
            this.btnLimpiar.Icon = null;
            this.btnLimpiar.Location = new System.Drawing.Point(1019, 54);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnLimpiar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Primary = false;
            this.btnLimpiar.Size = new System.Drawing.Size(75, 36);
            this.btnLimpiar.TabIndex = 124;
            this.btnLimpiar.Text = "limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // rbSerieNumero
            // 
            this.rbSerieNumero.AutoSize = true;
            this.rbSerieNumero.Depth = 0;
            this.rbSerieNumero.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbSerieNumero.Location = new System.Drawing.Point(33, 35);
            this.rbSerieNumero.Margin = new System.Windows.Forms.Padding(0);
            this.rbSerieNumero.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbSerieNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbSerieNumero.Name = "rbSerieNumero";
            this.rbSerieNumero.Ripple = true;
            this.rbSerieNumero.Size = new System.Drawing.Size(139, 30);
            this.rbSerieNumero.TabIndex = 112;
            this.rbSerieNumero.TabStop = true;
            this.rbSerieNumero.Text = "Por Serie-Número";
            this.rbSerieNumero.UseVisualStyleBackColor = true;
            this.rbSerieNumero.CheckedChanged += new System.EventHandler(this.rbSerieNumero_CheckedChanged);
            // 
            // txtNumero
            // 
            this.txtNumero.Depth = 0;
            this.txtNumero.Hint = "";
            this.txtNumero.Location = new System.Drawing.Point(90, 62);
            this.txtNumero.MaxLength = 32767;
            this.txtNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.Size = new System.Drawing.Size(82, 23);
            this.txtNumero.TabIndex = 109;
            this.txtNumero.TabStop = false;
            this.txtNumero.UseSystemPasswordChar = false;
            // 
            // txtFin
            // 
            this.txtFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFin.Location = new System.Drawing.Point(701, 4);
            this.txtFin.Name = "txtFin";
            this.txtFin.Size = new System.Drawing.Size(224, 21);
            this.txtFin.TabIndex = 110;
            // 
            // btnBuscar
            // 
            this.btnBuscar.AutoSize = true;
            this.btnBuscar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnBuscar.Depth = 0;
            this.btnBuscar.Icon = null;
            this.btnBuscar.Location = new System.Drawing.Point(1019, 12);
            this.btnBuscar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Primary = true;
            this.btnBuscar.Size = new System.Drawing.Size(74, 36);
            this.btnBuscar.TabIndex = 118;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // rbProveedor
            // 
            this.rbProveedor.AutoSize = true;
            this.rbProveedor.Depth = 0;
            this.rbProveedor.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbProveedor.Location = new System.Drawing.Point(751, 27);
            this.rbProveedor.Margin = new System.Windows.Forms.Padding(0);
            this.rbProveedor.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbProveedor.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbProveedor.Name = "rbProveedor";
            this.rbProveedor.Ripple = true;
            this.rbProveedor.Size = new System.Drawing.Size(117, 30);
            this.rbProveedor.TabIndex = 113;
            this.rbProveedor.TabStop = true;
            this.rbProveedor.Text = "Por Proveedor";
            this.rbProveedor.UseVisualStyleBackColor = true;
            this.rbProveedor.CheckedChanged += new System.EventHandler(this.rbProveedor_CheckedChanged);
            // 
            // txtInicio
            // 
            this.txtInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicio.Location = new System.Drawing.Point(201, 5);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(224, 21);
            this.txtInicio.TabIndex = 116;
            // 
            // btnAnular
            // 
            this.btnAnular.AutoSize = true;
            this.btnAnular.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAnular.Depth = 0;
            this.btnAnular.Icon = ((System.Drawing.Image)(resources.GetObject("btnAnular.Icon")));
            this.btnAnular.Location = new System.Drawing.Point(612, 147);
            this.btnAnular.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnAnular.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Primary = false;
            this.btnAnular.Size = new System.Drawing.Size(111, 36);
            this.btnAnular.TabIndex = 123;
            this.btnAnular.Text = "Eliminar";
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // FormCompras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1144, 602);
            this.Controls.Add(this.btnAnular);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnNuevaNota);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.materialFlatButton1);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnExportar);
            this.Name = "FormCompras";
            this.Text = "FormCompras";
            this.Load += new System.EventHandler(this.FormCompras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialFlatButton btnExportar;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private System.Windows.Forms.DataGridView grid;
        private MaterialSkin.Controls.MaterialFlatButton btnNuevo;
        private MaterialSkin.Controls.MaterialFlatButton materialFlatButton1;
        private MaterialSkin.Controls.MaterialFlatButton btnNuevaNota;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialRadioButton rbMoneda;
        private MaterialSkin.Controls.MaterialRadioButton rbTipoPago;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MaterialSkin.Controls.MaterialFlatButton btnLimpiar;
        private MaterialSkin.Controls.MaterialRadioButton rbSerieNumero;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumero;
        private System.Windows.Forms.DateTimePicker txtFin;
        private MaterialSkin.Controls.MaterialRaisedButton btnBuscar;
        private MaterialSkin.Controls.MaterialRadioButton rbProveedor;
        private System.Windows.Forms.DateTimePicker txtInicio;
        private System.Windows.Forms.ComboBox comboProveedor;
        private System.Windows.Forms.ComboBox comboMoneda;
        private MaterialSkin.Controls.MaterialRadioButton rbFecha;
        private System.Windows.Forms.Label lblGuionSerie;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSerie;
        private System.Windows.Forms.ComboBox comboTipoPago;
        private MaterialSkin.Controls.MaterialFlatButton btnAnular;
    }
}