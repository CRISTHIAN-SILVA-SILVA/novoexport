﻿using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormAgregarSobrante : Form
    {
        DetalleCompraImpl dservice = new DetalleCompraImpl();
        DetallCompra dc;
        public FormAgregarSobrante(DetallCompra detalle,string producto)
        {
            dc = detalle;
            InitializeComponent();
            label1.Text = producto;
        }

        private void FormAgregarSobrante_Load(object sender, EventArgs e)
        {

        }

        private void btnCompleto_Click(object sender, EventArgs e)
        {
            if (Util.esDouble(txt.Text))
            {
                if (Convert.ToDouble(txt.Text) > 0) {
                    DetallCompra d = new DetallCompra {
                        costo = 0, factor = dc.factor, idCompra = dc.idCompra, cantidad = Convert.ToDouble(txt.Text),
                        descuento = 0, idPresentacion=dc.idPresentacion, idProducto=dc.idProducto, igv=0, total=0,
                           
                    };
                    if (dservice.agregarSobrante(d, dc.id))
                    {
                        MessageBox.Show("AGREGADO");
                        //AGREGAR SOBRANTE A LA GRID
                        this.Close();
                    }
                    else MessageBox.Show("ERROR INESPERADO");
                }
                else MessageBox.Show("CANTIDAD INCORRECTA");
            }
            else MessageBox.Show("CANTIDAD INCORRECTA");
        }
    }
}
