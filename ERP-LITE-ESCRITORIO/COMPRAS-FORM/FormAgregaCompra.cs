﻿using MaterialSkin;
using MaterialSkin.Controls;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormAgregaCompra : MaterialForm
    {
        public delegate void pasar(double cantidad,double precio,double descuento,bool porcentaje, DataGridViewRow fila,bool mod);
        public event pasar pasado;
        DataGridViewRow fila =null,modifica=null;
        bool sobrante;

        public FormAgregaCompra(DataGridViewRow f, DataGridViewRow mod,bool esSobrante)
        {
            sobrante = esSobrante;
            
            fila = f;
            modifica = mod;
            InitializeComponent();
            this.Activate();
            txtCantidad.Focus();
            if (esSobrante)
            {
                txtDescuento.Enabled = false;
                txtValorUnitario.Enabled = false;
                txtDescuento.Text = "0.00";
                txtValorUnitario.Text ="0.00";
            }

            if (modifica != null) { 

            } 
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

        }

        private void FormAgregaCompra_Load(object sender, EventArgs e)
        {
            comboTipoDescuento.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Util.esDouble(txtCantidad.Text))
            {
                if (Util.esDouble(txtValorUnitario.Text))
                {
                    if (Util.esDouble(txtDescuento.Text))
                    {
                        bool porcentaje = false;
                        if (comboTipoDescuento.SelectedIndex == 0) {
                            porcentaje = true;
                        }
                        if (modifica == null)
                            pasado(Convert.ToDouble(txtCantidad.Text), Convert.ToDouble(txtValorUnitario.Text), Convert.ToDouble(txtDescuento.Text), porcentaje, fila, false);
                        else {
                            if (MessageBox.Show("SEGURO DE AGREGAR SOBRANTE?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
                                pasado(Convert.ToDouble(txtCantidad.Text), Convert.ToDouble(txtValorUnitario.Text), Convert.ToDouble(txtDescuento.Text), porcentaje, modifica, true);
                        }
                        this.Close();
                    }
                    else {
                        MessageBox.Show("DESCUENTO INCORRECTO");
                    }
                }
                else { MessageBox.Show("PRECIO INCORRECTO"); }
            }
            else { MessageBox.Show("CANTIDAD INCORRECTO"); }
        }
    }
}
