﻿using ERP_LITE_ESCRITORIO.COMPRAS_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.COMPRAS_FORM
{
    public partial class FormAdminCompras : MaterialForm
    {
        public FormAdminCompras()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormAdminCompras_Load(object sender, EventArgs e)
        {
            abrirForm(new FormCompras());
        }
        void abrirForm(Object formHijo)
        {
            if (this.contenedor.Controls.Count > 0)
                this.contenedor.Controls.RemoveAt(0);
            Form hijo = formHijo as Form;
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            this.contenedor.Controls.Add(hijo);
            this.contenedor.Tag = hijo;
            hijo.Show();
        }
        private void btnCompras_Click(object sender, EventArgs e)
        {
            abrirForm(new FormCompras());
        }

        private void btnNotaCredito_Click(object sender, EventArgs e)
        {
            abrirForm(new FormAdminNotasCompras());
        }
    }
}
