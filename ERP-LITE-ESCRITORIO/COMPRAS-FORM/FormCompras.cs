﻿using ERP_LITE_ESCRITORIO.CUENTAS_CORRIENTES_FORM;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormCompras : Form
    {
        CompraService cservice = new CompraImpl();
        DataTable tabla = new DataTable();
        ProveedorService pvservice = new ProveedorImpl();
        TipoPagoService tpService = new TipoPagoImpl();
        public FormCompras()
        {
            InitializeComponent();

            tabla.Columns.Add("ID");
            tabla.Columns.Add("COMPROBANTE");
            tabla.Columns.Add("PROVEEDOR");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("MONEDA");
            tabla.Columns.Add("TIPO PAGO");
            tabla.Columns.Add("GUIA");
            tabla.Columns.Add("DETRACCION");
            grid.DataSource = tabla;

            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 400;
            grid.Columns[3].Width = 80;
            grid.Columns[4].Width = 80;
            grid.Columns[5].Width = 100;
            grid.Columns[6].Width = 170;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 80;
            comboTipoPago.DataSource = tpService.listarNoAnulados();

            List<Proveedor> proveedores = pvservice.listarNoAnulados();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            proveedores.ForEach(c => coleccion.Add(c.razonSocial));
            comboProveedor.AutoCompleteCustomSource = coleccion;
            comboProveedor.DataSource = proveedores;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
     void   agregar(Compra c) {
            tabla.Clear();
           // string detraccion = "NO";
            foreach (var x in cservice.listarNoAnulados()) {
              //  if (x.detraccion > 0) detraccion = "SI"; else detraccion = "NO";
                tabla.Rows.Add(x.id, x.serie + "-" + Util.NormalizarCampo(x.numero.ToString(), 8),
             x.proveedor.razonSocial, x.fechaEmision.ToShortDateString(), String.Format("{0:0.00}", x.montoTotal), x.tipoMoneda.descripcion, x.tipoPago.nombre, x.guia,x.detraccion);
            }

        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaCompra form = new FormNuevaCompra(null);
            form.pasado += new FormNuevaCompra.pasar(agregar);
            form.ShowDialog();
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(grid.SelectedRows.Count==1)
            {
                Compra c = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null) {
                    FormNuevaCompra form = new FormNuevaCompra(c);
                    form.pasado += new FormNuevaCompra.pasar(agregar);
                    form.ShowDialog();
                }
            }
           
        }
       
        private void FormCompras_Load(object sender, EventArgs e)
        {
            comboMoneda.SelectedIndex = 0;
            rbFecha.Checked = true;
            agregar(null); }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
            {
                Compra c = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (!c.pagado)
                {
                    if (c != null)
                    {

                        if (!c.pagado)
                        {
                            FormLetraComprobante form = new FormLetraComprobante(c, false);

                            form.pasado += new FormLetraComprobante.pasar(agregar);
                            form.ShowDialog();
                        }
                    }
                }
                else MessageBox.Show("COMPRA YA ESTA PAGADA");
                
            }
        }

        private void btnNuevaNota_Click(object sender, EventArgs e)
        {
            Compra c=null;
            if (grid.SelectedRows.Count == 1)
            {
                c = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null)
                {  
                        FormEligenNotaCompra form = new FormEligenNotaCompra(c);
                        form.ShowDialog();
                    
                }
            }
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1) {
                Compra c = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null) {
                    FormAgregarDetraccion form = new FormAgregarDetraccion(c);
                    form.pasado += new FormAgregarDetraccion.pasar(agregar);
                    form.ShowDialog();
                }
            }
           
        }

        void habilitarBusqueda(bool fecha, bool tipoPago, bool tipoMoneda, bool serieN, bool proveedor) {
            comboMoneda.Visible = tipoMoneda;
            comboProveedor.Visible = proveedor;
            comboTipoPago.Visible = tipoPago;
            txtSerie.Visible = serieN;
            txtNumero.Visible = serieN;
            lblGuionSerie.Visible = serieN;
        }
        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFecha.Checked) 
                habilitarBusqueda(true,false,false,false,false);
        }

        private void rbSerieNumero_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSerieNumero.Checked)
                habilitarBusqueda(false, false, false, true, false);
        }

        private void rbTipoPago_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoPago.Checked)
                habilitarBusqueda(false, true, false, false, false);
        }

        private void rbMoneda_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMoneda.Checked)
                habilitarBusqueda(false, false, true, false, false);
        }

        private void rbProveedor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbProveedor.Checked)
                habilitarBusqueda(false, false, false, false, true);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            tabla.Clear();
            if (rbFecha.Checked) {              
                foreach (var x in cservice.listarPorFecha(txtInicio.Value.Date,txtFin.Value.AddDays(1).Date))
                    tabla.Rows.Add(x.id, x.serie + "-" + Util.NormalizarCampo(x.numero.ToString(), 8),
                 x.proveedor.razonSocial, x.fechaEmision.ToShortDateString(), String.Format("{0:0.00}", x.montoTotal),
                 x.tipoMoneda.descripcion, x.tipoPago.nombre, x.guia, x.detraccion);         
            }
            else if (rbMoneda.Checked) {
                foreach (var x in cservice.listarPorFechaTipoMoneda(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date,comboMoneda.SelectedIndex+180))
                    tabla.Rows.Add(x.id, x.serie + "-" + Util.NormalizarCampo(x.numero.ToString(), 8),
                 x.proveedor.razonSocial, x.fechaEmision.ToShortDateString(), String.Format("{0:0.00}", x.montoTotal),
                 x.tipoMoneda.descripcion, x.tipoPago.nombre, x.guia, x.detraccion);
            }
            else if (rbProveedor.Checked) {
                if (comboProveedor.SelectedValue != null)
                    foreach (var x in cservice.listarPorFechaProveedor(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date, (int)comboProveedor.SelectedValue))
                        tabla.Rows.Add(x.id, x.serie + "-" + Util.NormalizarCampo(x.numero.ToString(), 8),
                     x.proveedor.razonSocial, x.fechaEmision.ToShortDateString(), String.Format("{0:0.00}", x.montoTotal),
                     x.tipoMoneda.descripcion, x.tipoPago.nombre, x.guia, x.detraccion);
                else MessageBox.Show("NO EXISTE PROVEEDOR");
            }
            else if (rbSerieNumero.Checked) {
                if (Util.esDouble(txtNumero.Text))
                    foreach (var x in cservice.listarPorSerieNumero(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date, txtSerie.Text, Convert.ToInt32(txtNumero.Text)))
                        tabla.Rows.Add(x.id, x.serie + "-" + Util.NormalizarCampo(x.numero.ToString(), 8),
                     x.proveedor.razonSocial, x.fechaEmision.ToShortDateString(), String.Format("{0:0.00}", x.montoTotal),
                     x.tipoMoneda.descripcion, x.tipoPago.nombre, x.guia, x.detraccion);
                else MessageBox.Show("NUMERO DE COMPROBANTE INVALIDO");
            }
            else {
                if (comboTipoPago.SelectedValue != null)
                    foreach (var x in cservice.listarPorFechaTipoPago(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date, (int)comboTipoPago.SelectedValue))
                        tabla.Rows.Add(x.id, x.serie + "-" + Util.NormalizarCampo(x.numero.ToString(), 8),
                     x.proveedor.razonSocial, x.fechaEmision.ToShortDateString(), String.Format("{0:0.00}", x.montoTotal),
                     x.tipoMoneda.descripcion, x.tipoPago.nombre, x.guia, x.detraccion);
                else MessageBox.Show("NO HAY TIPO DE PAGO");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            agregar(null);
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {

            if (grid.SelectedRows.Count == 1)
            {
                if (MessageBox.Show("SEGURO DE ELIMINAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) { 
                
                    cservice.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    agregar(null);
               }
            }
        }
    }
}
