﻿using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.COMPRAS_SERVICE;
using SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormAdminNotasCompras : Form
    {
        DataTable tabla = new DataTable();
        NotaCompraService ncservice = new NotaCompraImpl();
        ProveedorService pvservice = new ProveedorImpl();
        CompraService cservice = new CompraImpl();
        public FormAdminNotasCompras()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("NUMERO");
            tabla.Columns.Add("PROVEEDOR");
            tabla.Columns.Add("MOTIVO NOTA");
            tabla.Columns.Add("MONTO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 100;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 380;
            grid.Columns[5].Width = 300;
            grid.Columns[6].Width = 100;

            List<Proveedor> proveedores = pvservice.listarNoAnulados();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            proveedores.ForEach(c => coleccion.Add(c.razonSocial));
            comboProveedor.AutoCompleteCustomSource = coleccion;
            comboProveedor.DataSource = proveedores;
        }
        void cargar() {
            tabla.Clear();
            ncservice.listarNoAnulados().ForEach(x=>tabla.Rows.Add(x.id,x.fechaEmision.ToShortDateString(),x.serie,x.numero,
                x.proveedor.razonSocial,x.tipoNota.descripcion,string.Format("{0:0.00}",x.total)));
        }
        private void FormAdminNotasCompras_Load(object sender, EventArgs e)
        {
            rbFecha.Checked = true;
            cargar();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
        void habilitarBusqueda(bool fecha,bool serie,bool proveedor)
        {
            txtSerie.Visible = serie;
            txtNumero.Visible = serie;
            lblGuionSerie.Visible = serie;
            comboProveedor.Visible = proveedor;

        }
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            tabla.Clear();
            if (rbFecha.Checked)
            {
                ncservice.listarFecha(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date).ForEach(x => tabla.Rows.Add(x.id, x.fechaEmision.ToShortDateString(), x.serie, x.numero,
                       x.proveedor.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
            
            }
            else if (rbSerieNumero.Checked)
            {
                if (Util.esDouble(txtNumero.Text))
                    ncservice.listarPorSerieNumero(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date, txtSerie.Text, Convert.ToInt32(txtNumero.Text)).ForEach(x => tabla.Rows.Add(x.id, x.fechaEmision.ToShortDateString(), x.serie, x.numero,
                          x.proveedor.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
                else MessageBox.Show("NUEMRO CON FORMATO INCORRECTO");
            }
            else if (rbProveedor.Checked)
            {
                if (comboProveedor.SelectedValue != null)
                    ncservice.listarPorProveedor(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date, (int)comboProveedor.SelectedValue).ForEach(x => tabla.Rows.Add(x.id, x.fechaEmision.ToShortDateString(), x.serie, x.numero,
                         x.proveedor.razonSocial, x.tipoNota.descripcion, string.Format("{0:0.00}", x.total)));
                else MessageBox.Show("NO HAY PROVEEDORES");
            }
        }

      

        private void rbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFecha.Checked) {
                habilitarBusqueda(false,false,false);
            }
        }

        private void rbSerieNumero_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSerieNumero.Checked) {
                habilitarBusqueda(false,true,false);
            }
        }

        private void rbProveedor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbProveedor.Checked)
                habilitarBusqueda(false,false,true);
        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NotaCompra nc = ncservice.buscar(Convert.ToInt32( grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            Compra c = cservice.buscar(nc.idCompra);
   
            FormNuevaNotaCItem form = new FormNuevaNotaCItem(nc,c,nc.tipoComprobante,nc.idTipoNota,true);
            form.ShowDialog();
        }
    }
}
