﻿namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    partial class FormAgregaCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboTipoDescuento = new System.Windows.Forms.ComboBox();
            this.materialLabel25 = new MaterialSkin.Controls.MaterialLabel();
            this.txtDescuento = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnAgregar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.txtValorUnitario = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCantidad = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnClose = new MaterialSkin.Controls.MaterialFlatButton();
            this.SuspendLayout();
            // 
            // comboTipoDescuento
            // 
            this.comboTipoDescuento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoDescuento.FormattingEnabled = true;
            this.comboTipoDescuento.Items.AddRange(new object[] {
            "PORCENTAJE",
            "MONTO"});
            this.comboTipoDescuento.Location = new System.Drawing.Point(175, 137);
            this.comboTipoDescuento.Name = "comboTipoDescuento";
            this.comboTipoDescuento.Size = new System.Drawing.Size(135, 21);
            this.comboTipoDescuento.TabIndex = 213;
            // 
            // materialLabel25
            // 
            this.materialLabel25.Depth = 0;
            this.materialLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.materialLabel25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel25.Location = new System.Drawing.Point(31, 138);
            this.materialLabel25.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel25.Name = "materialLabel25";
            this.materialLabel25.Size = new System.Drawing.Size(91, 22);
            this.materialLabel25.TabIndex = 212;
            this.materialLabel25.Text = "Descuento:";
            // 
            // txtDescuento
            // 
            this.txtDescuento.Depth = 0;
            this.txtDescuento.Hint = "";
            this.txtDescuento.Location = new System.Drawing.Point(375, 137);
            this.txtDescuento.MaxLength = 32767;
            this.txtDescuento.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDescuento.Name = "txtDescuento";
            this.txtDescuento.PasswordChar = '\0';
            this.txtDescuento.SelectedText = "";
            this.txtDescuento.SelectionLength = 0;
            this.txtDescuento.SelectionStart = 0;
            this.txtDescuento.Size = new System.Drawing.Size(100, 23);
            this.txtDescuento.TabIndex = 211;
            this.txtDescuento.TabStop = false;
            this.txtDescuento.Text = "0";
            this.txtDescuento.UseSystemPasswordChar = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Icon = null;
            this.btnAgregar.Location = new System.Drawing.Point(392, 181);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = true;
            this.btnAgregar.Size = new System.Drawing.Size(83, 36);
            this.btnAgregar.TabIndex = 210;
            this.btnAgregar.Text = "agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // materialLabel11
            // 
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel11.Location = new System.Drawing.Point(257, 95);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(77, 22);
            this.materialLabel11.TabIndex = 217;
            this.materialLabel11.Text = "Precio U.:";
            // 
            // txtValorUnitario
            // 
            this.txtValorUnitario.Depth = 0;
            this.txtValorUnitario.Hint = "";
            this.txtValorUnitario.Location = new System.Drawing.Point(375, 94);
            this.txtValorUnitario.MaxLength = 32767;
            this.txtValorUnitario.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtValorUnitario.Name = "txtValorUnitario";
            this.txtValorUnitario.PasswordChar = '\0';
            this.txtValorUnitario.SelectedText = "";
            this.txtValorUnitario.SelectionLength = 0;
            this.txtValorUnitario.SelectionStart = 0;
            this.txtValorUnitario.Size = new System.Drawing.Size(100, 23);
            this.txtValorUnitario.TabIndex = 216;
            this.txtValorUnitario.TabStop = false;
            this.txtValorUnitario.UseSystemPasswordChar = false;
            // 
            // materialLabel9
            // 
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel9.Location = new System.Drawing.Point(31, 95);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(79, 22);
            this.materialLabel9.TabIndex = 215;
            this.materialLabel9.Text = "Cantidad:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Depth = 0;
            this.txtCantidad.Hint = "";
            this.txtCantidad.Location = new System.Drawing.Point(128, 94);
            this.txtCantidad.MaxLength = 32767;
            this.txtCantidad.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.PasswordChar = '\0';
            this.txtCantidad.SelectedText = "";
            this.txtCantidad.SelectionLength = 0;
            this.txtCantidad.SelectionStart = 0;
            this.txtCantidad.Size = new System.Drawing.Size(100, 23);
            this.txtCantidad.TabIndex = 214;
            this.txtCantidad.TabStop = false;
            this.txtCantidad.UseSystemPasswordChar = false;
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnClose.Depth = 0;
            this.btnClose.Icon = null;
            this.btnClose.Location = new System.Drawing.Point(321, 181);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnClose.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnClose.Name = "btnClose";
            this.btnClose.Primary = false;
            this.btnClose.Size = new System.Drawing.Size(64, 36);
            this.btnClose.TabIndex = 218;
            this.btnClose.Text = "atras";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FormAgregaCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 244);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.txtValorUnitario);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.comboTipoDescuento);
            this.Controls.Add(this.materialLabel25);
            this.Controls.Add(this.txtDescuento);
            this.Controls.Add(this.btnAgregar);
            this.MaximizeBox = false;
            this.Name = "FormAgregaCompra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Compra";
            this.Load += new System.EventHandler(this.FormAgregaCompra_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboTipoDescuento;
        private MaterialSkin.Controls.MaterialLabel materialLabel25;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDescuento;
        private MaterialSkin.Controls.MaterialRaisedButton btnAgregar;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtValorUnitario;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCantidad;
        private MaterialSkin.Controls.MaterialFlatButton btnClose;
    }
}