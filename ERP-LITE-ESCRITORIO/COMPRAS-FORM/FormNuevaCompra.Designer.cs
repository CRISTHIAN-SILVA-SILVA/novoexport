﻿namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    partial class FormNuevaCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNuevaCompra));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.comboMedioPago = new System.Windows.Forms.ComboBox();
            this.comboTipoPago = new System.Windows.Forms.ComboBox();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.btnEliminar = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.gridProducto = new System.Windows.Forms.DataGridView();
            this.txtBuscarProducto = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.btnClose = new MaterialSkin.Controls.MaterialFlatButton();
            this.gridDetalle = new System.Windows.Forms.DataGridView();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtGuia = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtSerieNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.txtInicio = new System.Windows.Forms.DateTimePicker();
            this.txtFin = new System.Windows.Forms.DateTimePicker();
            this.comboMoneda = new System.Windows.Forms.ComboBox();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel16 = new MaterialSkin.Controls.MaterialLabel();
            this.lblCambio = new MaterialSkin.Controls.MaterialLabel();
            this.btnCompleto = new MaterialSkin.Controls.MaterialRaisedButton();
            this.comboProveedor = new System.Windows.Forms.ComboBox();
            this.comboTransporte = new System.Windows.Forms.ComboBox();
            this.materialLabel18 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel20 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel22 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel24 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel26 = new MaterialSkin.Controls.MaterialLabel();
            this.panelGuia = new System.Windows.Forms.Panel();
            this.txtDedGlobal = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.checkGuia = new MaterialSkin.Controls.MaterialCheckBox();
            this.txtNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.lblOpGravadas = new MaterialSkin.Controls.MaterialLabel();
            this.lblTotal = new MaterialSkin.Controls.MaterialLabel();
            this.lblIGV = new MaterialSkin.Controls.MaterialLabel();
            this.lblOpExoneradas = new MaterialSkin.Controls.MaterialLabel();
            this.btnSobrante = new MaterialSkin.Controls.MaterialFlatButton();
            this.checkNoEsFactura = new MaterialSkin.Controls.MaterialCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).BeginInit();
            this.panelGuia.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialLabel8
            // 
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(12, 114);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(80, 23);
            this.materialLabel8.TabIndex = 154;
            this.materialLabel8.Text = "Proveedor:";
            this.materialLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboMedioPago
            // 
            this.comboMedioPago.DisplayMember = "nombre";
            this.comboMedioPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMedioPago.Enabled = false;
            this.comboMedioPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboMedioPago.FormattingEnabled = true;
            this.comboMedioPago.Location = new System.Drawing.Point(608, 177);
            this.comboMedioPago.Name = "comboMedioPago";
            this.comboMedioPago.Size = new System.Drawing.Size(360, 26);
            this.comboMedioPago.TabIndex = 153;
            this.comboMedioPago.ValueMember = "id";
            // 
            // comboTipoPago
            // 
            this.comboTipoPago.DisplayMember = "nombre";
            this.comboTipoPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipoPago.FormattingEnabled = true;
            this.comboTipoPago.Location = new System.Drawing.Point(97, 177);
            this.comboTipoPago.Name = "comboTipoPago";
            this.comboTipoPago.Size = new System.Drawing.Size(386, 26);
            this.comboTipoPago.TabIndex = 152;
            this.comboTipoPago.ValueMember = "id";
            this.comboTipoPago.SelectedValueChanged += new System.EventHandler(this.comboTipoPago_SelectedValueChanged);
            // 
            // materialLabel7
            // 
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(504, 180);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(98, 23);
            this.materialLabel7.TabIndex = 151;
            this.materialLabel7.Text = "Medio Pago:";
            // 
            // materialLabel6
            // 
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(12, 179);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(84, 23);
            this.materialLabel6.TabIndex = 150;
            this.materialLabel6.Text = "Tipo Pago:";
            // 
            // btnEliminar
            // 
            this.btnEliminar.AutoSize = true;
            this.btnEliminar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnEliminar.Depth = 0;
            this.btnEliminar.Icon = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Icon")));
            this.btnEliminar.Location = new System.Drawing.Point(12, 600);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnEliminar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Primary = false;
            this.btnEliminar.Size = new System.Drawing.Size(97, 36);
            this.btnEliminar.TabIndex = 149;
            this.btnEliminar.Text = "Quitar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // materialLabel5
            // 
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel5.Location = new System.Drawing.Point(712, 646);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(60, 22);
            this.materialLabel5.TabIndex = 147;
            this.materialLabel5.Text = "Total:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(12, 427);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(139, 23);
            this.materialLabel3.TabIndex = 146;
            this.materialLabel3.Text = "Detalle de Pedido:";
            // 
            // gridProducto
            // 
            this.gridProducto.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.gridProducto.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridProducto.BackgroundColor = System.Drawing.Color.White;
            this.gridProducto.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.gridProducto.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridProducto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridProducto.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridProducto.EnableHeadersVisualStyles = false;
            this.gridProducto.GridColor = System.Drawing.Color.Black;
            this.gridProducto.Location = new System.Drawing.Point(12, 304);
            this.gridProducto.MultiSelect = false;
            this.gridProducto.Name = "gridProducto";
            this.gridProducto.ReadOnly = true;
            this.gridProducto.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridProducto.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridProducto.RowHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.gridProducto.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.gridProducto.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.gridProducto.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridProducto.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.gridProducto.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.gridProducto.RowTemplate.Height = 31;
            this.gridProducto.RowTemplate.ReadOnly = true;
            this.gridProducto.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridProducto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridProducto.Size = new System.Drawing.Size(957, 120);
            this.gridProducto.TabIndex = 145;
            this.gridProducto.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridProducto_CellDoubleClick);
            // 
            // txtBuscarProducto
            // 
            this.txtBuscarProducto.Depth = 0;
            this.txtBuscarProducto.Hint = "";
            this.txtBuscarProducto.Location = new System.Drawing.Point(97, 275);
            this.txtBuscarProducto.MaxLength = 32767;
            this.txtBuscarProducto.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtBuscarProducto.Name = "txtBuscarProducto";
            this.txtBuscarProducto.PasswordChar = '\0';
            this.txtBuscarProducto.SelectedText = "";
            this.txtBuscarProducto.SelectionLength = 0;
            this.txtBuscarProducto.SelectionStart = 0;
            this.txtBuscarProducto.Size = new System.Drawing.Size(441, 23);
            this.txtBuscarProducto.TabIndex = 144;
            this.txtBuscarProducto.TabStop = false;
            this.txtBuscarProducto.UseSystemPasswordChar = false;
            this.txtBuscarProducto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBuscarProducto_KeyUp);
            // 
            // materialLabel2
            // 
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(13, 275);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(79, 23);
            this.materialLabel2.TabIndex = 143;
            this.materialLabel2.Text = "Producto:";
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnClose.Depth = 0;
            this.btnClose.Icon = null;
            this.btnClose.Location = new System.Drawing.Point(904, 638);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnClose.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnClose.Name = "btnClose";
            this.btnClose.Primary = false;
            this.btnClose.Size = new System.Drawing.Size(64, 36);
            this.btnClose.TabIndex = 142;
            this.btnClose.Text = "atras";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gridDetalle
            // 
            this.gridDetalle.AllowUserToAddRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.gridDetalle.BackgroundColor = System.Drawing.Color.White;
            this.gridDetalle.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenHorizontal;
            this.gridDetalle.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridDetalle.EnableHeadersVisualStyles = false;
            this.gridDetalle.GridColor = System.Drawing.Color.Black;
            this.gridDetalle.Location = new System.Drawing.Point(12, 455);
            this.gridDetalle.MultiSelect = false;
            this.gridDetalle.Name = "gridDetalle";
            this.gridDetalle.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDetalle.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridDetalle.RowHeadersVisible = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.gridDetalle.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.gridDetalle.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.White;
            this.gridDetalle.RowTemplate.Height = 31;
            this.gridDetalle.RowTemplate.ReadOnly = true;
            this.gridDetalle.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDetalle.Size = new System.Drawing.Size(957, 136);
            this.gridDetalle.TabIndex = 140;
            this.gridDetalle.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDetalle_CellDoubleClick);
            // 
            // materialLabel1
            // 
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel1.Location = new System.Drawing.Point(6, 6);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(83, 22);
            this.materialLabel1.TabIndex = 161;
            this.materialLabel1.Text = "Transporte";
            // 
            // txtGuia
            // 
            this.txtGuia.Depth = 0;
            this.txtGuia.Hint = "";
            this.txtGuia.Location = new System.Drawing.Point(887, 5);
            this.txtGuia.MaxLength = 32767;
            this.txtGuia.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtGuia.Name = "txtGuia";
            this.txtGuia.PasswordChar = '\0';
            this.txtGuia.SelectedText = "";
            this.txtGuia.SelectionLength = 0;
            this.txtGuia.SelectionStart = 0;
            this.txtGuia.Size = new System.Drawing.Size(79, 23);
            this.txtGuia.TabIndex = 160;
            this.txtGuia.TabStop = false;
            this.txtGuia.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel4.Location = new System.Drawing.Point(12, 81);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(79, 22);
            this.materialLabel4.TabIndex = 164;
            this.materialLabel4.Text = "Serie:";
            // 
            // txtSerieNumero
            // 
            this.txtSerieNumero.Depth = 0;
            this.txtSerieNumero.Hint = "";
            this.txtSerieNumero.Location = new System.Drawing.Point(97, 81);
            this.txtSerieNumero.MaxLength = 4;
            this.txtSerieNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSerieNumero.Name = "txtSerieNumero";
            this.txtSerieNumero.PasswordChar = '\0';
            this.txtSerieNumero.SelectedText = "";
            this.txtSerieNumero.SelectionLength = 0;
            this.txtSerieNumero.SelectionStart = 0;
            this.txtSerieNumero.Size = new System.Drawing.Size(95, 23);
            this.txtSerieNumero.TabIndex = 163;
            this.txtSerieNumero.TabStop = false;
            this.txtSerieNumero.UseSystemPasswordChar = false;
            // 
            // materialLabel13
            // 
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel13.Location = new System.Drawing.Point(770, 78);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(127, 22);
            this.materialLabel13.TabIndex = 170;
            this.materialLabel13.Text = "Cambio Moneda:";
            // 
            // txtInicio
            // 
            this.txtInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicio.Location = new System.Drawing.Point(97, 145);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(259, 24);
            this.txtInicio.TabIndex = 171;
            // 
            // txtFin
            // 
            this.txtFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFin.Location = new System.Drawing.Point(470, 145);
            this.txtFin.Name = "txtFin";
            this.txtFin.Size = new System.Drawing.Size(259, 24);
            this.txtFin.TabIndex = 172;
            // 
            // comboMoneda
            // 
            this.comboMoneda.DisplayMember = "nombre";
            this.comboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboMoneda.FormattingEnabled = true;
            this.comboMoneda.Items.AddRange(new object[] {
            "SOLES",
            "DOLARES"});
            this.comboMoneda.Location = new System.Drawing.Point(809, 143);
            this.comboMoneda.Name = "comboMoneda";
            this.comboMoneda.Size = new System.Drawing.Size(160, 26);
            this.comboMoneda.TabIndex = 173;
            this.comboMoneda.ValueMember = "id";
            this.comboMoneda.SelectedIndexChanged += new System.EventHandler(this.comboMoneda_SelectedIndexChanged);
            // 
            // materialLabel14
            // 
            this.materialLabel14.Depth = 0;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel14.Location = new System.Drawing.Point(735, 147);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(68, 22);
            this.materialLabel14.TabIndex = 174;
            this.materialLabel14.Text = "Moneda:";
            // 
            // materialLabel15
            // 
            this.materialLabel15.Depth = 0;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.Location = new System.Drawing.Point(12, 147);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(79, 23);
            this.materialLabel15.TabIndex = 175;
            this.materialLabel15.Text = "Fecha E.:";
            // 
            // materialLabel16
            // 
            this.materialLabel16.Depth = 0;
            this.materialLabel16.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel16.Location = new System.Drawing.Point(369, 146);
            this.materialLabel16.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel16.Name = "materialLabel16";
            this.materialLabel16.Size = new System.Drawing.Size(95, 23);
            this.materialLabel16.TabIndex = 176;
            this.materialLabel16.Text = "Fecha Vcto:";
            // 
            // lblCambio
            // 
            this.lblCambio.Depth = 0;
            this.lblCambio.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCambio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCambio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCambio.Location = new System.Drawing.Point(913, 78);
            this.lblCambio.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCambio.Name = "lblCambio";
            this.lblCambio.Size = new System.Drawing.Size(55, 22);
            this.lblCambio.TabIndex = 177;
            this.lblCambio.Text = "1.0000";
            // 
            // btnCompleto
            // 
            this.btnCompleto.AutoSize = true;
            this.btnCompleto.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCompleto.Depth = 0;
            this.btnCompleto.Icon = null;
            this.btnCompleto.Location = new System.Drawing.Point(884, 597);
            this.btnCompleto.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCompleto.Name = "btnCompleto";
            this.btnCompleto.Primary = true;
            this.btnCompleto.Size = new System.Drawing.Size(84, 36);
            this.btnCompleto.TabIndex = 178;
            this.btnCompleto.Text = "GUARDAR";
            this.btnCompleto.UseVisualStyleBackColor = true;
            this.btnCompleto.Click += new System.EventHandler(this.btnCompleto_Click);
            // 
            // comboProveedor
            // 
            this.comboProveedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboProveedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboProveedor.DisplayMember = "razonSocial";
            this.comboProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboProveedor.FormattingEnabled = true;
            this.comboProveedor.Location = new System.Drawing.Point(97, 111);
            this.comboProveedor.MaxLength = 200;
            this.comboProveedor.Name = "comboProveedor";
            this.comboProveedor.Size = new System.Drawing.Size(871, 26);
            this.comboProveedor.TabIndex = 179;
            this.comboProveedor.ValueMember = "id";
            // 
            // comboTransporte
            // 
            this.comboTransporte.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboTransporte.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboTransporte.DisplayMember = "razonSocial";
            this.comboTransporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTransporte.FormattingEnabled = true;
            this.comboTransporte.Location = new System.Drawing.Point(95, 3);
            this.comboTransporte.MaxLength = 200;
            this.comboTransporte.Name = "comboTransporte";
            this.comboTransporte.Size = new System.Drawing.Size(675, 26);
            this.comboTransporte.TabIndex = 180;
            this.comboTransporte.ValueMember = "id";
            // 
            // materialLabel18
            // 
            this.materialLabel18.Depth = 0;
            this.materialLabel18.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel18.Location = new System.Drawing.Point(785, 6);
            this.materialLabel18.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel18.Name = "materialLabel18";
            this.materialLabel18.Size = new System.Drawing.Size(96, 22);
            this.materialLabel18.TabIndex = 181;
            this.materialLabel18.Text = "N° GUIA:";
            // 
            // materialLabel20
            // 
            this.materialLabel20.Depth = 0;
            this.materialLabel20.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel20.Location = new System.Drawing.Point(660, 602);
            this.materialLabel20.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel20.Name = "materialLabel20";
            this.materialLabel20.Size = new System.Drawing.Size(112, 22);
            this.materialLabel20.TabIndex = 184;
            this.materialLabel20.Text = "Op. Gravadas:";
            // 
            // materialLabel22
            // 
            this.materialLabel22.Depth = 0;
            this.materialLabel22.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel22.Location = new System.Drawing.Point(712, 624);
            this.materialLabel22.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel22.Name = "materialLabel22";
            this.materialLabel22.Size = new System.Drawing.Size(60, 22);
            this.materialLabel22.TabIndex = 186;
            this.materialLabel22.Text = "IGV:";
            // 
            // materialLabel24
            // 
            this.materialLabel24.Depth = 0;
            this.materialLabel24.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel24.Location = new System.Drawing.Point(407, 644);
            this.materialLabel24.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel24.Name = "materialLabel24";
            this.materialLabel24.Size = new System.Drawing.Size(120, 22);
            this.materialLabel24.TabIndex = 188;
            this.materialLabel24.Text = "Op. Exoneradas:";
            // 
            // materialLabel26
            // 
            this.materialLabel26.Depth = 0;
            this.materialLabel26.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel26.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel26.Location = new System.Drawing.Point(404, 600);
            this.materialLabel26.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel26.Name = "materialLabel26";
            this.materialLabel26.Size = new System.Drawing.Size(137, 22);
            this.materialLabel26.TabIndex = 193;
            this.materialLabel26.Text = "Descuento Global:";
            // 
            // panelGuia
            // 
            this.panelGuia.Controls.Add(this.materialLabel1);
            this.panelGuia.Controls.Add(this.txtGuia);
            this.panelGuia.Controls.Add(this.comboTransporte);
            this.panelGuia.Controls.Add(this.materialLabel18);
            this.panelGuia.Location = new System.Drawing.Point(2, 236);
            this.panelGuia.Name = "panelGuia";
            this.panelGuia.Size = new System.Drawing.Size(970, 33);
            this.panelGuia.TabIndex = 198;
            this.panelGuia.Visible = false;
            // 
            // txtDedGlobal
            // 
            this.txtDedGlobal.Depth = 0;
            this.txtDedGlobal.Hint = "";
            this.txtDedGlobal.Location = new System.Drawing.Point(554, 597);
            this.txtDedGlobal.MaxLength = 32767;
            this.txtDedGlobal.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDedGlobal.Name = "txtDedGlobal";
            this.txtDedGlobal.PasswordChar = '\0';
            this.txtDedGlobal.SelectedText = "";
            this.txtDedGlobal.SelectionLength = 0;
            this.txtDedGlobal.SelectionStart = 0;
            this.txtDedGlobal.Size = new System.Drawing.Size(79, 23);
            this.txtDedGlobal.TabIndex = 199;
            this.txtDedGlobal.TabStop = false;
            this.txtDedGlobal.Text = "0";
            this.txtDedGlobal.UseSystemPasswordChar = false;
            // 
            // checkGuia
            // 
            this.checkGuia.AutoSize = true;
            this.checkGuia.Depth = 0;
            this.checkGuia.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkGuia.Location = new System.Drawing.Point(9, 203);
            this.checkGuia.Margin = new System.Windows.Forms.Padding(0);
            this.checkGuia.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkGuia.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkGuia.Name = "checkGuia";
            this.checkGuia.Ripple = true;
            this.checkGuia.Size = new System.Drawing.Size(84, 30);
            this.checkGuia.TabIndex = 205;
            this.checkGuia.Text = "Con guia";
            this.checkGuia.UseVisualStyleBackColor = true;
            this.checkGuia.CheckedChanged += new System.EventHandler(this.checkGuia_CheckedChanged);
            // 
            // txtNumero
            // 
            this.txtNumero.Depth = 0;
            this.txtNumero.Hint = "";
            this.txtNumero.Location = new System.Drawing.Point(272, 80);
            this.txtNumero.MaxLength = 8;
            this.txtNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.Size = new System.Drawing.Size(95, 23);
            this.txtNumero.TabIndex = 206;
            this.txtNumero.TabStop = false;
            this.txtNumero.UseSystemPasswordChar = false;
            // 
            // materialLabel12
            // 
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel12.Location = new System.Drawing.Point(198, 81);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(68, 22);
            this.materialLabel12.TabIndex = 207;
            this.materialLabel12.Text = "Número:";
            // 
            // lblOpGravadas
            // 
            this.lblOpGravadas.Depth = 0;
            this.lblOpGravadas.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblOpGravadas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOpGravadas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOpGravadas.Location = new System.Drawing.Point(778, 600);
            this.lblOpGravadas.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblOpGravadas.Name = "lblOpGravadas";
            this.lblOpGravadas.Size = new System.Drawing.Size(79, 22);
            this.lblOpGravadas.TabIndex = 210;
            this.lblOpGravadas.Text = "0.0000";
            // 
            // lblTotal
            // 
            this.lblTotal.Depth = 0;
            this.lblTotal.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTotal.Location = new System.Drawing.Point(778, 646);
            this.lblTotal.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(79, 22);
            this.lblTotal.TabIndex = 211;
            this.lblTotal.Text = "0.0000";
            this.lblTotal.TextChanged += new System.EventHandler(this.lblTotal_TextChanged);
            // 
            // lblIGV
            // 
            this.lblIGV.Depth = 0;
            this.lblIGV.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblIGV.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblIGV.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblIGV.Location = new System.Drawing.Point(778, 624);
            this.lblIGV.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblIGV.Name = "lblIGV";
            this.lblIGV.Size = new System.Drawing.Size(79, 22);
            this.lblIGV.TabIndex = 212;
            this.lblIGV.Text = "0.0000";
            // 
            // lblOpExoneradas
            // 
            this.lblOpExoneradas.Depth = 0;
            this.lblOpExoneradas.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblOpExoneradas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOpExoneradas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOpExoneradas.Location = new System.Drawing.Point(554, 644);
            this.lblOpExoneradas.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblOpExoneradas.Name = "lblOpExoneradas";
            this.lblOpExoneradas.Size = new System.Drawing.Size(79, 22);
            this.lblOpExoneradas.TabIndex = 213;
            this.lblOpExoneradas.Text = "0.0000";
            // 
            // btnSobrante
            // 
            this.btnSobrante.AutoSize = true;
            this.btnSobrante.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSobrante.Depth = 0;
            this.btnSobrante.Icon = ((System.Drawing.Image)(resources.GetObject("btnSobrante.Icon")));
            this.btnSobrante.Location = new System.Drawing.Point(12, 638);
            this.btnSobrante.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSobrante.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSobrante.Name = "btnSobrante";
            this.btnSobrante.Primary = false;
            this.btnSobrante.Size = new System.Drawing.Size(184, 36);
            this.btnSobrante.TabIndex = 214;
            this.btnSobrante.Text = "AGREGAR SOBRANTE";
            this.btnSobrante.UseVisualStyleBackColor = true;
            this.btnSobrante.Visible = false;
            this.btnSobrante.Click += new System.EventHandler(this.btnSobrante_Click);
            // 
            // checkNoEsFactura
            // 
            this.checkNoEsFactura.AutoSize = true;
            this.checkNoEsFactura.Depth = 0;
            this.checkNoEsFactura.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkNoEsFactura.Location = new System.Drawing.Point(478, 70);
            this.checkNoEsFactura.Margin = new System.Windows.Forms.Padding(0);
            this.checkNoEsFactura.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkNoEsFactura.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkNoEsFactura.Name = "checkNoEsFactura";
            this.checkNoEsFactura.Ripple = true;
            this.checkNoEsFactura.Size = new System.Drawing.Size(116, 30);
            this.checkNoEsFactura.TabIndex = 215;
            this.checkNoEsFactura.Text = "No Es Factura";
            this.checkNoEsFactura.UseVisualStyleBackColor = true;
            // 
            // FormNuevaCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 679);
            this.Controls.Add(this.checkNoEsFactura);
            this.Controls.Add(this.btnSobrante);
            this.Controls.Add(this.lblOpExoneradas);
            this.Controls.Add(this.lblIGV);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblOpGravadas);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.checkGuia);
            this.Controls.Add(this.txtDedGlobal);
            this.Controls.Add(this.panelGuia);
            this.Controls.Add(this.materialLabel26);
            this.Controls.Add(this.materialLabel24);
            this.Controls.Add(this.materialLabel22);
            this.Controls.Add(this.materialLabel20);
            this.Controls.Add(this.comboProveedor);
            this.Controls.Add(this.btnCompleto);
            this.Controls.Add(this.lblCambio);
            this.Controls.Add(this.materialLabel16);
            this.Controls.Add(this.materialLabel15);
            this.Controls.Add(this.materialLabel14);
            this.Controls.Add(this.comboMoneda);
            this.Controls.Add(this.txtFin);
            this.Controls.Add(this.txtInicio);
            this.Controls.Add(this.materialLabel13);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.txtSerieNumero);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.comboMedioPago);
            this.Controls.Add(this.comboTipoPago);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.gridProducto);
            this.Controls.Add(this.txtBuscarProducto);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gridDetalle);
            this.MaximizeBox = false;
            this.Name = "FormNuevaCompra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compra";
            this.Load += new System.EventHandler(this.FormNuevaCompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDetalle)).EndInit();
            this.panelGuia.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private System.Windows.Forms.ComboBox comboMedioPago;
        private System.Windows.Forms.ComboBox comboTipoPago;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialFlatButton btnEliminar;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private System.Windows.Forms.DataGridView gridProducto;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtBuscarProducto;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialFlatButton btnClose;
        private System.Windows.Forms.DataGridView gridDetalle;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtGuia;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSerieNumero;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private System.Windows.Forms.DateTimePicker txtInicio;
        private System.Windows.Forms.DateTimePicker txtFin;
        private System.Windows.Forms.ComboBox comboMoneda;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialLabel materialLabel16;
        private MaterialSkin.Controls.MaterialLabel lblCambio;
        private MaterialSkin.Controls.MaterialRaisedButton btnCompleto;
        private System.Windows.Forms.ComboBox comboProveedor;
        private System.Windows.Forms.ComboBox comboTransporte;
        private MaterialSkin.Controls.MaterialLabel materialLabel18;
        private MaterialSkin.Controls.MaterialLabel materialLabel20;
        private MaterialSkin.Controls.MaterialLabel materialLabel22;
        private MaterialSkin.Controls.MaterialLabel materialLabel24;
        private MaterialSkin.Controls.MaterialLabel materialLabel26;
        private System.Windows.Forms.Panel panelGuia;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDedGlobal;
        private MaterialSkin.Controls.MaterialCheckBox checkGuia;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumero;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialLabel lblOpGravadas;
        private MaterialSkin.Controls.MaterialLabel lblTotal;
        private MaterialSkin.Controls.MaterialLabel lblIGV;
        private MaterialSkin.Controls.MaterialLabel lblOpExoneradas;
        private MaterialSkin.Controls.MaterialFlatButton btnSobrante;
        private MaterialSkin.Controls.MaterialCheckBox checkNoEsFactura;
    }
}