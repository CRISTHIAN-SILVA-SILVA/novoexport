﻿namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    partial class FormAgregarSobrante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCompleto = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel26 = new MaterialSkin.Controls.MaterialLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCompleto
            // 
            this.btnCompleto.AutoSize = true;
            this.btnCompleto.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCompleto.Depth = 0;
            this.btnCompleto.Icon = null;
            this.btnCompleto.Location = new System.Drawing.Point(253, 129);
            this.btnCompleto.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCompleto.Name = "btnCompleto";
            this.btnCompleto.Primary = true;
            this.btnCompleto.Size = new System.Drawing.Size(84, 36);
            this.btnCompleto.TabIndex = 179;
            this.btnCompleto.Text = "GUARDAR";
            this.btnCompleto.UseVisualStyleBackColor = true;
            this.btnCompleto.Click += new System.EventHandler(this.btnCompleto_Click);
            // 
            // txt
            // 
            this.txt.Depth = 0;
            this.txt.Hint = "";
            this.txt.Location = new System.Drawing.Point(285, 79);
            this.txt.MaxLength = 32767;
            this.txt.MouseState = MaterialSkin.MouseState.HOVER;
            this.txt.Name = "txt";
            this.txt.PasswordChar = '\0';
            this.txt.SelectedText = "";
            this.txt.SelectionLength = 0;
            this.txt.SelectionStart = 0;
            this.txt.Size = new System.Drawing.Size(79, 23);
            this.txt.TabIndex = 201;
            this.txt.TabStop = false;
            this.txt.Text = "0";
            this.txt.UseSystemPasswordChar = false;
            // 
            // materialLabel26
            // 
            this.materialLabel26.Depth = 0;
            this.materialLabel26.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel26.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel26.Location = new System.Drawing.Point(123, 80);
            this.materialLabel26.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel26.Name = "materialLabel26";
            this.materialLabel26.Size = new System.Drawing.Size(137, 22);
            this.materialLabel26.TabIndex = 200;
            this.materialLabel26.Text = "CANTIDAD:";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(537, 24);
            this.label1.TabIndex = 202;
            this.label1.Text = "ADMINISTRACION DE COMPRAS";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormAgregarSobrante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 209);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt);
            this.Controls.Add(this.materialLabel26);
            this.Controls.Add(this.btnCompleto);
            this.MaximizeBox = false;
            this.Name = "FormAgregarSobrante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAgregarSobrante";
            this.Load += new System.EventHandler(this.FormAgregarSobrante_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialRaisedButton btnCompleto;
        private MaterialSkin.Controls.MaterialSingleLineTextField txt;
        private MaterialSkin.Controls.MaterialLabel materialLabel26;
        private System.Windows.Forms.Label label1;
    }
}