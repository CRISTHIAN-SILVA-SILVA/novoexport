﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.COMPRAS_FORM
{
    public partial class FormBuscraProducto : Form
    {
        Almacen a;
        AlmacenService aService = new AlmacenImpl();
        public delegate void pasar(ProductoPresentacion p);
        public event pasar pasado;
        ProductoPresentacionService ppService = new ProductoPresentacionImpl();
        public FormBuscraProducto()
        {
            InitializeComponent();
            a = aService.buscarPrincipal();
        }

        private void FormBuscraProducto_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtBuscarProducto.Focus();
        }

        private void gridProducto_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtBuscarProducto_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                gridProducto.DataSource = null;
                if (Util.validaCadena(txtBuscarProducto.Text))
                {
                    gridProducto.DataSource = ppService.buscarPresentacionesPorAlmacen(a.id, txtBuscarProducto.Text.ToUpper());
                    gridProducto.Columns[0].Visible = false;
                    gridProducto.Columns[1].Visible = false;
                    gridProducto.Columns[2].Width = 690;
                    gridProducto.Columns[3].Width = 140;
                    gridProducto.Columns[4].Width = 100; gridProducto.Columns[5].Visible = false; gridProducto.Columns[6].Visible = false;
                }
            }
            catch (Exception k) { MessageBox.Show(k.Message + "  " + k.StackTrace + "    " + e.ToString()); }
        }

        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MessageBox.Show("SEGURO DE CAMBIAR PRODUCTO?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                pasado(ppService.buscar(Convert.ToInt32(gridProducto.Rows[e.RowIndex].Cells["IDPRESENTACION"].Value.ToString())));
                this.Close();
            }
        }
    }
}
