﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.RELACIONES
{
    public partial class FormAdminProveedores : MaterialForm
    {
        DataTable tabla = new DataTable();
        ProveedorImpl pService = new ProveedorImpl();
        public FormAdminProveedores()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            tabla.Columns.Add("ID");
            tabla.Columns.Add("RUC");
            tabla.Columns.Add("RAZON SOCIAL");
            tabla.Columns.Add("TELEFONO");
            tabla.Columns.Add("EMAIL");

            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 600;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 235;
        }
        void llenar(Proveedor x, int fila)
        {
            if (fila == -1)
                tabla.Rows.Add(x.id, x.ruc, x.razonSocial, x.telefono, x.email);
            else
            {
                grid.Rows[fila].Cells["RAZON SOCIAL"].Value = x.razonSocial;
                grid.Rows[fila].Cells["TELEFONO"].Value = x.telefono;
                grid.Rows[fila].Cells["EMAIL"].Value = x.email;
            }
        }
        private void FormAdminProveedores_Load(object sender, EventArgs e)
        {
            pService.listarNoAnulados().ForEach(x => tabla.Rows.Add(x.id, x.ruc, x.razonSocial, x.telefono, x.email));
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevoProveedor form = new FormNuevoProveedor(null, 0);
            form.pasado += new FormNuevoProveedor.pasar(llenar);
            form.ShowDialog();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ELIMINAR CLIENTE?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    pService.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    tabla.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Proveedor mod = pService.buscar(Convert.ToInt32(grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            if (mod != null)
            {
                FormNuevoProveedor form = new FormNuevoProveedor(mod, e.RowIndex);
                form.pasado += new FormNuevoProveedor.pasar(llenar);
                form.ShowDialog();
            }
        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            tabla.Clear();
            pService.buscars(txtBuscar.Text.ToUpper()).ForEach(x=>tabla.Rows.Add(x.id,x.ruc,x.razonSocial,x.telefono,x.email));
        }
    }
}
