﻿using ERP_LITE_ESCRITORIO.RELACIONES;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.RELACIONES
{
    public partial class FormNuevoProveedor : MaterialForm

    {
        DataTable tabla = new DataTable();
        ProveedorService pService = new ProveedorImpl();
        CuentaProveedorService cpservice = new CuentaProveedorImpl();
        public delegate void pasar(Proveedor cliente, int fila);
        public event pasar pasado;
        int filaMod = 0;
        Proveedor modifica = null;
        void habilitar(bool v)
        {
            txtRazonSocial.Enabled = v;

            txtDireccion.Enabled = v;
       
            
        }

        public FormNuevoProveedor(Proveedor p,int f)
        {
            modifica = p;
            filaMod = f;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            tabla.Columns.Add("ID");
            tabla.Columns.Add("BANCO");
            tabla.Columns.Add("CUENTA");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 400;
            grid.Columns[2].Width = 300;
            if (modifica != null)
            {
                panel1.Visible = true;
                
                habilitar(false);
                txtRuc.Enabled = false;
                txtRuc.Text = modifica.ruc;
                txtRazonSocial.Text = modifica.razonSocial;
                txtDireccion.Text = modifica.direccion;
                txtDni.Text = modifica.dniRepresentante;
                txtEmail.Text = modifica.email;
                txtTelefono.Text = modifica.telefono;
                lblTipoContribuyente.Text = modifica.tipoContribuyente;
                lblFechaInicio.Text = modifica.fechaInscripcion.ToShortDateString();
            }
            else { this.Height = 370; }
        }

        private void FormNuevoProveedor_Load(object sender, EventArgs e)
        {
            if(modifica!=null) foreach (var i in modifica.cuentasProveedor)
                    tabla.Rows.Add(i.id, i.banco, i.numeroCuenta);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConsultaSunat_Click(object sender, EventArgs e)
        {
            if (Util.esRUC(txtRuc.Text))
            {
                Cliente c = Consultas.consularRUC(txtRuc.Text);
                if (c == null)
                {
                    MessageBox.Show("EL SERVICIO NO RESPONDE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (c.ruc == null)
                {
                    MessageBox.Show("EL PROVEEDOR NO EXISTE EN SUNAT", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    txtRazonSocial.Text = c.razonSocial;

                    if (c.activoSUNAT)
                        lblEstado.Text = "HABILITADO";
                    else lblEstado.Text = "DESABILITADO";

                    txtDireccion.Text = c.direccion;
                    lblFechaInicio.Text = c.fechaInscripcion.ToShortDateString();
                    lblTipoContribuyente.Text = c.tipoContribuyente;
                    // habilitar(false);
                }
            }
            else MessageBox.Show("RUC INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE GUARDAR ?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)

                if (modifica == null)
                {
                    if (Util.validaCadena(txtRazonSocial.Text))

                        if (Util.esRUC(txtRuc.Text))
                        {

                            Proveedor cliente = new Proveedor();
                            cliente.telefono = txtTelefono.Text;
                            cliente.ruc = txtRuc.Text;
                            if (lblEstado.Text.Equals("HABILITADO"))
                                cliente.activoSUNAT = true;
                            else cliente.activoSUNAT = false;
                            cliente.direccion = txtDireccion.Text;
                            cliente.dniRepresentante = txtDni.Text;
                            cliente.email = txtEmail.Text;
                            //solo si fue buscado
                            if (!lblFechaInicio.Text.Trim().Equals(""))
                                cliente.fechaInscripcion = DateTime.Parse(lblFechaInicio.Text);
                            cliente.razonSocial = txtRazonSocial.Text;
                            cliente.tipoContribuyente = lblTipoContribuyente.Text;
                            Proveedor existe = pService.existe(cliente);
                            if (existe == null)
                            {
                                pService.crear(cliente);
                                pasado(cliente, -1);
                                this.Close();
                            }
                            else if (existe.anulado == true)
                            {
                                if (MessageBox.Show("YA EXISTE DESA RESTABLECERLO", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                {
                                    existe.anulado = false;
                                    pService.editar(existe);
                                    pasado(existe, -1);
                                    this.Close();
                                }
                            }
                            else
                                MessageBox.Show("YA EXISTE UN CLIENTE CON ESTE RUC", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        else MessageBox.Show("RUC INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else MessageBox.Show("FALTA RAZON SOCIAL", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    modifica.telefono = txtTelefono.Text;
                    if (lblEstado.Text.Equals("HABILITADO"))
                        modifica.activoSUNAT = true;
                    else modifica.activoSUNAT = false;
                    modifica.direccion = txtDireccion.Text;
                    modifica.dniRepresentante = txtDni.Text;
                    modifica.email = txtEmail.Text;
                    //solo si fue buscado
                    if (!lblFechaInicio.Text.Trim().Equals(""))
                        modifica.fechaInscripcion = DateTime.Parse(lblFechaInicio.Text);
                    modifica.razonSocial = txtRazonSocial.Text;
                    modifica.tipoContribuyente = lblTipoContribuyente.Text;
                    pService.editar(modifica);
                    pasado(modifica, filaMod);
                    this.Close();

                }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaCuentaProveedor form = new FormNuevaCuentaProveedor(modifica);
            form.pasado += new FormNuevaCuentaProveedor.pasar(agregar);
            form.ShowDialog();
        }
        void agregar(CuentaProveedor cp) {
            tabla.Rows.Add(cp.id,cp.banco,cp.numeroCuenta);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1) {
                CuentaProveedor c = cpservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                if (c != null)
                {
                    c.anulado = false;
                    cpservice.editar(c);
                    grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
            }
        }
    }
}
