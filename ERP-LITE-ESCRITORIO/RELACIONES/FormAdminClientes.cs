﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.RELACIONES
{
    public partial class FormAdminClientes : MaterialForm
    {
        ClienteImpl cService = new ClienteImpl();
        List<Cliente> clientes = new List<Cliente>();
        DataTable tabla = new DataTable();
        public FormAdminClientes()
        {
            InitializeComponent();
            clientes= cService.listarNoAnulados();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            tabla.Columns.Add("ID");
            tabla.Columns.Add("DOCUMENTO");
            tabla.Columns.Add("DENOMINACION");
            tabla.Columns.Add("TELEFONO");
            tabla.Columns.Add("EMAIL");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 600;
            grid.Columns[3].Width = 100;
            grid.Columns[4].Width = 235;
        }
        void llenar(Cliente x,int fila) {
            if (fila == -1)
            {
                if(x.esEmpresa)
                tabla.Rows.Add(x.id, x.ruc, x.razonSocial, x.telefono, x.email);
                else tabla.Rows.Add(x.id, x.dniRepresentante, x.razonSocial, x.telefono, x.email);

            }
            else
            {
                grid.Rows[fila].Cells["DENOMINACION"].Value = x.razonSocial;
                grid.Rows[fila].Cells["TELEFONO"].Value = x.telefono;
                grid.Rows[fila].Cells["EMAIL"].Value = x.email;
            }
        }
        private void FormAdminClientes_Load(object sender, EventArgs e)
        {
            rbEmpresas.Checked = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            if (rbClientes.Checked)
            {
                FormNuevoClientePersona form = new FormNuevoClientePersona(null, 0);
                form.pasado += new FormNuevoClientePersona.pasar(llenar);
                form.ShowDialog();

            }
            else {

                FormNuevoCliente form = new FormNuevoCliente(null, 0,true);
                form.pasado += new FormNuevoCliente.pasar(llenar);
                form.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ELIMINAR CLIENTE?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
               
                cService.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    tabla.Rows.RemoveAt(grid.SelectedRows[0].Index);
            }

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cliente mod = cService.buscar(Convert.ToInt32( grid.Rows[e.RowIndex].Cells["ID"].Value.ToString()));
            if (mod != null)
            {
                if (mod.esEmpresa)
                {
                    FormNuevoCliente form = new FormNuevoCliente(mod, e.RowIndex,true);
                    form.pasado += new FormNuevoCliente.pasar(llenar);
                    form.ShowDialog();
                }
                else {
                    FormNuevoClientePersona form = new FormNuevoClientePersona(mod, e.RowIndex);
                    form.pasado += new FormNuevoClientePersona.pasar(llenar);
                    form.ShowDialog();
                }
            }
        }

        private void rbClientes_CheckedChanged(object sender, EventArgs e)
        {
            if (rbClientes.Checked) {
                tabla.Clear();
                cService.listarClientes().ForEach(x => tabla.Rows.Add(x.id, x.dniRepresentante, x.razonSocial, x.telefono, x.email));
                
            }
        }

        private void rbEmpresas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbEmpresas.Checked) {
                tabla.Clear();
                cService.listarEmpresas().ForEach(x => tabla.Rows.Add(x.id, x.ruc, x.razonSocial, x.telefono, x.email));
            }
        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            tabla.Clear();
              cService.buscarCliente(txtBuscar.Text.ToUpper()).ForEach(x=> tabla.Rows.Add(x.id,x.ruc,x.razonSocial,x.telefono,x.email));
        }
    }
}
