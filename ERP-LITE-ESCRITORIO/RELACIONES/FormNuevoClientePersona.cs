﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.RELACIONES
{
    public partial class FormNuevoClientePersona : MaterialForm
    {
        ClienteService cservice = new ClienteImpl();
        public delegate void pasar(Cliente cliente, int fila);
        public event pasar pasado;
        int filaMod = 0;
        Cliente modifica = null;
        public FormNuevoClientePersona(Cliente m,int f)
        {
            modifica = m;
            filaMod = f;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            if (modifica != null)
            {
                txtDNI.Enabled = false;
                txtDNI.Text = modifica.dniRepresentante;
                txtNombre.Text = modifica.razonSocial;
                txtDireccion.Text = modifica.direccion;
                txtEmail.Text = modifica.email;
                txtTelefono.Text = modifica.telefono;
            }
        }

        private void FormNuevoClientePersona_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtDNI.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("SEGURO DE AGREGAR?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            if (Util.esDNI(txtDNI.Text))
            {
                    if (Util.validaCadena(txtNombre.Text))
                    {
                        if (modifica != null)
                        {
                            Cliente cliente = new Cliente();
                            cliente.razonSocial = txtNombre.Text;
                            cliente.dniRepresentante = txtDNI.Text;
                            cliente.direccion = txtDireccion.Text;
                            cliente.email = txtEmail.Text;
                            cliente.esEmpresa = false;
                            Cliente existe = cservice.existe(cliente);
                            if (existe == null)
                            {
                                cservice.crear(cliente);
                                pasado(cliente, -1);
                                this.Close();
                            }
                            else if (existe.anulado == true)
                            {
                                if (MessageBox.Show("YA EXISTE DESEA RESTABLECERLO", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                {
                                    existe.anulado = false;
                                    cservice.editar(existe);
                                    pasado(existe, -1);
                                    this.Close();
                                }
                            }
                            else
                                MessageBox.Show("YA EXISTE UN CLIENTE CON ESTE DNI", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            
                        }
                        else {
                            modifica.telefono = txtTelefono.Text;
                            modifica.direccion = txtDireccion.Text;
                            modifica.email = txtEmail.Text;
                            modifica.razonSocial = txtNombre.Text;
                            cservice.editar(modifica);
                            pasado(modifica, filaMod);
                            this.Close();
                        }

           
                  
                }
                else MessageBox.Show("FALTA NOMBRE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else MessageBox.Show("DNI INVALIDO","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information);

        }
    }
}
