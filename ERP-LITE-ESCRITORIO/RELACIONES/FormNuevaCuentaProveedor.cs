﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.RELACIONES
{
    public partial class FormNuevaCuentaProveedor : MaterialForm
    {
        CuentaProveedorService cpservice = new CuentaProveedorImpl();
        Proveedor proveedor = null;
        public delegate void pasar(CuentaProveedor cp);
        public event pasar pasado;
        
        public FormNuevaCuentaProveedor(Proveedor p)
        {
            InitializeComponent();
            proveedor = p;
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormNuevaCuentaProveedor_Load(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Util.validaCadena(txtBanco.Text) && Util.validaCadena(txtCuenta.Text))
            {
                CuentaProveedor cp = new CuentaProveedor();
                cp.banco = txtBanco.Text.ToUpper() ;
                cp.numeroCuenta = txtCuenta.Text;
                cp.idProveedor = proveedor.id;
                cpservice.crear(cp);
                pasado(cp);
                this.Close();
            }
            else MessageBox.Show("FALTA LLENAR UN CAMPO","AVISO",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
        }
    }
}
