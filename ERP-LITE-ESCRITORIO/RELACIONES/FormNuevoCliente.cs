﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.RELACIONES
{
    public partial class FormNuevoCliente : MaterialForm
    {
        ClienteService cService = new ClienteImpl();
        public delegate void pasar(Cliente cliente,int fila);
        public event pasar pasado;
        public delegate void pasarVenta(Cliente cliente);
        public event pasarVenta pasadoVenta;
        int filaMod = 0;
        Cliente modifica = null;
        bool esPrincipal = true;
        public FormNuevoCliente(Cliente m ,int f,bool prin)
        {
            esPrincipal = prin;
            modifica = m;
            filaMod = f;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            if (modifica != null) {
                habilitar(false);
                txtRuc.Enabled = false;
                txtRuc.Text = modifica.ruc;
                txtRazonSocial.Text = modifica.razonSocial;
                txtDireccion.Text = modifica.direccion;
                txtDni.Text = modifica.dniRepresentante;
                txtEmail.Text = modifica.email;
                txtTelefono.Text = modifica.telefono;
                lblTipoContribuyente.Text = modifica.tipoContribuyente;
                lblFechaInicio.Text = modifica.fechaInscripcion.ToShortDateString();
            }

        }

        private void FormNuevoCliente_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtRuc.Focus();
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE GUARDAR ?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                if (!lblEstado.Text.Trim().Equals("DESABILITADO"))
                {
                    if (modifica == null)
                    {
                        if (Util.validaCadena(txtRazonSocial.Text))

                            if (Util.esRUC(txtRuc.Text))
                            {

                                Cliente cliente = new Cliente();
                                cliente.telefono = txtTelefono.Text;
                                cliente.ruc = txtRuc.Text;
                                cliente.esEmpresa = true;
                                if (lblEstado.Text.Equals("HABILITADO"))
                                    cliente.activoSUNAT = true;
                                else cliente.activoSUNAT = false;
                                cliente.direccion = txtDireccion.Text;
                                cliente.dniRepresentante = txtDni.Text;
                                cliente.email = txtEmail.Text;
                                //solo si fue buscado
                                if (!lblFechaInicio.Text.Trim().Equals(""))
                                    cliente.fechaInscripcion = DateTime.Parse(lblFechaInicio.Text);
                                cliente.razonSocial = txtRazonSocial.Text;
                                cliente.tipoContribuyente = lblTipoContribuyente.Text;
                                Cliente existe = cService.existe(cliente);
                                if (existe == null)
                                {
                                    cService.crear(cliente);
                                    if (esPrincipal)
                                        pasado(cliente, -1);
                                    else pasadoVenta(cliente);
                                    this.Close();
                                }
                                else if (existe.anulado == true)
                                {
                                    if (MessageBox.Show("YA EXISTE DESEA RESTABLECERLO", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                                    {
                                        existe.anulado = false;
                                        cService.editar(existe);
                                        pasado(existe, -1);
                                        this.Close();
                                    }
                                }
                                else
                                    MessageBox.Show("YA EXISTE UN CLIENTE CON ESTE RUC", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                            else MessageBox.Show("RUC INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else MessageBox.Show("FALTA RAZON SOCIAL", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        modifica.telefono = txtTelefono.Text;
                        if (lblEstado.Text.Equals("HABILITADO"))
                            modifica.activoSUNAT = true;
                        else modifica.activoSUNAT = false;
                        modifica.direccion = txtDireccion.Text;
                        modifica.dniRepresentante = txtDni.Text;
                        modifica.email = txtEmail.Text;
                        //solo si fue buscado
                        if (!lblFechaInicio.Text.Trim().Equals(""))
                            modifica.fechaInscripcion = DateTime.Parse(lblFechaInicio.Text);
                        modifica.razonSocial = txtRazonSocial.Text;
                        modifica.tipoContribuyente = lblTipoContribuyente.Text;
                        cService.editar(modifica);
                        pasado(modifica, filaMod);
                        this.Close();

                    }
                }
                else MessageBox.Show("EMPRESA DESABILITADA POR SUNAT; ELIMINE REGISTRO PARA EVITAR PROBLEMAS");
        }
        void habilitar(bool v) {
            txtRazonSocial.Enabled = v;
            txtDireccion.Enabled = v;          
        }
     
        private void btnConsultaSunat_Click(object sender, EventArgs e)
        {
            if (Util.esRUC(txtRuc.Text))
            {
                Cliente c = Consultas.consularRUC(txtRuc.Text);
                if (c == null)
                {
                    MessageBox.Show("EL SERVICIO NO RESPONDE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (c.ruc == null)
                {
                    MessageBox.Show("EL CLIENTE NO EXISTE EN SUNAT", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    txtRazonSocial.Text = c.razonSocial;

                    if (c.activoSUNAT)
                        lblEstado.Text = "HABILITADO";
                    else lblEstado.Text = "DESABILITADO";

                    txtDireccion.Text = c.direccion;
                    lblFechaInicio.Text = c.fechaInscripcion.ToShortDateString();
                    lblTipoContribuyente.Text = c.tipoContribuyente;
                   // habilitar(false);
                }
            }
            else MessageBox.Show("RUC INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


    }
}
