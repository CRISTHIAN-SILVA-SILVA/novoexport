﻿using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CONFIGURACION_FORM
{
    public partial class FormLocal : Form
    {
        LocalService lService=new LocalImpl();
        public FormLocal()
        {
            InitializeComponent();
            txtRuc.Text= LocalImpl.getInstancia().ruc;
            txtRazonSocial.Text = LocalImpl.getInstancia().razonSocial;
            txtNombreComercial.Text = LocalImpl.getInstancia().nombreComercial;
            txtTelefono.Text = LocalImpl.getInstancia().telefono;
            txtDireccion.Text = LocalImpl.getInstancia().direccion;
            txtEmail.Text = LocalImpl.getInstancia().email;
            txtCuentaDetraccion.Text = LocalImpl.getInstancia().cuenta;
            txtBanco.Text = LocalImpl.getInstancia().banco;
        }

    

    
        private void FormLocal_Load_1(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            if (Util.esRUC(txtRuc.Text))
                if(Util.validaCadena(txtRazonSocial.Text))
            {
                if (MessageBox.Show("SEGURO DE GUARDAR SE RENICIARA EL SISTEMA?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    LocalImpl.local.ruc = txtRuc.Text;
                    LocalImpl.local.razonSocial = txtRazonSocial.Text;
                    LocalImpl.local.nombreComercial = txtNombreComercial.Text;
                    LocalImpl.local.telefono = txtTelefono.Text;
                    LocalImpl.local.direccion = txtDireccion.Text;
                    LocalImpl.local.email = txtEmail.Text;
                    LocalImpl.local.usuarioUpdate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                    LocalImpl.local.fechaUpdate = DateTime.Now;

                        LocalImpl.local.cuenta = txtCuentaDetraccion.Text;
                        LocalImpl.local.banco = txtBanco.Text;

                        lService.editar(LocalImpl.local);
                        MessageBox.Show ("GUARDADO");
                }
            }
                else MessageBox.Show("FALTA RAZON SOCIAL ", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else MessageBox.Show("EL RUC ES INCORRECTO","AVISO",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
