﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONFIGURACION_FORM
{
    public partial class FormNuevaCuenta : MaterialForm
    {
        CuentaBancoService cservice = new CuentaBancoService();
        public delegate void pasar();
        public event pasar pasado;
        public FormNuevaCuenta()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormNuevaCuenta_Load(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                if (Util.validaCadena(txtBanco.Text) && Util.validaCadena(txtCuenta.Text))
                {
                    CuentaBanco cuenta = new CuentaBanco
                    {
                        idBanco = 1,
                        cuenta = txtCuenta.Text + " - " + txtBanco.Text,

                    };
                    cservice.crear(cuenta);
                    this.Close();
                    pasado();
                }
                else {
                    MessageBox.Show("COMPLETE TODOS LOS CAMPOS");
                }

            }
        }
    }
}
