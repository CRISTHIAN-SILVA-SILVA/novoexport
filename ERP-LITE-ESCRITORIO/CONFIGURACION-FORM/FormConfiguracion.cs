﻿using ERP_LITE_ESCRITORIO.CONFIGURACION_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CONFIGURACION_FORM
{
    public partial class FormConfiguracion : MaterialForm
    {
        public FormConfiguracion()
        {

            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

            
        }
        void abrirForm(Object formHijo)
        {
            if (this.contenedor.Controls.Count > 0)
                this.contenedor.Controls.RemoveAt(0);
            Form hijo = formHijo as Form;
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            this.contenedor.Controls.Add(hijo);
            this.contenedor.Tag = hijo;
            hijo.Show();
        }
        private void FormConfiguracion_Load(object sender, EventArgs e)
        {
            abrirForm(new FormLocal());
        }

        private void btnLocal_Click(object sender, EventArgs e)
        {
            abrirForm(new FormLocal());
        }

        private void btnParametros_Click(object sender, EventArgs e)
        {
            abrirForm(new FormParametros());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            abrirForm(new FormCuentas());
        }
    }
}
 