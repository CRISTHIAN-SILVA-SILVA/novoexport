﻿using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CONFIGURACION_FORM
{
    public partial class FormParametros : Form
    {
        Parametro igv;
        Parametro impresora;
        ParametroService pService=new ParametroImpl();
        public FormParametros()
        {
            InitializeComponent();
            igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
            impresora =  ParametroImpl.IMPRESORA;
            txtIGV.Text =String.Format("{0:0.00}", igv.valorDouble);
            txtImpresora.Text = impresora.valorCadena;
          
            if (igv == null) { MessageBox.Show("NO EXISTE PARAMETRO IGV", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information); this.Close(); }
        }

        private void FormParametros_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Util.esDouble(txtIGV.Text))
                if (Util.validaCadena(txtImpresora.Text))
                {
                    if (MessageBox.Show("SEGURO DE GUARDAR SE RENICIARA EL SISTEMA?", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        igv.valorDouble = Convert.ToDouble(txtIGV.Text);
                        igv.usuarioUpdate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                        igv.fechaUpdate = DateTime.Now;



                        impresora.valorCadena = txtImpresora.Text;
                        impresora.usuarioUpdate = FormLogin.user.nombres + " " + FormLogin.user.dni;
                        impresora.fechaUpdate = DateTime.Now;

                        pService.editar(igv);
                        pService.editar(igv);
                        Application.Restart();


                    }
                }
                else MessageBox.Show("FALTA IMPRESORA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else MessageBox.Show("EL IGV ES INCORRECTO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
