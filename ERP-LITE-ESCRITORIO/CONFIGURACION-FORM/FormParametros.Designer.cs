﻿namespace ERP_LITE_DESKTOP.CONFIGURACION_FORM
{
    partial class FormParametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtIGV = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnGuardar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtImpresora = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(89, 86);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(37, 19);
            this.materialLabel1.TabIndex = 30;
            this.materialLabel1.Text = "IGV:";
            // 
            // txtIGV
            // 
            this.txtIGV.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtIGV.Depth = 0;
            this.txtIGV.Hint = "";
            this.txtIGV.Location = new System.Drawing.Point(158, 82);
            this.txtIGV.MaxLength = 32767;
            this.txtIGV.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.PasswordChar = '\0';
            this.txtIGV.SelectedText = "";
            this.txtIGV.SelectionLength = 0;
            this.txtIGV.SelectionStart = 0;
            this.txtIGV.Size = new System.Drawing.Size(474, 23);
            this.txtIGV.TabIndex = 31;
            this.txtIGV.TabStop = false;
            this.txtIGV.UseSystemPasswordChar = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.AutoSize = true;
            this.btnGuardar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnGuardar.Depth = 0;
            this.btnGuardar.Icon = null;
            this.btnGuardar.Location = new System.Drawing.Point(321, 295);
            this.btnGuardar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Primary = true;
            this.btnGuardar.Size = new System.Drawing.Size(84, 36);
            this.btnGuardar.TabIndex = 45;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(31, 131);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(95, 19);
            this.materialLabel2.TabIndex = 46;
            this.materialLabel2.Text = "IMPRESORA:";
            // 
            // txtImpresora
            // 
            this.txtImpresora.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtImpresora.Depth = 0;
            this.txtImpresora.Hint = "";
            this.txtImpresora.Location = new System.Drawing.Point(158, 127);
            this.txtImpresora.MaxLength = 32767;
            this.txtImpresora.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtImpresora.Name = "txtImpresora";
            this.txtImpresora.PasswordChar = '\0';
            this.txtImpresora.SelectedText = "";
            this.txtImpresora.SelectionLength = 0;
            this.txtImpresora.SelectionStart = 0;
            this.txtImpresora.Size = new System.Drawing.Size(474, 23);
            this.txtImpresora.TabIndex = 47;
            this.txtImpresora.TabStop = false;
            this.txtImpresora.UseSystemPasswordChar = false;
            // 
            // FormParametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(644, 367);
            this.Controls.Add(this.txtImpresora);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.txtIGV);
            this.Name = "FormParametros";
            this.Text = "FormParametros";
            this.Load += new System.EventHandler(this.FormParametros_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtIGV;
        private MaterialSkin.Controls.MaterialRaisedButton btnGuardar;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtImpresora;
    }
}