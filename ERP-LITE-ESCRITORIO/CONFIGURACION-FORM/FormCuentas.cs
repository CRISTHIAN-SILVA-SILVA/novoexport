﻿using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONFIGURACION_FORM
{
    public partial class FormCuentas : Form
    {
        DataTable tabla = new DataTable();
        CuentaBancoService cservice = new CuentaBancoService();
        public FormCuentas()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("CUENTA");
            grid.DataSource = tabla;
            grid.Columns[0].Width = 100;
            grid.Columns[1].Width = 590;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevaCuenta form = new FormNuevaCuenta();
            form.pasado += new FormNuevaCuenta.pasar(pasado);
            form.ShowDialog();
        }

        void pasado() {
            tabla.Clear();
            cservice.listar().ForEach(x=>tabla.Rows.Add(x.id,x.cuenta));
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
            if (grid.SelectedRows.Count == 1) {
                if (MessageBox.Show("SEGURO DE ELIMINAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    CuentaBanco cuenta = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    cuenta.anulado = true;
                    cservice.editar(cuenta);
                    grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
            }
        }

        private void FormCuentas_Load(object sender, EventArgs e)
        {
            pasado();
        }
    }
}
