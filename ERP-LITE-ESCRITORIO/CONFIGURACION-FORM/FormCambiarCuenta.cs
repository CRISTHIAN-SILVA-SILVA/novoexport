﻿using ERP_LITE_DESKTOP;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CONFIGURACION_FORM
{
    public partial class FormCambiarCuenta : MaterialForm
    {
        UsuarioService uservice = new UsuarioImp();
        public FormCambiarCuenta()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

        }

        private void FormCambiarCuenta_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtAntigua.Focus();
            lblNombre.Text = FormLogin.user.nombres;
            lblEmail.Text = FormLogin.user.email;
            lblUsuario.Text = FormLogin.user.usuario;
            lblEmail.Text = FormLogin.user.email;
            lblRol.Text = FormLogin.user.rol.nombre;
            lblDni.Text =FormLogin.user.dni;

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE GUARDAR?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes) {
                if (txtAntigua.Text.Equals(FormLogin.user.password)) {
                    if (txtNueva.Text.Equals(txtconfirmaPass.Text))
                    {
                        if (Util.validaCadena(txtNueva.Text))
                        {
                            Usuario u = uservice.buscar(FormLogin.user.id);
                            u.rol = null;
                            u.password = txtNueva.Text;
                            u.antiguaPassword = FormLogin.user.password;
                            uservice.editar(u);

                            FormLogin.user.password = txtNueva.Text;
                            this.Close();
                        }
                        else MessageBox.Show("LA NUEVA CONTRASEÑA NO ES VALIDA");

                    }
                    else MessageBox.Show("LA CONFIRMACION DE LA CONTRASEÑA NO COINCIDE");
                } else MessageBox.Show("LA CONTRASEÑA ANTIGUA NO ES CORRECTA");
            }
        }
    }
}
