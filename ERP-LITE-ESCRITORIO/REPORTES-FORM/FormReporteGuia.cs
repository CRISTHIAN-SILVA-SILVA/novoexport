﻿using MaterialSkin;
using MaterialSkin.Controls;
using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormReporteGuia : MaterialForm
    {
        string RAZON_SOCIAL, DIRECCION, RUC, SERIE_NUMERO, FECHA_EMISION, TIPO_MONEDA
           , DOCUMENTO_IDENTIDAD, CLIENTE, DESCRIPCION_MOTIVO, PARTIDA, LLEGADA, FECHA_IMPRESION, MARCAYPLACA, LICENCIA,TRANSPORTISTA,RUC_TRANSPOR;
        List<DetalleNotaPlantilla> lista = new List<DetalleNotaPlantilla>();
        VentaService vservice = new VentaImpl();
        //   DetalleNotaService dnservice = new DetalleNotaImpl();
        GuiaRemisonService gservice = new GuiaRemisionImpl();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        ComprobantePagoService cpservice = new ComprobantePagoImpl();
        public FormReporteGuia(int idGuia)
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            GuiaRemision g = gservice.buscar(idGuia);
           ComprobantePago cp= cpservice.buscar(g.idComprobante);

            RAZON_SOCIAL= LocalImpl.getInstancia().razonSocial;
            DIRECCION = LocalImpl.getInstancia().direccion.ToUpper();
            RUC = LocalImpl.getInstancia().ruc.ToUpper();
            SERIE_NUMERO =  g.serie+ " - " + Util.NormalizarCampo(g.numero.ToString(), 8);
            FECHA_EMISION = g.fechaCreate.ToShortDateString();

            FECHA_IMPRESION = DateTime.Now.ToShortDateString();
            LICENCIA = g.licencia;
            MARCAYPLACA = g.conductor + " - " + g.placa;
            CLIENTE = cp.cliente.razonSocial;
            DESCRIPCION_MOTIVO =g.motivoTranslado.descripcion;

            DOCUMENTO_IDENTIDAD = cp.cliente.ruc+" / " +cp.cliente.dniRepresentante;
            PARTIDA = DIRECCION;
            LLEGADA = cp.cliente.direccion;

            TRANSPORTISTA = g.transportista.razonSocial;
            RUC_TRANSPOR = g.transportista.ruc;

            Venta venta = vservice.buscar(cp.idVenta);

            foreach (var i in venta.detalles)
            {
                ProductoPresentacion pp = ppservice.buscar(i.idProductoPresentacion);
                DetalleNotaPlantilla d = new DetalleNotaPlantilla
                {
                    descripcion = pp.nombre,
                    cantidad = String.Format("{0:0.0}", i.cantidad),
                    valorUnitario = String.Format("{0:0.0000}", i.precio),
                    unidad = pp.unidad.codigo
                };
                lista.Add(d);
            } 
        }

        private void FormReporteGuia_Load(object sender, EventArgs e)
        {

        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            ReportParameter[] parametros = new ReportParameter[15];
            parametros[0] = new ReportParameter("EMPRESA", RAZON_SOCIAL);
            parametros[1] = new ReportParameter("DIRECCION", DIRECCION);
            parametros[2] = new ReportParameter("RUC", RUC);
            parametros[3] = new ReportParameter("FECHA_IMPRESION", FECHA_IMPRESION);
            parametros[4] = new ReportParameter("SERIE", SERIE_NUMERO);

            parametros[5] = new ReportParameter("CLEINTE", CLIENTE);
            parametros[6] = new ReportParameter("MOTIVO", DESCRIPCION_MOTIVO);
            parametros[7] = new ReportParameter("FECHA", FECHA_EMISION);
            parametros[8] = new ReportParameter("PARTIDA",PARTIDA);
            parametros[9] = new ReportParameter("RUC_CLIENTE", DOCUMENTO_IDENTIDAD);
            parametros[10] = new ReportParameter("LLEGADA",LLEGADA);

            parametros[11] = new ReportParameter("TRANSPORTISTA", TRANSPORTISTA);
            parametros[12] = new ReportParameter("RUC_TRANSPORTISTA", RUC_TRANSPOR);
            
            parametros[13] = new ReportParameter("MARCA_PLACA", MARCAYPLACA);
            parametros[14] = new ReportParameter("L", LICENCIA);

            reportViewer1.LocalReport.SetParameters(parametros);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();
        }
    }
}
