﻿using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormRegistroPagosVenta : Form
    {
        DataTable tabla = new DataTable();
        MovimientoCajaImpl mcservice = new MovimientoCajaImpl();
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        DateTime inicio,fin;
        public FormRegistroPagosVenta(DateTime i,DateTime f)
        {
            inicio = i;
            fin = f;
            InitializeComponent();
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("CODIGO");
            tabla.Columns.Add("TDOC");
            tabla.Columns.Add("SERIE");
            tabla.Columns.Add("N°");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("MONEDA");
            tabla.Columns.Add("SUBTOTAL");
            tabla.Columns.Add("IGV");
            tabla.Columns.Add("TOTAL");
            tabla.Columns.Add("SALDO");
            tabla.Columns.Add("COBRANZA.FECHA");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("CUADRO");
            tabla.Columns.Add("LETRA");
            tabla.Columns.Add("CUADRO C2");
            tabla.Columns.Add("FORMA PAGO");
        }

        private void FormRegistroPagosVenta_Load(object sender, EventArgs e)
        {
            foreach (var x in mcservice.listarVentas(inicio, fin)) {
                tabla.Rows.Add(x.fecha.ToShortDateString(),"codigo","TDOC",x.serieNumeroComprobante.Substring(0,4),x.serieNumeroComprobante.Substring(5),
                   "CLEITE" );
            }
        }
    }
}
