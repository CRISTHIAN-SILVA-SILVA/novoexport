﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class btnComprobante : MaterialForm
    {
        public btnComprobante()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);

        }

        private void FormAdminReportes_Load(object sender, EventArgs e)
        {

        }

        private void btnRecordVendedores_Click(object sender, EventArgs e)
        {
            FormRecordVendedores form = new FormRecordVendedores(txtInicio.Value,txtFin.Value);
            form.ShowDialog();
        }

        private void btnRecordClientes_Click(object sender, EventArgs e)
        {
            FormRecordClientes form = new FormRecordClientes(txtInicio.Value,txtFin.Value);
            form.ShowDialog();
        }

        private void btnRecordProveedores_Click(object sender, EventArgs e)
        {
            FormRecordProveedores form = new FormRecordProveedores(txtInicio.Value,txtFin.Value);
            form.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormRComprobante form = new FormRComprobante(txtInicio.Value,txtFin.Value);
            form.ShowDialog();
        }

        private void btnSerie_Click(object sender, EventArgs e)
        {
            FormRVSerie form = new FormRVSerie(txtInicio.Value,txtFin.Value);
            form.ShowDialog();
        }

        private void btnResumen_Click(object sender, EventArgs e)
        {
            FormFacturasNoPagadas form =new FormFacturasNoPagadas();
            form.ShowDialog();
        }
    }
}
