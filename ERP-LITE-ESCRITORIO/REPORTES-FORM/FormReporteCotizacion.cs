﻿using MaterialSkin;
using MaterialSkin.Controls;
using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormReporteCotizacion : MaterialForm
    {
        CuentaBancoService cuseervice = new CuentaBancoService();
        Cotizacion cotizacion;
        List<DetalleFactura> lista = new List<DetalleFactura>();
        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        public FormReporteCotizacion(Cotizacion c)
        {
            cotizacion = c;
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormReporteCotizacion_Load(object sender, EventArgs e)
        {

        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            ProductoPresentacion pp;
            foreach (var i in cotizacion.detalles)
            {
                pp = ppservice.buscar(i.idProducto);
                DetalleFactura d = new DetalleFactura
                {
                    descricion = pp.nombre,
                    cantidad = String.Format("{0:0.0}", i.cantidad),
                    valorUnitario = String.Format("{0:0.0000}", i.precio),
                    unidad =pp.unidad.nombre,
                    codigo = string.Format("{0:0000}",pp.id),
                };
                lista.Add(d);
            }

            ReportParameter[] parametros = new ReportParameter[11];
            parametros[0] = new ReportParameter("FECHA_EMISION", DateTime.Now.ToLongDateString());
            parametros[1] = new ReportParameter("CLIENTE", cotizacion.cliente.razonSocial);
            parametros[2] = new ReportParameter("DIRECCION_CLIENTE",cotizacion.cliente.direccion);
            parametros[3] = new ReportParameter("DIRECCION", LocalImpl.getInstancia().direccion);
            parametros[4] = new ReportParameter("RUC", LocalImpl.getInstancia().ruc);

            string TIPO_PAGO = cotizacion.tipoPago.nombre;
            if (!cotizacion.tipoPago.nombre.Equals("PAGO UNICO")) { TIPO_PAGO = TIPO_PAGO + " A " + cotizacion.diasCredito + " DIAS"; }
            parametros[5] = new ReportParameter("TIPO_PAGO", TIPO_PAGO);

            string CUENTAS = "";
            foreach (var i in cuseervice.listar()) {
                CUENTAS = CUENTAS + i.cuenta.ToUpper() + "\n";
            }
            parametros[6] = new ReportParameter("CUENTAS", CUENTAS);

            parametros[7] = new ReportParameter("TOTAL",string.Format("{0:0.00}", cotizacion.total));
            parametros[8] = new ReportParameter("TELEFONO", LocalImpl.getInstancia().telefono);
            parametros[9] = new ReportParameter("EMAIL", LocalImpl.getInstancia().email);
            parametros[10] = new ReportParameter("SERIE_NUMERO",cotizacion.serie+"-"+Util.NormalizarCampo( cotizacion.numero.ToString(),8));

            reportViewer1.LocalReport.SetParameters(parametros);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();
        }
    }
}
