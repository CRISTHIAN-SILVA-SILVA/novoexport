﻿using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormFacturasNoPagadas : Form
    {
        ComprobantePagoImpl cpservice = new ComprobantePagoImpl();
        List<FacturaNoPagada> lista = new List<FacturaNoPagada>();
        double deuda=0,pago=0,total=0;
        string sdeuda, spago, stotal;
        public FormFacturasNoPagadas()
        {
            InitializeComponent();
            FacturaNoPagada f;
            foreach (var x in cpservice.listarNoPagadas()) {
                f = new FacturaNoPagada {
                    baseImponible = string.Format("{0:0.00}", x.montoPagado),
                    cliente =x.cliente.razonSocial,
                    exonerado = string.Format("{0:0.00}", x.venta.totalExonerado),
                    fecha = x.fechaCreate.ToString("dd/MM/yyyy"),
                    fechaPago = x.fechaVencimiento.ToString("dd/MM/yyyy"),
                    numero = x.numero.ToString(),
                    igv = string.Format("{0:0.00}", x.montoTotal-x.montoPagado),
                    serie = x.serie,
                    tipo = x.tipoComprobante.codigo_sunat,
                    total = string.Format("{0:0.00}", x.montoTotal),
                };
                total += x.montoTotal;
                pago += x.montoPagado;
                
                lista.Add(f);
            }
          
            spago = string.Format("{0:0.00}", pago);
            stotal = string.Format("{0:0.00}", total);
            sdeuda = string.Format("{0:0.00}", total-pago);
            

        }

        private void FormFacturasNoPagadas_Load(object sender, EventArgs e)
        {
            ReportParameter[] parametros = new ReportParameter[6];
            parametros[0] = new ReportParameter("D", LocalImpl.getInstancia().direccion);
            parametros[1] = new ReportParameter("r", LocalImpl.getInstancia().ruc);
            parametros[2] = new ReportParameter("f",DateTime.Now.ToLongDateString());
            parametros[3] = new ReportParameter("DEUDA",sdeuda);
            parametros[4] = new ReportParameter("PAGO", spago);
            parametros[5] = new ReportParameter("TOTAL", stotal);
           

            reportViewer1.LocalReport.SetParameters(parametros);

            this.reportViewer1.LocalReport.DataSources.Clear();
   

            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();
        }
    }
}
