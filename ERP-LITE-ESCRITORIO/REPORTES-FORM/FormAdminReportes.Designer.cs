﻿namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    partial class btnComprobante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(btnComprobante));
            this.btnRecordVendedores = new System.Windows.Forms.Button();
            this.btnRecordClientes = new System.Windows.Forms.Button();
            this.btnRecordProveedores = new System.Windows.Forms.Button();
            this.btnResumen = new System.Windows.Forms.Button();
            this.btnSerie = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFin = new System.Windows.Forms.DateTimePicker();
            this.txtInicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRecordVendedores
            // 
            this.btnRecordVendedores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btnRecordVendedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRecordVendedores.FlatAppearance.BorderSize = 0;
            this.btnRecordVendedores.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnRecordVendedores.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnRecordVendedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecordVendedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecordVendedores.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRecordVendedores.Image = ((System.Drawing.Image)(resources.GetObject("btnRecordVendedores.Image")));
            this.btnRecordVendedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordVendedores.Location = new System.Drawing.Point(76, 241);
            this.btnRecordVendedores.Name = "btnRecordVendedores";
            this.btnRecordVendedores.Size = new System.Drawing.Size(228, 49);
            this.btnRecordVendedores.TabIndex = 3;
            this.btnRecordVendedores.Text = "          Venderdores(as)";
            this.btnRecordVendedores.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordVendedores.UseVisualStyleBackColor = false;
            this.btnRecordVendedores.Click += new System.EventHandler(this.btnRecordVendedores_Click);
            // 
            // btnRecordClientes
            // 
            this.btnRecordClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btnRecordClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRecordClientes.FlatAppearance.BorderSize = 0;
            this.btnRecordClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnRecordClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnRecordClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecordClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecordClientes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRecordClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnRecordClientes.Image")));
            this.btnRecordClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordClientes.Location = new System.Drawing.Point(405, 241);
            this.btnRecordClientes.Name = "btnRecordClientes";
            this.btnRecordClientes.Size = new System.Drawing.Size(228, 49);
            this.btnRecordClientes.TabIndex = 4;
            this.btnRecordClientes.Text = "          Clientes";
            this.btnRecordClientes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordClientes.UseVisualStyleBackColor = false;
            this.btnRecordClientes.Click += new System.EventHandler(this.btnRecordClientes_Click);
            // 
            // btnRecordProveedores
            // 
            this.btnRecordProveedores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btnRecordProveedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRecordProveedores.FlatAppearance.BorderSize = 0;
            this.btnRecordProveedores.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnRecordProveedores.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnRecordProveedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecordProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecordProveedores.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRecordProveedores.Image = ((System.Drawing.Image)(resources.GetObject("btnRecordProveedores.Image")));
            this.btnRecordProveedores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordProveedores.Location = new System.Drawing.Point(719, 241);
            this.btnRecordProveedores.Name = "btnRecordProveedores";
            this.btnRecordProveedores.Size = new System.Drawing.Size(228, 49);
            this.btnRecordProveedores.TabIndex = 5;
            this.btnRecordProveedores.Text = "          Proveedores";
            this.btnRecordProveedores.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordProveedores.UseVisualStyleBackColor = false;
            this.btnRecordProveedores.Click += new System.EventHandler(this.btnRecordProveedores_Click);
            // 
            // btnResumen
            // 
            this.btnResumen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btnResumen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnResumen.FlatAppearance.BorderSize = 0;
            this.btnResumen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnResumen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnResumen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResumen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResumen.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnResumen.Image = ((System.Drawing.Image)(resources.GetObject("btnResumen.Image")));
            this.btnResumen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResumen.Location = new System.Drawing.Point(719, 386);
            this.btnResumen.Name = "btnResumen";
            this.btnResumen.Size = new System.Drawing.Size(228, 49);
            this.btnResumen.TabIndex = 6;
            this.btnResumen.Text = "          Facturas por Cobrar";
            this.btnResumen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResumen.UseVisualStyleBackColor = false;
            this.btnResumen.Click += new System.EventHandler(this.btnResumen_Click);
            // 
            // btnSerie
            // 
            this.btnSerie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.btnSerie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSerie.FlatAppearance.BorderSize = 0;
            this.btnSerie.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnSerie.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnSerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSerie.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSerie.Image = ((System.Drawing.Image)(resources.GetObject("btnSerie.Image")));
            this.btnSerie.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSerie.Location = new System.Drawing.Point(401, 386);
            this.btnSerie.Name = "btnSerie";
            this.btnSerie.Size = new System.Drawing.Size(228, 49);
            this.btnSerie.TabIndex = 7;
            this.btnSerie.Text = "          Por Serie";
            this.btnSerie.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSerie.UseVisualStyleBackColor = false;
            this.btnSerie.Click += new System.EventHandler(this.btnSerie_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(76, 386);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(228, 49);
            this.button5.TabIndex = 8;
            this.button5.Text = "          Por Comprobante";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label1.Location = new System.Drawing.Point(72, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 24);
            this.label1.TabIndex = 27;
            this.label1.Text = "RECORDS ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label2.Location = new System.Drawing.Point(72, 330);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(244, 24);
            this.label2.TabIndex = 28;
            this.label2.Text = "REPORTES DE VENTAS";
            // 
            // txtFin
            // 
            this.txtFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFin.Location = new System.Drawing.Point(557, 120);
            this.txtFin.Name = "txtFin";
            this.txtFin.Size = new System.Drawing.Size(261, 24);
            this.txtFin.TabIndex = 29;
            // 
            // txtInicio
            // 
            this.txtInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicio.Location = new System.Drawing.Point(233, 120);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(261, 24);
            this.txtInicio.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label3.Location = new System.Drawing.Point(333, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 24);
            this.label3.TabIndex = 31;
            this.label3.Text = "Inicio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.label4.Location = new System.Drawing.Point(648, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 24);
            this.label4.TabIndex = 32;
            this.label4.Text = "Fin";
            // 
            // btnComprobante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 522);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtInicio);
            this.Controls.Add(this.txtFin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btnSerie);
            this.Controls.Add(this.btnResumen);
            this.Controls.Add(this.btnRecordProveedores);
            this.Controls.Add(this.btnRecordClientes);
            this.Controls.Add(this.btnRecordVendedores);
            this.MaximizeBox = false;
            this.Name = "btnComprobante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reportes";
            this.Load += new System.EventHandler(this.FormAdminReportes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRecordVendedores;
        private System.Windows.Forms.Button btnRecordClientes;
        private System.Windows.Forms.Button btnRecordProveedores;
        private System.Windows.Forms.Button btnResumen;
        private System.Windows.Forms.Button btnSerie;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker txtFin;
        private System.Windows.Forms.DateTimePicker txtInicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}