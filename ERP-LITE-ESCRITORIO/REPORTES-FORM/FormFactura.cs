﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using MaterialSkin;
using MaterialSkin.Controls;
using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormFactura : MaterialForm
    {
        Venta venta;
        ComprobantePago cp;
        List<DetalleFactura> lista = new List<DetalleFactura>();
        // DetalleVentaService dnservice = new DetalleNotaImpl();

        UnidadMedidaService umservice = new UnidadMedidaImpl();
        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
        DetalleVentaImpl dservice = new DetalleVentaImpl();

        string RAZON_SOCIAL, DIRECCION, RUC, TIPO_NOTA, SERIE_NUMERO, MOTIVO_NOTA, COMPROBANTE_AFECTADO, FECHA_EMISION, TIPO_MONEDA
          , DOCUMENTO_IDENTIDAD, CLIENTE, DESCRIPCION_MOTIVO, MONTO_TEXT0, VALOR_VENTA, IMPORTE_TOTAL, IGV, EXONERADO;

        private void reportViewer1_Load_1(object sender, EventArgs e)
        {

            IGV = string.Format("{0:0.00}", venta.igv);
            IMPORTE_TOTAL = string.Format("{0:0.00}", venta.montoTotal);
            VALOR_VENTA = string.Format("{0:0.00}", venta.opGravadas);
            RAZON_SOCIAL = LocalImpl.getInstancia().razonSocial;
            DIRECCION = LocalImpl.getInstancia().direccion;
            RUC = LocalImpl.getInstancia().ruc.ToUpper();
            //   TIPO_NOTA = n.tipoComprobante.descripcion.ToUpper();
            SERIE_NUMERO = cp.serie + " - " + Util.NormalizarCampo(cp.numero.ToString(), 8);
            //    MOTIVO_NOTA = n.tipoNota.descripcion;
            //   COMPROBANTE_AFECTADO = n.comprobante.serie + " - " + Util.NormalizarCampo(n.comprobante.numero.ToString(), 8);
            FECHA_EMISION = cp.fechaCreate.ToShortDateString();
            TIPO_MONEDA = "PEN - SOLES";
            //  EXONERADO = String.Format("{0:0.00}", n.totalExonerado);


            DOCUMENTO_IDENTIDAD = cp.cliente.ruc;
            if (cp.tipoComprobante.codigo_sunat == CodigosSUNAT.BOLETA) DOCUMENTO_IDENTIDAD = cp.cliente.dniRepresentante;
                CLIENTE = cp.cliente.razonSocial;
            //  DESCRIPCION_MOTIVO = n.tipoNota.descripcion;
            MONTO_TEXT0 = Util.Convertir(string.Format("{0:0.00}", venta.montoTotal), true);

            foreach (var i in dservice.listarPorVenta(venta.id))
            {

                DetalleFactura d = new DetalleFactura
                {
                    descricion = i.productoPresentacion.nombre,
                    cantidad =  i.cantidad.ToString(),
                    valorUnitario = String.Format("{0:0.0000}", i.precio/(1+igv.valorDouble)),
                    unidad = umservice.buscar(i.productoPresentacion.idUnidadMedida).nombre,
                    codigo = string.Format("{0:0000}", i.productoPresentacion.id),
                };
                lista.Add(d);
            }


            QrEncoder qr = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            string tipoDocCliente = "";
            string docDocIdentidad = "";

            if (cp.tipoComprobante.codigo_sunat == CodigosSUNAT.BOLETA)
            {
                docDocIdentidad = cp.cliente.dniRepresentante; tipoDocCliente = CodigosSUNAT.BOLETA;
            }
            else docDocIdentidad = cp.cliente.ruc; tipoDocCliente = CodigosSUNAT.RUC;


            docDocIdentidad = cp.cliente.ruc; tipoDocCliente = CodigosSUNAT.RUC; 
            string numero = Util.NormalizarCampo(cp.numero.ToString(), 8);
            qr.TryEncode(LocalImpl.getInstancia().ruc + "|" + cp.tipoComprobante.codigo_sunat + "|" + cp.serie + "|" + numero + "|" + string.Format("{0:0.00}", venta.igv)
            + "|" + string.Format("{0:0.00}", cp.montoTotal) + "|" + cp.fechaCreate.ToShortDateString() + "|" + tipoDocCliente + "|" + docDocIdentidad + "|" + "|", out qrCode);

            GraphicsRenderer render = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), Brushes.Black, Brushes.White);
            MemoryStream ms = new MemoryStream();
            render.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, ms);

            Bitmap imgTemp = new Bitmap(ms);
            Bitmap img = new Bitmap(imgTemp, new Size(new Point(200, 200)));

            img.Save("D://REPORTES//qr.png", System.Drawing.Imaging.ImageFormat.Png);
            reportViewer1.LocalReport.EnableExternalImages = true;
            reportViewer1.LocalReport.EnableHyperlinks = true;
            string DETRACCION = "";
            if (cp.montoDetraccion > 0) DETRACCION = String.Format("{0:0.00}",cp.montoDetraccion);

            ReportParameter[] parametros = new ReportParameter[22];
            parametros[0] = new ReportParameter("FECHA_VENCIMIENTO", cp.fechaVencimiento.ToShortDateString());
            parametros[1] = new ReportParameter("DIRECCION", DIRECCION);
            parametros[2] = new ReportParameter("RUC", RUC);
            parametros[3] = new ReportParameter("TIPO_MONEDA", TIPO_MONEDA);
            parametros[4] = new ReportParameter("SERIE_NUMERO", SERIE_NUMERO);
            parametros[5] = new ReportParameter("OBSERVACION", "");
            GuiaRemisionImpl guias = new GuiaRemisionImpl();
            GuiaRemision guiaRemision= guias.buscarPorComprobante(cp.id);
            if(guiaRemision!=null)  parametros[6] = new ReportParameter("GUIA", guiaRemision.serie+"-"+Util.NormalizarCampo( guiaRemision.numero.ToString(),8    ));
            else parametros[6] = new ReportParameter("GUIA", "");
            parametros[7] = new ReportParameter("FECHA_EMISION", FECHA_EMISION);
            parametros[8] = new ReportParameter("ORDEN", venta.orden);

            parametros[9] = new ReportParameter("RUC_CLIENTE", DOCUMENTO_IDENTIDAD);

            parametros[10] = new ReportParameter("CLIENTE", CLIENTE);
            parametros[11] = new ReportParameter("DIRECCION_CLIENTE", cp.cliente.direccion);
            parametros[12] = new ReportParameter("MONTO_TEXTO", MONTO_TEXT0);
            parametros[13] = new ReportParameter("VALOR_VENTA", VALOR_VENTA);
            parametros[14] = new ReportParameter("TOTAL", IMPORTE_TOTAL);
            parametros[15] = new ReportParameter("IGV", IGV);
            parametros[16] = new ReportParameter("IGV_P", (igv.valorDouble*100).ToString()+"%");
            parametros[17] = new ReportParameter("Path", @"file:///D:\REPORTES\qr.png",true );
            parametros[18] = new ReportParameter("HASH", cp.hash);
          

            if (cp.tipoComprobante.codigo_sunat == CodigosSUNAT.BOLETA) {
                parametros[19] = new ReportParameter("TIPO_COMPROBANTE", "BOLETA DE VENTA ELECTRONICA");
            }
            else parametros[19] = new ReportParameter("TIPO_COMPROBANTE", "FACTURA ELECTRONICA");
            parametros[20] = new ReportParameter("MONTO_DETRACCION","DETRACCION: S/   "+ DETRACCION);
            parametros[21] = new ReportParameter("BANCO_DETRACCION","CUENTA: "+LocalImpl.getInstancia().cuenta+"  "+ LocalImpl.getInstancia().banco);
            reportViewer1.LocalReport.SetParameters(parametros);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();
        }



        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

     

        public FormFactura(ComprobantePago c,Venta v,string hash)
        {
            cp = c; venta = v;
            InitializeComponent();
          
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormFactura_Load(object sender, EventArgs e)
        {
          
        }
    }
}
