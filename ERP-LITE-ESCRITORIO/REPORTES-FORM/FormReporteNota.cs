﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using MaterialSkin;
using MaterialSkin.Controls;
using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormReporteNota : MaterialForm
    {
        string RAZON_SOCIAL, DIRECCION, RUC, TIPO_NOTA, SERIE_NUMERO, MOTIVO_NOTA, COMPROBANTE_AFECTADO, FECHA_EMISION, TIPO_MONEDA
            , DOCUMENTO_IDENTIDAD, CLIENTE, DESCRIPCION_MOTIVO, MONTO_TEXT0,VALOR_VENTA,IMPORTE_TOTAL,IGV,EXONERADO;

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ComprobantePagoService cpservice = new ComprobantePagoImpl();

                TxtSUNAT.ConstruirNotaCreditoDebito(nota, cpservice.buscar(nota.idComprobanteVenta), true);
            }
            catch(Exception d) {
                MessageBox.Show(d.ToString()+"  "+d.StackTrace);
            }
        }

        List<DetalleNotaPlantilla> lista = new List<DetalleNotaPlantilla>();
        ComprobantePagoService cpservice = new ComprobantePagoImpl();
        DetalleNotaService dnservice =new DetalleNotaImpl();
        Nota nota;
        UnidadMedidaService umservice = new UnidadMedidaImpl();
        private void reportViewer1_Load(object sender, EventArgs e)
        {

            QrEncoder qr = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            string tipoDocCliente = "";
            string docDocIdentidad = "";

            ComprobantePago cp = cpservice.buscar(nota.idComprobanteVenta);
            if (cp.tipoComprobante.codigo_sunat == CodigosSUNAT.BOLETA)
            {
                docDocIdentidad = nota.cliente.dniRepresentante; tipoDocCliente = CodigosSUNAT.BOLETA;
            }
            else docDocIdentidad = nota.cliente.ruc; tipoDocCliente = CodigosSUNAT.RUC;


            docDocIdentidad = nota.cliente.ruc; tipoDocCliente = CodigosSUNAT.RUC;
            string numero = Util.NormalizarCampo(nota.numero.ToString(), 8);

            qr.TryEncode(LocalImpl.getInstancia().ruc + "|" + nota.tipoComprobante.codigo_sunat + "|" + nota.serie + "|" + numero + "|"
                + string.Format("{0:0.00}", nota.igv)
            + "|" + string.Format("{0:0.00}", nota.total) + "|" + nota.fechaCreate.ToShortDateString() + "|" + tipoDocCliente + "|" 
            + docDocIdentidad + "|" + "|", out qrCode);

            GraphicsRenderer render = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), Brushes.Black, Brushes.White);
            MemoryStream ms = new MemoryStream();
            render.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, ms);

            Bitmap imgTemp = new Bitmap(ms);
            Bitmap img = new Bitmap(imgTemp, new Size(new Point(200, 200)));

            img.Save("D://REPORTES//qr.png", System.Drawing.Imaging.ImageFormat.Png);
            reportViewer1.LocalReport.EnableExternalImages = true;



            ReportParameter[] parametros = new ReportParameter[19];
            parametros[0] = new ReportParameter("RAZON_SOCIAL", RAZON_SOCIAL);
            parametros[1] = new ReportParameter("DIRECCION", DIRECCION);
            parametros[2] = new ReportParameter("RUC", RUC);
            parametros[3] = new ReportParameter("TIPO_NOTA", TIPO_NOTA);
            parametros[4] = new ReportParameter("SERIE_NUMERO", SERIE_NUMERO);
            parametros[5] = new ReportParameter("TIPO_MOTIVO", MOTIVO_NOTA);
            parametros[6] = new ReportParameter("COMPROBANTE", COMPROBANTE_AFECTADO);
            parametros[7] = new ReportParameter("FECHA_EMISION", FECHA_EMISION);
            parametros[8] = new ReportParameter("TIPO_MONEDA", TIPO_MONEDA);
            parametros[9] = new ReportParameter("DOCUMENTO_IDENTIDAD", DOCUMENTO_IDENTIDAD);
            parametros[10] = new ReportParameter("CLIENTE", CLIENTE);
            parametros[11] = new ReportParameter("DESCRIPCION_MOTIVO", DESCRIPCION_MOTIVO);

            parametros[12] = new ReportParameter("TOTAL_LETRAS", MONTO_TEXT0);
            parametros[13] = new ReportParameter("VALOR_VENTA",VALOR_VENTA);
            parametros[14] = new ReportParameter("IMPORTE_TOTAL", IMPORTE_TOTAL);
            parametros[15] = new ReportParameter("IGV", IGV);
            parametros[16] = new ReportParameter("EXONERADO", EXONERADO);

            parametros[17] = new ReportParameter("IMAGEN", @"file:///D:\REPORTES\qr.png", true);
            parametros[18] = new ReportParameter("HASH", nota.hash);


            reportViewer1.LocalReport.SetParameters(parametros);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();
        }

        public FormReporteNota(Nota n)
        {
            nota = n;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            //reportViewer1.LocalReport.ReportPath=Path.Combine(Application.StartupPath,"ReporteNota.rdlc") ;
            //CLINETE//tipo nota//tipocomprobante/comprobante/cliente/registro
            IGV =string.Format("{0:0.00}", n.igv);
            IMPORTE_TOTAL= string.Format("{0:0.00}", n.total);
            VALOR_VENTA = string.Format("{0:0.00}", n.total-n.igv);
            RAZON_SOCIAL = LocalImpl.getInstancia().razonSocial ;
            DIRECCION = LocalImpl.getInstancia().ruc;
            RUC = LocalImpl.getInstancia().direccion.ToUpper();
            TIPO_NOTA = n.tipoComprobante.descripcion.ToUpper();
            SERIE_NUMERO = n.serie + " - " + Util.NormalizarCampo(n.numero.ToString(),8);
            MOTIVO_NOTA = n.tipoNota.descripcion;
            COMPROBANTE_AFECTADO=n.comprobante.serie + " - " + Util.NormalizarCampo(n.comprobante.numero.ToString(), 8);
            FECHA_EMISION = n.fechaCreate.ToShortDateString();
            TIPO_MONEDA = "PEN - SOLES";
            EXONERADO =String.Format("{0:0.00}", n.totalExonerado);

            if (n.comprobante.serie.First() == 'F')
                DOCUMENTO_IDENTIDAD = n.cliente.ruc;
            else DOCUMENTO_IDENTIDAD = n.cliente.dniRepresentante;

            CLIENTE = n.cliente.razonSocial;
            DESCRIPCION_MOTIVO = n.tipoNota.descripcion;
            MONTO_TEXT0 = Util.Convertir(String.Format("{0:0.00}", n.total),true);
            
            foreach (var i in dnservice.listarPorNota(n.id)) {

                DetalleNotaPlantilla d = new DetalleNotaPlantilla {
                    descripcion = i.presentacion.nombre,
                    cantidad = String.Format("{0:0.0}", i.cantidad),
                    valorUnitario = String.Format("{0:0.0000}", i.precio),
                    unidad = umservice.buscar(i.presentacion.idUnidadMedida).nombre
                };
                lista.Add(d);
            }

     }
        private void FormReporteNota_Load(object sender, EventArgs e)  {
          
        }
    }
}
