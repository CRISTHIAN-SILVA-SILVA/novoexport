﻿using MaterialSkin;
using MaterialSkin.Controls;
using Microsoft.Reporting.WinForms;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.REPORTES_SERVICE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.REPORTES_FORM
{
    public partial class FormRComprobante : MaterialForm
    {
        ReporteVentasService service = new ReporteVentasService();
        DateTime i, f;
        public FormRComprobante(DateTime inicio, DateTime fin)
        {
            i = inicio; f = fin;
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

        }

        private void FormRComprobante_Load(object sender, EventArgs e)
        {

        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            ReportParameter[] parametros = new ReportParameter[5];
            parametros[0] = new ReportParameter("EMPRESA", LocalImpl.getInstancia().razonSocial);
            parametros[1] = new ReportParameter("DIRECCION", LocalImpl.getInstancia().direccion);
            parametros[2] = new ReportParameter("F1", i.ToShortDateString());
            parametros[3] = new ReportParameter("F2", f.ToShortDateString());
            List<DetalleRVenta> lista = service.listaComprobantes(i.Date, f.Date.AddDays(1));
            double total = 0;
            for (int i =0;i< lista.Count;i++)
            {
                lista[i].fechaS = lista[i].fecha.ToShortDateString();
                total += lista[i].importe;
            }
            parametros[4] = new ReportParameter("TOTAL", string.Format("{0:0.00}", total));
            reportViewer1.LocalReport.SetParameters(parametros);

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DETALLE", lista));
            this.reportViewer1.RefreshReport();
        }
    }
}
