﻿using MaterialSkin;
using MaterialSkin.Controls;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO
{
    public partial class Form2 : MaterialForm
    {
        public delegate void pasar(double cambio);
        public event pasar pasado;
        public Form2()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);


            if (CambioMoneda.getTablaDolar() == null) { MessageBox.Show("NO RESPONDE SERVICIO CAMBIO DE MONEDA");  }
            gridProducto.DataSource = CambioMoneda.getTablaDolar();
            if (gridProducto.DataSource != null)
            {
                gridProducto.Columns[0].Width = 100;
                gridProducto.Columns[1].Width = 107;
                gridProducto.Columns[2].Width = 180;
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Util.esDouble(txtCambio.Text)) {
                pasado(Convert.ToDouble(txtCambio.Text));
                this.Close();
            }

        }

        private void gridProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            pasado(Convert.ToDouble(gridProducto.Rows[e.RowIndex].Cells["Compra"].Value.ToString()));
            this.Close();
        }
    }
}
