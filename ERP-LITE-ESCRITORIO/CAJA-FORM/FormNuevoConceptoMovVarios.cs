﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    public partial class FormNuevoConceptoMovVarios : MaterialForm
    {
        public delegate void pasar(ConceptoMovimientoVarios concepto);
        public event pasar pasado;
        ConceptoMovimientoVariosService cservice = new ConceptoMovimientoVariosImpl();
        public FormNuevoConceptoMovVarios()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
        }

        private void FormNuevoConceptoMovVarios_Load(object sender, EventArgs e)
        {
            
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("SEGURO DE GUARDAR?","CONFIRMAR",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes) {
                if (Util.validaCadena(txtDescripcion.Text)) {
                    ConceptoMovimientoVarios concepto = new ConceptoMovimientoVarios {
                         descripcion=txtDescripcion.Text.ToUpper().Trim()                         
                    };
                    ConceptoMovimientoVarios existe = cservice.existe(concepto.descripcion);
                    if (existe == null) {
                        cservice.crear(concepto);
                        pasado(concepto);
                        this.Close();
                    }
                    else if (existe.anulado == true)
                    {
                        existe.anulado = false;
                        cservice.editar(existe);
                        pasado(existe);
                        this.Close();
                    }
                    else MessageBox.Show("CONCEPTO YA EXISTE", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else MessageBox.Show("COMPLETA LA DESCRIPCION", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
