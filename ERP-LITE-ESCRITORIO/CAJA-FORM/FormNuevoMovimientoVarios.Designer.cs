﻿namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    partial class FormNuevoMovimientoVarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.btnAgregar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCancelar = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtSerie = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtMonto = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtDescripcion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.comboMedioPago = new System.Windows.Forms.ComboBox();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.comboProveedor = new System.Windows.Forms.ComboBox();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.txtInicio = new System.Windows.Forms.DateTimePicker();
            this.txtDetraccion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.txtPorcentaje = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.txtFechaVencimiento = new System.Windows.Forms.DateTimePicker();
            this.comboTipoComprobante = new System.Windows.Forms.ComboBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCuenta = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.txtFechaDetraccion = new System.Windows.Forms.DateTimePicker();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.checkDetraccion = new MaterialSkin.Controls.MaterialCheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNoGravado = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.txtNumero = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.checkDolares = new MaterialSkin.Controls.MaterialCheckBox();
            this.txtCambio = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.txtDecuento = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel16 = new MaterialSkin.Controls.MaterialLabel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(12, 143);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(74, 19);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "SERIE-N°:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(558, 370);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(57, 19);
            this.materialLabel3.TabIndex = 2;
            this.materialLabel3.Text = "Monto:";
            this.materialLabel3.Click += new System.EventHandler(this.materialLabel3_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoSize = true;
            this.btnAgregar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAgregar.Depth = 0;
            this.btnAgregar.Icon = null;
            this.btnAgregar.Location = new System.Drawing.Point(680, 483);
            this.btnAgregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Primary = true;
            this.btnAgregar.Size = new System.Drawing.Size(83, 36);
            this.btnAgregar.TabIndex = 48;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.AutoSize = true;
            this.btnCancelar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCancelar.Depth = 0;
            this.btnCancelar.Icon = null;
            this.btnCancelar.Location = new System.Drawing.Point(582, 483);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCancelar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Primary = false;
            this.btnCancelar.Size = new System.Drawing.Size(91, 36);
            this.btnCancelar.TabIndex = 47;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtSerie
            // 
            this.txtSerie.Depth = 0;
            this.txtSerie.Hint = "";
            this.txtSerie.Location = new System.Drawing.Point(112, 139);
            this.txtSerie.MaxLength = 4;
            this.txtSerie.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.PasswordChar = '\0';
            this.txtSerie.SelectedText = "";
            this.txtSerie.SelectionLength = 0;
            this.txtSerie.SelectionStart = 0;
            this.txtSerie.Size = new System.Drawing.Size(63, 23);
            this.txtSerie.TabIndex = 49;
            this.txtSerie.TabStop = false;
            this.txtSerie.UseSystemPasswordChar = false;
            // 
            // txtMonto
            // 
            this.txtMonto.Depth = 0;
            this.txtMonto.Hint = "";
            this.txtMonto.Location = new System.Drawing.Point(621, 370);
            this.txtMonto.MaxLength = 32767;
            this.txtMonto.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.PasswordChar = '\0';
            this.txtMonto.SelectedText = "";
            this.txtMonto.SelectionLength = 0;
            this.txtMonto.SelectionStart = 0;
            this.txtMonto.Size = new System.Drawing.Size(141, 23);
            this.txtMonto.TabIndex = 50;
            this.txtMonto.TabStop = false;
            this.txtMonto.Text = "0";
            this.txtMonto.UseSystemPasswordChar = false;
            this.txtMonto.Click += new System.EventHandler(this.txtMonto_Click);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Depth = 0;
            this.txtDescripcion.Hint = "";
            this.txtDescripcion.Location = new System.Drawing.Point(111, 275);
            this.txtDescripcion.MaxLength = 40;
            this.txtDescripcion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.PasswordChar = '\0';
            this.txtDescripcion.SelectedText = "";
            this.txtDescripcion.SelectionLength = 0;
            this.txtDescripcion.SelectionStart = 0;
            this.txtDescripcion.Size = new System.Drawing.Size(651, 23);
            this.txtDescripcion.TabIndex = 84;
            this.txtDescripcion.TabStop = false;
            this.txtDescripcion.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(12, 279);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(93, 19);
            this.materialLabel4.TabIndex = 85;
            this.materialLabel4.Text = "Descripción:";
            // 
            // comboMedioPago
            // 
            this.comboMedioPago.DisplayMember = "nombre";
            this.comboMedioPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMedioPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboMedioPago.FormattingEnabled = true;
            this.comboMedioPago.Location = new System.Drawing.Point(112, 315);
            this.comboMedioPago.Name = "comboMedioPago";
            this.comboMedioPago.Size = new System.Drawing.Size(276, 26);
            this.comboMedioPago.TabIndex = 121;
            this.comboMedioPago.ValueMember = "id";
            // 
            // materialLabel7
            // 
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(13, 322);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(93, 23);
            this.materialLabel7.TabIndex = 120;
            this.materialLabel7.Text = "MD PAGO:";
            // 
            // comboProveedor
            // 
            this.comboProveedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboProveedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboProveedor.DisplayMember = "razonSocial";
            this.comboProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboProveedor.FormattingEnabled = true;
            this.comboProveedor.Location = new System.Drawing.Point(112, 229);
            this.comboProveedor.MaxLength = 200;
            this.comboProveedor.Name = "comboProveedor";
            this.comboProveedor.Size = new System.Drawing.Size(650, 26);
            this.comboProveedor.TabIndex = 181;
            this.comboProveedor.ValueMember = "id";
            // 
            // materialLabel8
            // 
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(11, 232);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(80, 23);
            this.materialLabel8.TabIndex = 180;
            this.materialLabel8.Text = "Proveedor:";
            this.materialLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInicio
            // 
            this.txtInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInicio.Location = new System.Drawing.Point(111, 174);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(259, 24);
            this.txtInicio.TabIndex = 182;
            // 
            // txtDetraccion
            // 
            this.txtDetraccion.Depth = 0;
            this.txtDetraccion.Hint = "";
            this.txtDetraccion.Location = new System.Drawing.Point(102, 10);
            this.txtDetraccion.MaxLength = 32767;
            this.txtDetraccion.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDetraccion.Name = "txtDetraccion";
            this.txtDetraccion.PasswordChar = '\0';
            this.txtDetraccion.SelectedText = "";
            this.txtDetraccion.SelectionLength = 0;
            this.txtDetraccion.SelectionStart = 0;
            this.txtDetraccion.Size = new System.Drawing.Size(120, 23);
            this.txtDetraccion.TabIndex = 185;
            this.txtDetraccion.TabStop = false;
            this.txtDetraccion.Text = "0";
            this.txtDetraccion.UseSystemPasswordChar = false;
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(13, 305);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(0, 19);
            this.materialLabel5.TabIndex = 184;
            // 
            // txtPorcentaje
            // 
            this.txtPorcentaje.Depth = 0;
            this.txtPorcentaje.Hint = "";
            this.txtPorcentaje.Location = new System.Drawing.Point(349, 10);
            this.txtPorcentaje.MaxLength = 32767;
            this.txtPorcentaje.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtPorcentaje.Name = "txtPorcentaje";
            this.txtPorcentaje.PasswordChar = '\0';
            this.txtPorcentaje.SelectedText = "";
            this.txtPorcentaje.SelectionLength = 0;
            this.txtPorcentaje.SelectionStart = 0;
            this.txtPorcentaje.Size = new System.Drawing.Size(156, 23);
            this.txtPorcentaje.TabIndex = 187;
            this.txtPorcentaje.TabStop = false;
            this.txtPorcentaje.Text = "0";
            this.txtPorcentaje.UseSystemPasswordChar = false;
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(242, 14);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(101, 19);
            this.materialLabel6.TabIndex = 186;
            this.materialLabel6.Text = "% Detracción:";
            // 
            // txtFechaVencimiento
            // 
            this.txtFechaVencimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaVencimiento.Location = new System.Drawing.Point(503, 174);
            this.txtFechaVencimiento.Name = "txtFechaVencimiento";
            this.txtFechaVencimiento.Size = new System.Drawing.Size(259, 24);
            this.txtFechaVencimiento.TabIndex = 188;
            // 
            // comboTipoComprobante
            // 
            this.comboTipoComprobante.DisplayMember = "descripcion";
            this.comboTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoComprobante.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboTipoComprobante.FormattingEnabled = true;
            this.comboTipoComprobante.Location = new System.Drawing.Point(379, 136);
            this.comboTipoComprobante.Name = "comboTipoComprobante";
            this.comboTipoComprobante.Size = new System.Drawing.Size(383, 26);
            this.comboTipoComprobante.TabIndex = 189;
            this.comboTipoComprobante.ValueMember = "id";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(424, 179);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(48, 19);
            this.materialLabel1.TabIndex = 190;
            this.materialLabel1.Text = "VNC.:";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(12, 179);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(73, 19);
            this.materialLabel9.TabIndex = 191;
            this.materialLabel9.Text = "EMISION:";
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(291, 143);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(79, 19);
            this.materialLabel10.TabIndex = 192;
            this.materialLabel10.Text = "TIPO DOC:";
            // 
            // txtCuenta
            // 
            this.txtCuenta.Depth = 0;
            this.txtCuenta.Hint = "";
            this.txtCuenta.Location = new System.Drawing.Point(612, 10);
            this.txtCuenta.MaxLength = 32767;
            this.txtCuenta.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCuenta.Name = "txtCuenta";
            this.txtCuenta.PasswordChar = '\0';
            this.txtCuenta.SelectedText = "";
            this.txtCuenta.SelectionLength = 0;
            this.txtCuenta.SelectionStart = 0;
            this.txtCuenta.Size = new System.Drawing.Size(141, 23);
            this.txtCuenta.TabIndex = 194;
            this.txtCuenta.TabStop = false;
            this.txtCuenta.Text = "0";
            this.txtCuenta.UseSystemPasswordChar = false;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(534, 14);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(72, 19);
            this.materialLabel11.TabIndex = 193;
            this.materialLabel11.Text = "CUENTA:";
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(143, 49);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(200, 19);
            this.materialLabel12.TabIndex = 202;
            this.materialLabel12.Text = "FECHA PAGO DETRACCION:";
            // 
            // txtFechaDetraccion
            // 
            this.txtFechaDetraccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaDetraccion.Location = new System.Drawing.Point(349, 44);
            this.txtFechaDetraccion.Name = "txtFechaDetraccion";
            this.txtFechaDetraccion.Size = new System.Drawing.Size(257, 24);
            this.txtFechaDetraccion.TabIndex = 201;
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(3, 14);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(57, 19);
            this.materialLabel13.TabIndex = 203;
            this.materialLabel13.Text = "Monto:";
            // 
            // checkDetraccion
            // 
            this.checkDetraccion.AutoSize = true;
            this.checkDetraccion.Depth = 0;
            this.checkDetraccion.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkDetraccion.Location = new System.Drawing.Point(17, 379);
            this.checkDetraccion.Margin = new System.Windows.Forms.Padding(0);
            this.checkDetraccion.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkDetraccion.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkDetraccion.Name = "checkDetraccion";
            this.checkDetraccion.Ripple = true;
            this.checkDetraccion.Size = new System.Drawing.Size(114, 30);
            this.checkDetraccion.TabIndex = 204;
            this.checkDetraccion.Text = "DETRACCION";
            this.checkDetraccion.UseVisualStyleBackColor = true;
            this.checkDetraccion.CheckedChanged += new System.EventHandler(this.checkDetraccion_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.materialLabel13);
            this.panel1.Controls.Add(this.txtDetraccion);
            this.panel1.Controls.Add(this.materialLabel6);
            this.panel1.Controls.Add(this.materialLabel12);
            this.panel1.Controls.Add(this.txtPorcentaje);
            this.panel1.Controls.Add(this.txtFechaDetraccion);
            this.panel1.Controls.Add(this.materialLabel11);
            this.panel1.Controls.Add(this.txtCuenta);
            this.panel1.Location = new System.Drawing.Point(12, 406);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(769, 71);
            this.panel1.TabIndex = 205;
            // 
            // txtNoGravado
            // 
            this.txtNoGravado.Depth = 0;
            this.txtNoGravado.Hint = "";
            this.txtNoGravado.Location = new System.Drawing.Point(621, 344);
            this.txtNoGravado.MaxLength = 32767;
            this.txtNoGravado.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNoGravado.Name = "txtNoGravado";
            this.txtNoGravado.PasswordChar = '\0';
            this.txtNoGravado.SelectedText = "";
            this.txtNoGravado.SelectionLength = 0;
            this.txtNoGravado.SelectionStart = 0;
            this.txtNoGravado.Size = new System.Drawing.Size(141, 23);
            this.txtNoGravado.TabIndex = 207;
            this.txtNoGravado.TabStop = false;
            this.txtNoGravado.Text = "0";
            this.txtNoGravado.UseSystemPasswordChar = false;
            // 
            // materialLabel14
            // 
            this.materialLabel14.AutoSize = true;
            this.materialLabel14.Depth = 0;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.Location = new System.Drawing.Point(422, 344);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(193, 19);
            this.materialLabel14.TabIndex = 206;
            this.materialLabel14.Text = "Monto Inafecto/Exonerado:";
            // 
            // txtNumero
            // 
            this.txtNumero.Depth = 0;
            this.txtNumero.Hint = "";
            this.txtNumero.Location = new System.Drawing.Point(181, 139);
            this.txtNumero.MaxLength = 8;
            this.txtNumero.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.Size = new System.Drawing.Size(104, 23);
            this.txtNumero.TabIndex = 208;
            this.txtNumero.TabStop = false;
            this.txtNumero.UseSystemPasswordChar = false;
            // 
            // checkDolares
            // 
            this.checkDolares.AutoSize = true;
            this.checkDolares.Depth = 0;
            this.checkDolares.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkDolares.Location = new System.Drawing.Point(351, 81);
            this.checkDolares.Margin = new System.Windows.Forms.Padding(0);
            this.checkDolares.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkDolares.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkDolares.Name = "checkDolares";
            this.checkDolares.Ripple = true;
            this.checkDolares.Size = new System.Drawing.Size(90, 30);
            this.checkDolares.TabIndex = 209;
            this.checkDolares.Text = "DOLARES";
            this.checkDolares.UseVisualStyleBackColor = true;
            this.checkDolares.CheckedChanged += new System.EventHandler(this.checkDolares_CheckedChanged);
            // 
            // txtCambio
            // 
            this.txtCambio.Depth = 0;
            this.txtCambio.Hint = "";
            this.txtCambio.Location = new System.Drawing.Point(672, 81);
            this.txtCambio.MaxLength = 32767;
            this.txtCambio.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCambio.Name = "txtCambio";
            this.txtCambio.PasswordChar = '\0';
            this.txtCambio.SelectedText = "";
            this.txtCambio.SelectionLength = 0;
            this.txtCambio.SelectionStart = 0;
            this.txtCambio.Size = new System.Drawing.Size(90, 23);
            this.txtCambio.TabIndex = 210;
            this.txtCambio.TabStop = false;
            this.txtCambio.Text = "1";
            this.txtCambio.UseSystemPasswordChar = false;
            // 
            // materialLabel15
            // 
            this.materialLabel15.AutoSize = true;
            this.materialLabel15.Depth = 0;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.Location = new System.Drawing.Point(503, 85);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(112, 19);
            this.materialLabel15.TabIndex = 211;
            this.materialLabel15.Text = "CAMBIO EN S/:";
            // 
            // txtDecuento
            // 
            this.txtDecuento.Depth = 0;
            this.txtDecuento.Hint = "";
            this.txtDecuento.Location = new System.Drawing.Point(621, 315);
            this.txtDecuento.MaxLength = 32767;
            this.txtDecuento.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtDecuento.Name = "txtDecuento";
            this.txtDecuento.PasswordChar = '\0';
            this.txtDecuento.SelectedText = "";
            this.txtDecuento.SelectionLength = 0;
            this.txtDecuento.SelectionStart = 0;
            this.txtDecuento.Size = new System.Drawing.Size(141, 23);
            this.txtDecuento.TabIndex = 213;
            this.txtDecuento.TabStop = false;
            this.txtDecuento.Text = "0";
            this.txtDecuento.UseSystemPasswordChar = false;
            // 
            // materialLabel16
            // 
            this.materialLabel16.AutoSize = true;
            this.materialLabel16.Depth = 0;
            this.materialLabel16.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel16.Location = new System.Drawing.Point(530, 318);
            this.materialLabel16.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel16.Name = "materialLabel16";
            this.materialLabel16.Size = new System.Drawing.Size(85, 19);
            this.materialLabel16.TabIndex = 212;
            this.materialLabel16.Text = "Descuento:";
            // 
            // FormNuevoMovimientoVarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 526);
            this.Controls.Add(this.txtDecuento);
            this.Controls.Add(this.materialLabel16);
            this.Controls.Add(this.materialLabel15);
            this.Controls.Add(this.txtCambio);
            this.Controls.Add(this.checkDolares);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.txtNoGravado);
            this.Controls.Add(this.materialLabel14);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkDetraccion);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.comboTipoComprobante);
            this.Controls.Add(this.txtFechaVencimiento);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.txtInicio);
            this.Controls.Add(this.comboProveedor);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.comboMedioPago);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.txtMonto);
            this.Controls.Add(this.txtSerie);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel2);
            this.MaximizeBox = false;
            this.Name = "FormNuevoMovimientoVarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gastos";
            this.Load += new System.EventHandler(this.FormNuevoMovimientoVarios_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialRaisedButton btnAgregar;
        private MaterialSkin.Controls.MaterialFlatButton btnCancelar;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtSerie;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtMonto;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDescripcion;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private System.Windows.Forms.ComboBox comboMedioPago;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private System.Windows.Forms.ComboBox comboProveedor;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private System.Windows.Forms.DateTimePicker txtInicio;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDetraccion;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtPorcentaje;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private System.Windows.Forms.DateTimePicker txtFechaVencimiento;
        private System.Windows.Forms.ComboBox comboTipoComprobante;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCuenta;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private System.Windows.Forms.DateTimePicker txtFechaDetraccion;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialCheckBox checkDetraccion;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNoGravado;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtNumero;
        private MaterialSkin.Controls.MaterialCheckBox checkDolares;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCambio;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtDecuento;
        private MaterialSkin.Controls.MaterialLabel materialLabel16;
    }
}