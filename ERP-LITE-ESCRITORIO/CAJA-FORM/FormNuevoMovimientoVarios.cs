﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    public partial class FormNuevoMovimientoVarios : MaterialForm
    {
        public delegate void pasar(MovimientoVarios m);
        public event pasar pasado;
        ConceptoMovimientoVariosService cservice = new ConceptoMovimientoVariosImpl();
        MovimientoVariosService mvservice = new MovimientoVariosImpl();
        CajaDiariaService cdService = new CajaDiariaImpl();
        MedioPagoService mpService = new MedioPagoImpl();
        TipoComprobantePagoService tcservice = new TipoComprobanteImpl();
        String CONCEPTO = "";

        ProveedorService pvservice = new ProveedorImpl();
        bool esIngreso = false;
        public FormNuevoMovimientoVarios(string ingresoOegreso)
        {
            InitializeComponent();
            txtCambio.Enabled = false;
        
        comboMedioPago.DataSource = mpService.listarNoAnulados();
            if (ingresoOegreso.Equals("INGRESO"))
            {
                CONCEPTO = "INGRESO VARIOS";
                esIngreso = true;
            }
            else {
                CONCEPTO = "SALIDA VARIOS";
                esIngreso = false;
            }
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);

            List<Proveedor> proveedores = pvservice.listarNoAnulados();
            AutoCompleteStringCollection coleccion = new AutoCompleteStringCollection();
            proveedores.ForEach(c => coleccion.Add(c.razonSocial));
            comboProveedor.AutoCompleteCustomSource = coleccion;
            comboProveedor.DataSource = proveedores;
        }

        private void FormNuevoMovimientoVarios_Load(object sender, EventArgs e)
        {
            checkDetraccion.Checked = true;
            checkDetraccion.Checked = false;
            comboTipoComprobante.DataSource = tcservice.listarNoAnulados();
       
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Visible = false;
            if (!Util.esDouble(txtCambio.Text)) { MessageBox.Show("EL TIPO DE CAMBIO ES INCORRECTO"); btnAgregar.Visible = true; return; }
            if (!Util.esDouble(txtDecuento.Text)) { MessageBox.Show("EL DESCUENTO ES INCORRECTO"); btnAgregar.Visible = true; return; }
            if (comboProveedor.SelectedValue != null)
                if (comboMedioPago.SelectedValue != null)
                    if (Util.validaCadena(txtSerie.Text)) {
                        if (Util.validaCadena(txtDescripcion.Text))
                        {
                            if (Util.esDouble(txtNoGravado.Text))
                            {
                                if (Util.esDouble(txtMonto.Text))
                                {
                                    if (Util.esDouble(txtDetraccion.Text))
                                    {

                                        if (Util.esDouble(txtPorcentaje.Text))
                                        {

                                            if (MessageBox.Show("SEGURO DE GUARDAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                            {
                                                int idcaja = cdService.getCaja();
                                                if (idcaja == 0) { MessageBox.Show("ERROR INESPERADO AL ABRIR CAJA INTENTELO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); btnAgregar.Visible = true; return; }
                                                double descuento = Convert.ToDouble(txtDecuento.Text);
                                                MovimientoCaja mc = new MovimientoCaja
                                                {
                                                    concepto = txtDescripcion.Text,
                                                    esIngresoVarios = esIngreso,
                                                    esSalidaVarios = !esIngreso,
                                                    fecha = DateTime.Now,
                                                    idMedioPago = (int)comboMedioPago.SelectedValue,
                                                    monto = Convert.ToDouble(txtMonto.Text),
                                                    serieNumeroComprobante = txtSerie.Text.ToUpper()+"-"+txtNumero.Text,
                                                    idDiaCaja = idcaja, 
                                                };
                                                MovimientoVarios mv = new MovimientoVarios
                                                {
                                                    esDetraccion = checkDetraccion.Checked,
                                                    idTipoComprobante = (int)comboTipoComprobante.SelectedValue,
                                                    idConcepto = 4,
                                                    movimientoCaja = mc,
                                                    fechaComprobante = txtInicio.Value,
                                                    idProveedor = (int)comboProveedor.SelectedValue,
                                                    montoDetraccion = Convert.ToDouble(txtDetraccion.Text),
                                                    pocentajeDetraccion = Convert.ToDouble(txtPorcentaje.Text),
                                                    numero = txtNumero.Text,
                                                    serie =txtSerie.Text, esDolar=checkDetraccion.Checked
                                                    ,descuento=Convert.ToDouble(txtDecuento.Text),valorNoGravadas=Convert.ToDouble(txtNoGravado.Text),
                                                };

                                                if (mvservice.crear2(mv, txtFechaVencimiento.Value, txtFechaDetraccion.Value, txtCuenta.Text,Convert.ToDouble(txtCambio.Text)))
                                                {
                                                    this.Close();
                                                    pasado(mvservice.buscar(mv.id));
                                                }
                                                else { MessageBox.Show("ERROR INTENTALO DE NUEVO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); }


                                            }
                                        }
                                        else MessageBox.Show("PORCENTAJE INVALIDO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    else MessageBox.Show("MONTO DETRACCION INVALIDO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else MessageBox.Show("MONTO INVALIDO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }else MessageBox.Show("MONTO OP NO GRAVADAS INVALIDO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else MessageBox.Show("COMPLETA DESCRIPCION", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                else MessageBox.Show("COMPLETA LA SERIE NUMERO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("NO EXISTE MEDIO DE PAGO", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else MessageBox.Show("NO EXISTE PROVEEDOR", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            btnAgregar.Visible = true;
        }

        private void txtMonto_Click(object sender, EventArgs e)
        {

        }

        private void materialLabel3_Click(object sender, EventArgs e)
        {

        }

        private void checkDetraccion_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDetraccion.Checked) panel1.Visible = true; else panel1.Visible = false;
        }

        private void checkDolares_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDolares.Checked)
            {
                txtCambio.Enabled = true;

            }
            else txtCambio.Enabled = false;
        }
    }
}
