﻿namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    partial class FormAdminCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdminCaja));
            this.contenedor = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMovimientosVarios = new System.Windows.Forms.Button();
            this.btnCajaPiura = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contenedor
            // 
            this.contenedor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.contenedor.Location = new System.Drawing.Point(224, 64);
            this.contenedor.Name = "contenedor";
            this.contenedor.Size = new System.Drawing.Size(1124, 634);
            this.contenedor.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnMovimientosVarios);
            this.panel1.Controls.Add(this.btnCajaPiura);
            this.panel1.Location = new System.Drawing.Point(0, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 634);
            this.panel1.TabIndex = 5;
            // 
            // btnMovimientosVarios
            // 
            this.btnMovimientosVarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMovimientosVarios.FlatAppearance.BorderSize = 0;
            this.btnMovimientosVarios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnMovimientosVarios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnMovimientosVarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMovimientosVarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMovimientosVarios.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMovimientosVarios.Image = ((System.Drawing.Image)(resources.GetObject("btnMovimientosVarios.Image")));
            this.btnMovimientosVarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMovimientosVarios.Location = new System.Drawing.Point(0, 58);
            this.btnMovimientosVarios.Name = "btnMovimientosVarios";
            this.btnMovimientosVarios.Size = new System.Drawing.Size(228, 49);
            this.btnMovimientosVarios.TabIndex = 4;
            this.btnMovimientosVarios.Text = "          Gastos varios";
            this.btnMovimientosVarios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMovimientosVarios.UseVisualStyleBackColor = true;
            this.btnMovimientosVarios.Click += new System.EventHandler(this.btnMovimientosVarios_Click);
            // 
            // btnCajaPiura
            // 
            this.btnCajaPiura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCajaPiura.FlatAppearance.BorderSize = 0;
            this.btnCajaPiura.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.btnCajaPiura.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.btnCajaPiura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajaPiura.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajaPiura.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCajaPiura.Image = ((System.Drawing.Image)(resources.GetObject("btnCajaPiura.Image")));
            this.btnCajaPiura.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCajaPiura.Location = new System.Drawing.Point(0, 3);
            this.btnCajaPiura.Name = "btnCajaPiura";
            this.btnCajaPiura.Size = new System.Drawing.Size(228, 49);
            this.btnCajaPiura.TabIndex = 1;
            this.btnCajaPiura.Text = "          Caja Diaria";
            this.btnCajaPiura.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCajaPiura.UseVisualStyleBackColor = true;
            this.btnCajaPiura.Click += new System.EventHandler(this.btnCajaPiura_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(187)))), ((int)(((byte)(106)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(199)))), ((int)(((byte)(132)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 113);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 49);
            this.button1.TabIndex = 5;
            this.button1.Text = "          Bancos";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormAdminCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 692);
            this.Controls.Add(this.contenedor);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "FormAdminCaja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Módulo Caja";
            this.Load += new System.EventHandler(this.FormAdminCaja_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel contenedor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMovimientosVarios;
        private System.Windows.Forms.Button btnCajaPiura;
        private System.Windows.Forms.Button button1;
    }
}