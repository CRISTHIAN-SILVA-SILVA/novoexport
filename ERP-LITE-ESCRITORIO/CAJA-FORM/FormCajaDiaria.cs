﻿using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    public partial class FormCajaDiaria : Form
    {
        DataTable tabla = new DataTable();
        CajaDiariaService cdservice = new CajaDiariaImpl();
        MovimientoCajaImpl mservice = new MovimientoCajaImpl();
        public FormCajaDiaria()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("HORA");
            tabla.Columns.Add("SERIE-NUMERO");
            tabla.Columns.Add("CONCEPTO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("MEDIO DE PAGO");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 150;
            grid.Columns[2].Width = 200;
            grid.Columns[3].Width = 300;
            grid.Columns[4].Width = 200;
            grid.Columns[5].Width = 220;
        }

        private void FormCajaDiaria_Load(object sender, EventArgs e)
        {
            txtDia_ValueChanged(null,null);
        }

        private void txtDia_ValueChanged(object sender, EventArgs e)
        {
           CajaDiaria cajaDiaria= cdservice.buscarPorDia(txtDia.Value.Date);
            if (cajaDiaria != null)
            {
                lblExisteCaja.Text = "";
                lblMontoInicial.Text = string.Format("{0:0.00}",cajaDiaria.montoInicial);
                lblMontoFinal.Text = string.Format("{0:0.00}", cajaDiaria.montoFinal);
                tabla.Clear();
                foreach (var x in mservice.listarPorCaja(cajaDiaria.id)) {
                    if(!x.anulado)
                        tabla.Rows.Add(x.id, x.fecha.ToShortTimeString(), x.serieNumeroComprobante, x.concepto, string.Format("{0:0.00}", x.monto),x.medioPago.nombre);
                }  }
            else {
                lblMontoInicial.Text = string.Format("{0:0.00}", 0);
                lblMontoFinal.Text = string.Format("{0:0.00}", 0);
                tabla.Clear();
                lblExisteCaja.Text = "NO SE ABRIO CAJA ESTE DIA";
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
    }
}
