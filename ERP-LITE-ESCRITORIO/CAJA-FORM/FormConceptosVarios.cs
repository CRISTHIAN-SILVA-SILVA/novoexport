﻿using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    public partial class FormConceptosVarios : Form
    {
        ConceptoMovimientoVariosService cservice = new ConceptoMovimientoVariosImpl();
        DataTable tabla=new DataTable();
        public FormConceptosVarios()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("DESCRIPCION");
            grid.DataSource = tabla;
            grid.Columns[0].Width=200;
            grid.Columns[1].Width = 900;
        }

        void agregar(ConceptoMovimientoVarios x) {
            tabla.Rows.Add(x.id, x.descripcion);
        }
        private void FormConceptosVarios_Load(object sender, EventArgs e)
        {
           cservice.listarNoAnulados().ForEach(x=> tabla.Rows.Add(x.id,x.descripcion));
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(grid.SelectedRows.Count==1)
            if (MessageBox.Show("SEGURO DE ELIMINAR CONCEPTO?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    ConceptoMovimientoVarios concepto = cservice.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString())) ;
                    concepto.anulado = true;
                    cservice.editar(concepto);
                    grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevoConceptoMovVarios form = new FormNuevoConceptoMovVarios();

            form.pasado += new FormNuevoConceptoMovVarios.pasar(agregar);
            form.ShowDialog();

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }
    }
}
