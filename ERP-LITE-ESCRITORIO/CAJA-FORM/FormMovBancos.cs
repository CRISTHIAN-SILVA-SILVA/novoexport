﻿using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_ESCRITORIO.CAJA_FORM
{
    public partial class FormMovBancos : Form
    {
        DataTable tabla = new DataTable();
        PagosBancoImpl pservice = new PagosBancoImpl();
        public FormMovBancos()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("CONCEPTO");
            tabla.Columns.Add("COMPROBANTE");
            tabla.Columns.Add("MEDIO PAGO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("CUENTA");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 100;
            grid.Columns[2].Width = 200;
            grid.Columns[3].Width = 160;
            grid.Columns[4].Width = 120;
            grid.Columns[5].Width = 120;
            grid.Columns[6].Width = 300;
        }
        void cargar() {

            tabla.Clear();
            foreach (var i in pservice.listar())
            {
                if (!i.movimiento.anulado)
                {
                    tabla.Rows.Add(i.id, i.movimiento.fecha.ToShortDateString(),
                        "VENTA", i.comprobante, i.medioPago.nombre,i.movimiento.monto, i.cuentaBanco.cuenta);
                }
            };
        }
        private void FormMovBancos_Load(object sender, EventArgs e)
        {
            cargar();
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            tabla.Clear();
            foreach (var i in pservice.listarPorFecha(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date))
            {
               
                    tabla.Rows.Add(i.id, i.movimiento.fecha.ToShortDateString(),
                        "VENTA", i.comprobante, i.medioPago.nombre,i.movimiento.monto, i.cuentaBanco.cuenta);
                
            };
        }
    }
}
