﻿using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.RELACIONES_SERVICE;
using SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.CAJA_FORM
{
    public partial class FormMovimientosVarios : Form
    {
        DataTable tabla = new DataTable();
        MovimientoVariosService mvservice = new MovimientoVariosImpl();
        public FormMovimientosVarios()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("FECHA");
            tabla.Columns.Add("PROVEEDOR");
            tabla.Columns.Add("SERIE-NUMERO");
            tabla.Columns.Add("CONCEPTO");
            tabla.Columns.Add("MONTO");
            tabla.Columns.Add("DETRACCION");
            tabla.Columns.Add("% DETRACCION");
            tabla.Columns.Add("MONEDA");

            tabla.Columns.Add("NO GRAVADO");
            tabla.Columns.Add("DESCUENTO");

            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 80;
            grid.Columns[2].Width = 340 ;
            grid.Columns[3].Width = 120;
            grid.Columns[4].Width = 220;
            grid.Columns[5].Width = 80;
            grid.Columns[6].Width = 100;
            grid.Columns[7].Width = 80;
            grid.Columns[8].Width = 80;
        }
        void agregar(MovimientoVarios x)
        {
            tabla.Rows.Add(x.id,x.fechaComprobante.ToShortDateString(),x.proveedor.razonSocial,x.movimientoCaja.serieNumeroComprobante,x.movimientoCaja.concepto,String.Format("{0:0.00}", x.movimientoCaja.monto), String.Format("{0:0.00}", x.montoDetraccion)
                , String.Format("{0:0.00}", x.pocentajeDetraccion),x.esDolar, x.valorNoGravadas, x.descuento);
        }
        private void btnNuevoIngreso_Click(object sender, EventArgs e)
        {
            FormNuevoMovimientoVarios form = new FormNuevoMovimientoVarios("INGRESO");

            form.pasado += new FormNuevoMovimientoVarios.pasar(agregar);
            form.ShowDialog();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            Exportar.exportarAExcelAsync(grid);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(grid.SelectedRows.Count==1)
            if (MessageBox.Show("SEGURO DE ELIMINAR?", "CONFIRMAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                    if (mvservice.eliminar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString())))
                    {
                        grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                    }
                    else {
                        MessageBox.Show("ERROR INESPERADO PUEDE QUE EXCEDIERA EL LIMETE DEL MONTO DE CAJA", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    

            }
        }

        private void btnNuevaSalida_Click(object sender, EventArgs e)
        {
            FormNuevoMovimientoVarios form = new FormNuevoMovimientoVarios("SALIDA");

            form.pasado += new FormNuevoMovimientoVarios.pasar(agregar);
            form.ShowDialog();
        }
        public void cargar() {
            tabla.Clear();
            string moneda = "";
            foreach (var x in mvservice.listarNoAnulados())
            {
                if (x.esDolar) moneda = "DOLARES"; else moneda = "SOLES";
                tabla.Rows.Add(x.id, x.fechaComprobante.ToShortDateString(), x.proveedor.razonSocial, x.movimientoCaja.serieNumeroComprobante,
                     x.movimientoCaja.concepto, String.Format("{0:0.00}", x.movimientoCaja.monto), String.Format("{0:0.00}", x.montoDetraccion)
                     , String.Format("{0:0.00}", x.pocentajeDetraccion), moneda, x.valorNoGravadas, x.descuento);
            }

        }
        private void FormMovimientosVarios_Load(object sender, EventArgs e)
        {
            cargar();  }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cargar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            tabla.Clear();
            string moneda = "";
            foreach (var x in mvservice.listarPorFecha(txtInicio.Value.Date, txtFin.Value.AddDays(1).Date)) {
                if (x.esDolar) moneda = "DOLARES"; else moneda = "SOLES";
                tabla.Rows.Add(x.id, x.fechaComprobante.ToShortDateString(), x.proveedor.razonSocial, x.movimientoCaja.serieNumeroComprobante,
                     x.movimientoCaja.concepto, String.Format("{0:0.00}", x.movimientoCaja.monto), String.Format("{0:0.00}", x.montoDetraccion)
                     , String.Format("{0:0.00}", x.pocentajeDetraccion), moneda, x.valorNoGravadas, x.descuento);
               }
        }
    }
}
