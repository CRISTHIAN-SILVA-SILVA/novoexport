﻿using ERP_LITE_DESKTOP.AUTH_FORM;
using ERP_LITE_DESKTOP.CAJA_FORM;
using ERP_LITE_DESKTOP.COMPRAS_FORM;
using ERP_LITE_DESKTOP.CONFIGURACION_FORM;
using ERP_LITE_DESKTOP.CUENTAS_CORRIENTES_FORM;
using ERP_LITE_DESKTOP.FACTURACION_FORM;
using ERP_LITE_DESKTOP.INVENTARIO_FORM;
using ERP_LITE_DESKTOP.PEDIDOS_FORM;
using ERP_LITE_DESKTOP.RELACIONES;
using ERP_LITE_ESCRITORIO.CONFIGURACION_FORM;
using ERP_LITE_ESCRITORIO.CONTABILIDAD_FORM;
using ERP_LITE_ESCRITORIO.PEDIDOS_FORM;
using ERP_LITE_ESCRITORIO.REPORTES_FORM;
using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP
{
    public partial class Principal : MaterialForm
    {
        PermisoService pservice = new PermisoImpl();
        List<Permiso> permisos = new List<Permiso>();
        public Principal()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.Green700, TextShade.WHITE);
        }
        bool existePermiso(string menu) {
           Permiso p= permisos.FirstOrDefault(x=>x.menu.nombre==menu);
            if (p != null) return true;
            else return false;
        }
        private void Principal_Load(object sender, EventArgs e)
        {
            this.Activate();
          
            btnInventario.Focus();
            btnPedidos.Focus();
            this.Text = FormLogin.user.nombres;
            permisos= pservice.listarNoAnuladosPorRol(FormLogin.user.idRol);
            if (existePermiso("PEDIDOS")) 
                btnPedidos.Enabled = true;
            if (existePermiso("COTIZACIONES"))
                btnCotizacion.Enabled = true;
            if (existePermiso("COMPRAS"))
                btnCompras.Enabled = true;
            if (existePermiso("INVENTARIO"))
                btnInventario.Enabled = true;
            if (existePermiso("FACTURACION"))
                btnFacturacion.Enabled = true;

            if (existePermiso("CAJA"))
                btnCaja.Enabled = true;
            if (existePermiso("CUENTAS CORRIENTES"))
                btnCuentas.Enabled = true;
            if (existePermiso("CLIENTES"))
                btnClientes.Enabled = true;
            if (existePermiso("PROVEEDORES"))
                btnProveedores.Enabled = true;
            if (existePermiso("TRANSPORTISTAS"))
                btnTransportistas.Enabled = true;

            if (existePermiso("RECURSOS HUMANOS"))
                btnRecursosHumanos.Enabled = true;
            if (existePermiso("CONTABILIDAD"))
                btnContabilidad.Enabled = true;
            if (existePermiso("USUARIOS"))
                btnUsuarios.Enabled = true;
            if (existePermiso("REPORTES"))
                btnReporte.Enabled = true;
            if (existePermiso("CONFIGURACION"))
                btnConfiguracion.Enabled = true;
        }
        private void btnPedidos_Click(object sender, EventArgs e)
        {
            FormNuevoPedido form = new FormNuevoPedido();
            form.ShowDialog();
        }

        private void btnCotizacion_Click(object sender, EventArgs e)
        {
            FormAdminCotizaciones form = new FormAdminCotizaciones(false);
            form.Show() ;
        }

        private void btnConfiguracion_Click(object sender, EventArgs e)
        {
            FormConfiguracion form = new FormConfiguracion();
            form.ShowDialog();
        }

        private void btnInventario_Click_1(object sender, EventArgs e)
        {
            FormAdminInventario form = new FormAdminInventario();
            form.Show();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            FormAdminAuth form = new FormAdminAuth();
            form.Show();
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            FormAdminClientes form = new FormAdminClientes();
            form.ShowDialog();
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            FormAdminProveedores form = new FormAdminProveedores();
            form.ShowDialog();
        }

        private void btnTransportistas_Click(object sender, EventArgs e)
        {
            FormAdminTransportistas form = new FormAdminTransportistas();
            form.ShowDialog();
          }

        private void btnFacturacion_Click(object sender, EventArgs e)
        {
            FormAdminFacturacion form = new FormAdminFacturacion();
            form.Show();
        }

        private void btnCaja_Click(object sender, EventArgs e)
        {
            FormAdminCaja form = new FormAdminCaja();
            form.Show();
        }

        private void btnCuentas_Click(object sender, EventArgs e)
        {
            FormAdminCuentasCorrientes form = new FormAdminCuentasCorrientes();
            form.Show();
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            FormAdminCompras form = new FormAdminCompras();
           form.Show();
        }

        private void btnContabilidad_Click(object sender, EventArgs e)
        {
            FormAdminContabilidad form = new FormAdminContabilidad();
            form.Show();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            btnComprobante form = new btnComprobante();
            form.ShowDialog();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            FormCambiarCuenta form = new FormCambiarCuenta();
            form.ShowDialog();
        }
    }
}
