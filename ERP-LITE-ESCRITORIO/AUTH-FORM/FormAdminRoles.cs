﻿using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.AUTH_FORM
{
    public partial class FormAdminRoles : Form
    {
        DataTable tabla = new DataTable();
        RolService srol = new RoImpl();
        public FormAdminRoles()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NOMBRE");
            tabla.Columns.Add("DESCRIPCION");
            tabla.Columns.Add("CREADO POR");
            tabla.Columns.Add("MODIFICADO POR");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 250;
            grid.Columns[2].Width = 402;
            grid.Columns[3].Width = 215;
            grid.Columns[4].Width = 215;
        }
        void llenar()
        {
            tabla.Clear();
            srol.listarNoAnulados().ForEach(i => tabla.Rows.Add(i.id, i.nombre, i.descripcion, i.fechaCreacion.ToShortDateString() + " DNI: " + i.usuario, i.fechaMod.ToShortDateString() + " DNI: " + i.usuarioMod));
        }
        private void FormAdminRoles_Load(object sender, EventArgs e)
        {
            llenar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevoRol form = new FormNuevoRol(null);
            form.pasado += new FormNuevoRol.pasar(llenar);
            form.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ANULAR ROL?", "ANULAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Rol ro = srol.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    ro.anulado = true;
                    ro.usuarioMod = FormLogin.user.dni;
                    ro.fechaMod = DateTime.Now;
                    if (!srol.editar(ro)) MessageBox.Show("NO SE LOGRO ELIMINAR", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    else grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
            btnEliminar.Enabled = true;
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            btnExportar.Enabled = false;
            Exportar.exportarAExcelAsync(grid);
            btnExportar.Enabled = true;
        }

        private void grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Rol r = srol.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
            if (r != null)
            {
                FormNuevoRol form = new FormNuevoRol(r);
                form.pasado += new FormNuevoRol.pasar(llenar);
                form.ShowDialog();
            }
        }
    }
}
