﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.AUTH_FORM
{
    public partial class FormNuevoRol : MaterialForm
    {
        MenuService smenu = new MenuImpl();
        RolService srol = new RoImpl();
        PermisoService spermiso = new PermisoImpl();
        DataTable tabla = new DataTable();
        Rol rol = null;
        public delegate void pasar();
        public event pasar pasado;
        public FormNuevoRol(Rol r)
        {
            rol = r;
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("MENU");
            tabla.Columns.Add("CREADO POR");
            tabla.Columns.Add("MODIFICADO POR");
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 200;
            grid.Columns[2].Width = 282;
            grid.Columns[3].Width = 285;
        }

        private void FormNuevoRol_Load(object sender, EventArgs e)
        {
            comboRol.DataSource = smenu.listarNoAnulados();
            txtNombre.Focus();
            if (rol != null)
            {
                txtNombre.Text = rol.nombre;
                txtDescripcion.Text = rol.descripcion;
                foreach (var i in spermiso.listarNoAnuladosPorRol(rol.id))
                    tabla.Rows.Add(i.id, i.menu.nombre, i.fechaCreacion.ToShortDateString() + " " + i.usuario, i.fechaMod.ToShortDateString() + " " + i.usuarioMod);
                inabilitar();
            }
        }
        void inabilitar()
        {
            txtNombre.Enabled = false;
            txtDescripcion.Enabled = false;
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            if (Util.validaCadena(txtNombre.Text))
                if (comboRol.SelectedValue != null)
                {
                    if (rol == null)
                    {
                        Rol r = new Rol { nombre = txtNombre.Text.ToUpper().Trim(), descripcion = txtDescripcion.Text, usuario = FormLogin.user.dni, usuarioMod = FormLogin.user.dni };
                        Rol existe = srol.buscarPorNombre(r.nombre);
                        if (existe == null)
                        {
                            if (srol.crear(r))
                            {
                                pasado();
                                rol = r;
                                Permiso permiso = new Permiso { idMenu = (int)comboRol.SelectedValue, idRol = rol.id, usuario = FormLogin.user.dni, usuarioMod = FormLogin.user.dni };
                                if (!spermiso.crear(permiso)) MessageBox.Show("NO SE LOGRO AGIGNAR EL PERMISO", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                tabla.Rows.Add(permiso.id, comboRol.Text, permiso.fechaCreacion.ToShortDateString() + " DNI: " + permiso.usuario, permiso.fechaMod.ToShortDateString() + " DNI: " + permiso.usuarioMod);
                                inabilitar();
                            }
                            else { MessageBox.Show("NO SE LOGRO CREAR EL ROL", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                        }
                        else
                          if (existe.anulado)
                        {
                            if (MessageBox.Show("YA EXISTE UN ROL CON ESTE NOMBRE DESEA RESTAURARLO?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                existe.anulado = false;
                                existe.usuarioMod = FormLogin.user.dni;
                                existe.fechaMod = DateTime.Now;
                                srol.editar(existe);
                                rol = existe;
                                txtDescripcion.Text = existe.descripcion;
                                foreach (var i in spermiso.listarNoAnuladosPorRol(rol.id))
                                    tabla.Rows.Add(i.id, i.menu.nombre, i.fechaCreacion.ToShortDateString() + " DNI: " + i.usuario, i.fechaMod.ToShortDateString() + " DNI: " + i.usuarioMod);
                                inabilitar();
                                pasado();
                            }
                        }
                        else { MessageBox.Show("YA EXISTE UN ROL CON ESTE NOMBRE"); }
                    }
                    else
                    {
                        Permiso permiso = new Permiso { idMenu = (int)comboRol.SelectedValue, idRol = rol.id, usuario = FormLogin.user.dni, usuarioMod = FormLogin.user.dni };
                        Permiso existe = spermiso.buscarPorMenuRol(permiso.idMenu, permiso.idRol);
                        if (existe == null)
                            if (!spermiso.crear(permiso)) MessageBox.Show("NO SE LOGRO AGIDNAR EL PERMISO", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            else
                                tabla.Rows.Add(permiso.id, comboRol.Text, permiso.fechaCreacion.ToShortDateString() + " DNI: " + permiso.usuario, permiso.fechaMod.ToShortDateString() + " DNI: " + permiso.usuarioMod);
                        else
                        {
                            if (existe.anulado)
                            {
                                existe.anulado = false;
                                existe.fechaMod = DateTime.Now;
                                existe.usuarioMod = FormLogin.user.dni;
                                spermiso.editar(existe); tabla.Rows.Add(existe.id, comboRol.Text, existe.fechaCreacion.ToShortDateString() + " DNI: " + existe.usuario, existe.fechaMod.ToShortDateString() + " DNI: " + existe.usuarioMod);
                            }
                            else MessageBox.Show("EL PERMISO YA SE ASIGNO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                else MessageBox.Show("NO HAY MENUS PARA SELECCIONAR", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else MessageBox.Show("EL NOMBRE DEL ROL ESTA VACIO", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            btnAgregar.Enabled = true;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("Seguro de eliminar Permiso?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Permiso anulado = spermiso.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    anulado.anulado = true;
                    anulado.usuarioMod = FormLogin.user.dni;
                    anulado.fechaMod = DateTime.Now;
                    if (!spermiso.editar(anulado)) MessageBox.Show("NO SE LOGRO ELIMINAR PERMISO", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
            btnEliminar.Enabled = true;

        }
    }
}
