﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Windows.Forms;
namespace ERP_LITE_DESKTOP.AUTH_FORM
{
    public partial class FormNuevoUsuario : MaterialForm
    {
        public delegate void pasar();
        UsuarioService susuario = new UsuarioImp();
        public event pasar pasado;
        RolService srol = new RoImpl();
        public FormNuevoUsuario()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green300, Primary.Grey800, Primary.BlueGrey500, Accent.LightGreen200, TextShade.WHITE);
            comboRol.DataSource = srol.listarNoAnulados();
        }

        private void FormNuevoUsuario_Load(object sender, EventArgs e)
        {
            this.Activate();
            txtNombres.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnAgregar.Enabled = false;
            if (Util.validaCadena(txtNombres.Text))
                if (Util.esDNI(txtDNI.Text))
                    if (comboRol.SelectedValue != null)
                    {
                        Usuario user = new Usuario
                        {
                            password = txtDNI.Text,
                            nombres = txtNombres.Text.ToUpper().Trim(),
                            email = txtEmail.Text.ToUpper().Trim(),
                            antiguaPassword = txtDNI.Text,
                            dni = txtDNI.Text,
                            idRol = (int)comboRol.SelectedValue,
                            userName = txtDNI.Text,
                            usuario = FormLogin.user.dni,
                            usuarioMod = FormLogin.user.dni,
                        };
                        Usuario existe = susuario.buscaraPorDNI(user.dni);
                        if (existe == null)
                            if (susuario.crear(user)) { pasado(); this.Close(); MessageBox.Show("LA CONTRASEÑA ES: " + user.dni, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information); } else MessageBox.Show("NO SE LOGRO CREAR", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else
                        {
                            if (existe.anulado)
                            {
                                if (MessageBox.Show("YA EXISTE UN USUARIO CON ESTE DNI DESEA RESTAURARLO", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                                {
                                    existe.anulado = false;
                                    existe.password = existe.dni;
                                    existe.fechaMod = DateTime.Now;
                                    existe.usuarioMod = FormLogin.user.dni;
                                    susuario.editar(existe);
                                    pasado(); MessageBox.Show("LA CONTRASEÑA ES: " + existe.dni, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information); this.Close();
                                }
                            }
                            else { MessageBox.Show("YA EXISTE UN USUARIO CON ESTE DNI: " + existe.dni); }
                        }
                    }
                    else MessageBox.Show("NO HAY NINGUN ROL DISPONIBLE", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else MessageBox.Show("DNI NO VALIDO", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else MessageBox.Show("FALTA NOMBRE", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            btnAgregar.Enabled =true;
        }
    }
}
