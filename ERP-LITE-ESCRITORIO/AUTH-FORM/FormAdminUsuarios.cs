﻿using MaterialSkin;
using MaterialSkin.Controls;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.AUTH_SERVICE;
using SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERP_LITE_DESKTOP.AUTH_FORM
{
    public partial class FormAdminUsuarios : Form
    {
        UsuarioService susuario = new UsuarioImp();
        DataTable tabla = new DataTable();
        public FormAdminUsuarios()
        {
            InitializeComponent();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("NOMBRES");
            tabla.Columns.Add("DNI");
            tabla.Columns.Add("ROL");
            tabla.Columns.Add("CREADO POR");
            tabla.Columns.Add("MODIFICADO POR");
            grid.DataSource = tabla;
            grid.Columns[0].Visible = false;
            grid.Columns[1].Width = 450;
            grid.Columns[2].Width = 100;
            grid.Columns[3].Width = 150;
            grid.Columns[4].Width = 190;
            grid.Columns[5 ].Width = 192;
        }
        void llenar()
        {
            tabla.Clear();
            susuario.listarNoAnulados().ForEach(i => tabla.Rows.Add(i.id, i.nombres, i.dni, i.rol.nombre, i.fechaCreacion.ToShortDateString() + " DNI: " + i.usuario, i.fechaMod.ToShortDateString() + " DNI: " + i.usuarioMod));
        }
        private void FormAdminUsuarios_Load(object sender, EventArgs e)
        {
            llenar();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FormNuevoUsuario form = new FormNuevoUsuario();
            form.pasado += new FormNuevoUsuario.pasar(llenar);
            form.ShowDialog();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            btnEliminar.Enabled = false;
            if (grid.SelectedRows.Count == 1)
                if (MessageBox.Show("SEGURO DE ANULAR ROL?", "ANULAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Usuario anulado = susuario.buscar(Convert.ToInt32(grid.SelectedRows[0].Cells["ID"].Value.ToString()));
                    anulado.anulado = true;
                    anulado.usuarioMod = FormLogin.user.dni;
                    anulado.fechaMod = FormLogin.user.fechaMod = DateTime.Now;
                    if (!susuario.editar(anulado)) MessageBox.Show("NO SE LOGRO ELIMINAR", "AVISO", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    else grid.Rows.RemoveAt(grid.SelectedRows[0].Index);
                }
            btnEliminar.Enabled = true;
        }
        private void btnExportar_Click(object sender, EventArgs e)
        {
            btnExportar.Enabled = false;
            Exportar.exportarAExcelAsync(grid);
            btnExportar.Enabled = true;
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
