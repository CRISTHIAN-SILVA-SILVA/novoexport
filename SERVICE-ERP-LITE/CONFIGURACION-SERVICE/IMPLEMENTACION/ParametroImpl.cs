﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION
{
    public class ParametroImpl : ParametroService
    {
        public static List<Parametro> parametros = null;
        public static Parametro IMPRESORA= null;
        public static Parametro IGV = null;
        public static List<Parametro> getInstancia()
        {
            if (parametros == null)
            {
                using (var db = new BaseDeDatos(""))
                {
                    IMPRESORA = db.parametros.FirstOrDefault(z=>z.nombre=="IMPRESORA");
                    parametros= db.parametros.ToList();
                    IMPRESORA = db.parametros.FirstOrDefault(z => z.nombre == "IGV");
                }
            }
            return parametros;
        }
        public Parametro buscar(int id)
        {
            throw new NotImplementedException();

        }

        public bool crear(Parametro objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(Parametro objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Parametro> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.parametros.ToList();
            }
        }
    }
}
