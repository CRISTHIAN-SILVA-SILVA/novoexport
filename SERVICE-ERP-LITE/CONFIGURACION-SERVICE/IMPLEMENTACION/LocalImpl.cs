﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION
{
    public class LocalImpl : LocalService
    {
       public static Local local=null;
        public static Local getInstancia()
        {
            if (local == null)
            {
                using (var db = new BaseDeDatos(""))
                {
                    local= db.locales.FirstOrDefault(x => x.id == 1);
                }
            }
            return local;
        }
        public Local buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.locales.FirstOrDefault(x=>x.id==1);
            }
        }

        public bool crear(Local objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(Local objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Local> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
