﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.CONFIGURACION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION
{
  public  class BancoService
    {
        public List<Banco> listar()
        {
           
                using (var db = new BaseDeDatos(""))
                {
                   return db.bancos.Where(x => x.anulado == false).ToList();
                }
            }

        public Banco buscar(int id)
        {

            using (var db = new BaseDeDatos(""))
            {
                return db.bancos.FirstOrDefault(x => x.id == id);
            }
        }

    }
}
