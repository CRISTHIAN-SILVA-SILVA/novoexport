﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.CONFIGURACION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CONFIGURACION_SERVICE
{
  public  class CuentaBancoService
    {
        public List<CuentaBanco> listar()
        {

            using (var db = new BaseDeDatos(""))
            {
                return db.cuetasBanco.Include("banco").Where(x => x.anulado == false).ToList();
            }
        }

        public bool crear(CuentaBanco c)
        {

            using (var db = new BaseDeDatos(""))
            {
                db.cuetasBanco.Add(c);
                db.SaveChanges();
                return true;
            }
        }
        public bool editar(CuentaBanco objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }



        public CuentaBanco buscar(int id)
        {

            using (var db = new BaseDeDatos(""))
            {
                return db.cuetasBanco.Include("banco").FirstOrDefault(x => x.id == id);
            }
        }
    }
}
