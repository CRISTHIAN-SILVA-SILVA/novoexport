﻿using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CONFIGURACION_SERVICE
{
  public  interface ParametroService:CrudService<Parametro>
    {
    }
}
