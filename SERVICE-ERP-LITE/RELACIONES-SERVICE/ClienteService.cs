﻿using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.RELACIONES_SERVICE
{
   public interface ClienteService:CrudService<Cliente>
    {
        Cliente existe(Cliente cliente);
        Cliente buscarPorDNI(string dni);
        Cliente buscarPorRUC(string ruc);
        List< Cliente> listarClientes();
        List<Cliente> listarEmpresas();
    }
}
