﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.RELACIONES;

namespace SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION
{
    public class TransportistaImpl : TransportistaService
    {
        public Transportista buscar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.transportistas.FirstOrDefault(x => x.id == id);

            }
        }

        public bool crear(Transportista objeto)
        {
            try
            {
                using (var db = new BaseDeDatos(""))
                {
                    db.transportistas.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
            }
            catch { return false; }
        }

        public bool editar(Transportista objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Transportista u = db.transportistas.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public Transportista existe(Transportista transportista)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.transportistas.FirstOrDefault(x => x.ruc == transportista.ruc);
            }
        }

        public List<Transportista> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.transportistas.Where(x => x.anulado == false).ToList();
            }
        }
    }
}
