﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.RELACIONES;

namespace SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION
{
    public class CuentaProveedorImpl : CuentaProveedorService
    {
        public CuentaProveedor buscar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
               return db.cuentasProveeedores.Find(id);
            }
        }

        public bool crear(CuentaProveedor objeto)
        {
            
                using (var db = new BaseDeDatos(""))
                {
                    db.cuentasProveeedores.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
        }

        public bool editar(CuentaProveedor objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<CuentaProveedor> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
