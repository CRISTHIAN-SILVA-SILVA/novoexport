﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.RELACIONES;

namespace SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION
{
    public class ProveedorImpl : ProveedorService
    {
        public Proveedor buscar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.proveedores.Include("cuentasProveedor").FirstOrDefault(x => x.id == id);

            }
        }
        public List<Proveedor> buscars(string T)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.proveedores.Include("cuentasProveedor").Where(x => x.anulado==false&&x.razonSocial.Contains(T)).ToList();

            }
        }

        public bool crear(Proveedor objeto)
        {
            try
            {
                using (var db = new BaseDeDatos(""))
                {
                    db.proveedores.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
            }
            catch { return false; }
        }

        public bool editar(Proveedor objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Proveedor u = db.proveedores.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public Proveedor existe(Proveedor proveedor)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.proveedores.FirstOrDefault(x => x.ruc == proveedor.ruc);
            }
        }

        public List<Proveedor> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.proveedores.Where(x => x.anulado == false).ToList();
            }
        }
    }
}
