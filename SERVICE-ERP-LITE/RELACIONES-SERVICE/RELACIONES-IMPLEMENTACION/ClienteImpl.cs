﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.RELACIONES;

namespace SERVICE_ERP_LITE.RELACIONES_SERVICE.RELACIONES_IMPLEMENTACION
{
    public class ClienteImpl : ClienteService
    {
        public Cliente buscar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
              return  db.clientes.FirstOrDefault(x=>x.id==id);
               
            }
        }
        public List<Cliente> buscarCliente(string texto)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.clientes.Where(x=>x.razonSocial.Contains(texto)&&x.anulado==false).ToList();

            }
        }


        public Cliente buscarPorDNI(string dni)
        {

            using (var db = new BaseDeDatos("public"))
            {
                
                    return db.clientes.FirstOrDefault(x => x.dniRepresentante ==dni);
             

            }
        }

        public Cliente buscarPorRUC(string ruc)
        {

            using (var db = new BaseDeDatos("public"))
            {
             
                    return db.clientes.FirstOrDefault(x => x.ruc ==ruc);
                

            }
        }

        public bool crear(Cliente objeto)
        {
            try
            {
                using (var db = new BaseDeDatos(""))
                {
                    db.clientes.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
            }
            catch { return false; }
        }

        public bool editar(Cliente objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Cliente u = db.clientes.Find(id);
                        u.anulado =true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public Cliente existe(Cliente cliente)
        {
         
            using (var db = new BaseDeDatos("public"))
            {
                if (cliente.esEmpresa)
                {
                    return db.clientes.FirstOrDefault(x => x.ruc == cliente.ruc);
                }
                else
                {
                    return db.clientes.FirstOrDefault(x => x.dniRepresentante == cliente.dniRepresentante);
                }
               
            }
        }

        public List<Cliente> listarClientes()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.clientes.Where(x => x.anulado == false&&x.esEmpresa==false&&x.dniRepresentante!="00000000").ToList();
            }
        }

        public List<Cliente> listarEmpresas()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.clientes.Where(x => x.anulado == false &&x.esEmpresa==true).ToList();
            }
        }

        public List<Cliente> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.clientes.Where(x => x.anulado == false).ToList();
            }
        }
    }
}
