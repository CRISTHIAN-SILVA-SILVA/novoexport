﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE
{
   public interface CrudService<T>
    {
        bool crear(T objeto);
        bool editar(T objeto);
        bool eliminar(int id);
        T buscar(int id);
        List<T> listarNoAnulados();
    }
}
