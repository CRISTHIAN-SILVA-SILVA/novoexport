﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
    public class TipoNotaImpl
    {
        private static List<TipoNota> todas = null;
        private static string esquema = "public";
        public static List<TipoNota> getInstancia()
        {
            if (todas == null)
            {
                using (var db = new BaseDeDatos(esquema))
                {
                    todas = db.tiposNotaCredito.ToList();
                }
            }
            return todas;
        }
        public TipoNota buscar(int id)
        {
            throw new NotImplementedException();
        }

        public bool crear(TipoNota objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(TipoNota objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<TipoNota> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
