﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
    public class TipoPagoImpl : TipoPagoService
    {
        public TipoPago buscar(int id)
        {
            throw new NotImplementedException();
        }

        public bool crear(TipoPago objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(TipoPago objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<TipoPago> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.tiposPago.Where(x => x.habilitado == true).ToList();
            }
        }
    }
}
