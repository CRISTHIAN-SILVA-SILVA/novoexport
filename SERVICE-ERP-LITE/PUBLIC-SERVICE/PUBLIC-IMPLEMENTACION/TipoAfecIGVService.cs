﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
  public  class TipoAfecIGVService
    {
        public TipoAfectacionIGV buscar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.tiposAfectacioIGV.FirstOrDefault(x => x.id == id);
            }
        }

        public bool crear(MedioPago objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(MedioPago objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<MedioPago> listarNoAnulados()
        {

            using (var db = new BaseDeDatos(""))
            {
                return db.mediosPago.Where(x => x.habilitado == true).ToList();
            }
        }
    }
}
