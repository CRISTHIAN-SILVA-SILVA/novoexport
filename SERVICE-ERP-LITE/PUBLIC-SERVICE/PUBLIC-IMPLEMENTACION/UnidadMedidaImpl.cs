﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
  public  class UnidadMedidaImpl : UnidadMedidaService
    {
        private static List<UnidadMedida> todas = null;
        private static string esquema = "public";
        public UnidadMedida buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.unidades.FirstOrDefault(x => x.id == id);
            }
        }

        public static List<UnidadMedida> getInstancia()
        {
            if (todas == null)
            {
                using (var db = new BaseDeDatos(esquema))
                {
                    todas = db.unidades.ToList();
                }
            }
            return todas;
        }
    }
}
