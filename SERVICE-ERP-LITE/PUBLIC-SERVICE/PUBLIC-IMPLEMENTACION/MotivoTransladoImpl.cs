﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION
{
  public   class MotivoTransladoImpl
    {
        private static List<MotivoTranslado> todas = null;
        private static string esquema = "public";
        public static List<MotivoTranslado> getInstancia()
        {
            if (todas == null)
            {
                using (var db = new BaseDeDatos(esquema))
                {
                    todas = db.motivosTranslado.ToList();
                }
            }
            return todas;
        }
    }
}
