﻿using MODEL_ERP_LITE.PUBLIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PUBLIC_SERVICE
{
    public interface UnidadMedidaService
    {
        UnidadMedida buscar(int id);
        //   UnidadMedida buscarPorCodigo(string id);
        // static   List<UnidadMedida> listarNoAnulados();
    }
}
