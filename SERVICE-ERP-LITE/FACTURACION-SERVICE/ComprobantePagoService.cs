﻿using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE
{
 public    interface ComprobantePagoService:CrudService<ComprobantePago>
    {
        string guardarComprobanteCompleto(ComprobantePago comprobante,Venta venta,int idCaja,int idAlmacen, bool esPagoUnico,PagoTarjeta pt,string guia);
        List<ComprobantePago> listarPorTipoComprobante(int tipoComprobante);
        List<ComprobantePago> listarPorDeudaCliente(int cliente);
        List<ComprobantePago> listarPorFechas(DateTime inicio,DateTime fin);
        List<ComprobantePago> listarPorMontos(double inicio,double fin);
        List<ComprobantePago> listarPorNumero(int numero);
        ComprobantePago listarPorSerieNumero(string serie, int numero);
        bool pagar(int comprobanteVenta,int idmedioPago,PagoTarjeta pt,DateTime fechaPago,double monto);
    }
}
