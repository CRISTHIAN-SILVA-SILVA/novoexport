﻿using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE
{
   public interface TipoComprobantePagoService:CrudService<TipoComprobantePago>
    {
        TipoComprobantePago buscarPorNombre(string nombre);
    }
}
