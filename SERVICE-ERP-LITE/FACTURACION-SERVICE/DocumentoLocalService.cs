﻿using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE
{
   public interface DocumentoLocalService:CrudService<DocumentoLocal>
    {
        DocumentoLocal buscarPorNombre(string nombre);
        bool incrementarNumero(int id);
    }
}
