﻿using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE
{
   public interface GuiaRemisonService:CrudService<GuiaRemision>
    {
        GuiaRemision buscarPorNumero(int numero);
       List< GuiaRemision> buscarPorFecha(DateTime inicio,DateTime fin);
    }
}
