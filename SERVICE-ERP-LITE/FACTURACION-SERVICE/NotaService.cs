﻿using MODEL_ERP_LITE.FACTURACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE
{
 public   interface NotaService:CrudService<Nota>
    {
        List<Nota> listarPorTipoComprobante(int idTipo);
        List<Nota> listarPorComprobante(int idComprobante);
        bool guardarNotaCompleta(Nota nota,Venta venta, bool esAnulado,bool esDisminucion,bool esDescuento,bool esAumento,bool esDevolucion);
        List<Nota> listarPorFechas(DateTime inicio, DateTime fin);
        List<Nota> listarPorMontos(double inicio, double fin);
        List<Nota> listarPorNumero(int numero);
        Nota buscarPorserieNumero(string serie ,int numero);
    }
}
