﻿using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE
{
   public  interface DetalleNotaService
    {
        List<DetalleNota> listarPorNota(int nota);
    }
}
