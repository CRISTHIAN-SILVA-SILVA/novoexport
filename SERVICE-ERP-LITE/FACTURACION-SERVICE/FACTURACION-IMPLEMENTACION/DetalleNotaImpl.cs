﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class DetalleNotaImpl : DetalleNotaService
    {
        public List<DetalleNota> listarPorNota(int nota)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.detalleNotas.Include("presentacion").Include("tipoAfectacionIgv").Where(x => x.idNota ==nota&&x.anulado==false).ToList();
            }
        }
    }
}
