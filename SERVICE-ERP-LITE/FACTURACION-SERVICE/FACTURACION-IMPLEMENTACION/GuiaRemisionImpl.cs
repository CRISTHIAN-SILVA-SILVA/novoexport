﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PUBLIC;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class GuiaRemisionImpl : GuiaRemisonService
    {
        public GuiaRemision buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
              return  db.guiasRemision.Include("motivoTranslado").Include("transportista").FirstOrDefault(x=>x.id==id);
            }
        }

        public GuiaRemision buscarPorComprobante(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.guiasRemision.Include("motivoTranslado").Include("transportista").FirstOrDefault(x => x.idComprobante == id);
            }
        }
        public List<GuiaRemision> buscarPorFecha(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.guiasRemision.Include("motivoTranslado").Include("transportista").Where(x => x.anulado == false&&x.fechaCreate>=inicio&&x.fechaCreate<fin).ToList();
            }
        }

        public GuiaRemision buscarPorNumero(int numero)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.guiasRemision.Include("motivoTranslado").Include("transportista").FirstOrDefault(x => x.numero == numero);
            }
        }

        public bool crear(GuiaRemision objeto)
        {

            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        TipoComprobantePago tc = db.tiposComprobantePago.FirstOrDefault(x=>x.descripcion== "Guia de remision - Remitente");
                      //  objeto.idComprobante = tc.id;
                        objeto.idTipoComprobante = tc.id;
                        objeto.serie = tc.serie;
                      //  objeto.numero = tc.numeroActual;
                        db.guiasRemision.Add(objeto);
                       // tc.numeroActual += 1;
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool editar(GuiaRemision objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<GuiaRemision> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.guiasRemision.Include("motivoTranslado").Include("transportista").Where(x => x.anulado == false).Take(30).ToList();
            }
        }
    }
}
