﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class DocumentoLocalImpl : DocumentoLocalService
    {
        public bool incrementarNumero(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        DocumentoLocal d= db.documentosLocales.Find(id);
                        if (d != null)
                        {
                            d.numero = d.numero + 1;

                            db.SaveChanges();
                            dbTransaction.Commit(); return true;
                        }
                        else { return false; }
                    }
                    catch (Exception ex)
                    {//DbUpdateConcurrencyException e
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public DocumentoLocal buscar(int id)
        {
            throw new NotImplementedException();
        }

        public DocumentoLocal buscarPorNombre(string nombre)
        {
            
                using (var db = new BaseDeDatos("public"))
                {
                    return db.documentosLocales.FirstOrDefault(x => x.nombre == "PEDIDO");
                }
        }

        public bool crear(DocumentoLocal objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(DocumentoLocal objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<DocumentoLocal> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
