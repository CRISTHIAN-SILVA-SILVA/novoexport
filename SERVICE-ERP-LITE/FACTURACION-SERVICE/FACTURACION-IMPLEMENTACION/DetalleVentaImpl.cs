﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
  public  class DetalleVentaImpl
    {
        public List<DetalleVenta> listarPorVenta(int venta)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.detallesVenta.Include("productoPresentacion").Include("tipoAfectacionIgv").Where(x => x.idVenta == venta && x.anulado == false).ToList();
            }
        }
    }
}
