﻿   using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE;
using System.Windows.Forms;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using MODEL_ERP_LITE.RELACIONES;
using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;
using MODEL_ERP_LITE.CONFIGURACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class NotaImpl : NotaService
    {
        public Nota buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("detalles").Include("tipoNota").Include("tipoComprobante").Include("comprobante").FirstOrDefault(x => x.id == id );
            }
        }
        public Nota buscarSolo(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.FirstOrDefault(x => x.id == id);
            }
        }

        public bool crear(Nota objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(Nota objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public bool guardarNotaCompleta(Nota nota,Venta venta, bool esAnulado, bool esDisminucion, bool esDescuento, bool esAumento, bool esDevolucion)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        
                        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
                        int signoNota = -1;
                        CajaDiariaService cdservice = new CajaDiariaImpl();
                        TipoComprobantePago tipoComprobante;
                        ComprobantePago comprobante = db.comprobantesPago.Find(nota.idComprobanteVenta);
                        if (comprobante.idTipoComprobante==2)
                        {
                            tipoComprobante = db.tiposComprobantePago.Find(nota.idTipoComprobante);                           
                        }
                        else {
                            if(nota.idTipoComprobante==8)
                            tipoComprobante = db.tiposComprobantePago.FirstOrDefault(x=>x.descripcion=="NCB");
                            else tipoComprobante = db.tiposComprobantePago.FirstOrDefault(x => x.descripcion == "NDB");
                        }
                        nota.serie = tipoComprobante.serie;
                        nota.numero = tipoComprobante.numeroActual;
                        tipoComprobante.numeroActual += 1;

                   


                       // MovimientoCaja mcaja;
                        if (esAnulado) {
                            //ANULAR COMPROBANTE
                            //TipoNota tipoNota = db.tiposNotaCredito.FirstOrDefault(x => x.descripcion == "DEVOLUCION_POR_ITEM");
                            //List<Nota> notas = db.notas.Where(x => x.idTipoNota == tipoNota.id && x.anulado == false && x.idComprobanteVenta == comprobante.id).ToList();
                            Venta v = db.ventas.Find(venta.id);
                            v.anulado = true;
                            DetalleMovimientoProducto mpv;
                            nota.total = comprobante.montoTotal;
                            comprobante.anulado=true;
                            string serie = comprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(), 8);
                            // mcaja= db.movimientosCaja.FirstOrDefault(x=>x.esPorVenta&&x.serieNumeroComprobante==serie) ;
                            //   if (mcaja != null)
                            //   { mcaja.anulado = true; }
                            /*foreach (var n in notas) 
                                foreach (var d in n.detalles) {
                                    mp = db.detallesMovimientos.Find(d.idMovimiento);
                                    mp.anulado = true;
                                    db.SaveChanges();
                                }
                            */
                            DetalleMovimientoProducto mp;
                            AlmacenProducto ap;
                            DetalleVenta dv;
                            Producto p;
                            for (int i = 0; i < nota.detalles.Count; i++)
                            {
                                //ANULAR MOVIMIENTOS PRODUCTO
                                p = db.productos.Find(nota.detalles[i].idProducto);
                                mp = new DetalleMovimientoProducto
                                {
                                    cantidad = nota.detalles[i].cantidad * nota.detalles[i].factor,
                                    fecha = DateTime.Now,
                                    numero = nota.numero,
                                    serie = nota.serie,
                                    tbl10 = tipoComprobante.codigo_sunat
                                       ,
                                    tbl12 = "99",
                                    costoUnitario = p.costo,
                                    idProducto = p.id,
                                    esIngreso = true,
                                    nuevoCostoUnitario = p.costo
                                };
                                mp.idAnterior = p.idMovimiento + 1;
                                p.idMovimiento = mp.idAnterior;
                                mp.total = mp.costoUnitario * mp.cantidad;
                                p.stock += mp.cantidad;
                                mp.nuevoStock = p.stock;
                                db.detallesMovimientos.Add(mp);
                                db.SaveChanges();
                                nota.detalles[i].idMovimiento = mp.id;
                    
                                db.SaveChanges();
                            }


                         /*   foreach (var i in  venta.detalles) {
                                //ANULAR MOVIMIENTOS PRODUCTO
                                mp = db.detallesMovimientos.Find(i.idMovimiento);
                                mp.anulado = true;
                                db.SaveChanges();
                            }*/

                        }

                        if (esDescuento)
                        {
                            //ANULAR COMPROBANTE
                            DetalleVenta dv;
                            int idPresentacion = 0;
                            for (int i = 0; i < nota.detalles.Count; i++)
                            {
                                idPresentacion = nota.detalles[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion && x.idVenta == comprobante.idVenta);
                                dv.descuentoN+= nota.detalles[i].total;
                                db.SaveChanges();
                            }
                        }

                        if (esDisminucion)
                        {
                            DetalleVenta dv;
                            int idPresentacion = 0;
                            for (int i = 0; i < nota.detalles.Count; i++)
                            {
                                idPresentacion = nota.detalles[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion&& x.idVenta == comprobante.idVenta);
                                if (nota.detalles[i].igv == 0)
                                    dv.precioN -= nota.detalles[i].precio;
                                else
                                    dv.precioN -= nota.detalles[i].precio+ nota.detalles[i].precio*igv.valorDouble;
                                db.SaveChanges();
                            }
                        }

                        if (esAumento)
                        {
                            signoNota = 1;
                            int idPresentacion = 0;
                            DetalleVenta dv;
                            for (int i = 0; i < nota.detalles.Count; i++)
                            {
                                idPresentacion = nota.detalles[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion && x.idVenta == comprobante.idVenta);
                                if (nota.detalles[i].igv == 0)
                                    dv.precioN -= nota.detalles[i].precio;
                                else
                                    dv.precioN += nota.detalles[i].precio + nota.detalles[i].precio * igv.valorDouble;
                                db.SaveChanges();
                            }
                        }

                        if (esDevolucion) {

                            DetalleMovimientoProducto mp;
                            AlmacenProducto ap;
                            DetalleVenta dv;
                            Producto p;
                            Almacen principal = db.almacenes.FirstOrDefault(x=>x.esPrincipal);
                            for (int i= 0;i < nota.detalles.Count; i++)
                            {
                                //ANULAR MOVIMIENTOS PRODUCTO
                                p = db.productos.Find(nota.detalles[i].idProducto);
                                ap = db.almacenesProductos.FirstOrDefault(x=>x.idAlmacen==principal.id&&x.idProducto==p.id);
                                mp = new DetalleMovimientoProducto {
                                    cantidad = nota.detalles[i].cantidad * nota.detalles[i].factor,
                                    fecha = DateTime.Now, numero = nota.numero, serie = nota.serie, tbl10 = tipoComprobante.codigo_sunat
                                       , tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "DEVOLUCION RECIBIDA").codigo_sunat,
                                    costoUnitario = p.costo,
                                    idProducto = p.id, esIngreso = true,
                                    nuevoCostoUnitario=p.costo
                                };
                                mp.idAnterior = p.idMovimiento + 1;
                                p.idMovimiento = mp.idAnterior;
                                mp.total = mp.costoUnitario * mp.cantidad;
                                ap.cantidadLogica += mp.cantidad;
                                p.stock += mp.cantidad;
                                mp.nuevoStock = p.stock;
                                db.detallesMovimientos.Add(mp);
                                db.SaveChanges();
                                nota.detalles[i].idMovimiento = mp.id;
                                int idPresentacion = nota.detalles[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x=>x.idProductoPresentacion==idPresentacion&&x.idVenta==comprobante.idVenta) ;
                                dv.cantidadN = dv.cantidad - nota.detalles[i].cantidad;
                                if (dv.cantidadN == 0) dv.anulado = true;
                                db.SaveChanges();
                            }
                        }


                      


                        nota.fechaCreate = DateTime.Now;
                       
                        db.notas.Add(nota);


                        ComprobantePagoVenta registroVenta = new ComprobantePagoVenta
                        {
                            codTipoComprobante = tipoComprobante.codigo_sunat,
                            total = -nota.total * signoNota,
                            fechaEmision = nota.fechaCreate,
                            fechaReferencia = comprobante.fechaCreate.ToString("dd/MM/yyyy"),
                            fechaVencimiento = "",
                            idCliente = comprobante.idCliente,
                            numero = nota.numero,
                            numeroReferencia = comprobante.numero,
                            serie = nota.serie,
                            serieReferencia = comprobante.serie,
                            tipoCambio = 1, tipoR = comprobante.tipoComprobante.codigo_sunat, baseImponible = nota.total - nota.totalExonerado - nota.igv,
                        };
                        Cliente cliente = db.clientes.FirstOrDefault(x=>x.id==comprobante.idCliente);
                        if (comprobante.tipoComprobante.descripcion.Equals("Factura"))
                        { registroVenta.docCliente = cliente.ruc;registroVenta.codTipoDocCliente = TipoDocIdentidadImpl.getCodigoRUC(); }
                        else { registroVenta.docCliente = cliente.dniRepresentante; registroVenta.codTipoDocCliente = TipoDocIdentidadImpl.getCodigoDNI(); }
                        if (registroVenta.codTipoDocCliente.Equals(""))
                        {
                            dbTransaction.Rollback(); 
                            return false;
                        }
                        db.registroVentas.Add(registroVenta);

                        int idcaja;
                        MovimientoCaja mc = null;
                        CajaDiaria cajaDiaria = null;
                        if (comprobante.pagado)
                        {
                            idcaja = cdservice.getCaja();
                            TipoNota tipo = db.tiposNotaCredito.Find(nota.idTipoNota);
                            mc = new MovimientoCaja
                            {
                                idDiaCaja = idcaja,
                                idComprobante = comprobante.id,
                                serieNumeroComprobante = nota.serie + "-" + Util.NormalizarCampo(nota.numero.ToString(), 8),
                                monto = nota.total,
                                esPorNotaCredito = true, 
                                concepto = "NOTA DE CREDITO"+" - "+tipo.descripcion,
                                idMedioPago = 1
                            };
                            
                            cajaDiaria = db.cajasDiarias.FirstOrDefault(x => x.id == idcaja);
                            cajaDiaria.montoFinal = cajaDiaria.montoFinal - mc.monto;

                            /*     if (esAumento)
                                 {
                                     cajaDiaria.montoFinal = cajaDiaria.montoFinal - mc.monto;
                                 }
                                 else {
                                     cajaDiaria.montoFinal = cajaDiaria.montoFinal - mc.monto;
                                     comprobante.montoN += nota.total; mc.esPorNotaDebito = true; mc.esPorNotaCredito = false; mc.concepto = "NOTA DE DEBITO"; }
     */
                            db.movimientosCaja.Add(mc);
                        }

                        if (!esAumento)
                            comprobante.montoN -= nota.total;
                            else { comprobante.montoN += nota.total;}

                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Nota> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("tipoNota").OrderByDescending(x => x.id).Where(x => x.anulado == false).Take(30).ToList();
            }
        }
        public List<Nota> listarPorFechas(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("tipoNota").OrderByDescending(x => x.id).Where(x => x.anulado == false&&x.fechaCreate>=inicio&&x.fechaCreate<fin).ToList();
            }
        }
        public List<Nota> listarPorMontos(double inicio,double fin)
        {
            DateTime date = DateTime.Now.AddMonths(-1);
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("tipoNota").OrderByDescending(x => x.id).Where(x => x.anulado == false&& x.fechaCreate>date  && x.total>=inicio &&x.total<=fin  ).ToList();
            }
        }
        public List<Nota> listarPorNumero(int numero)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("tipoNota").OrderByDescending(x => x.id).Where(x => x.anulado == false&&x.numero==numero).ToList();
            }
        }
        public List<Nota> listarPorComprobante(int idComprobante)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("tipoNota").Include("cliente").OrderByDescending(x => x.id).Where(x => x.idComprobanteVenta == idComprobante&&x.anulado==false).Take(30).ToList();
            }
        }
        public List<Nota> listarPorTipoComprobante(int idTipo)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("tipoNota").OrderByDescending(x => x.id).Where(x => x.idTipoComprobante == idTipo && x.anulado == false).Take(30).ToList();
            }
        }

        public Nota buscarPorserieNumero(string serie, int numero)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notas.Include("cliente").Include("tipoNota").FirstOrDefault(x => x.serie== serie && x.numero ==numero);
            }
        }
    }
}
