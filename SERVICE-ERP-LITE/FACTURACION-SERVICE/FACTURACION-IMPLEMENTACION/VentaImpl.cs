﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE;
using System.Windows.Forms;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PEDIDO;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class VentaImpl : VentaService
    {
        public Venta buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.ventas.Include("detalles").FirstOrDefault(x=>x.id==id);
            }
        }

        public bool crear(Venta objeto)
        {
            using (var db = new BaseDeDatos("public"))  {
                using (var dbTransaction = db.Database.BeginTransaction()) {
                    try {
                        Pedido pedido;
                        if (objeto.idPedido != 0)  {
                            pedido = db.pedidos.Find(objeto.idPedido);
                            pedido.completado = true;  }
                        DetalleMovimientoProducto dmp;
                        MovimientoProducto mp = new MovimientoProducto {
                            esSalida = true,
                            total = 0,
                            usuarioCreate = objeto.usuarioCreate, };
                        db.movimientosProductos.Add(mp);
                        db.SaveChanges();
                        Producto p;
                        ProductoPresentacion pp;
                        int j = 0;
                        foreach (var i in objeto.detalles) {
                            p = db.productos.Find(i.idProducto);
                            p.stock = p.stock - i.cantidad;
                            dmp = new DetalleMovimientoProducto {
                                costoUnitario = p.costo,
                                idProducto = p.id,
                                nuevoStock = p.stock,
                                total = i.total    };
                            objeto.detalles[j].movimiento = dmp;
                            db.SaveChanges();
                            j++;    }
                        db.ventas.Add(objeto);
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool editar(Venta objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Venta> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
