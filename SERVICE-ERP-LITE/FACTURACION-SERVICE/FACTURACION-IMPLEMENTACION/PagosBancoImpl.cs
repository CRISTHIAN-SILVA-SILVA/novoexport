﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
  public  class PagosBancoImpl
    {
        public List<PagoTarjeta> listar() {
            using (var db = new BaseDeDatos("public"))
            {
                return db.pagosTarjeta.Include("movimiento").Include("cuentaBanco").Include("medioPago").OrderByDescending(x=>x.id).Where(x=>x.anulado==false).Take(30).ToList();
            }
        }
        public PagoTarjeta buscarPorVenta(string serieNumero)
        {
            using (var db = new BaseDeDatos("public"))
            {

                
                return db.pagosTarjeta.Include("movimiento").Include("cuentaBanco").Include("medioPago").FirstOrDefault(x=>x.esVenta==true&&x.comprobante==serieNumero&&x.anulado==false);
            }
        }
        public PagoTarjeta buscarPorMov(int idMov)
        {
            using (var db = new BaseDeDatos("public"))
            {


                return db.pagosTarjeta.Include("movimiento").Include("cuentaBanco").Include("medioPago").FirstOrDefault(x => x.idMovimiento==idMov&&x.anulado==false);
            }
        }
        public List<PagoTarjeta> listarPorFecha(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return db.pagosTarjeta.Include("movimiento").Include("cuentaBanco").Include("medioPago").OrderByDescending(x => x.id).Where(x=>x.fecha>=inicio&&x.fecha<fin&&x.anulado==false).ToList();

            }
        }
    }
}
