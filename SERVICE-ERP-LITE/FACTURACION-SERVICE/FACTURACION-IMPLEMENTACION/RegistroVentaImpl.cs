﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
  public  class RegistroVentaImpl
    {
        public List<ComprobantePagoVenta> listarPorFechas(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.registroVentas.Include("cliente").OrderBy(x=>x.fechaEmision).Where(x => x.anulado ==false && x.fechaEmision>= inicio&&x.fechaEmision<fin).ToList();
            }
        }
    }
}
