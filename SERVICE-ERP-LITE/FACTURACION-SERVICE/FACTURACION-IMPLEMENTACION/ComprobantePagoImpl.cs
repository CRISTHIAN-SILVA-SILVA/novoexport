﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE.INVENTARIO;
using System.Windows.Forms;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.UTILITARIO;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CAJA_SERVICE;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class ComprobantePagoImpl : ComprobantePagoService
    {
        public ComprobantePago buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").Include("tipoComprobante").Include("venta").FirstOrDefault(x => x.id == id);
            }
        }
        public bool cambiarAEnviado(ComprobantePago c)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ComprobantePago cp = db.comprobantesPago.Find(c.id);
                        if (cp.enviado)
                        {
                            cp.enviado = false;
                            

               
                        }
                        else
                        {
                            cp.enviado = true;

                        }
                        db.SaveChanges();


                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }
        public bool cambiarAPagado(ComprobantePago c)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ComprobantePago cp = db.comprobantesPago.Find(c.id);
                        if (cp.pagado)
                        {
                            cp.pagado = false;
                           
                            cp.montoPagado =0;
                            cp.idTipoPago = 2;
                           
                            //BORRA TODOS LOS MOVIMIENTOS
                            MovimientoCaja mc = db.movimientosCaja.FirstOrDefault(x=>x.serieNumeroComprobante==""&&x.esPorVenta);
                            PagoTarjeta pt;
                            if (mc != null)
                            {
                                mc.anulado = true;

                                pt = db.pagosTarjeta.FirstOrDefault(x => x.idMovimiento == mc.id);
                                if (pt != null)
                                    pt.anulado = true;
                            }
                            db.SaveChanges();
                        }
                        else {
                            cp.pagado = true;
                         //   cp.fechaPago = DateTime.Now;
                            cp.montoPagado = cp.montoTotal;
                            
                        }
                        db.SaveChanges();
                   

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }
        public ComprobantePago buscarSolo(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.FirstOrDefault(x => x.id == id);
            }
        }
        public ComprobantePago buscarPorSerieNumero(string serie, int numero)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").Include("tipoComprobante").Include("venta").FirstOrDefault(x => x.serie==serie&&x.numero==numero );
            }
        }

        public bool crear(ComprobantePago objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(ComprobantePago objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }
        public bool editarAsync(ComprobantePago objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ComprobantePago e = db.comprobantesPago.Find(objeto.id);
                        e.hash = objeto.hash;
                        e.enviado = objeto.enviado;
                        db.SaveChangesAsync();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public string guardarComprobanteCompleto(ComprobantePago comprobante, Venta venta, int idCaja, int idAlmacen, bool esPagoUnico,PagoTarjeta pt,string guia)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                   {
                        Pedido pedido;
                        DetalleMovimientoProducto dmp;

                        TipoComprobantePago tc = db.tiposComprobantePago.Find(comprobante.idTipoComprobante);
                        comprobante.tipoPago = null;
                        Producto p;
                        ProductoPresentacion pp;
                        int j = 0;
                        foreach (var i in venta.detalles)
                        {
                            p = db.productos.Find(i.idProducto);
                            AlmacenProducto ap = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == idAlmacen && x.idProducto == i.idProducto);
                            ap.cantidadLogica = ap.cantidadLogica - i.cantidad * i.factor;
                            /*    if (ap.cantidadLogica < 0)
                                {
                                    dbTransaction.Rollback();
                                    return "NO HAY STOCK SUFICIENTE DEL PRODUCTO EN EL ALMACEN PRINCIPAL: " + p.nombre;
                                }*/
                            p.stock = p.stock - i.cantidad * i.factor;
                          
                            dmp = new DetalleMovimientoProducto
                            {
                                costoUnitario = p.costo,
                                nuevoCostoUnitario = p.costo,
                                cantidad = (i.cantidad * i.factor),
                                idProducto = p.id,
                                nuevoStock = p.stock,
                                total = i.cantidad * i.factor * p.costo,
                                idAnterior=p.idMovimiento+1,
                                fecha = comprobante.fechaCreate,
                                numero = tc.numeroActual,
                                serie = tc.serie,
                                tbl10 = tc.codigo_sunat
                                       ,
                                tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "VENTA").codigo_sunat,
                            };
                            p.idMovimiento = dmp.idAnterior;
                          venta.detalles[j].movimiento = dmp;
                            db.SaveChanges();
                            j++;
                        }
                        if (venta.idPedido != 0)
                        {
                            pedido = db.pedidos.Find(venta.idPedido);
                            pedido.completado = true;
                        }
                        db.ventas.Add(venta);

                        db.SaveChanges();
                        comprobante.idVenta = venta.id;
                        comprobante.montoN = comprobante.montoTotal;
                        comprobante.tipoComprobante = null;
                        comprobante.numero = tc.numeroActual;
                      
                        tc.numeroActual = tc.numeroActual + 1;
                        db.comprobantesPago.Add(comprobante);
                        db.SaveChanges();

                     /*   if (!(String.IsNullOrEmpty(guia) || String.IsNullOrWhiteSpace(guia))) {
                            TipoComprobantePago tcg = db.tiposComprobantePago.FirstOrDefault(x => x.descripcion == "Guia de remision - Remitente");
                            //  objeto.idComprobante = tc.id;
                            GuiaRemision objeto = new GuiaRemision();
                            objeto.idTipoComprobante = tcg.id;
                            objeto.serie = tcg.serie;
                            objeto.numero =Convert.ToInt32( guia);
                            objeto.idComprobante = comprobante.id;
                            objeto.idMotivo =1;
                            objeto.idTransportista=2;
                            objeto.serieNumeroComprobante = comprobante.serie + Util.NormalizarCampo(comprobante.numero.ToString(),8);
                            //  objeto.numero = tc.numeroActual;
                            db.guiasRemision.Add(objeto);
                            // tc.numeroActual += 1;
                      
                        }*/
                        if (pt != null)
                        {
                            pt.idMedioPago = comprobante.idMedioPago;
                            pt.monto =comprobante.montoTotal+ (pt.porcentaje / 100) * comprobante.montoTotal;

                            pt.comprobante = comprobante.serie + Util.NormalizarCampo(comprobante.numero.ToString(), 8);
                            if (pt.monto > 0)
                            {
                              /*  MovimientoCaja mc1 = new MovimientoCaja
                                {
                                    idDiaCaja = idCaja,
                                    idComprobante = comproban4te.id,
                                    serieNumeroComprobante = pt.post,
                                    monto = pt.monto,
                                    esPorVenta = true,
                                    concepto = "EXTRA PAGO TARJETA (VENTA)",
                                    idMedioPago = comprobante.idMedioPago,
                                };
                                pt.idMovimiento = mc1.id;

                                db.pagosTarjeta.Add(pt);
                                CajaDiaria cajaDiaria1 = db.cajasDiarias.FirstOrDefault(x => x.id == idCaja);
                                cajaDiaria1.montoFinal = cajaDiaria1.montoFinal + mc1.monto;*/
                               // db.movimientosCaja.Add(mc1);
                            }
                           
                        }
                        db.SaveChanges();
                        if (esPagoUnico)
                        {

                            MovimientoCaja mc = new MovimientoCaja
                            {
                                idDiaCaja = idCaja,
                                idComprobante = comprobante.id,
                                serieNumeroComprobante = comprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(), 8),
                                monto = comprobante.montoTotal,
                                esPorVenta = true,
                                concepto = "VENTA", fecha=DateTime.Now,
                                idMedioPago=comprobante.idMedioPago
                            };
                            db.SaveChanges();
                            if (pt != null) {
                                pt.idMovimiento = mc.id;

                                db.pagosTarjeta.Add(pt);
                            }

                            CajaDiaria cajaDiaria = db.cajasDiarias.FirstOrDefault(x => x.id == idCaja);
                           // cajaDiaria.montoFinal = cajaDiaria.montoFinal + mc.monto;
                            db.movimientosCaja.Add(mc);
                            db.SaveChanges();
                        }

                        ComprobantePagoVenta registroVenta = new ComprobantePagoVenta
                        {
                            codTipoComprobante = tc.codigo_sunat,
                            total = comprobante.montoTotal,
                            fechaEmision = comprobante.fechaCreate,
                            fechaReferencia = "",
                            fechaVencimiento =comprobante.fechaVencimiento.ToString("dd/MM/yyyy") ,
                            idCliente = comprobante.idCliente,
                            numero = comprobante.numero,
                            serie = comprobante.serie,
                            tipoCambio = 1,
                            tipoR = "",
                            baseImponible = venta.opGravadas,
                            totalExonerado = venta.totalExonerado, serieReferencia = "",
                        };
                        Cliente cliente = db.clientes.FirstOrDefault(x => x.id == comprobante.idCliente);
                        if (comprobante.tipoComprobante.descripcion.Equals("Factura"))
                        { registroVenta.docCliente = cliente.ruc; registroVenta.codTipoDocCliente = TipoDocIdentidadImpl.getCodigoRUC(); }
                        else { registroVenta.docCliente = cliente.dniRepresentante; registroVenta.codTipoDocCliente = TipoDocIdentidadImpl.getCodigoDNI(); }
                        if (registroVenta.codTipoDocCliente.Equals(""))
                        {
                            dbTransaction.Rollback();
                            return "ERROR";
                        }
                        db.registroVentas.Add(registroVenta);
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return "CORRECTO";
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return "ERROR";
                    }
                }
            }
        }
        public List<ComprobantePago> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("venta").Include("cliente").Include("tipoPago").Include("medioPago").OrderByDescending(x => x.id).Where(x => x.anulado == false).Take(50).ToList();
            }
        }
        public List<ComprobantePago> listarNoPagadas()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoComprobante").Include("medioPago").Include("venta").OrderByDescending(x => x.idCliente).Where(x => x.anulado == false&&x.pagado==false).ToList();
            }
        }
        public List<ComprobantePago> listarPorTipoComprobante(int tipoComprobante)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").OrderByDescending(x => x.id).Where(x => x.idTipoComprobante == tipoComprobante && x.anulado == false).Take(50).ToList();
            }
        }

        public List<ComprobantePago> listarPorDeudaCliente(int cliente)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("tipoPago").Include("medioPago").Include("venta").OrderByDescending(x => x.id).Where(x => x.idCliente == cliente && x.pagado == false && x.anulado == false).ToList();
            }
        }
        public List<ComprobantePago> listarPorFechas(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("tipoComprobante").Include("venta").Include("medioPago").OrderByDescending(x => x.id).Where(x =>  x.anulado == false&&x.fechaCreate>=inicio&&x.fechaCreate<fin).ToList();
            }
        }
        public List<ComprobantePago> listarPorNumero(int numero)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").OrderByDescending(x => x.id).Where(x => x.anulado == false  && x.numero==numero).ToList();
            }
        }
        public ComprobantePago listarPorSerieNumero(string serie ,int numero)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").Include("tipoComprobante").FirstOrDefault(x =>x.serie==serie && x.numero == numero);
            }
        }
        public List<ComprobantePago> listarPorMontos(double inicio,double fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                DateTime date = DateTime.Now.AddMonths(-1);
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").OrderByDescending(x => x.id).Where(x =>x.fechaCreate>date&& x.anulado == false && x.montoTotal >=inicio && x.montoTotal <=fin).ToList();
            }
        }
        public List<ComprobantePago> listarPorCliente50(int idCliente)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.comprobantesPago.Include("cliente").Include("tipoPago").Include("medioPago").OrderByDescending(x => x.id).Where(x =>x.anulado == false && x.idCliente==idCliente).Take(50).ToList();
            }
        }
        public bool pagar(int comprobanteVenta,int idmedioPago,PagoTarjeta pt,DateTime fechaPago,double monto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        ComprobantePago cp = db.comprobantesPago.Find(comprobanteVenta);
              
                        cp.idMedioPago = idmedioPago;
                       if(monto >= (cp.montoN - cp.montoPagado)) cp.pagado = true;
                        cp.montoPagado += monto;
                        cp.fechaPago = fechaPago;
                        CajaDiariaService cdservice = new CajaDiariaImpl();

                        int idCaja = cdservice.getCaja() ;
                        MovimientoCaja mc = new MovimientoCaja
                        {
                            idDiaCaja = idCaja,
                            idComprobante = cp.id,
                            serieNumeroComprobante = cp.serie + "-" + Util.NormalizarCampo(cp.numero.ToString(), 8),
                            monto = monto,
                            esPorVenta = true,
                            concepto = "VENTA",
                            idMedioPago = idmedioPago, fecha = fechaPago,
                        };
                        db.movimientosCaja.Add(mc);
                        if (pt != null) {
                            pt.monto = (pt.porcentaje / 100) * monto;
                            pt.comprobante = cp.serie + Util.NormalizarCampo(cp.numero.ToString(), 8);
                            pt.idMedioPago = idmedioPago;
                            pt.idMovimiento = mc.id;
                            db.pagosTarjeta.Add(pt);
                        }
                        DateTime hoy = DateTime.Now.Date;
                        DateTime mañana = hoy.AddDays(1);
                        CajaDiaria cajaDiaria = db.cajasDiarias.FirstOrDefault(x => x.fecha ==hoy);
                        if (cajaDiaria == null)
                        {
                            cajaDiaria = new CajaDiaria { fecha = DateTime.Now, estaAbierta = false, montoInicial = 0, montoFinal = monto, };
                            db.cajasDiarias.Add(cajaDiaria);
                        }
                        else {
                            cajaDiaria.montoFinal += monto;
                        }
                        cajaDiaria.montoFinal = cajaDiaria.montoFinal +monto;
                   
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }
            }
        }
    }
}
