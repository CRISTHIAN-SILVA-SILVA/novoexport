﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION
{
    public class TipoComprobanteImpl : TipoComprobantePagoService
    {
        public TipoComprobantePago buscar(int id)
        {
            throw new NotImplementedException();
        }

        public TipoComprobantePago buscarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.tiposComprobantePago.FirstOrDefault(x => x.descripcion == nombre);
            }
        }

        public bool crear(TipoComprobantePago objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(TipoComprobantePago objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<TipoComprobantePago> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
                return db.tiposComprobantePago.ToList();

        }
    }
}
