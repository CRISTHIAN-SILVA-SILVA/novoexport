﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
    public class TransferenciaProductoImpl : TransferenciaProductoService
    {
        string esquema = "inventario";
        public TransferenciaProducto buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.transferenciasProductos.Include("almacenesTransferencia").FirstOrDefault(x=>x.id==id);
            }
        }

        public bool crear(TransferenciaProducto objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.transferenciasProductos.Add(objeto);
                 
                        int idOrigen = 0, idDestino = 0;
                        foreach (var j in objeto.almacenesTransferencia)
                        {
                            if (j.esOrigen) idOrigen = j.idAlmacen;
                            if (j.esDestino) idDestino = j.idAlmacen;
                        }
                        AlmacenProducto apOrigen, apDestino;
                        foreach (var i in objeto.detalles)
                        {
                            apOrigen = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == idOrigen && x.idProducto == i.idProducto);
                            apDestino = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == idDestino && x.idProducto == i.idProducto);
                            apDestino.cantidadLogica += i.cantidad * i.factor;
                            apOrigen.cantidadLogica -= i.cantidad * i.factor;
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }
            }
        }

        public bool editar(TransferenciaProducto objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            return true;
        }

        public bool eliminarCompleto(int id, string usuarioElimina)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        TransferenciaProducto tp = db.transferenciasProductos.Include("detalles").Include("almacenesTransferencia").FirstOrDefault(x=>x.id==id);
                        tp.anulado = true;
                        DetalleTransferenciaProducto d;

                        int idOrigen = 0, idDestino = 0;
                        foreach (var j in tp.almacenesTransferencia)
                        {
                            if (j.esOrigen) idOrigen = j.idAlmacen;
                            if (j.esDestino) idDestino = j.idAlmacen;
                        }
                        AlmacenProducto apOrigen, apDestino;
                        foreach (var i in tp.detalles) {
                            apOrigen = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == idOrigen && x.idProducto == i.idProducto);
                            apDestino = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == idDestino && x.idProducto == i.idProducto);
                            apDestino.cantidadLogica += i.cantidad * i.factor;
                            apOrigen.cantidadLogica -= i.cantidad * i.factor;
                            db.SaveChanges();
                        }
                        tp.fechaUpdate = DateTime.Now;
                        tp.usuarioUpdate =usuarioElimina;

                        db.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }
            }
        }

        public List<TransferenciaProducto> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {

                return db.transferenciasProductos.OrderByDescending(x => x.id).Where(x => x.anulado==false).Take(20).ToList();
            }
        }
    }
}
