﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE;
using System.Windows.Forms;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
    public class MermaImpl : MermaService
    {
        public Merma buscar(int id)
        {
            throw new NotImplementedException();
        }

        public bool crear(Merma objeto)
        {
            using (var db = new BaseDeDatos(""))
            {

                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        DetalleMovimientoProducto mp;
                        AlmacenProducto ap;
                        Producto p;
                        Almacen almacen;
                        double total = 0;
                        for (int i = 0; i < objeto.detalles.Count;i++) {
                            int idAlmacen = objeto.detalles[i].idAlmacen;
                            int idP = objeto.detalles[i].idProducto;
                            almacen = db.almacenes.FirstOrDefault(x => x.id==idAlmacen );
                            p = db.productos.Find(idP);
                            ap = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == almacen.id && x.idProducto == idP);
                            mp = new DetalleMovimientoProducto
                            {
                                cantidad = objeto.detalles[i].cantidad* objeto.detalles[i].factor,
                                fecha = DateTime.Now,
                                numero = 0,
                                serie = "ME01",
                                tbl10 = "00",
                              costoUnitario = p.costo,
                                idProducto = p.id,
                            };

                            mp.tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "DESTRUCCION").codigo_sunat;
                           
                            mp.total = mp.costoUnitario * mp.cantidad;
                            total += mp.total;
                            ap.cantidadLogica -= mp.cantidad;
                            if (ap.cantidadLogica < 0)
                            {
                                dbTransaction.Rollback();
                                return false;
                            }
                            p.stock -= mp.cantidad;
                            mp.nuevoStock = p.stock;
                            objeto.detalles[i].detalleMovimiento = mp;
                        }
                        objeto.total = total;
                        db.mermas.Add(objeto);
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool editar(Merma objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Merma> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.mermas.OrderByDescending(x=>x.id).Where(x => x.anulado == false).Take(20).ToList();
            }
       }
        }
}
