﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
  public  class CategoriaImpl : CategoriaService
    {
        string esquema = "inventario";
        public Categoria buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.categorias.FirstOrDefault(x => x.id == id);
            }
        }

        public Categoria buscarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.categorias.FirstOrDefault(x => x.nombre == nombre);
            }
        }

        public bool crear(Categoria objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {

                db.categorias.Add(objeto);
                db.SaveChanges();
                return true;
            }
        }

        public bool editar(Categoria objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Categoria u = db.categorias.Find(id);
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Categoria> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.categorias.OrderBy(x => x.id).ToList();
            }
        }
    }
}
