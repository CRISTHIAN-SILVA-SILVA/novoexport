﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
   public class AlmacenImpl : AlmacenService
    {

        string esquema = "inventario";
        public Almacen buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.almacenes.FirstOrDefault(x => x.id == id);
            }
        }

        public Almacen buscarPrincipal()
        {
            try
            {
                using (var db = new BaseDeDatos(esquema))
                {
                    return db.almacenes.Where(x => x.esPrincipal == true).First();
                }
            }
            catch {
                return null;
            }
        }

        public bool crear(Almacen objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {//  conprobar si es principal y desabilitar el otro principal
                        db.almacenes.Add(objeto);
                        if (objeto.esPrincipal)
                        {
                            Almacen principal = db.almacenes.FirstOrDefault(x => x.esPrincipal == true);
                            if (principal != null)
                            {
                                principal.esPrincipal = false;
                                db.SaveChanges();
                            }
                        }
                        db.SaveChanges();
                        // crear referencia con productos
                        foreach (var i in db.productos.Where(x => x.anulado == false))
                        {
                            AlmacenProducto almacenProducto = new AlmacenProducto();
                            almacenProducto.idAlmacen = objeto.id;
                            almacenProducto.idProducto = i.id;
                            db.almacenesProductos.Add(almacenProducto);
                        }                    
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                    

                }
            }
        }

        public bool editar(Almacen objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                        if (objeto.esPrincipal)
                        {
                            Almacen a = db.almacenes.FirstOrDefault(x => x.esPrincipal == true);
                            if(a!=null)
                            a.esPrincipal = false;
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Almacen u = db.almacenes.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Almacen> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.almacenes.Where(x => x.anulado == false).OrderBy(x => x.id).ToList();
            }
        }
    }
}
