﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
  public  class DetalleMovimientoProductoImpl
    {
        public List<DetalleMovimientoProducto> listarPorProducto(int idProducto)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.detallesMovimientos.Where(x => x.anulado == false&&x.idProducto==idProducto).OrderByDescending(x => x.fecha).ToList();
            }
        }
        public DetalleMovimientoProducto buscarPorIndex(int index,int idProducto)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.detallesMovimientos.FirstOrDefault(x => x.anulado == false && x.idProducto == idProducto&&x.idAnterior==index);
            }
        }
        public List<DetalleMovimientoProducto> listarKardexProducto(int idProducto, DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.detallesMovimientos.OrderBy(x => x.idAnterior).Where(x => x.anulado == false && x.idProducto == idProducto&&x.fecha>=inicio&&x.fecha<fin).ToList();
            }
        }
        public bool multiplicarTipoCambio()
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        List<Compra> compras = db.compras.Include("detallesCompra").Where(x => x.anulado == false&&x.idMoneda==181).ToList();
                        DetalleMovimientoProducto dm;
                        foreach (var c in compras) { 
                            foreach (var d in c.detallesCompra) {
                                dm = db.detallesMovimientos.FirstOrDefault(x=>x.id==d.idMovimiento);
                                dm.costoUnitario = d.costo * c.cambio;
                                dm.total = dm.costoUnitario * dm.cantidad;
                                db.SaveChanges(); 
                            }
                        }

                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }

            }
        }
        public bool normalizarFechaCompras()
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        List<Compra> compras = db.compras.Where(x => x.anulado == false).ToList();
                        List < DetalleMovimientoProducto> dm;
                        foreach (var c in compras)
                        {
                            dm = db.detallesMovimientos.Where(x=>x.serie==c.serie&&x.numero==c.numero&&x.esIngreso==true).ToList();
                            for (int d=0;d<dm.Count;d++)
                            {
                                dm[d].fecha = c.fechaEmision;
                                db.SaveChanges();
                            }
                         
                        }

                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }

            }
        }
        public bool ordenarMovsProducto(int idProducto,double stockI,double costoI)
        {
            using (var db = new BaseDeDatos(""))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        double costoInicial=0, stockInicial=0;
                        Producto producto = db.productos.Find(idProducto);
                        producto.costoInicial = costoI;producto.stockInicial = stockI;
                        if (stockI != 0) { costoInicial = costoI; stockInicial = stockI; }
                        List<DetalleMovimientoProducto> d = db.detallesMovimientos.OrderBy(x => x.fecha).Where(x => x.anulado == false && x.idProducto == producto.id).ToList();
                        List<DetalleMovimientoProducto> noagregados = new List<DetalleMovimientoProducto>();
                        List<DetalleMovimientoProducto> lista = new List<DetalleMovimientoProducto>();

                        DateTime dia = DateTime.Now;

                        if(d.Count>0)dia=d[0].fecha;

                        bool mismoDia=true;
                        foreach (var item in d) {
                            if (item.fecha != dia) mismoDia = false; else mismoDia = true;

                            if (!mismoDia) {
                                lista.AddRange(noagregados);
                                noagregados.Clear();
                            }
                      
                            dia = item.fecha;
                            if (!item.esIngreso)
                            {
                                noagregados.Add(item);
                            }
                            else {
                                lista.Add(item);
                            }
                        }
                        lista.AddRange(noagregados);
                        DetalleMovimientoProducto dm=null;

                        for (int i=0;i<lista.Count;i++) {
                            dm = db.detallesMovimientos.Find(lista[i].id);
                            dm.idAnterior = i+1;
                            if (dm.esIngreso)            {
                                    if (dm.costoUnitario == 0 || dm.tbl10 == "07")  {           //ES GRATUITO O SOBRANTE O DEVOLUCION VENTA
                                        dm.nuevoStock = stockInicial + dm.cantidad;
                            
                                        dm.nuevoCostoUnitario = costoInicial;
                                        dm.costoUnitario = costoInicial;
                                        stockInicial = dm.nuevoStock; dm.total = dm.costoUnitario * dm.cantidad;
                                }
                                    else   {
                                        dm.nuevoStock = stockInicial + dm.cantidad;
                                        dm.total = dm.costoUnitario * dm.cantidad;
                                        dm.nuevoCostoUnitario = ((costoInicial * stockInicial) + (dm.total)) / dm.nuevoStock;
                                        costoInicial = dm.nuevoCostoUnitario;
                                        stockInicial = dm.nuevoStock;
                                    } }
                             else {
                                    if (dm.tbl10 == "07")  {                                  //ES DEVOLUCION COMPRA
                                        dm.nuevoStock = stockInicial - dm.cantidad;
                                        dm.total = dm.costoUnitario * dm.cantidad;
                                        dm.nuevoCostoUnitario = ((costoInicial * stockInicial) - (dm.total)) / dm.nuevoStock;
                                        costoInicial = dm.nuevoCostoUnitario;
                                        stockInicial = dm.nuevoStock;  }
                                    else {                                                      //ES VENTA
                                        dm.nuevoCostoUnitario = costoInicial;
                                    dm.costoUnitario = costoInicial;
                                        dm.nuevoStock = stockInicial - dm.cantidad;
                                        dm.total = dm.costoUnitario * dm.cantidad;
                                        stockInicial = dm.nuevoStock;  }  }
                            if (stockInicial == 0) {
                                dm.nuevoCostoUnitario= 0;
                                costoInicial = 0; }
                            db.SaveChanges();
                        }                     
                        producto.costo = costoInicial;
                        producto.stock =stockInicial;
                        producto.idMovimiento = d.Count;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }

            }
        }
        public bool ordenarKardex() {
            using (var db = new BaseDeDatos("")){
                using (var dbTransaction = db.Database.BeginTransaction()) {
                    try  {
                        double costoInicial = 0;
                    double stockInicial = 0;
                        foreach (var producto in db.productos.Where(x => x.anulado == false).ToList())  {
                            costoInicial = producto.costoInicial;
                            stockInicial = producto.stockInicial;
                            int index = 0;
                            List<DetalleMovimientoProducto> d = db.detallesMovimientos.OrderBy(x => x.fecha).Where(x => x.anulado == false && x.idProducto == producto.id).ToList();
                            for (int i = 0; i < d.Count; i++)
                            {
                                index++;
                                d[i].idAnterior = index;
                                if (d[i].esIngreso) 
                                {
                                    if (d[i].costoUnitario == 0 || d[i].tbl10 == "07") {
                                        d[i].nuevoStock = stockInicial + d[i].cantidad;
                                        d[i].total = d[i].costoUnitario * d[i].cantidad;
                                        d[i].nuevoCostoUnitario = costoInicial;
                                        stockInicial = d[i].nuevoStock;
                                    }
                                    else //if (d[i].tbl10 == "01" || d[i].tbl10 == "07")
                                    {
                                        d[i].nuevoStock = stockInicial + d[i].cantidad;
                                        d[i].total = d[i].costoUnitario * d[i].cantidad;
                                        d[i].nuevoCostoUnitario = ((costoInicial * stockInicial) + (d[i].total)) / d[i].nuevoStock;
                                        costoInicial = d[i].nuevoCostoUnitario;
                                        stockInicial = d[i].nuevoStock;
                                    }
                                }
                                else
                                {
                                    if (d[i].tbl10 != "07")
                                    {
                                        d[i].nuevoStock = stockInicial - d[i].cantidad;
                                        d[i].total = d[i].costoUnitario *d [i].cantidad;
                                        d[i].nuevoCostoUnitario = ((costoInicial * stockInicial) - (d[i].total)) / d[i].nuevoStock;
                                        costoInicial = d[i].nuevoCostoUnitario;
                                        stockInicial = d[i].nuevoStock; 
                                    }
                                    else
                                    {
                                        d[i].nuevoCostoUnitario = costoInicial;
                                        d[i].nuevoStock = stockInicial - d[i].cantidad;
                                        d[i].total = d[i].costoUnitario * d[i].cantidad;
                                        stockInicial = d[i].nuevoStock;
                                    }
                                }
                                if (i + 1 == d.Count) {
                                    producto.costo = d[i].nuevoCostoUnitario;
                                    producto.stock = d[i].nuevoStock;
                                    producto.idMovimiento = d[i].idAnterior;
                                }
                                db.SaveChanges();
                            }

                        }
                       
                            db.SaveChanges();
                            dbTransaction.Commit(); return true;
                        }
                        catch (Exception ex)
                        {
                            dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                            return false;
                        }

                    }
                
            }
        }

        public  List<DetalleMovimientoProducto> listar()
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.detallesMovimientos.Where(x => x.anulado == false).ToList();
            }
        }
        public DetalleMovimientoProducto listarKardexProducto(int idProducto, DateTime fecha)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.detallesMovimientos.OrderByDescending(x => x.idAnterior).FirstOrDefault(x => x.anulado == false && x.idProducto == idProducto && x.fecha <=fecha);
                //if (ds.Count == 1)
               /* {
                    return ds[0];
                }
                else return null;*/
            }
        }
    }
}
