﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.INVENTARIO.JOIN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
   public class ProductoPresentacionImpl : ProductoPresentacionService
    {
        string esquema = "inventario";
        public ProductoPresentacion buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.Include("unidad").Include("producto").FirstOrDefault(x => x.id == id);
            }
        }
        
        public ProductoPresentacion buscarPorUnidad(int unidad)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.FirstOrDefault(x => x.idUnidadMedida == unidad);
            }
        }
        public List<ProductoPresentacion> buscarPorProdcuto(int producto) {

            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.Include("unidad").Include("producto").Where(x => x.idProducto== producto&&x.anulado==false).ToList();
            }
        }
        public List<DetalleProducto> buscarPresentacionesPorAlmacen(int idAlmacen, string cadena)
        {
            using (var db = new BaseDeDatos("public"))
            {
                    List<DetalleProducto> lista = new List<DetalleProducto>();
                     lista = (from pp in db.presentanciones
                                 join p in db.productos on pp.idProducto equals p.id
                                 join ap in db.almacenesProductos on p.id equals ap.idProducto

                                 where ap.idAlmacen == idAlmacen && pp.anulado == false && pp.nombre.Contains(cadena)
                              //   group pp by new {pp.id, pp.idProducto, pp.nombre, pp.precio} into grupo
                                 select  new DetalleProducto
                                 {
                                    IDPRESENTACION = pp.id,
                                    IDPRODUCTO =pp.idProducto,
                                    PRESENTACION = pp.nombre,
                                    PRECIO = pp.precio,
                                    CANTIDAD_ALMACEN = ap.cantidadLogica/pp.factor,
                                    FACTOR=pp.factor,
                                    ES_EXONERADO=p.esExonerado
                                 }

                          ).ToList();
                    return lista;
                    
            }
        }
        
        public bool crear(ProductoPresentacion objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {

                db.presentanciones.Add(objeto);
                db.SaveChanges();
                return true;
            }
        }

        public bool editar(ProductoPresentacion objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {

                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ProductoPresentacion pp = db.presentanciones.Find(objeto.id);
                        pp.precio = objeto.precio;
                        pp.usuarioUpdate = objeto.usuarioUpdate;
                        pp.fechaUpdate = objeto.fechaUpdate;
                        Producto p = null;
                        if (objeto.factor == 1)
                        {
                            p = db.productos.Find(objeto.idProducto);
                            p.usuarioUpdate = objeto.usuarioUpdate;
                            p.fechaUpdate = DateTime.Now;
                            p.precio = objeto.precio;
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                        
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;



                    }
                }
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ProductoPresentacion u = db.presentanciones.Find(id);
                        if (u.factor == 1) return false;
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<ProductoPresentacion> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.Where(x => x.anulado == false).OrderBy(x => x.id).ToList();
            }
        }

        public List<ProductoPresentacion> listarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.Where(x => x.nombre.Contains(nombre)).ToList();
            }
        }

        public List<ProductoPresentacion> listarPorProductoNoAnuladas(int producto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.Include("unidad").Where(x => x.anulado == false && x.idProducto == producto).OrderBy(x => x.id).ToList();
            }
        }

        ProductoPresentacion ProductoPresentacionService.buscarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.presentanciones.FirstOrDefault(x => x.nombre==nombre);
            }
        }
    }
}
