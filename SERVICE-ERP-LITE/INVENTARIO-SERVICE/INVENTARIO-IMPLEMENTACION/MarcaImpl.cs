﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
   public class MarcaImpl : MarcaService
    {
        string esquema = "inventario";
        public Marca buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.marcas.FirstOrDefault(x => x.id == id);
            }
        }

        public bool crear(Marca objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {

                db.marcas.Add(objeto);
                db.SaveChanges();
                return true;
            }
        }

        public bool editar(Marca objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Marca u = db.marcas.Find(id);
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Marca> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.marcas.OrderBy(x => x.id).ToList();
            }
        }
        public Marca buscarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.marcas.FirstOrDefault(x => x.nombre == nombre);
            }
        }
    }
}
