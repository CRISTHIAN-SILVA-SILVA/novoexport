﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
    public class DetalleTransferenciaProductoImpl : DetalleTransferenciaProductoService
    {
        public List<DetalleTransferenciaProducto> listarPorTranferencia(int idTransferencia)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.detallesTransferencia.Include("presentacion").Where(x => x.idTransferencia==idTransferencia).ToList();
            }
        }
    }
}
