﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.INVENTARIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION
{
   public class ProductoImpl : ProductoService
    {
        string esquema = "inventario";
        public Producto buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.productos.Include("unidad").Include("categoria").Include("marca").FirstOrDefault(x => x.id == id);
            }
        }

        public Producto buscarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.productos.Include("unidad").Include("categoria").Include("marca").FirstOrDefault(x => x.nombre == nombre);
            }
        }
        public List<Producto> listarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.productos.Include("unidad").Include("categoria").Include("marca").Where(x => x.nombre.Contains(nombre)&& x.anulado==false).ToList();
            }
        }
        public bool crear(Producto objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.productos.Add(objeto);
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool editar(Producto objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }
        public bool restaurar(Producto existe)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Producto u = db.productos.Find(existe.id);
                        ProductoPresentacion presentacion = db.presentanciones.Where(x => x.factor == 1 && x.idProducto == u.id).FirstOrDefault();
                        presentacion.anulado = false;
                        presentacion.fechaUpdate = DateTime.Now;
                        presentacion.precio = existe.precio;
                        u.stock = 0;
                        u.precio = existe.precio;
                        u.costo = existe.costo;
                        u.fechaUpdate = DateTime.Now;
                        u.anulado = false;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }
        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Producto u = db.productos.Find(id);

                        for (int i = 0; i < u.presentaciones.Count; i++)
                        {
                            u.presentaciones[i].anulado = true;
                        }
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Producto> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.productos.Include("categoria").Include("unidad").OrderBy(x => x.id).Where(x => x.anulado == false).ToList();
            }
        }

        public List<Producto> listarPorCadena(string cadena)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.productos.Include("categoria").Include("unidad").OrderBy(x => x.id).Where(x => x.anulado == false&&x.nombre.Contains(cadena)).ToList();
            }
        }

    }
}
