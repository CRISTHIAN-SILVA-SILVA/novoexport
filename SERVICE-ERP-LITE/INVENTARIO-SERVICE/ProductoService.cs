﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE
{
    public interface ProductoService : CrudService<Producto>
    {
        Producto buscarPorNombre(string nombre);
        bool restaurar(Producto existe);
        List<Producto> listarPorNombre(string nombre);
        List<Producto> listarPorCadena(string cadena);
    }
}
