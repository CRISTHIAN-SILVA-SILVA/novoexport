﻿using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.INVENTARIO.JOIN;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE
{
    public interface ProductoPresentacionService : CrudService<ProductoPresentacion>
    {
        List<ProductoPresentacion> listarPorProductoNoAnuladas(int producto);
        List<ProductoPresentacion> listarPorNombre(string nombre);
        ProductoPresentacion buscarPorNombre(string nombre);
        ProductoPresentacion buscarPorUnidad(int unidad);
        List<ProductoPresentacion> buscarPorProdcuto(int unidad);
        List<DetalleProducto> buscarPresentacionesPorAlmacen(int idAlmacen,string cadena); 
    }
}
