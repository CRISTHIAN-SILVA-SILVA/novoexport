﻿using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.INVENTARIO_SERVICE
{
    public interface AlmacenService : CrudService<Almacen>
    {
        Almacen buscarPrincipal();
    }
}
