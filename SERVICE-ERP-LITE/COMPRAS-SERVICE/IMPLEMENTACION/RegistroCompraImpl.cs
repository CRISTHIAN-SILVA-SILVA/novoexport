﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.COMPRAS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION
{
  public  class RegistroCompraImpl
    {
        public RegistroCompra buscarPorCompra(int idCompra)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.registroCompras.FirstOrDefault(x =>x.idComprobante==idCompra&&x.esGasto==false);
            }
        }
        public List<RegistroCompra> listarPorFechas(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.registroCompras.Include("proveedor").Include("moneda").Include("medioPago").OrderBy(x=>x.fechaEmision).Where(x =>
                x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin&&x.esGasto==false).ToList();
            }
        }

        public List<RegistroCompra> listarPorFechasGastos(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.registroCompras.Include("proveedor").Include("moneda").Include("medioPago").OrderBy(x => x.fechaEmision).Where(x =>
                  x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin && x.esGasto == true).ToList();
            }
        }

    }
}
