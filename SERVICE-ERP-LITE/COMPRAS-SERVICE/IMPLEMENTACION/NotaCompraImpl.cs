﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION
{
    public class NotaCompraImpl : NotaCompraService
    {
        public NotaCompra buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.notasCompra.Include("compra").Include("detalle").Include("tipoComprobante").Include("tipoNota").FirstOrDefault(x => x.anulado == false && x.id == id);
            }
        }

        public bool crear(NotaCompra objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(NotaCompra objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public bool guardarNotaCompleta(NotaCompra nota, bool esAnulado, bool esDisminucion, bool esDescuento, bool esAumento, bool esDevolucion,bool esDevTital,bool DescGlobal)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
                      
                        int signoNota = -1;
                        CajaDiariaService cdservice = new CajaDiariaImpl();            
                        Compra comprobante = db.compras.Include("detallesCompra").Include("proveedor").FirstOrDefault(x=>x.id==nota.idCompra);
                        TipoComprobantePago tipoComprobante = db.tiposComprobantePago.Find(nota.idTipoComprobante);
                        if (esAnulado)
                        {
                            comprobante.anulado = true;
                            DetalleMovimientoProducto mp;
                            nota.total = comprobante.montoTotal;
                            comprobante.anulado = true;
                            string sn = comprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(), 8);                     
                            AlmacenProducto ap;
                            Producto p;
                            Almacen principal = db.almacenes.FirstOrDefault(x => x.esPrincipal);
                            foreach (var d in comprobante.detallesCompra)
                            {
                                p = db.productos.Find(d.idProducto);
                                ap = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == principal.id && x.idProducto == p.id);
                                mp = new DetalleMovimientoProducto
                                {
                                    cantidad = d.cantidad * d.factor,
                                    fecha = DateTime.Now,
                                    numero = nota.numero,
                                    serie = nota.serie,
                                    tbl10 = tipoComprobante.codigo_sunat,
                                    tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "DEVOLUCION ENTREGADA").codigo_sunat,
                                    costoUnitario = p.costo/(1+igv.valorDouble),
                                    idProducto = p.id,
                                    nuevoCostoUnitario = p.costo / (1 + igv.valorDouble), 
                                };
                                mp.total = mp.costoUnitario * mp.cantidad;
                                ap.cantidadLogica -= mp.cantidad;
                                p.stock -= mp.cantidad;
                                mp.nuevoStock = p.stock;
                                db.detallesMovimientos.Add(mp);
                                db.SaveChanges();
                            }
                        }
                        if (esDevTital) {

                       
                                   DetalleMovimientoProducto mp;
                                   nota.total = comprobante.montoTotal;
                                   comprobante.anulado = true;
                                   string sn = comprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(), 8);                     
                                   AlmacenProducto ap;
                                   Producto p;
                                   Almacen principal = db.almacenes.FirstOrDefault(x => x.esPrincipal);
                                   foreach (var d in comprobante.detallesCompra)
                                   {
                                       p = db.productos.Find(d.idProducto);
                                       ap = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == principal.id && x.idProducto == p.id);
                                       mp = new DetalleMovimientoProducto
                                       {
                                           cantidad = d.cantidad * d.factor,
                                           fecha = DateTime.Now,
                                           numero = nota.numero,
                                           serie = nota.serie,
                                           tbl10 = tipoComprobante.codigo_sunat,
                                           tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "DEVOLUCION ENTREGADA").codigo_sunat,
                                           costoUnitario = p.costo/(1+igv.valorDouble),
                                           idProducto = p.id,
                                           nuevoCostoUnitario = p.costo / (1 + igv.valorDouble),
                                       };
                                       mp.total = mp.costoUnitario * mp.cantidad;
                                       ap.cantidadLogica -= mp.cantidad;
                                       p.stock -= mp.cantidad;
                                       mp.nuevoStock = p.stock;
                                       db.detallesMovimientos.Add(mp);
                                       db.SaveChanges();
                                   }
                        }
                        if (DescGlobal) {
                            int presentacion=     nota.detalle[0].idPresentacion;
                           int producto = nota.detalle[0].idProducto;
                            List<DetalleNotaCompra> ds = new List<DetalleNotaCompra>();
                            ds.Add(new DetalleNotaCompra
                            {
                                cantidad = 1,
                                factor = 0,
                                valorUnitario = nota.total,
                                total = nota.total,
                                idPresentacion =presentacion,
                                idProducto = producto,
                            });

                            comprobante.descuentoGlobal =nota.total;
                            comprobante.montoN -= nota.total;
                        } 
                        if (esDescuento)
                        {
                            comprobante.montoN -= nota.total;

                            // DetallCompra dv;
                         //   int idPresentacion = 0;
                        /*    for (int i = 0; i < nota.detalle.Count; i++)
                             {
                                idPresentacion = nota.detalle[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion && x.idVenta == comprobante.idVenta);
                                dv.descuentoN += nota.detalles[i].total;
                                db.SaveChanges();*/
                          //  }
                        }

                        if (esDisminucion)
                        {
                            comprobante.montoN -= nota.total;
                          /*  DetalleVenta dv;
                            int idPresentacion = 0;
                            for (int i = 0; i < nota.detalles.Count; i++)
                            {
                                idPresentacion = nota.detalles[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion && x.idVenta == comprobante.idVenta);
                                if (nota.detalles[i].igv == 0)
                                    dv.precioN -= nota.detalles[i].precio;
                                else
                                    dv.precioN -= nota.detalles[i].precio + nota.detalles[i].precio * igv.valorDouble;
                                db.SaveChanges();
                            }*/
                        }

                        if (esAumento)
                        {
                            comprobante.montoN += nota.total;
                            signoNota = 1;
                         /*   int idPresentacion = 0;
                            DetalleVenta dv;
                            for (int i = 0; i < nota.detalles.Count; i++)
                            {
                                idPresentacion = nota.detalles[i].idPresentacion;
                                dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion && x.idVenta == comprobante.idVenta);
                                if (nota.detalles[i].igv == 0)
                                    dv.precioN -= nota.detalles[i].precio;
                                else 
                                    dv.precioN += nota.detalles[i].precio + nota.detalles[i].precio * igv.valorDouble;
                                db.SaveChanges();
                            }*/
                        }

                        if (esDevolucion)
                        {
                            DetalleMovimientoProducto mp;
                            AlmacenProducto ap;
                            DetallCompra dv;
                            Producto p;
                            DetalleMovimientoProducto  dm2;
                            Almacen principal = db.almacenes.FirstOrDefault(x => x.esPrincipal);
                            double costoInicial = 0;
                            double stockInicial = 0; int idUltimoMov = 0;
                            for (int i = 0; i < nota.detalle.Count; i++)
                            {
                               
                          
                             
                                //ANULAR MOVIMIENTOS PRODUCTO
                                p = db.productos.Find(nota.detalle[i].idProducto);
                                mp = new DetalleMovimientoProducto
                                {
                                    cantidad = nota.detalle[i].cantidad * nota.detalle[i].factor,
                                    fecha =nota.fechaEmision,
                                    numero = nota.numero,
                                    serie = nota.serie,
                                    tbl10 = tipoComprobante.codigo_sunat,
                                    tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "DEVOLUCION ENTREGADA").codigo_sunat,
                                    costoUnitario = nota.detalle[i].valorUnitario,
                                    idProducto = p.id, 
                                  
                                };
                                double cantidadUnitaria = mp.cantidad;
                                double valorCostoDetalle = nota.detalle[i].valorUnitario * comprobante.cambio;

                                mp. nuevoCostoUnitario = (p.costo * p.stock +mp.cantidad*mp.costoUnitario) /( p.stock+mp.cantidad);
                                mp.total = mp.costoUnitario * mp.cantidad;
                                p.stock -= mp.cantidad;
                                mp.nuevoStock = p.stock;

                                DetalleMovimientoProducto dma = null;
                                DateTime fechaC=nota.fechaEmision.Date.AddDays(1);
                                    dma = db.detallesMovimientos.OrderByDescending(x => x.idAnterior).FirstOrDefault(x => x.anulado == false && x.idProducto == p.id && x.fecha < fechaC);

                                if (dma != null)
                                {
                                    mp.nuevoStock = dma.nuevoStock - cantidadUnitaria;

                                    if (mp.costoUnitario != 0) p.costo = ((dma.nuevoStock*dma.nuevoCostoUnitario - mp.total) /mp.nuevoStock);

                                    mp.nuevoCostoUnitario = p.costo;
                                    
                                    mp.idAnterior = dma.idAnterior + 1;
                                    idUltimoMov = mp.idAnterior;
                                    costoInicial = mp.nuevoCostoUnitario;
                                    stockInicial = mp.nuevoStock;
                                }
                                else
                                {
                                    if (mp.costoUnitario!= 0) p.costo = ((mp.cantidad * valorCostoDetalle - p.stockInicial * p.costoInicial) / (p.stockInicial - mp.cantidad));
                                    mp.nuevoCostoUnitario = p.costo;
                                    costoInicial = p.costo;
                                    mp.nuevoStock = p.stockInicial - cantidadUnitaria;
                                    mp.idAnterior = 1;
                                    idUltimoMov = 1;
                                    db.SaveChanges();
                                    stockInicial = p.stockInicial - cantidadUnitaria;
                                }
                                List<DetalleMovimientoProducto> ultimos;
                                if (dma != null) ultimos = db.detallesMovimientos.OrderBy(x => x.idAnterior).Where(x => x.idProducto ==p.id && x.idAnterior > dma.idAnterior && x.anulado == false).ToList();
                                else ultimos = db.detallesMovimientos.OrderBy(x => x.idAnterior).Where(x => x.idProducto ==p.id && x.anulado == false).ToList();
                                if (ultimos.Count > 0)
                                {
                                    foreach (var x in ultimos)
                                    {
                                        dm2 = db.detallesMovimientos.Find(x.id);
                                        dm2.idAnterior = dm2.idAnterior + 1;
                                        idUltimoMov = dm2.idAnterior;
                                        if (dm2.esIngreso)
                                        {//GRATUITO , SOBRANTE O NOTA DE CREDITO DEVOLUCION VENTA O ANULACION
                                            if (dm2.costoUnitario == 0 || dm2.tbl10 == "07")
                                            {
                                                dm2.nuevoStock = stockInicial + dm2.cantidad;
                                                dm2.total = dm2.costoUnitario * dm2.cantidad;
                                                dm2.nuevoCostoUnitario = costoInicial;
                                                stockInicial = dm2.nuevoStock;
                                            }
                                            else
                                            {    //ES COMPRA 
                                                dm2.nuevoStock = stockInicial + dm2.cantidad;
                                                dm2.total = dm2.costoUnitario * dm2.cantidad;
                                                dm2.nuevoCostoUnitario = ((costoInicial * stockInicial) + (dm2.total)) / dm2.nuevoStock;
                                                costoInicial = dm2.nuevoCostoUnitario;
                                                stockInicial = dm2.nuevoStock;
                                            }
                                        }
                                        else
                                        {
                                            //SI ES NOTA DE CREDITO POR DEVOLUCION DE COMPRA SI ES ANULACION COMPRA
                                            if (dm2.tbl10 == "07")
                                            {
                                                dm2.nuevoStock = stockInicial - dm2.cantidad;
                                                dm2.total = dm2.costoUnitario * dm2.cantidad;
                                                dm2.nuevoCostoUnitario = ((costoInicial * stockInicial) - (dm2.total)) / dm2.nuevoStock;
                                                costoInicial = dm2.nuevoCostoUnitario;
                                                stockInicial = dm2.nuevoStock;
                                            }
                                            else
                                            {
                                                dm2.nuevoCostoUnitario = costoInicial;
                                                dm2.nuevoStock = stockInicial - dm2.cantidad;
                                                dm2.total = dm2.costoUnitario * dm2.cantidad;
                                                stockInicial = dm2.nuevoStock;
                                            }
                                        }
                                        db.SaveChanges();
                                    }
                                }
                                p.costo = costoInicial;
                                p.stock = stockInicial;
                                p.idMovimiento = idUltimoMov;       
                          


                            db.detallesMovimientos.Add(mp);
                                db.SaveChanges();
                                nota.detalle[i].idMovimiento = mp.id;
                                int idPresentacion = nota.detalle[i].idPresentacion;
                           //     dv = db.detallesVenta.FirstOrDefault(x => x.idProductoPresentacion == idPresentacion && x.idVenta == comprobante.idVenta);
                         //       dv.cantidadN = dv.cantidad - nota.detalle[i].cantidad;
                       //         if (dv.cantidadN == 0) dv.anulado = true;
                                db.SaveChanges();
                            }
                        }
                        db.notasCompra.Add(nota);
                        RegistroCompra registroVenta = new RegistroCompra {
                            tipoComprobante = tipoComprobante.codigo_sunat,
                            total = nota.total * signoNota,
                            fechaEmision = nota.fechaCreate,
                            fechaNota = comprobante.fechaEmision.ToString("yyyy-MM-dd"),
                            fechaVencimiento = nota.fechaCreate,
                            idProveedor = comprobante.idProveedor,
                            numeroComprobante = nota.numero,
                            numeroNota = comprobante.numero,
                            serie = nota.serie,
                            serieNota = comprobante.serie,
                            tipoCambio = comprobante.cambio,
                            tipoNota = comprobante.tipoComprobante.codigo_sunat, idMoneda = comprobante.idMoneda, idMedioPago = comprobante.idMedioPago,
                            razonSocial = comprobante.proveedor.razonSocial,
                            tipoDocIdentidad = "01", docIdentidad = comprobante.proveedor.ruc, opExoneradas = nota.totalExonerado,
                        };
                        Proveedor cliente = db.proveedores.FirstOrDefault(x => x.id == comprobante.idProveedor);
                        registroVenta.docIdentidad = cliente.ruc; registroVenta.tipoDocIdentidad = TipoDocIdentidadImpl.getCodigoRUC(); 
                       if (registroVenta.tipoDocIdentidad.Equals(""))
                        {
                            dbTransaction.Rollback();
                            return false;
                        }
                        db.registroCompras.Add(registroVenta);
                        comprobante.montoN = comprobante.montoN + signoNota * nota.total;

                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<NotaCompra> listarNoAnulados()
        {
            using (var db = new BaseDeDatos(""))
            {
               
                return db.notasCompra.Include("tipoNota").Include("proveedor").OrderByDescending(x => x.id).Where(x=>x.anulado==false).Take(30).ToList();
            }
        }
        public List<NotaCompra> listarFecha(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {    List<NotaCompra> ns = db.notasCompra.Include("tipoNota").Include("proveedor").OrderByDescending(x => x.id)
                     .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin).ToList();
 
            return ns; }
        }
        public List<NotaCompra> listarPorProveedor(DateTime inicio, DateTime fin, int idproveedor)
        {
            using (var db = new BaseDeDatos("public"))
                return db.notasCompra.Include("tipoNota").Include("proveedor").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin && x.idProveedor ==idproveedor).ToList();
        }
        public List<NotaCompra> listarPorSerieNumero(DateTime inicio, DateTime fin, string serie, int numero)
        {
            using (var db = new BaseDeDatos("public"))
                return db.notasCompra.Include("tipoNota").Include("proveedor").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin && x.serie == serie && x.numero == numero).ToList();
        }

        public List<NotaCompra> listarPorComprobante(int idComprobante)
        {
            throw new NotImplementedException();
        }

        public List<NotaCompra> listarPorFechas(DateTime inicio, DateTime fin)
        {
            throw new NotImplementedException();
        }

        public List<NotaCompra> listarPorNumero(int numero)
        {
            throw new NotImplementedException();
        }
    }
}
