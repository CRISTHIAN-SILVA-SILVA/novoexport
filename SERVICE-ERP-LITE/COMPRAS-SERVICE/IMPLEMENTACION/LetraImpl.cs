﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.COMPRAS;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION
{
    public class LetraImpl : LetraService
    {
        public LetraCompra buscar(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.letrasCompra.FirstOrDefault(x => x.id == id);
            }
        }

        public bool crear(LetraCompra objeto)
        {
            throw new NotImplementedException();
        }
        public bool guardarVarios(List<LetraCompra> letras,int numeroInicio) {
            using (var db = new BaseDeDatos("public")) {
                using (var dbTransaction = db.Database.BeginTransaction()){
                    try{
                        int idCompra = 0;
                        double total = 0;
                        for (int i = 0; i < letras.Count; i++){
                            idCompra = letras[i].idCompra; 
                            db.letrasCompra.Add(letras[i]);
                            total += letras[i].monto;}
                   
                        List<LetraCompra> letrasAntiguas= db.letrasCompra.Where(x => x.anulado == false && x.pagado == false && x.idCompra == idCompra).ToList();
                        for (int i = 0; i < letrasAntiguas.Count; i++)
                        { db.letrasCompra.Find(letrasAntiguas[i].id).anulado = true;
                            db.SaveChanges();
                        }

                        Compra c = db.compras.Find(idCompra);
                        c.idTipoPago = 3;
                        c.montoN = total+c.montoPagado;
                        c.intereses = c.intereses + total -( c.montoN - c.montoPagado);
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;}
                    catch (Exception ex){
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;}}}
        }
        public bool editar(LetraCompra objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<LetraCompra> listarNoAnulados()
        {
            throw new NotImplementedException();
        }

        public List<LetraCompra> listarPorCompra(int compra)
        {
            using (var db=new BaseDeDatos("")) {
               return db.letrasCompra.OrderBy(x=>x.numero).Where(x=>x.idCompra==compra &&x.anulado==false).ToList();
            }
        }

        public bool pagar(int letra,bool todo)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        LetraCompra lc = db.letrasCompra.Find(letra);
                        lc.fechaPago = DateTime.Now;
                        lc.pagado = true;
                        lc.serieNumero ="";
                        Compra compra = db.compras.Find(lc.idCompra);
                        compra.montoPagado += lc.monto;
                        
                        if (todo) {
                            compra.montoPagado = compra.montoN;
                            compra.pagado =true;
                        }
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }
            }
        }
    }
}
