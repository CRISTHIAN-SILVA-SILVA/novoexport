﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.COMPRAS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION
{
 public   class GuiaCompraImpl
    {
        public GuiaCompra buscarPorCompra(int id)
        {
            using (var db = new BaseDeDatos(""))
            {
                return db.guiasEmision.Include("transportista").FirstOrDefault(x => x.idComprobante == id);
            }
        }
    }
}
