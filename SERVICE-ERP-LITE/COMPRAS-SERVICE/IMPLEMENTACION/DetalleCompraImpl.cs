﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION
{
    public class DetalleCompraImpl : DetalleCompraService
    {
        public DetallCompra buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.detallesCompras.Find(id);
            }
            }

        public bool guardar(DetallCompra objeto,Compra c)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                      double cantidadProductoBasico = objeto.factor * objeto.cantidad;
                        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
                        Producto producto = db.productos.Find(objeto.idProducto);
                        producto.costo = (objeto.costo / objeto.factor) / (1 + igv.valorDouble);

                        double valorCostoDetalle = (objeto.costo / objeto.factor) / (1 + igv.valorDouble);

                        producto.stock += cantidadProductoBasico;
                        if (producto.esExonerado) valorCostoDetalle = objeto.costo;
                        DetalleMovimientoProducto detalleProducto = new DetalleMovimientoProducto { idProducto=objeto.idProducto,
                            cantidad = cantidadProductoBasico,
                            esIngreso = true,
                            nuevoStock = producto.stock,
                            serie = c.serie,
                            fecha = c.fechaEmision,
                            tbl10 = CodigosSUNAT.FACTURA,
                            tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "COMPRA").codigo_sunat,
                            total =objeto.total,
                            costoUnitario=valorCostoDetalle,
                            numero=c.numero,
                        };

                        detalleProducto.total = cantidadProductoBasico * detalleProducto.costoUnitario;
                        if (objeto.total != 0) producto.costo = ((detalleProducto.cantidad * valorCostoDetalle + producto.stock * producto.costo) / (producto.stock + detalleProducto.cantidad));
                        detalleProducto.nuevoCostoUnitario = producto.costo;

                        objeto.movimiento = detalleProducto;
                        db.detallesCompras.Add(objeto);

                      
                        Almacen a = db.almacenes.FirstOrDefault(x => x.esPrincipal == true && x.anulado == false);
                        AlmacenProducto ap = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == a.id && x.idProducto == objeto.idProducto);
                        ap.cantidadLogica+= cantidadProductoBasico;
                        ap.cantidadFisica = ap.cantidadLogica;

                        Compra cm = db.compras.Find(c);
                        cm.montoTotal = c.montoTotal;
                        cm.opExoneradas = c.opExoneradas;
                        cm.igv = c.igv;
                        c.opGravadas = c.opGravadas;

                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString()); return false;
                    }
                }
            }
        }

        public bool agregarSobrante(DetallCompra objeto, int idDetalle)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        double cantidadProductoBasico = objeto.factor * objeto.cantidad;
                        Producto producto = db.productos.Find(objeto.idProducto);
                        Compra c = db.compras.FirstOrDefault(x=>x.id==objeto.idCompra);
                        double valorCostoDetalle =0;

                        producto.stock += cantidadProductoBasico;
                   
                        DetallCompra da = db.detallesCompras.Include("movimiento").FirstOrDefault(x=>x.id==idDetalle);

                        DetalleMovimientoProducto detalleProducto = new DetalleMovimientoProducto
                        {
                            idProducto = objeto.idProducto,
                            cantidad = cantidadProductoBasico,
                            esIngreso = true,
                            nuevoStock = producto.stock,
                            serie = c.serie,
                            fecha = c.fechaEmision,
                            tbl10 = CodigosSUNAT.FACTURA,
                            tbl12 = "99",
                            total = 0,
                            costoUnitario = valorCostoDetalle,
                            numero = c.numero, idAnterior=da.movimiento.idAnterior+1,  
                        };
                        detalleProducto.nuevoCostoUnitario = da.movimiento.nuevoCostoUnitario;

                        objeto.movimiento = detalleProducto;
                        db.detallesCompras.Add(objeto);
                        DetalleMovimientoProducto dg;
                        List<DetalleMovimientoProducto> l = db.detallesMovimientos.Where(x => x.idAnterior > detalleProducto.idAnterior && x.anulado == false && x.idProducto == detalleProducto.idProducto).ToList();

                        producto.idMovimiento = producto.idMovimiento + 1;
                        
                        foreach (var x in l) {
                            dg = db.detallesMovimientos.Find(x.id);
                            dg.idAnterior = dg.idAnterior + 1;
                            db.SaveChanges();
                        }

                       

                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString()); return false;
                    }
                }
            }
        }
        public bool editar(DetallCompra objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        DetallCompra detalle = db.detallesCompras.Find(id);
                        detalle.anulado = true;
                        DetalleMovimientoProducto detalleProducto = db.detallesMovimientos.Find(detalle.idMovimiento);
                        detalleProducto.anulado = true;
                        DetalleMovimientoProducto dmpAnterior;
                        Producto producto = db.productos.Find(detalle.idProducto);
                        double cantidadAnterior = 0, costoAnterior = 0;
                        int idAnterior = 0;
                        if (detalleProducto.idAnterior != 0)
                        { dmpAnterior = db.detallesMovimientos.Find(detalleProducto.idAnterior);
                            cantidadAnterior = dmpAnterior.nuevoStock;
                            costoAnterior = dmpAnterior.nuevoCostoUnitario;
                           
                            idAnterior = dmpAnterior.id;
                        }
                        else { costoAnterior = producto.costo;cantidadAnterior = producto.stock; }

                       


                        List<DetalleMovimientoProducto> DETS = db.detallesMovimientos.Where(x => x.id > detalleProducto.id&&x.anulado==false).ToList();
                        //recalcular
                        for (int i = 0; i < DETS.Count; i++)
                        { 
                        DETS[i].nuevoStock =cantidadAnterior + DETS[i].cantidad;
                        DETS[i].nuevoCostoUnitario= (DETS[i].total+cantidadAnterior*costoAnterior) / DETS[i].nuevoStock;
                            DETS[i].idAnterior =idAnterior;
                            cantidadAnterior = DETS[i].nuevoStock; costoAnterior = DETS[i].nuevoCostoUnitario;
       
                        }
                        producto.costo =costoAnterior;
                        Compra c = db.compras.Find(detalle.idCompra);
                        double total = c.montoTotal-detalle.total, igv = c.montoTotal-detalle.igv, exonerado = c.opExoneradas;
                       
                            c.montoTotal -=detalle.total;
                        if (producto.esExonerado)
                            c.opExoneradas-=c.opExoneradas;
                        
                        c.igv-=detalle.igv;
                        c.opGravadas =c.montoTotal-c.igv-c.opExoneradas;

                        double cantidadProductoBasico = detalle.factor * detalle.cantidad;
                        producto.stock -= cantidadProductoBasico;
                        
                        Almacen a = db.almacenes.FirstOrDefault(x => x.esPrincipal == true && x.anulado == false);
                        AlmacenProducto ap = db.almacenesProductos.FirstOrDefault(x => x.idAlmacen == a.id && x.idProducto == detalle.idProducto);
                        ap.cantidadLogica-= cantidadProductoBasico;
                        ap.cantidadFisica = ap.cantidadLogica;

                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString()); return false;
                    }
                }
            }
        }

        public List<DetallCompra> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
        public List<JoinDetalleCompra> listarPorProdcutoFecha(int producto, DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return (from
                       p in db.proveedores  join
                     c in db.compras
                             on p.id equals c.idProveedor
                             join
                             d in db.detallesCompras
                    on c.id equals d.idCompra join
                             m in db.detallesMovimientos
                             on d.idMovimiento equals m.id

                        where d.anulado == false && c.fechaEmision >= inicio && c.fechaEmision < fin&&d.idProducto==producto
               orderby c.fechaEmision
                        select new JoinDetalleCompra
                        {
                             cantidad=d.cantidad*d.factor,
                              costo=d.costo,
                               fecha=c.fechaEmision,
                                id=d.id,
                                 idCompra=c.id,
                                 idMovimiento=d.idMovimiento,
                                  proveedor=p.razonSocial,
                                   numero=c.numero,
                                    serie=c.serie,
                                     total=d.total,
                                      totalMov=m.total
                        }
                   ).ToList();


            }
        }
        public bool modificar(int idDetalle, int idNuevoProducto,int idNuevaPresentacion) {
            using (var db = new BaseDeDatos("public")) {
                using (var dbTransaction = db.Database.BeginTransaction())  {
                    try {
                        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
                        DetallCompra detalle = db.detallesCompras.Find(idDetalle);
                        detalle.idPresentacion = idNuevaPresentacion;
                        detalle.idProducto = idNuevoProducto;

                        Producto productoAnterior = db.productos.Find(detalle.idProducto);
                        Producto productoNuevo = db.productos.Find(idNuevoProducto);

                        DetalleMovimientoProducto detalleProducto = db.detallesMovimientos.Find(detalle.idMovimiento);

                        detalleProducto.idProducto =idNuevoProducto;


                        productoAnterior.stock -= detalleProducto.cantidad;
                        productoNuevo.stock += detalleProducto.cantidad;

                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;  }
                    catch (Exception ex)  {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString()); return false;  }  } }
        }

        public bool crear(DetallCompra objeto)
        {
            throw new NotImplementedException();
        }
    }
}
