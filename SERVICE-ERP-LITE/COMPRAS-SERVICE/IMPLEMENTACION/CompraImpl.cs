﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.INVENTARIO;
using MODEL_ERP_LITE.PUBLIC;
using MODEL_ERP_LITE.RELACIONES;
using SERVICE_ERP_LITE.CAJA_SERVICE;
using SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.PUBLIC_SERVICE.PUBLIC_IMPLEMENTACION;
using SERVICE_ERP_LITE.UTILITARIO;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE.IMPLEMENTACION
{
    public class CompraImpl : CompraService
    {
        public Compra buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoComprobante").Include("medioPago").Include("detallesCompra"). Include("tipoMoneda").FirstOrDefault(x=>x.anulado==false&&x.id==id);
            }
        }

        public bool crear(Compra objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(Compra objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }
        public bool normalizarCompras()
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        List<Compra> compras = db.compras.Include("proveedor").Where(x => x.anulado == false).ToList();
                        RegistroCompra rc;
                        foreach (var c in compras) {
                            rc = db.registroCompras.FirstOrDefault(x=>x.anulado==false&&x.idComprobante==c.id&&x.esGasto==false);
                            if (rc == null) {
                                RegistroCompra registroCompra = new RegistroCompra
                                {
                                    docIdentidad = c.proveedor.ruc,
                                    fechaDetraccion = "",
                                    fechaEmision = c.fechaEmision,
                                    fechaVencimiento = c.fechaVencimiento,
                                    fechaNota = "",
                                    idComprobante = c.id,
                                    igv = c.igv,
                                    total = c.montoTotal,
                                    numeroComprobante = c.numero,
                                    opExoneradas = c.opExoneradas,
                                    opGravadas = c.opGravadas,
                                    razonSocial = c.proveedor.razonSocial
                         ,
                                    serieNota = "",
                                    tipoComprobante = "01",
                                    tipoDocIdentidad = "6",
                                    tipoNota = "",
                                    idProveedor = c.idProveedor,
                                    tipoCambio = c.cambio,
                                    idMoneda = c.idMoneda,
                                    idMedioPago = c.idMedioPago
                                };
                                db.registroCompras.Add(registroCompra);

                            }
                            else {
                                rc.fechaEmision = c.fechaEmision;
                                rc.fechaVencimiento = c.fechaVencimiento;
                            }
                            db.SaveChanges();
                        }
                      
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }
        public bool eliminar(int id)
        { 
            using (var db = new BaseDeDatos("public")){
                using (var dbTransaction = db.Database.BeginTransaction()) {
                    try {
                        Compra c = db.compras.FirstOrDefault(x=>x.id==id);
                        List<DetallCompra> detalles = db.detallesCompras.Where(x=>x.idCompra==c.id&&x.anulado==false).ToList();                      
                        c.anulado = true;
                        RegistroCompra rc = db.registroCompras.FirstOrDefault(x=>x.idComprobante==c.id&&x.esGasto==false);
                        if(rc!=null)
                        rc.anulado = true;
                        DetallCompra dc;
                        DetalleMovimientoProducto dm,dm2,dma;
                        double costoInicial = 0;
                        double stockInicial = 0;int idUltimoMov=0,indexAnterior=0;
                        foreach (var d in detalles) {
                            dc = db.detallesCompras.Find(d.id);
                            dc.anulado = true;
                            dm = db.detallesMovimientos.Find(d.idMovimiento);
                            Producto p = db.productos.Find(dc.idProducto);
                            dm.anulado = true;
                            idUltimoMov = 0;indexAnterior = dm.idAnterior - 1;
                            dma = db.detallesMovimientos.FirstOrDefault(o => o.idAnterior == indexAnterior);
                            if (dma == null)
                            {
                                costoInicial = p.costoInicial;
                                stockInicial = p.stockInicial;
                            }
                            else { costoInicial = dma.nuevoCostoUnitario;stockInicial = dma.nuevoStock; }
                            List<DetalleMovimientoProducto> detallesMov = db.detallesMovimientos.OrderBy(x => x.idAnterior).Where(x =>x.idProducto==d.idProducto&& x.anulado == false && x.idAnterior > d.movimiento.idAnterior).ToList();
                            if (detallesMov.Count == 0) {
                                idUltimoMov = indexAnterior;
                            }
                            foreach (var x in detallesMov ) {                         
                                dm2 = db.detallesMovimientos.Find(x.id);
                                dm2.idAnterior = dm2.idAnterior - 1;
                                idUltimoMov = dm2.idAnterior;
                                if (dm2.esIngreso) {//GRATUITO , SOBRANTE O NOTA DE CREDITO DEVOLUCION VENTA O ANULACION
                                    if (dm2.costoUnitario == 0 || dm2.tbl10 == "07") {
                                        dm2.nuevoStock = stockInicial + dm2.cantidad;
                                        dm2.total = dm2.costoUnitario*dm2.cantidad;
                                        dm2.nuevoCostoUnitario = costoInicial;
                                        stockInicial = dm2.nuevoStock; }
                                    //ES COMPRA   
                                    else  {
                                        dm2.nuevoStock = stockInicial + dm2.cantidad;
                                        dm2.total = dm2.costoUnitario * dm2.cantidad;
                                        dm2.nuevoCostoUnitario = ((costoInicial * stockInicial) + (dm2.total)) / dm2.nuevoStock;
                                        costoInicial = dm2.nuevoCostoUnitario;
                                        stockInicial = dm2.nuevoStock; } }
                                else {//SI ES NOTA DE CREDITO POR DEVOLUCION DE COMPRA SI ES ANULACION COMPRA
                                    if (dm2.tbl10 == "07"){
                                        dm2.nuevoStock = stockInicial - dm2.cantidad;
                                        dm2.total = dm2.costoUnitario * dm2.cantidad;
                                        dm2.nuevoCostoUnitario = ((costoInicial * stockInicial) - (dm2.total)) / dm2.nuevoStock;
                                        costoInicial = dm2.nuevoCostoUnitario;
                                        stockInicial = dm2.nuevoStock; }
                                    else {
                                        dm2.nuevoCostoUnitario = costoInicial;
                                        dm2.nuevoStock = stockInicial - dm2.cantidad;
                                        dm2.total = dm2.costoUnitario * dm2.cantidad;
                                        stockInicial = dm2.nuevoStock; } }
                                db.SaveChanges();   
                            }p.stock = stockInicial;p.costo = costoInicial;p.idMovimiento = idUltimoMov;
                            db.SaveChanges();}
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool guardarCompleto(Compra c, GuiaCompra guia, bool esPagoUnico,bool noEsFactura) {
            using (var db = new BaseDeDatos("public")){
                using (var dbTransaction = db.Database.BeginTransaction()){
                    try{
                        c.tipoPago = null;
                        DetalleMovimientoProducto dmp,dm2;
                        double costoInicial = 0;
                        double stockInicial = 0; int idUltimoMov= 0;
                        Producto p;
                        c.serie = c.serie.ToUpper();
                        TipoComprobantePago tc = db.tiposComprobantePago.Find(c.idTipoComprobante);
                        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
                        c.montoN = c.montoTotal * c.cambio;
                        if (esPagoUnico) { c.pagado = true; c.montoPagado = c.montoTotal; }
                        List<DetallCompra> detalles = c.detallesCompra;
                        c.detallesCompra = null;
                        db.compras.Add(c);
                        db.SaveChanges();
                        int j = 0;
                        string tbl12 = "";
                        foreach (var i     in detalles){
                            p = db.productos.Find(i.idProducto);
                            double cantidadUnitaria = i.cantidad * i.factor;                
                            double valorCostoDetalle = (i.costo/i.factor)*c.cambio / (1 + igv.valorDouble);
                            if (p.esExonerado) valorCostoDetalle = i.costo*c.cambio;
                            if (i.costo == 0) tbl12 = "99";
                            else tbl12 = TipoOperacionImpl.getInstancia().FirstOrDefault(x => x.descripcion == "COMPRA").codigo_sunat;
                            dmp = new DetalleMovimientoProducto
                            { esIngreso = true,
                                costoUnitario = valorCostoDetalle,
                                cantidad = cantidadUnitaria,
                                idProducto = p.id,
                                nuevoStock = p.stock + cantidadUnitaria,
                                fecha = c.fechaEmision,
                                numero = c.numero,
                                serie = c.serie,
                                tbl10 = tc.codigo_sunat,
                                tbl12 = tbl12,                              
                            };                            
                            dmp.total = cantidadUnitaria * dmp.costoUnitario;
                            DetalleMovimientoProducto dma=null;
                                if(dmp.esIngreso)
                             dma   =db.detallesMovimientos.OrderByDescending(x => x.idAnterior).FirstOrDefault(x => x.anulado == false && x.idProducto == p.id&& x.fecha < c.fechaEmision.Date);

                            if (dma != null) {
                                if (i.total != 0) p.costo = ((dmp.cantidad * valorCostoDetalle + dma.nuevoStock *dma.nuevoCostoUnitario) / (dma.nuevoStock + dmp.cantidad));
                                dmp.nuevoCostoUnitario = p.costo;
                                dmp.nuevoStock = dma.nuevoStock + cantidadUnitaria;
                                dmp.idAnterior = dma.idAnterior + 1;
                                idUltimoMov = dmp.idAnterior;
                                costoInicial = dmp.nuevoCostoUnitario;
                                stockInicial = dmp.nuevoStock;                                 }
                            else {
                                if (i.total != 0) p.costo = ((dmp.cantidad * valorCostoDetalle + p.stockInicial * p.costoInicial) / (p.stockInicial + dmp.cantidad));
                                dmp.nuevoCostoUnitario = p.costo;
                                costoInicial = p.costo;
                                dmp.nuevoStock=p.stockInicial+cantidadUnitaria;
                                dmp.idAnterior = 1;
                                idUltimoMov = 1;
                                db.SaveChanges();                          
                               stockInicial= p.stockInicial + cantidadUnitaria;                         }
                            List<DetalleMovimientoProducto> ultimos;
                            if(dma!=null) ultimos = db.detallesMovimientos.OrderBy(x => x.idAnterior).Where(x =>x.idProducto==i.idProducto && x.idAnterior > dma.idAnterior&&x.anulado==false).ToList();
                            else ultimos = db.detallesMovimientos.OrderBy(x => x.idAnterior).Where(x =>x.idProducto==i.idProducto&&x.anulado==false).ToList();
                            if (ultimos.Count > 0)
                            {
                                foreach (var x in ultimos) {
                                    dm2 = db.detallesMovimientos.Find(x.id);                                  
                                   dm2.idAnterior = dm2.idAnterior + 1;
                                    idUltimoMov = dm2.idAnterior;
                                    if (dm2.esIngreso) {//GRATUITO , SOBRANTE O NOTA DE CREDITO DEVOLUCION VENTA O ANULACION
                                        if (dm2.costoUnitario == 0 || dm2.tbl10 == "07") {
                                            dm2.nuevoStock = stockInicial + dm2.cantidad;
                                            dm2.total = dm2.costoUnitario * dm2.cantidad;
                                            dm2.nuevoCostoUnitario = costoInicial;
                                            stockInicial = dm2.nuevoStock; }
                                        else {    //ES COMPRA 
                                            dm2.nuevoStock = stockInicial + dm2.cantidad;
                                            dm2.total = dm2.costoUnitario * dm2.cantidad;
                                            dm2.nuevoCostoUnitario = ((costoInicial * stockInicial) + (dm2.total)) / dm2.nuevoStock;
                                            costoInicial = dm2.nuevoCostoUnitario;
                                            stockInicial = dm2.nuevoStock; } }
                                    else  {
                                        //SI ES NOTA DE CREDITO POR DEVOLUCION DE COMPRA SI ES ANULACION COMPRA
                                        if (dm2.tbl10 == "07")  {
                                            dm2.nuevoStock = stockInicial - dm2.cantidad;
                                            dm2.total = dm2.costoUnitario * dm2.cantidad;
                                            dm2.nuevoCostoUnitario = ((costoInicial * stockInicial) - (dm2.total)) / dm2.nuevoStock;
                                            costoInicial = dm2.nuevoCostoUnitario;
                                            stockInicial = dm2.nuevoStock; }
                                        else  {
                                            dm2.nuevoCostoUnitario = costoInicial;
                                            dm2.nuevoStock = stockInicial - dm2.cantidad;
                                            dm2.total = dm2.costoUnitario * dm2.cantidad;
                                            stockInicial = dm2.nuevoStock; } }
                                     db.SaveChanges();    }
                            }
                            p.costo = costoInicial;
                            p.stock = stockInicial;
                            p.idMovimiento =idUltimoMov;                        
                            detalles[j].idCompra = c.id ;
                            detalles[j].movimiento = dmp;
                            db.detallesCompras.Add(detalles[j]);
                            db.SaveChanges();
                            j++;
                        }
                                       
                        db.SaveChanges();
                        Proveedor proveedor = db.proveedores.Find(c.idProveedor);
                        if (!noEsFactura)
                        {
                            RegistroCompra registroCompra = new RegistroCompra
                            {
                                docIdentidad = proveedor.ruc,
                                fechaDetraccion = "",
                                fechaEmision = c.fechaEmision,
                                fechaVencimiento = c.fechaVencimiento,
                                fechaNota = "",
                                idComprobante = c.id,
                                igv = c.igv,
                                total = c.montoTotal,
                                numeroComprobante = c.numero,
                                opExoneradas = c.opExoneradas,
                                opGravadas = c.opGravadas,
                                razonSocial = proveedor.razonSocial
                                ,
                                serieNota = "",
                                tipoComprobante = tc.codigo_sunat,
                                tipoDocIdentidad = "6",
                                tipoNota = "",
                                idProveedor = c.idProveedor,
                                tipoCambio = c.cambio,
                                idMoneda = c.idMoneda,
                                idMedioPago = c.idMedioPago
                            };
                            db.registroCompras.Add(registroCompra);
                        }
                        if (guia != null) { guia.idComprobante = c.id; db.guiasEmision.Add(guia); }
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }   
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Compra> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoMoneda").OrderByDescending(x=>x.id).Where(x => x.anulado == false).Take(30).ToList();          
        }

        public List<Compra> listarPorFecha(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoMoneda").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false&& x.fechaEmision>=inicio&&x.fechaEmision<fin).ToList();
        }
        public List<Compra> listarPorFechaTipoPago(DateTime inicio,DateTime fin,int idTipoPago)
        {
            using (var db = new BaseDeDatos("public"))
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoMoneda").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin&&x.idTipoPago==idTipoPago).ToList();
        }
        public List<Compra> listarPorFechaTipoMoneda(DateTime inicio, DateTime fin, int idTipoMoneda)
        {
            using (var db = new BaseDeDatos("public"))
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoMoneda").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin&&x.idMoneda==idTipoMoneda).ToList();
        }
        public List<Compra> listarPorSerieNumero(DateTime inicio, DateTime fin, string serie, int numero)
        {
            using (var db = new BaseDeDatos("public"))
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoMoneda").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin &&x.serie==serie&&x.numero==numero).ToList();
        }
        public List<Compra> listarPorFechaProveedor(DateTime inicio, DateTime fin, int idProveedor)
        {
            using (var db = new BaseDeDatos("public"))
                return db.compras.Include("proveedor").Include("tipoPago").Include("tipoMoneda").OrderByDescending(x => x.id)
                    .Where(x => x.anulado == false && x.fechaEmision >= inicio && x.fechaEmision < fin &&x.idProveedor==idProveedor).ToList();
        }


        public List<Compra> listarPorDeudaProveedor(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.compras.Include("tipoPago").Include("tipoMoneda").OrderByDescending(x => x.id).Where(x => x.idProveedor == id && x.pagado == false && x.anulado == false).ToList();
            }
        }

        public bool modificaConRegistro(Compra c,string docDetraccion,DateTime fechaDetraccion)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        Compra compraModifica = db.compras.Include("detallesCompra").FirstOrDefault(x=>x.id==c.id);
                        compraModifica.idProveedor = c.idProveedor;
                        
                        Proveedor p = db.proveedores.Find(c.idProveedor);
                        compraModifica.cambio = c.cambio;
                        compraModifica.descuentoGlobal = c.descuentoGlobal;
                        compraModifica.fechaEmision =c.fechaEmision;
                        compraModifica.fechaVencimiento = c.fechaVencimiento;
                        compraModifica.idMoneda = c.idMoneda;
                        compraModifica.idMedioPago = c.idMedioPago;
                        compraModifica.idProveedor = c.idProveedor;
                        compraModifica.igv = c.igv;
                        compraModifica.idTipoPago = c.idTipoPago;
                        compraModifica.montoTotal =c.montoTotal;
                        compraModifica.montoN = c.montoN;
                        compraModifica.opExoneradas = c.opExoneradas;
                        compraModifica.opGravadas = c.opGravadas;
                        compraModifica.serie = c.serie.ToUpper();
                        compraModifica.numero = c.numero;
                        compraModifica.usuarioUpdate = c.usuarioUpdate;
                        compraModifica.pagado = c.pagado;
                        compraModifica.montoPagado = c.montoPagado;
                        compraModifica.completa = true;
                        compraModifica.detraccion = c.detraccion;
                        RegistroCompra rc = db.registroCompras.FirstOrDefault(x => x.idComprobante == c.id);
                        if (rc != null)
                        {
                            rc.fechaVencimiento = compraModifica.fechaVencimiento;
                            rc.docIdentidad = p.ruc;
                            rc.fechaEmision = c.fechaEmision;
                            rc.idProveedor = c.idProveedor;
                            rc.igv = c.igv;
                            rc.montoDetraccion = c.detraccion;
                            rc.numeroComprobante = c.numero;
                            rc.serie = c.serie;
                            rc.idMedioPago = c.idMedioPago;
                            rc.total = c.montoTotal;
                            rc.tipoCambio = c.cambio;
                            if (!docDetraccion.Equals(""))
                                rc.fechaDetraccion = fechaDetraccion.ToString("dd/MM/yyyy");
                            else rc.fechaDetraccion = "-";
                            rc.numeroDetraccion = docDetraccion;

                            rc.total = c.montoTotal;
                        }
                        DetalleMovimientoProducto detalle;
                        foreach (var i in compraModifica.detallesCompra) {
                            if (i.anulado == false)
                            {
                                detalle = db.detallesMovimientos.FirstOrDefault(x => x.id==i.idMovimiento);
                                detalle.fecha = c.fechaEmision;
                              
                                detalle.serie = compraModifica.serie;
                                detalle.numero = c.numero;
                                db.SaveChanges();
                            }
                        }
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool pagar(int idCompra)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Compra cp = db.compras.Find(idCompra);
                        cp.pagado = true;
                        cp.montoPagado = cp.montoN;
                      //  CajaDiariaService cdservice = new CajaDiariaImpl();
                        cp.fechaPago = DateTime.Now;
                     //   int idCaja = cdservice.getCaja();
                    /*    MovimientoCaja mc = new MovimientoCaja
                        {

                            idDiaCaja = idCaja,
                            idComprobante = cp.id,
                            serieNumeroComprobante = cp.serie + "-" + Util.NormalizarCampo(cp.numero.ToString(), 8),
                            monto = cp.montoTotal,
                            esPorCompra = true,
                            concepto = "COMPRA",
                            idMedioPago = cp.idMedioPago
                        };
                        db.movimientosCaja.Add(mc);
                        CajaDiaria cajaDiaria = db.cajasDiarias.FirstOrDefault(x => x.id == idCaja);
                        cajaDiaria.montoFinal = cajaDiaria.montoFinal - mc.monto;*/
                  /*      foreach (var i in db.notas.Include("tipoComprobante").Where(x => x.anulado == false && x.idComprobanteVenta == cp.id).ToList())
                        {
                            mc = new MovimientoCaja
                            {

                                idDiaCaja = idCaja,
                                idComprobante = i.id,
                                serieNumeroComprobante = i.serie + "-" + Util.NormalizarCampo(i.numero.ToString(), 8),
                                monto = i.total,
                                idMedioPago = cp.idMedioPago
                            };
                            if (i.tipoComprobante.descripcion.ToUpper() == "NOTA DE CREDITO")
                            {
                                mc.esPorNotaCredito = true;
                                mc.concepto = "NOTA DE CREDITO";
                                cajaDiaria.montoFinal = cajaDiaria.montoFinal - mc.monto;
                            }
                            else
                            {
                                mc.esPorNotaDebito = true;
                                cajaDiaria.montoFinal = cajaDiaria.montoFinal + mc.monto;
                                mc.concepto = "NOTA DE DEBITO";
                            }
                            db.movimientosCaja.Add(mc);
                        }*/
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }

                }
            }
        }
    }
}
