﻿using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE
{
   public interface LetraService : CrudService<LetraCompra>
    {
        List<LetraCompra> listarPorCompra(int compra);
        bool guardarVarios(List<LetraCompra> letras, int inicio);
        bool pagar(int letra,bool todo);
    }
}
