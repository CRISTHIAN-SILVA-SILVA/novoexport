﻿using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE
{
  public  interface DetalleCompraService:CrudService<DetallCompra>
    {
       bool modificar(int idDetalle,int producto,int prsentacion);
       bool guardar(DetallCompra objeto, Compra c);
    }
}
