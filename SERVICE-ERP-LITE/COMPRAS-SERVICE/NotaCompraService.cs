﻿using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE
{
   public interface NotaCompraService:CrudService<NotaCompra>
    {
        List<NotaCompra> listarPorComprobante(int idComprobante);
        bool guardarNotaCompleta(NotaCompra nota, bool esAnulado, bool esDisminucion, bool esDescuento, bool esAumento, bool esDevolucion,bool esDevTotal,bool esDDescGlobal);
        List<NotaCompra> listarPorFechas(DateTime inicio, DateTime fin);
        List<NotaCompra> listarPorNumero(int numero);
        List<NotaCompra> listarFecha(DateTime inicio, DateTime fin);
        List<NotaCompra> listarPorProveedor (DateTime inicio, DateTime fin, int idproveedor);
        List<NotaCompra> listarPorSerieNumero(DateTime inicio, DateTime fin, string serie, int numero);
    }
}
