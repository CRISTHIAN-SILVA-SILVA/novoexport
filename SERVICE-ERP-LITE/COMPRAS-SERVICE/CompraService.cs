﻿using MODEL_ERP_LITE.COMPRAS;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.COMPRAS_SERVICE
{
  public  interface CompraService:CrudService<Compra>
    {
        bool guardarCompleto(Compra c,GuiaCompra guia, bool esPagoUnico,bool noEsFactura);
        List<Compra> listarPorDeudaProveedor(int id);
        bool pagar(int idCompra);
        bool modificaConRegistro(Compra c, string docDetraccion, DateTime fechaDetraccion);
        List<Compra> listarPorFecha(DateTime inicio, DateTime fin);
        List<Compra> listarPorFechaTipoPago(DateTime inicio, DateTime fin, int idTipoPago);
        List<Compra> listarPorFechaTipoMoneda(DateTime inicio, DateTime fin, int idTipoMoneda);
        List<Compra> listarPorSerieNumero(DateTime inicio, DateTime fin, string serie, int numero);
        List<Compra> listarPorFechaProveedor(DateTime inicio, DateTime fin, int idProveedor);
    }
}
