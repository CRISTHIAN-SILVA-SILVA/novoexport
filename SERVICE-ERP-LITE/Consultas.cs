﻿using MODEL_ERP_LITE.RELACIONES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE
{
  public   class Consultas
    {
        public static  Cliente consularRUC(string ruc) {
           
                String url = "https://api.sunat.cloud/ruc/"+ruc;
            var json="";
            try {json = new WebClient().DownloadString(url); } catch { return null; }
                string s = Newtonsoft.Json.JsonConvert.DeserializeObject(json).ToString();
                dynamic m = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json);
                Cliente cliente = new Cliente();
                
            try { cliente.ruc = m.ruc; } catch { return cliente; }
            try { cliente.razonSocial = m.razon_social; } catch { return cliente; }
            try { cliente.direccion = m.domicilio_fiscal; } catch { return cliente; }
            try { cliente.tipoContribuyente= m.contribuyente_tipo; } catch { return cliente; }
            try { string fecha = m.fecha_inscripcion; cliente.fechaInscripcion =DateTime.Parse(fecha); } catch { return cliente; }
            try { String estado = m.contribuyente_estado; if ( estado.Equals("ACTIVO")) cliente.activoSUNAT = true; else cliente.activoSUNAT = false; } catch { return cliente; }
            return cliente;
            
            
        }
    }
}
