﻿using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.AUTH_SERVICE
{
    public interface RolService : CrudService<Rol>
    {
        Rol buscarPorNombre(string nombre);

    }
}
