﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.AUTH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION
{
  public  class RoImpl : RolService
    {
      
        public Rol buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.roles.Find(id);
            }
        }

        public Rol buscarPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    return (from o in db.roles
                            orderby o.id
                            where o.nombre == nombre
                            select o).First();

                }
                catch { return null; }
            }
        }

        public bool crear(Rol objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.roles.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
                catch { return false; }
            }
        }

        public bool editar(Rol objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Rol u = db.roles.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Rol> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from o in db.roles
                        orderby o.id
                        where o.anulado == false//.Contains(salida.ToUpper())  //SqlMethods.Like(c.Hierarchy, "%/12/%")
                        select o).ToList();
            }
        }
    }
}
