﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;

namespace SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION
{
  public  class MenuImpl : MenuService
    {
        string esquema = "auth";

        public Menuu buscar(int id)
        {
            throw new NotImplementedException();
        }

        public Usuario buscarConRol(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                try
                {
                    return (from o in db.usuarios
                            orderby o.id
                            where o.anulado == false
                            select o).First();
                }
                catch { return null; }
            }
        }

        public bool crear(Menuu objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                try
                {
                    db.menus.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
                catch { return false; }
            }
        }

    

        public bool editar(Menuu objeto)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

       
        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Usuario u = db.usuarios.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Menuu> listarNoAnulados()
        {
            throw new NotImplementedException();
        }

        Menuu CrudService<Menuu>.buscar(int id)
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return db.menus.Find();
            }
        }


        List<Menuu> CrudService<Menuu>.listarNoAnulados()
        {
            using (var db = new BaseDeDatos(esquema))
            {
                return (from o in db.menus
                        orderby o.id
                        where o.anulado == false//.Contains(salida.ToUpper())  //SqlMethods.Like(c.Hierarchy, "%/12/%")
                        select o).ToList();
            }
        }
    }
}
