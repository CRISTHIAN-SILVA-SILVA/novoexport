﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.AUTH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION
{
   public class PermisoImpl : PermisoService
    {
        public Permiso buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.permisos.FirstOrDefault(x => x.id == id);
            }
        }

        public Permiso buscarPorMenuRol(int menu, int rol)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.permisos.FirstOrDefault(x => x.idMenu == menu && x.idRol == rol);

            }
        }

        public bool crear(Permiso objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.permisos.Add(objeto);

                    db.SaveChanges();
                    return true;
                }
                catch { return false; }
            }
        }

        public bool editar(Permiso objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Permiso u = db.permisos.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public List<Permiso> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.permisos.Where(x => x.anulado == false).OrderBy(x => x.id).ToList();
                /*     return (from o in db.permisos
                             orderby o.id
                             where o.anulado == false//.Contains(salida.ToUpper())  //SqlMethods.Like(c.Hierarchy, "%/12/%")
                             select o).ToList();*/
            }
        }
        public List<Permiso> listarNoAnuladosPorRol(int rol)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from o in db.permisos
                        .Include("menu")
                        orderby o.id
                        where o.anulado == false && o.idRol == rol//.Contains(salida.ToUpper())  //SqlMethods.Like(c.Hierarchy, "%/12/%")
                        select o).ToList();
            }
        }
    }
}
