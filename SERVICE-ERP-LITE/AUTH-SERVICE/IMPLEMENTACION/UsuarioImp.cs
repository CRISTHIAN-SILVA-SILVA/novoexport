﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.AUTH;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.AUTH_SERVICE.IMPLEMENTACION
{

    public class UsuarioImp : UsuarioService
    {
        public void ejecutaFuncionPostgres(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                var numero = new Npgsql.NpgsqlParameter("@numero", 2);

                bool r = Convert.ToBoolean( db.Database.ExecuteSqlCommand("select ejemplo(@numero);", numero));
                Console.WriteLine("XXXXXXXXXXXXXXXXX"+r.ToString());
            }
        }
        public Usuario buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.usuarios.Find(id);
            }
        }

        public Usuario buscaraPorDNI(string dni)
        {

            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    return (from o in db.usuarios
                            .Include("rol")
                            orderby o.id
                            where o.dni == dni
                            select o).First();
                }
                catch { return null; }
            }
        }



        public Usuario buscarConRol(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    return (from o in db.usuarios
                            .Include("rol")
                            orderby o.id
                            where o.anulado == false && o.id == id
                            select o).First();
                }
                catch { return null; }
            }
        }

  
        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Usuario u = db.usuarios.Find(id);
                        u.anulado = true;
                        db.SaveChanges();
                        dbTransaction.Commit(); return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); //MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public Usuario iniciarSesion(string dni, string password)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    return (from o in db.usuarios
                            .Include("rol")
                            orderby o.id
                            where o.anulado == false && o.userName == dni && o.password == password
                            select o).First();
                }
                catch (Exception e) {  return null; }
            }

        }

        public List<Usuario> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from o in db.usuarios
                        .Include("rol")
                        orderby o.id
                        where o.anulado == false//.Contains(salida.ToUpper())  //SqlMethods.Like(c.Hierarchy, "%/12/%")
                        select o).ToList();
            }
        }
        bool CrudService<Usuario>.crear(Usuario objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.usuarios.Add(objeto);
                    db.SaveChanges();
                    return true;
                }
                catch { return false; }
            }
        }
        bool CrudService<Usuario>.editar(Usuario objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }
    }
    }
