﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using MODEL_ERP_LITE.CONFIGURACION;
//using LibPrintTicket;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.UTILITARIO
{
   public class TicketService
    {
        string ticket = "";
        string parte1, parte2;  
        //  string impresora = "\\\\FARMACIA-PVENTA\\Generic / Text Only"; // nombre exacto de la impresora como esta en el panel de control
    public static    string impresora = ParametroImpl.IMPRESORA.valorCadena;
      
        int max, cort;

     public  static void imprimirTicket(ComprobantePago c, Venta v,string hash) {

            QrEncoder qr = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            string tipoDocCliente = "";
            string docDocIdentidad = "";
            if (c.tipoComprobante.descripcion.ToUpper().Equals("FACTURA"))
            { docDocIdentidad = c.cliente.ruc; tipoDocCliente = CodigosSUNAT.RUC; }
            else { docDocIdentidad = c.cliente.dniRepresentante; tipoDocCliente = CodigosSUNAT.DNI; }
            string numero = Util.NormalizarCampo(c.numero.ToString(), 8);
            qr.TryEncode(LocalImpl.getInstancia().ruc+"|"+c.tipoComprobante.codigo_sunat+"|"+c.serie+"|"+numero+"|"+string.Format("{0:0.00}",v.igv)
            +"|"+string.Format("{0:0.00}",c.montoTotal)+"|"+c.fechaCreate.ToShortDateString()+"|"+tipoDocCliente+"|"+docDocIdentidad+"|"+"|", out qrCode);

            GraphicsRenderer render = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), Brushes.Black, Brushes.White);
            MemoryStream ms = new MemoryStream();
            render.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, ms);

            Bitmap imgTemp = new Bitmap(ms);
            Bitmap img = new Bitmap(imgTemp, new Size(new Point(110, 110)));
       //     img.Save("D://REPORTES//qr.png", System.Drawing.Imaging.ImageFormat.Png);


            System.Drawing.Image codigo = img;
    

            Ticket ticket = new Ticket();
            ticket.HeaderImage = codigo;
            ticket.MaxChar = 45;
            ticket.MaxCharDescription = 24;
          
            ticket.AddHeaderLine(LocalImpl.getInstancia().razonSocial);
            ticket.AddHeaderLine("");
            ticket.AddHeaderLine("");
            ticket.FontSize = 7;
            ticket.AddHeaderLine(LocalImpl.getInstancia().direccion);
            ticket.AddHeaderLine("RUC: " + LocalImpl.getInstancia().ruc);
            ticket.AddHeaderLine("CORREO: " + LocalImpl.getInstancia().email);
            ticket.AddHeaderLine("TELEFONO/CEL: " + LocalImpl.getInstancia().telefono);
            //   ticket.AddHeaderLine("PUEBLA, PUEBLA");
            ticket.AddHeaderLine("");


            ticket.AddHeaderLine(c.tipoComprobante.descripcion.ToUpper()+" ELECTRONICA   "+ c.serie + "-" + numero);
           

            ticket.AddHeaderLine("");
            ticket.AddHeaderLine("CLIENTE:");
            ticket.AddHeaderLine(c.cliente.razonSocial);
            ticket.AddHeaderLine("DIRECCION:");
            ticket.AddHeaderLine(c.cliente.direccion);
            ticket.AddHeaderLine("RUC/DNI:  "+docDocIdentidad);

            int finCadena = c.usuarioCreate.Length-9;
            ticket.AddHeaderLine("");
            ticket.AddHeaderLine("CAJERO(A):"+c.usuarioCreate.Substring(0,finCadena));
            finCadena=v.usuarioCreate.Length-9;
            ticket.AddHeaderLine("VENDEDOR(A):" + v.usuarioCreate.Substring(0,finCadena));

            ticket.AddSubHeaderLine("FECHA Y HORA:   "+c.fechaCreate.ToShortDateString() + " " + c.fechaCreate.ToShortTimeString());
           // ticket.AddHeaderLine("ser");
         
         
            ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
            ProductoPresentacion pp = null;
            foreach (var i in v.detalles)
            {
              pp=  ppservice.buscar(i.idProductoPresentacion);
                
                ticket.AddItem(string.Format("{0:0.00}", i.cantidad),pp.unidad.codComun, pp.producto.nombre+ " "+ i.observacion, string.Format("{0:0.00}", i.total));
            }
            ticket.AddTotal("OP GRAVADAS:      S/", string.Format("{0:0.00}", v.opGravadas+v.totalExonerado));
            ticket.AddTotal("IGV (18%):        S/", string.Format("{0:0.00}", v.igv));
            ticket.AddTotal("OP. EXONERADAS:   S/", string.Format("{0:0.00}", v.totalExonerado));
            ticket.AddTotal("TOTAL:            S/", string.Format("{0:0.00}", c.montoTotal));
 
            ticket.AddFooterLine(hash); ticket.AddFooterLine("");
            ticket.AddFooterLine("Son:" + Util.Convertir(string.Format("{0:0.00}", c.montoTotal),true));
            ticket.AddFooterLine("");
          ticket.  AddFooterLine("Esta es una  representacion  impresa  de  la " + c.tipoComprobante.descripcion.ToUpper() + " ELECTRONICA," +
               " generada por el sistema Sunat. Puede verificarla usando su clave SOL.");
            ticket.AddFooterLine("");
            ticket.AddFooterLine("GRACIAS POR SU COMPRA");

            ticket.PrintTicket(impresora);
        }

  
}
}
