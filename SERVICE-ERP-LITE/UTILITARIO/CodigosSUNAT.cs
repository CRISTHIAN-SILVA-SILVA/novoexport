﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.UTILITARIO
{
   public class CodigosSUNAT
    {
        public static string FACTURA = "01";
        public static string BOLETA= "03";
        public static string NOTA_CREDITO = "07";
        public static string NOTA_DEBITO = "08";
        public static string SOLES = "PEN";
        public static string DNI = "1";
        public static string RUC = "6";
    }

}
