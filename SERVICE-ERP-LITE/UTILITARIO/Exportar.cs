﻿using Microsoft.Office.Interop.Excel;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.UTILITARIO
{
   public class Exportar
    {
        public static void exportarAExcelAsync(DataGridView grd)
        {
            ThreadStart delegarProceso = new ThreadStart(() => { exportarAExcel(grd); });
            Thread subproceso = new Thread(delegarProceso);
            subproceso.Start();
        }
        public static void exportarATXTAsync(DataGridView grd, string archivo)
        {
            ThreadStart delegarProceso = new ThreadStart(() => { exportarATXT(grd,archivo); });
            Thread subproceso = new Thread(delegarProceso);
            subproceso.Start();
        }
        public static void exportarAExcel(DataGridView grd)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application aplicacion = new Microsoft.Office.Interop.Excel.Application();
                Workbook libro = aplicacion.Workbooks.Add(XlSheetType.xlWorksheet);
                Worksheet hoja = (Worksheet)aplicacion.ActiveSheet;

                int filas = grd.Rows.Count; char columnasLetras = 'A';
                int numeroColumnas = grd.Columns.Count;
                for (int h = 0; h < grd.Columns.Count; h++)
                    hoja.Cells[1, h + 1] = grd.Columns[h].HeaderText;

                columnasLetras = (char)(numeroColumnas + 64);
                hoja.Range["A1:" + columnasLetras + "1"].Font.Bold = true;
        //        hoja.Range["A1:" + columnasLetras + "1"].Interior.Color = Color.FromArgb(129, 199, 132);
                hoja.Range["A1:" + columnasLetras + "1"].BorderAround2(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous);
                for (int i = 0; i < grd.Rows.Count; i++)
                    for (int j = 0; j < grd.Columns.Count; j++)
                        if ((grd.Rows[i].Cells[j].Value == null) == false)
                        {
                            hoja.Cells[i + 2, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                aplicacion.Visible = true;
                hoja.Range["A1:" + columnasLetras + filas + 1.ToString()].Columns.AutoFit();
                //  hoja.Range["A1:" + columnasLetras + filas.ToString()].AutoFilter(1);


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al exportar la informacion debido a: " + ex.ToString());
            }

        }

        public static void exportarATXT(DataGridView g,string archivo)
        {
            String SEPARADOR = "|";
                try
                {
                    string RUTA = @"D:\LIBROS_CONTABLES";
                   
                    string rutaTxt = @RUTA + @"\" + archivo;
                    FileInfo fileInfo = new FileInfo(rutaTxt);
                    if (fileInfo.Exists) fileInfo.Delete();
                    string txt = "";
                    using (StreamWriter sw = fileInfo.CreateText())
                    {
                    for (int i = 0; i < g.Rows.Count; i++)
                    {
                        txt = "";
                        for (int j = 0; j < g.Columns.Count; j++)
                            if ((g.Rows[i].Cells[j].Value == null) == false)
                            { txt = txt + g.Rows[i].Cells[j].Value.ToString() + SEPARADOR; }
                        
                        sw.WriteLine(
                                    txt
                                );
                    }
                }
                        

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }   
    }
}
