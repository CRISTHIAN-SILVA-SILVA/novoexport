﻿  using MODEL_ERP_LITE.CONFIGURACION;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.INVENTARIO;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using SERVICE_ERP_LITE.FACTURACION_SERVICE;
using SERVICE_ERP_LITE.FACTURACION_SERVICE.FACTURACION_IMPLEMENTACION;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE;
using SERVICE_ERP_LITE.INVENTARIO_SERVICE.INVENTARIO_IMPLEMENTACION;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
namespace SERVICE_ERP_LITE.UTILITARIO{
  public  class TxtSUNAT  {
        private static string SEPARADOR = "|";
        private static string RUTA = @"D:\SFS_v1.2\sunat_archivos\sfs\DATA";
      
       
        public static void ConstruirComprobante(ComprobantePago factura) {
              try {  // RUC-01-Serie-Numero.CAB.txt
                Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
                string docCliente= factura.cliente.dniRepresentante, tipoDocUser=CodigosSUNAT.DNI;
                if (factura.tipoComprobante.descripcion.ToUpper().Equals("FACTURA"))
                { docCliente = factura.cliente.ruc; tipoDocUser = CodigosSUNAT.RUC; }
                string filename = LocalImpl.getInstancia().ruc +"-"+factura.tipoComprobante.codigo_sunat+"-"+factura.serie+"-" +Util.NormalizarCampo(factura.numero.ToString(),8)+".CAB";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
                   using (StreamWriter sw = fileInfo.CreateText()) { sw.WriteLine(
                   "0101" +SEPARADOR+                                                               // 1  CODIGO DE TIPO DE OPERACION CATALOGO 51 (VENTA INTERNA) 
                   factura.fechaCreate.ToString("yyyy-MM-dd") + SEPARADOR +                         // 2  FECHA DE EMISION
                   factura.fechaCreate.ToString("hh:mm:ss") + SEPARADOR +                           // 3  HORA DE EMISION
                   factura.fechaVencimiento.ToString("yyyy-MM-dd") + SEPARADOR+                     // 4  FECHA DE VENCIMIENTO
                   "0"+ SEPARADOR +                                                                 // 5  CODIGO DE DOMICILIO FISCAL O DE LOCAL ANEXO DEL EMISOR
                   tipoDocUser + SEPARADOR +                                                        // 6  TIPO DE DOC USUARIO
                   docCliente + SEPARADOR +                                                         // 7  NUMERO DOC USUARIO
                   factura.cliente.razonSocial.Replace('&', 'y') + SEPARADOR +                      // 8  RAZON SOCIAL DEL CLIUNTE
                   CodigosSUNAT.SOLES + SEPARADOR +                                                 // 9  TIPO MONEDA 
                   string.Format("{0:0.00}", factura.venta.igv) + SEPARADOR +                       // 10 SUMATORIA DE TRIBUTOS
                   string.Format("{0:0.00}", factura.venta.opGravadas+factura.venta.totalExonerado) + SEPARADOR + // 11 TOTAL VALOR VENTA
                   string.Format("{0:0.00}", factura.montoTotal) + SEPARADOR +                                    // 12 TOTAL PRECIO VENTA 
                   string.Format("{0:0.00}", factura.descuentosGlobales) + SEPARADOR +                            // 13 TOTAL DESCUENTOS 
                   string.Format("{0:0.00}", 0) + SEPARADOR +                                                     // 14 OTRSO CARGOS
                   string.Format("{0:0.00}", 0) + SEPARADOR +                                                     // 15 TOTAL ANTICIPO
                   string.Format("{0:0.00}", factura.montoTotal-factura.descuentosGlobales) + SEPARADOR +         // 16 IMPORTE TOTAL
                   "2.1"  + SEPARADOR   +    "2.0" + SEPARADOR
                ); }
                ConstruirDetalleFactura(factura,igv);
                construirDocAdicionalFactura(factura);
              //  construirLeyenda(factura);
                construirTRI(factura);         
            } catch (Exception ex) { MessageBox.Show("1"+ex.Message);}
        }
        public static void ConstruirDetalleFactura(ComprobantePago comprobante,Parametro igv) {
            try {
                string filename = LocalImpl.getInstancia().ruc + "-" + comprobante.tipoComprobante.codigo_sunat + "-" + comprobante.serie + "-" + Util.NormalizarCampo(comprobante.numero.ToString(), 8) + ".DET";// RUC + "-" + documento.tipoComprobante + "-" + documento.serie.Substring(documento.nuevaSerie.Length - 4) + "-" + Utilitario.NormalizarCampo(documento.nuevoNumero, 8) + ".DET";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
                DetalleVentaImpl dservice = new DetalleVentaImpl();
                List<DetalleVenta> detalles = dservice.listarPorVenta(comprobante.idVenta);
                using (StreamWriter sw = fileInfo.CreateText()) {
                    foreach (DetalleVenta docDetalle in detalles) {
                        ProductoPresentacionService ppservice = new ProductoPresentacionImpl();
                        ProductoPresentacion presentacion =  ppservice.buscar(docDetalle.idProductoPresentacion);       
                        double porcentajeTrubuto = igv.valorDouble;
                        double baseImponible = docDetalle.total / (1 + igv.valorDouble);
                        double valorUnitario = docDetalle.precio / (1 + igv.valorDouble);
                        double igvv = docDetalle.igv;
                        double gratuito = 0;
                        if (docDetalle.total == 0) {  porcentajeTrubuto = 0;gratuito = docDetalle.precio; }
                        else if(docDetalle.tipoAfectacionIgv.exonerado) {
                            valorUnitario = docDetalle.precio;
                            baseImponible = docDetalle.total;
                            porcentajeTrubuto = 0;igvv = 0; }
                        if (docDetalle.igv == 0) { }
                        sw.WriteLine(
                         presentacion.unidad.codigo + SEPARADOR +                      // 1  UNIDAD MEDIDA CODIGO
                             //  "NIU" + SEPARADOR +
                         string.Format("{0:0.00}", docDetalle.cantidad*docDetalle.factor) + SEPARADOR +  // 2  CANTIDAD DE UNIDADES
                         presentacion.id + SEPARADOR +                                 // 3  CODIGO DE PRODUCTO
                         "-" + SEPARADOR +                                             // 4  CODIGO DE  PRODUCTO SUNAT
                         presentacion.nombre + SEPARADOR +                             // 5  DESCRIPCION PRODUCTO
                         string.Format("{0:0.00000}", valorUnitario/docDetalle.factor) + SEPARADOR +               // 5  VALOR UNITARIO
                         string.Format("{0:0.00}", igvv) + SEPARADOR +                 // 6 SUMATORIA DE TRIBUTOS POR ITEM
                         docDetalle.tipoAfectacionIgv.codigoTributo + SEPARADOR +                                                        // 7  COD TIPOS TRIBUTOS IGV TABLA 5
                         string.Format("{0:0.00}", igvv ) + SEPARADOR +                                         // 8  MONTO IGV
                         string.Format("{0:0.00}",baseImponible    ) + SEPARADOR +                              // 9  BASE IMPONIBLE IGV TBL5
                         docDetalle.tipoAfectacionIgv.nombreTributo+SEPARADOR+
                         docDetalle.tipoAfectacionIgv.codigoInternacionalTributo+SEPARADOR+                          // 10 NOMBRE TIPO TRIB TBL5 Y COD TIPO TRIB TBL5
                         docDetalle.tipoAfectacionIgv.codigo+SEPARADOR+string.Format("{0:0.00}",porcentajeTrubuto*100)+SEPARADOR+  // 12 AFCTACION AL IGV TABLA 7 Y % DE IGV 18.00
                                "-" + SEPARADOR +"" + SEPARADOR +
                         "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" 
                         + SEPARADOR + "" + SEPARADOR +
                                "-" + SEPARADOR +"" + SEPARADOR +"" + SEPARADOR +
                         "" + SEPARADOR + "" + SEPARADOR + ""  + SEPARADOR +
                         string.Format("{0:0.00000}", docDetalle.precio/docDetalle.factor) +SEPARADOR+   // 28 PRECIO DE VENTA UNITARIO    
                         string.Format("{0:0.00}", baseImponible) + SEPARADOR+      // 29 VALOR DE VENTA POR ITEM
                         string.Format("{0:0.00000}", gratuito/docDetalle.factor) + SEPARADOR); } } }    // 30 VALOR REFERENCIAL UNITARIO GRATUITOS
            catch (Exception ex) { MessageBox.Show("2" + ex.Message); }  }


        public static void construirLeyenda(ComprobantePago c){
            try
            {
                string filename = LocalImpl.getInstancia().ruc + "-" + c.tipoComprobante.codigo_sunat + "-" + c.serie + "-" + Util.NormalizarCampo(c.numero.ToString(), 8) + ".LEY";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();

                using (StreamWriter sw = fileInfo.CreateText())
                {
                    sw.WriteLine(
                        "1000" + SEPARADOR + 
                      Util.Convertir( string.Format("{0:0.00}", c.montoTotal),true) + SEPARADOR 
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        public static void construirTRI(ComprobantePago c)
        {
            try
            {
                string filename = LocalImpl.getInstancia().ruc + "-" + c.tipoComprobante.codigo_sunat + "-" + c.serie + "-" + Util.NormalizarCampo(c.numero.ToString(), 8) + ".TRI";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();

                using (StreamWriter sw = fileInfo.CreateText())
                {
                    sw.WriteLine(
                        "1000" + SEPARADOR +"IGV"+SEPARADOR+"VAT"+SEPARADOR + string.Format("{0:0.00}", c.venta.opGravadas)+SEPARADOR+
                      string.Format("{0:0.00}",c.venta.igv) + SEPARADOR
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        public static void construirTRINota(Nota c,Parametro igv)
        {
            try
            {
                string filename = LocalImpl.getInstancia().ruc + "-" + c.tipoComprobante.codigo_sunat + "-" + c.serie + "-" + Util.NormalizarCampo(c.numero.ToString(), 8) + ".TRI";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();

                using (StreamWriter sw = fileInfo.CreateText())
                {
                    sw.WriteLine(
                       "1000" + SEPARADOR + "IGV" + SEPARADOR + "VAT" + SEPARADOR + string.Format("{0:0.00}", c.total/(1+igv.valorDouble)) + SEPARADOR +
                      string.Format("{0:0.00}", c.igv) + SEPARADOR
                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        public static void construirGuia(GuiaRemision guia)
        {
            try
            {
                ComprobantePagoService cservice = new ComprobantePagoImpl();
                ComprobantePago c = cservice.buscar(guia.idComprobante);
                string filename = LocalImpl.getInstancia().ruc + "-" + c.tipoComprobante.codigo_sunat + "-" + c.serie+"-"+Util.NormalizarCampo(c.numero.ToString(),8)+ ".REL";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
               

                string tipodocIdentidad = CodigosSUNAT.BOLETA;
              //  if (c.tipoComprobante.descripcion.ToUpper() == "FACTURA") {
                    tipodocIdentidad = CodigosSUNAT.FACTURA;
               // }
                using (StreamWriter sw = fileInfo.CreateText())
                {
                    sw.WriteLine(

                        "1" + SEPARADOR +"-" + SEPARADOR +c.tipoComprobante.codigo_sunat + SEPARADOR+ //tipo doc - ndoc identidad -monto
                       guia.serie+"-"+Util.NormalizarCampo(guia.numero.ToString(),8)+SEPARADOR+
                       tipodocIdentidad+SEPARADOR+
                       LocalImpl.getInstancia().ruc +SEPARADOR+
                       string.Format("{0:0.00}",guia.monto)+SEPARADOR
                        //guia.tipDocRelacionado + SEPARADOR +  // solo cuando es nota de debito si no sera null
                       // guia.numDocRelacionado + SEPARADOR +
                       // guia.tipDocEmisor + SEPARADOR +
                       // guia.numDocEmisor + SEPARADOR +
                        //string.Format("{0:0.00}", guia.mtoDocRelacionado) + SEPARADOR

                    );
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        public static void construirDocAdicionalFactura(ComprobantePago doc)
        {
            try
            {
                string filename =LocalImpl.getInstancia().ruc + "-" +doc.tipoComprobante.codigo_sunat + "-" + doc.serie + "-"+Util.NormalizarCampo(doc.numero.ToString(), 8) + ".ACA";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
                using (StreamWriter sw = fileInfo.CreateText())  {
                    sw.WriteLine(
                        "" + SEPARADOR +
                        "" + SEPARADOR +  // solo cuando es nota de debito si no sera null
                        "" + SEPARADOR +
                        "" + SEPARADOR +
                       "" + SEPARADOR +
                       
                        "PE" + SEPARADOR +
                        "20" + SEPARADOR +
                       doc.cliente.direccion.Replace('|', '/') + SEPARADOR +

                        "-" + SEPARADOR +
                        "20" + SEPARADOR +
                        "-" + SEPARADOR);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        public static void construirDocAdicionalNota(Nota doc)
        {
            try
            {
                string filename = LocalImpl.getInstancia().ruc + "-" + doc.tipoComprobante.codigo_sunat + "-" + doc.serie + "-" + Util.NormalizarCampo(doc.numero.ToString(), 8) + ".ACA";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
                using (StreamWriter sw = fileInfo.CreateText())
                {
                    sw.WriteLine(
                        "" + SEPARADOR +
                        "" + SEPARADOR +  // solo cuando es nota de debito si no sera null
                        "" + SEPARADOR +
                        "" + SEPARADOR +
                       "" + SEPARADOR +

                        "PE" + SEPARADOR +
                        "20" + SEPARADOR +
                       doc.cliente.direccion + SEPARADOR +

                        "-" + SEPARADOR +
                        "20" + SEPARADOR +
                        "-" + SEPARADOR);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        public static void construirDocAdicionalDetalle(ComprobantePago doc,Parametro igv)
        {
            if (doc != null)
            {
                try
                {
                    //Composicion del Nombre del archivo RUC-01-Serie-Numero.NOT.txt
                    string filename = LocalImpl.getInstancia().ruc + "-" +doc.tipoComprobante.codigo_sunat + "-" + doc.serie + Util.NormalizarCampo(doc.numero.ToString(), 8) + ".ADE";
                    string rutaTxt = @RUTA + @"\" + filename;
                    FileInfo fileInfo = new FileInfo(rutaTxt);
                    if (fileInfo.Exists) fileInfo.Delete();
                    using (StreamWriter sw = fileInfo.CreateText())
                    {
                        foreach (DetalleVenta docDetalle in doc.venta.detalles)
                        {
                            string opGratuitas = "";

                            if (docDetalle.total == 0) opGratuitas = string.Format("{0:0.00}", docDetalle.precio /( 1+igv.valorDouble));

                            sw.WriteLine(
                              opGratuitas + SEPARADOR +
                                "" + SEPARADOR
                            );
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public static void ConstruirNotaCreditoDebito(Nota nota, ComprobantePago cp, bool esCredito)
        { string tipDocUsuario = CodigosSUNAT.RUC;
            string tipDocAfectado = cp.tipoComprobante.codigo_sunat;
            string numDocUsuario = nota.cliente.ruc;
            if (cp.tipoComprobante.codigo_sunat.Equals(CodigosSUNAT.BOLETA))
            { numDocUsuario = nota.cliente.dniRepresentante; tipDocUsuario = CodigosSUNAT.DNI; }
         

            Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");
            string nombreArchivo = LocalImpl.getInstancia().ruc + "-" + nota.tipoComprobante.codigo_sunat+ "-" + nota.serie + "-" +Util.NormalizarCampo( nota.numero.ToString(),8) + ".NOT";
            try {
                string rutaTxt = @RUTA + @"\" + nombreArchivo;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
                string  codMotivo = nota.tipoNota.codigo_sunat;
                using (StreamWriter sw = fileInfo.CreateText())
                {
                    sw.WriteLine(
                        "0101"+SEPARADOR+
                        nota.fechaCreate.ToString("yyyy-MM-dd") + SEPARADOR +nota.fechaCreate.ToString("hh:mm:ss") + SEPARADOR +
                     ""+SEPARADOR + tipDocUsuario + SEPARADOR +
                                     numDocUsuario + SEPARADOR +   
                                      nota.cliente.razonSocial + SEPARADOR +CodigosSUNAT.SOLES+SEPARADOR+
                      
                                      codMotivo + SEPARADOR +
                       nota.tipoNota.descripcion+"-"+ nota.motivo+SEPARADOR+
                        
                        tipDocAfectado + SEPARADOR +
                        nota.comprobante.serie+"-"+Util.NormalizarCampo(nota.comprobante.numero.ToString(),8) + SEPARADOR +

                            string.Format("{0:0.00}", nota.igv) + SEPARADOR +

                        string.Format("{0:0.00}",nota.total/(1+igv.valorDouble)) + SEPARADOR +
                        string.Format("{0:0.00}", nota.total) + SEPARADOR +

                        string.Format("{0:0.00}", 0) + SEPARADOR +//descuentos
                         string.Format("{0:0.00}", 0) + SEPARADOR +//ptros cargos
                          string.Format("{0:0.00}", 0) + SEPARADOR +//anticipos

                          
                    
                        string.Format("{0:0.00}", nota.total) + SEPARADOR+"2.1"+SEPARADOR+"2.0"+SEPARADOR
                    );
                }
                ConstruirDetalleNota(nota); 
                construirTRINota(nota, igv);

                construirDocAdicionalNota(nota);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        public static void ConstruirDetalleNota(Nota nota)
        {
            try {
                Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");

                string filename = LocalImpl.getInstancia().ruc + "-" + nota.tipoComprobante.codigo_sunat + "-" + nota.serie + "-" + Util.NormalizarCampo(nota.numero.ToString(), 8) + ".DET";// RUC + "-" + documento.tipoComprobante + "-" + documento.serie.Substring(documento.nuevaSerie.Length - 4) + "-" + Utilitario.NormalizarCampo(documento.nuevoNumero, 8) + ".DET";
                string rutaTxt = @RUTA + @"\" + filename;
                FileInfo fileInfo = new FileInfo(rutaTxt);
                if (fileInfo.Exists) fileInfo.Delete();
                //MIL|10.00|25|-|PAPEL BOND A4 MILLARES FABER CASTELL|7.63|13.73|1000|13.73|76.27|IGV|VAT|10|18.00|-|||||||-||||||9.00|76.27|0.00|
                DetalleNotaService dnservice = new DetalleNotaImpl();
                using (StreamWriter sw = fileInfo.CreateText())  {
                    ProductoPresentacionService pService = new ProductoPresentacionImpl();
                    List<DetalleNota> detalles = dnservice.listarPorNota(nota.id);
                    foreach (DetalleNota docDetalle in detalles) {
                        
                        ProductoPresentacion pp  = pService.buscar(docDetalle.idPresentacion);
                   
                        double porcentajeTrubuto = igv.valorDouble;
                        double baseImponible = docDetalle.total / (1 + igv.valorDouble);
                        double igvv = docDetalle.igv; double gratuito = 0;
                        if (docDetalle.igv == 0)  {
                            baseImponible = docDetalle.total; 
                            porcentajeTrubuto = 0; igvv = 0;  }
                        if (docDetalle.igv == 0) { } 
                        sw.WriteLine(
                              pp.unidad.codigo + SEPARADOR +
                         //   "NIU" + SEPARADOR +
                            string.Format("{0:0.00}", docDetalle.cantidad*docDetalle.factor) + SEPARADOR +
                            docDetalle.idPresentacion+ SEPARADOR +
                            "-" + SEPARADOR +      
                            pp.nombre + SEPARADOR +
                            string.Format("{0:0.00000}", (docDetalle.precio /(1+igv.valorDouble))/docDetalle.factor ) + SEPARADOR +
                            string.Format("{0:0.00}",igvv)+SEPARADOR+
                             docDetalle.tipoAfectacionIgv.codigoTributo + SEPARADOR +
                                 string.Format("{0:0.00}", igvv) + SEPARADOR +
                                   string.Format("{0:0.00}", baseImponible) + SEPARADOR +
                                    docDetalle.tipoAfectacionIgv.nombreTributo+ SEPARADOR + docDetalle.tipoAfectacionIgv.codigoInternacionalTributo + SEPARADOR +
                            docDetalle.tipoAfectacionIgv.codigo + SEPARADOR + String.Format("{0:0.00}", porcentajeTrubuto*100) + SEPARADOR +
                        "-" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR +
                       "-" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + "" + SEPARADOR + 

              

                   string.Format("{0:0.00000}", docDetalle.precio/docDetalle.factor) +SEPARADOR+
                     string.Format("{0:0.00}",baseImponible/docDetalle.factor) + SEPARADOR +


                     string.Format("{0:0.00000}", gratuito) + SEPARADOR
                        );
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message + " " + ex.StackTrace.ToString());

            }
        }







    }
}
