﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.UTILITARIO
{
   public class CambioMoneda
    {

        private static DataTable TABLA_DOLAR =null;
        public static DataTable getTablaDolar() {
            if (TABLA_DOLAR == null)
            {
               TABLA_DOLAR=cambioDolar();
                    return TABLA_DOLAR; }
            else return TABLA_DOLAR;
        }
        public static DataTable cambioDolar()
        {
            string ultimaCompra = "0";
            try
            {

                string sURL = "http://e-consulta.sunat.gob.pe/cl-at-ittipcam/tcS01Alias";
                Encoding objetoEncondig = Encoding.GetEncoding("ISO-8859-1");
                CookieCollection objetoCokies = new CookieCollection();
                HttpWebRequest getRequest = (HttpWebRequest)WebRequest.Create(sURL);
                getRequest.Method = "GET";
                getRequest.CookieContainer = new CookieContainer();
                getRequest.CookieContainer.Add(objetoCokies);
                string sGetResponse = string.Empty;
                using (HttpWebResponse getResponse = (HttpWebResponse)getRequest.GetResponse())
                {
                    objetoCokies = getResponse.Cookies;
                    using (StreamReader srGetResponse = new StreamReader(getResponse.GetResponseStream(), objetoEncondig))
                    {
                        sGetResponse = srGetResponse.ReadToEnd();
                    }
                    HtmlAgilityPack.HtmlDocument documento = new HtmlAgilityPack.HtmlDocument();
                    documento.LoadHtml(sGetResponse);
                    HtmlNodeCollection nodeTR = documento.DocumentNode.SelectNodes("//table[@class='class=\"form-table\"']//tr");

                    DataTable tabla = new DataTable();
                    if (nodeTR != null)
                    {
                  
                         tabla.Columns.Add("Día", typeof(string));
                         tabla.Columns.Add("Compra", typeof(string));
                         tabla.Columns.Add("Venta", typeof(string));
                        int iNumFila = 0;

                        foreach (HtmlNode Node in nodeTR)
                        {
                            if (iNumFila > 0)
                            {
                                int iNumColumna = 0;
                                DataRow dr = tabla.NewRow();
                                foreach (HtmlNode subNode in Node.Elements("td"))
                                {
                                         if (iNumColumna == 0) dr = tabla.NewRow();
                                    string sValue = subNode.InnerHtml.ToString().Trim();
                                    sValue = System.Text.RegularExpressions.Regex.Replace(sValue, "<.*?>", " ");
                                    dr[iNumColumna] = sValue;
                                    iNumColumna++;
                                    if (iNumColumna == 2) ultimaCompra = sValue;
                                    if (iNumColumna == 3)
                                    {tabla.Rows.Add(dr);
                                        iNumColumna = 0;
                                    }
                                }
                            }
                            iNumFila++;

                        }
                    }
                    return tabla;
                }
            }
            catch { return null; }
           
        }
    }
}