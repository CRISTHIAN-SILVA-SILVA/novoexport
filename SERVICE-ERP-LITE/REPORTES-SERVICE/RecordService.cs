﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.REPORTES_SERVICE
{
  public  class RecordService
    {

        public List<DetalleRecordVendedor> listaVendedores(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from
                     //   c in db.clientes  join
                        v in db.ventas
                     //   on c.id equals v.idCliente

                        where  v.anulado == false  &&v.fechaCreate>=inicio&&v.fechaCreate<fin// && c.anulado == false
                        group v by new { v.usuarioCreate } into grupo
                        select new DetalleRecordVendedor
                        {
                            vendedor = grupo.Key.usuarioCreate,
                            numeroVentas = grupo.Count(),
                            montoVendido = grupo.Sum(x=>x.montoTotal),
                        }
                      ).ToList();
            }
        }
        public List<DetalleRecordCliente> listaClientes(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from
                        c in db.clientes join
                       v in db.ventas
                       on c.id equals v.idCliente

                        where v.anulado == false&&c.anulado==false && v.fechaCreate >= inicio && v.fechaCreate < fin&& c.esEmpresa==true// && c.anulado == false
                        group v by new { v.usuarioCreate, c.razonSocial, c.ruc } into grupo
                        select new DetalleRecordCliente
                        {
                            cliente = grupo.Key.razonSocial,
                            documento = grupo.Key.ruc,    
                            numeroVentas = grupo.Count(),
                            monto= grupo.Sum(x => x.montoTotal),
                        }
                      ).ToList();
            }
        }

        public List<DetalleRecordCliente> listaProveedores(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from
                        c in db.proveedores
                        join
      v in db.compras
      on c.id equals v.idProveedor

                        where v.anulado == false && v.fechaEmision >= inicio && v.fechaEmision < fin && c.anulado == false// && c.anulado == false
                        group v by new { v.usuarioCreate, c.razonSocial, c.ruc } into grupo
                        select new DetalleRecordCliente
                        {
                            cliente = grupo.Key.razonSocial,
                            documento = grupo.Key.ruc,
                            numeroVentas = grupo.Count(),
                            monto = grupo.Sum(x => x.montoTotal*x.cambio),
                        }
                      ).ToList();
            }
        }

    }
}
