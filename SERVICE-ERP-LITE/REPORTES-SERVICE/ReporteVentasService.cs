﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.PLANTILLAS_REPORTES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.REPORTES_SERVICE
{
   public class ReporteVentasService
    {
        public List<DetalleRVenta> listaComprobantes(DateTime inicio, DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from
                           c in db.clientes  join
                        v in db.comprobantesPago
                               on c.id equals v.idCliente

                        where v.anulado == false && v.fechaCreate >= inicio && v.fechaCreate < fin// && c.anulado == false
                        select new DetalleRVenta
                        {
                            fecha = v.fechaCreate,
                            cliente = c.razonSocial,
                            serie = v.serie,
                            numero =v.numero,
                            importe =v.montoTotal,

                        }
                      ).ToList();
            }
        }

    }
}
