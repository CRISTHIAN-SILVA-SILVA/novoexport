﻿using MODEL_ERP_LITE.PEDIDO;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.PEDIDO_SERVICE
{
 public   interface DetallePedidoService:CrudService<DetallePedido>
    {
        List<DetallePedido> listarPorPedido(int pedido);
    }
}
