﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION
{
    public class DetalleCotizacionImpl : DetalleCotizacionService
    {
        public DetalleCotizacion buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return db.detallesCotizacion.Include("producto").FirstOrDefault(x => x.id == id);
            }
        }

        public bool crear(DetalleCotizacion objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {

                db.detallesCotizacion.Add(objeto);
                db.SaveChanges();
                return true;
            }
        }

        public bool editar(DetalleCotizacion objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<DetalleCotizacion> listarNoAnulados()
        {
            throw new NotImplementedException();
        }

        public List<DetalleCotizacion> listarPorCotizacion(int cotizacion)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return db.detallesCotizacion.Include("producto").Where(x => x.idCotizacion == cotizacion && x.anulado == false).ToList();

            }
        }
    }
}
