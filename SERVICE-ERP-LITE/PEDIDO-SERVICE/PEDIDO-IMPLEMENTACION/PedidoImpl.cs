﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;
using MODEL_ERP_LITE.PEDIDO;

namespace SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION
{
    public class PedidoImpl : PedidoService
    {
        public Pedido buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                
                 return   db.pedidos.Include("cliente").FirstOrDefault(x=>x.id==id);
             
            }
        }

        public bool crear(Pedido objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //actualizar numero pedido
                        DocumentoLocal d = db.documentosLocales.FirstOrDefault(x => x.nombre == "PEDIDO");
                    
                        if (d != null)
                        {
                            
                           if (d.fecha.ToShortDateString().Equals(DateTime.Now.ToShortDateString()))
                            {
                                objeto.numeroPedido = d.numero;
                                d.numero = d.numero + 1;
                          
                               
                            }
                            else {
                                objeto.numeroPedido = 1;
                                d.numero = 2;
                         
                           }
                            d.fecha = DateTime.Now;

                        }
                        else { dbTransaction.Rollback(); return false; }
                        db.pedidos.Add(objeto);
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch(Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                          return false; }
                }
            }
        }

        public bool editar(Pedido objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Pedido> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                DateTime hoy = DateTime.Parse(DateTime.Now.ToShortDateString());
                return db.pedidos.Include("cliente").Where(x => x.anulado == false && x.fechaCreate >= hoy && x.completado == false ).ToList();
            }
        }   
        public List<Pedido> listarDiarioPorVendedor(int idVendedor)
        {
            
            using (var db = new BaseDeDatos("public"))
            {
                DateTime hoy = DateTime.Parse(DateTime.Now.ToShortDateString());
                return db.pedidos.Include("cliente").OrderByDescending(x => x.id).Where(x => x.anulado == false && x.fechaCreate >= hoy && x.completado == false && x.idVendedor == idVendedor).ToList();
            }
        }
    }
}
