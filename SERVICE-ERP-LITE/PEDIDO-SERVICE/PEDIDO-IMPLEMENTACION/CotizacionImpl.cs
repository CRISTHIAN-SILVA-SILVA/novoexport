﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE;
using MODEL_ERP_LITE.FACTURACION;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION
{
    public class CotizacionImpl : CotizacionService
    {
        public Cotizacion buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return db.cotizaciones.Include("cliente").Include("tipoPago").Include("detalles").FirstOrDefault(x => x.id == id);

            }
        }

        public bool crear(Cotizacion objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //actualizar numero pedido
                        DocumentoLocal d = db.documentosLocales.FirstOrDefault(x => x.nombre == "COTIZACION");

                        if (d != null)
                        {
                            objeto.serie = d.serie;
                         
                                objeto.numero= d.numero;
                                d.numero = d.numero + 1;


                             
                            d.fecha = DateTime.Now;

                        }
                        else { dbTransaction.Rollback(); return false; }
                        db.cotizaciones.Add(objeto);
                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool editar(Cotizacion objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<Cotizacion> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
               DateTime hoy = DateTime.Parse(DateTime.Now.AddMonths(-3).ToShortDateString());
                return db.cotizaciones.Include("cliente").Include("tipoPago").Where(x => x.anulado == false && x.fechaCreate >= hoy).ToList();
            }
        }
    }
}
