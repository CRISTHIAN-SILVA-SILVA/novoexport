﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.PEDIDO;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.PEDIDO_SERVICE.PEDIDO_IMPLEMENTACION
{
    public class DetallePedidoImpl : DetallePedidoService
    {
        public DetallePedido buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {

               return db.detallesPedidos.FirstOrDefault(x=>x.id==id);
            }
        }

        public bool crear(DetallePedido objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {

               db.detallesPedidos.Add(objeto);
                db.SaveChanges();
                return true;
            }
        }

        public bool editar(DetallePedido objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                try
                {
                    db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); return true;
                }
                catch { return false; }
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<DetallePedido> listarNoAnulados()
        {
            throw new NotImplementedException();
        }

        public List<DetallePedido> listarPorPedido(int pedido)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return db.detallesPedidos.Include("producto").Where(x => x.idPedido ==pedido&&x.anulado==false).ToList();

            }
        }
    }
}
