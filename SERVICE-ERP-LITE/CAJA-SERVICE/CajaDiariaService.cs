﻿using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CAJA_SERVICE
{
   public interface CajaDiariaService:CrudService<CajaDiaria>
    {
        int getCaja();
        CajaDiaria buscarPorDia(DateTime dia);
    }
}
