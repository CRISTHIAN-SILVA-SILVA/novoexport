﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION
{
    public class MovimientoCajaImpl : MovimientoCajaService
    {
        public MovimientoCaja buscar(int id)
        {
            throw new NotImplementedException();
        }
        public List< MovimientoCaja> listarPorCaja(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.movimientosCaja.Include("medioPago").Where(x => x.idDiaCaja == id).ToList();
            }
        }
        public List<JoinMovCajaCliente> listarPorCLiente200(int idCliente)
        {
            using (var db = new BaseDeDatos("public"))
            {

                return (from
                       p in db.clientes
                        join
 c in db.comprobantesPago
         on p.id equals c.idCliente
                        join
                        m in db.movimientosCaja
               on c.id equals m.idComprobante

                        join
                               me in db.mediosPago
                      on m.idMedioPago equals me.id
                        where m.esPorVenta==true&& m.anulado == false&& p.id ==idCliente 
                      orderby  c.fechaCreate
                        select new JoinMovCajaCliente
                        {
                             idMov=m.id, comprobante=m.serieNumeroComprobante,fechaMov=m.fecha, monto=m.monto,medioPago=me.nombre
                        }
                   ).Take(50).ToList();
            }
        }
        public List<MovimientoCaja> listarVentas(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.movimientosCaja.Include("medioPago").OrderBy(x => x.fecha).Where(x =>x.fecha>=inicio&&x.fecha<fin&&x.esPorVenta).ToList();
            }
        }
        public List<MovimientoCaja> listarVenta(int comprobante,DateTime fechaLimite)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.movimientosCaja.Include("medioPago").OrderBy(x => x.fecha).Where(x => x.idComprobante==comprobante && x.esPorVenta&&x.fecha<fechaLimite).ToList();
            }
        }
        public bool crear(MovimientoCaja objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(MovimientoCaja objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public List<MovimientoCaja> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
