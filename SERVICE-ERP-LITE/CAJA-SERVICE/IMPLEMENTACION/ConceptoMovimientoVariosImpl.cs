﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE;

namespace SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION
{
    public class ConceptoMovimientoVariosImpl : ConceptoMovimientoVariosService
    {
        public ConceptoMovimientoVarios buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.conceptosMovimientosVarios.FirstOrDefault(x => x.id ==id);
            }
        }

        public bool crear(ConceptoMovimientoVarios objeto)
        {
            using (var db = new BaseDeDatos("public"))
            {
                db.conceptosMovimientosVarios.Add(objeto);
                db.SaveChanges();
                return true;
            }
        }

        public bool editar(ConceptoMovimientoVarios objeto)
        {
            using (var db = new BaseDeDatos(""))
            {
                db.Entry(objeto).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges(); return true;
            }
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public ConceptoMovimientoVarios existe(string descripcion)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.conceptosMovimientosVarios.FirstOrDefault(x => x.descripcion == descripcion);
            }
        }

        public List<ConceptoMovimientoVarios> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.conceptosMovimientosVarios.OrderByDescending(x=>x.id).Where(x => x.anulado==false).ToList();
            }
        }
    }
}
