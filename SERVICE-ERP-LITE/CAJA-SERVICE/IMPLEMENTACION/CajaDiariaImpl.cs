﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE;
using System.Windows.Forms;

namespace SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION
{
    public class CajaDiariaImpl : CajaDiariaService
    {
        public CajaDiaria buscar(int id)
        {
            throw new NotImplementedException();
        }

        public CajaDiaria buscarPorDia(DateTime dia)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.cajasDiarias.Include("movimientos").FirstOrDefault(x=>x.dia==dia);
            }
        }

        public bool crear(CajaDiaria objeto)
        {
            throw new NotImplementedException();
        }

        public bool editar(CajaDiaria objeto)
        {
            throw new NotImplementedException();
        }

        public bool eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public int getCaja()
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        DateTime hoy = DateTime.Parse(DateTime.Now.ToShortDateString());
                        CajaDiaria cajaHoy = db.cajasDiarias.FirstOrDefault(x=>x.fecha>=hoy);
                        if (cajaHoy == null)
                        {
                            CajaDiaria cajaAnterior = db.cajasDiarias.OrderByDescending(x=>x.id).FirstOrDefault();
                            cajaHoy = new CajaDiaria
                            {
                                estaAbierta = true,
                                dia=DateTime.Now,
                            };
                            if (cajaAnterior !=null)
                            {
                                cajaHoy.montoInicial = cajaAnterior.montoFinal;
                                cajaHoy.montoFinal = cajaHoy.montoInicial;
                                cajaAnterior.estaAbierta = false;
                            }
                            db.cajasDiarias.Add(cajaHoy);
                            db.SaveChanges();
                        }                        
                        dbTransaction.Commit();
                        return cajaHoy.id;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return 0;
                    }
                }
            }
        }

        public List<CajaDiaria> listarNoAnulados()
        {
            throw new NotImplementedException();
        }
    }
}
