﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MODEL_ERP_LITE.CAJA;
using MODEL_ERP_LITE;
using System.Windows.Forms;
using MODEL_ERP_LITE.COMPRAS;
using MODEL_ERP_LITE.RELACIONES;
using MODEL_ERP_LITE.PUBLIC;
using SERVICE_ERP_LITE.CONFIGURACION_SERVICE.IMPLEMENTACION;
using MODEL_ERP_LITE.CONFIGURACION;

namespace SERVICE_ERP_LITE.CAJA_SERVICE.IMPLEMENTACION
{
    public class MovimientoVariosImpl : MovimientoVariosService
    {
        public MovimientoVarios buscar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.movimientosCajaVarios.Include("movimientoCaja").Include("concepto").Include("proveedor").FirstOrDefault(x=>x.id==id);
            }
            }

        public bool crear(MovimientoVarios objeto)
        {
            throw new NotImplementedException();
        }

        public bool crear2(MovimientoVarios objeto,DateTime fechaV,DateTime fechaD,string cuenta,double cambio)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                   
   
                        //ACTUALIZAR CAJA DIARIA.
                        Proveedor proveedor = db.proveedores.Find(objeto.idProveedor);
                        CajaDiaria caja = db.cajasDiarias.Find(objeto.movimientoCaja.idDiaCaja);
                        TipoComprobantePago tc = db.tiposComprobantePago.Find(objeto.idTipoComprobante);
                     /*   if (objeto.movimientoCaja.esIngresoVarios)
                          //  caja.montoFinal += objeto.movimientoCaja.monto*cambio;
                        else
                        {
                           // caja.montoFinal -= objeto.movimientoCaja.monto*cambio;
                        }*/
                        Parametro igv = ParametroImpl.getInstancia().FirstOrDefault(x => x.nombre == "IGV");

                        int numero = 0;
                        if (UTILITARIO.Util.esDouble(objeto.numero)) numero = Convert.ToInt32(objeto.numero);
                        int idMon = 181;
                        if (cambio == 1) idMon = 180;
                        RegistroCompra registroCompra = new RegistroCompra
                        {
                            docIdentidad = proveedor.ruc,
                          
                            fechaNota = "",
                            total=objeto.movimientoCaja.monto,
                            igv = (objeto.movimientoCaja.monto - objeto.valorNoGravadas)-(objeto.movimientoCaja.monto - objeto.valorNoGravadas)/(1+igv.valorDouble),
                            numeroComprobante = numero,
                            opExoneradas = 0,
                            fechaEmision = objeto.fechaComprobante, serie=objeto.serie,
                            razonSocial = proveedor.razonSocial
                            ,montoDetraccion = objeto.montoDetraccion,
                            serieNota = "",
                            
                            tipoComprobante = tc.codigo_sunat,
                            tipoDocIdentidad = "6"
                            ,descuento=objeto.descuento,
                            tipoNota = "",
                            idProveedor = objeto.idProveedor,
                            tipoCambio = cambio, esGasto = true, valorNoGravadas=objeto.valorNoGravadas, fechaVencimiento=fechaV
                           ,numeroDetraccion=cuenta,idMedioPago=objeto.movimientoCaja.idMedioPago,idMoneda=idMon
                        };
                        if (objeto.esDetraccion)
                            registroCompra.fechaDetraccion = fechaD.ToString("dd/MM/yyyy");
                        else { registroCompra.fechaDetraccion = ""; registroCompra.numeroDetraccion = ""; }
                    
                        registroCompra.opGravadas = registroCompra.total - registroCompra.igv-registroCompra.valorNoGravadas;
                        db.movimientosCajaVarios.Add(objeto);
                        db.SaveChanges();
                        registroCompra.idComprobante = objeto.id;
                        db.registroCompras.Add(registroCompra);
                        objeto.idRegistro = registroCompra.id;
                        db.SaveChanges();

                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return false;
                    }
                }
            }
        }

        public bool editar(MovimientoVarios objeto)
        {
            return true;
        }

        public bool eliminar(int id)
        {
            using (var db = new BaseDeDatos("public"))
            {
                using (var dbTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        MovimientoVarios mv = db.movimientosCajaVarios.Find(id);
                        MovimientoCaja mc = db.movimientosCaja.Find(mv.idMovimientoCaja);
                        mv.anulado = true;
                        mc.anulado = true;
                        db.SaveChanges();
                        int numero = Convert.ToInt32(mv.numero);
                        CajaDiaria caja = db.cajasDiarias.Find(mc.idDiaCaja);
                        RegistroCompra registro = db.registroCompras.FirstOrDefault(x =>x.numeroComprobante==numero&&x.esGasto&&x.serie==mv.serie&&x.fechaEmision==mv.fechaComprobante);
                        registro.anulado = true;
                        if (mc.esIngresoVarios)
                            caja.montoFinal -= mc.monto;
                        else
                        {

                            caja.montoFinal +=mc.monto;
                        }
                        if (caja.montoFinal < 0)
                        {
                            dbTransaction.Rollback();
                            return false;
                        }

                        db.SaveChanges();
                        dbTransaction.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        dbTransaction.Rollback(); MessageBox.Show(ex.Message + "    " + ex.ToString());
                        return true;
                    }
                }
            }
        }

        public List<MovimientoVarios> listarNoAnulados()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.movimientosCajaVarios.Include("movimientoCaja").Include("concepto").Include("proveedor").OrderByDescending(x => x.id).Where(x => x.anulado == false).Take(30).ToList();
            }
        }
        public List<MovimientoVarios> listarPorFecha(DateTime inicio,DateTime fin)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return db.movimientosCajaVarios.Include("movimientoCaja").Include("concepto").Include("proveedor").OrderByDescending(x => x.id).Where(x => x.anulado == false&& x.fechaComprobante>=inicio&&x.fechaComprobante<fin).ToList();
            }
        }

       
    }
}
