﻿using MODEL_ERP_LITE.CAJA;
using SERVICE_ERP_LITE.PUBLIC_SERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CAJA_SERVICE
{
   public interface MovimientoVariosService:CrudService<MovimientoVarios>
    {
        List<MovimientoVarios> listarPorFecha(DateTime inicio, DateTime fin);
        bool crear2( MovimientoVarios m,DateTime inicio, DateTime fin,string cuenta,double cambio);
    }
}
