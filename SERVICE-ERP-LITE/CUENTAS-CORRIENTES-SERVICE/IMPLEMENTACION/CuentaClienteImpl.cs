﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.CUENTAS_CORRIENTES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CUENTAS_CORRIENTES_SERVICE.IMPLEMENTACION
{
 public   class CuentaClienteImpl
    {
        public List<CuentaCliente> listaDeudasCliente() {
            using (var db = new BaseDeDatos("public"))
            {
                return  (from c in db.clientes
                             join v in db.comprobantesPago
                             on c.id equals v.idCliente

                             where v.pagado==false&&v.anulado==false&&c.anulado==false
                             group v by new { v.idCliente, c.razonSocial, c.dniRepresentante, c.ruc } into grupo
                             select new CuentaCliente
                             {
                                 idCliente = grupo.Key.idCliente,
                                 nombre = grupo.Key.razonSocial,
                                 dni = grupo.Key.dniRepresentante,
                                 ruc = grupo.Key.ruc,
                                 monto = grupo.Sum(x => x.montoN- x.montoPagado),
                             }
                      ).ToList();         
            }
    }



        public List<CuentaCliente> listaDeudasClientePorNombre(String nombre)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from c in db.clientes
                        join v in db.comprobantesPago
                        on c.id equals v.idCliente

                        where v.pagado == false && v.anulado == false && c.anulado == false&&c.razonSocial.Contains(nombre)
                        group v by new { v.idCliente, c.razonSocial, c.dniRepresentante, c.ruc } into grupo
                        select new CuentaCliente
                        {
                            idCliente = grupo.Key.idCliente,
                            nombre = grupo.Key.razonSocial,
                            dni = grupo.Key.dniRepresentante,
                            ruc = grupo.Key.ruc,
                            monto = grupo.Sum(x => x.montoN - x.montoPagado),
                        }
                      ).ToList();
            }
        }

    }
}
