﻿using MODEL_ERP_LITE;
using MODEL_ERP_LITE.CUENTAS_CORRIENTES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICE_ERP_LITE.CUENTAS_CORRIENTES_SERVICE.IMPLEMENTACION
{
    public class CuentaProveedorImpl
    {
        public List<CuentaCliente> listaDeudasProveedor()
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from c in db.proveedores
                        join v in db.compras
                        on c.id equals v.idProveedor

                        where v.pagado == false && v.anulado == false
                        group v by new { v.idProveedor, c.razonSocial, c.ruc } into grupo
                        select new CuentaCliente
                        {
                            idCliente = grupo.Key.idProveedor,
                            nombre = grupo.Key.razonSocial,
                            dni = "",
                            ruc = grupo.Key.ruc,
                            monto = grupo.Sum(x => x.montoN - x.montoPagado),
                        }
                      ).ToList();
            }
        }




        public List<CuentaCliente> listaDeudasProveedorPorNombre(string nombre)
        {
            using (var db = new BaseDeDatos("public"))
            {
                return (from c in db.proveedores
                        join v in db.compras
                        on c.id equals v.idProveedor

                        where v.pagado == false && v.anulado == false && c.razonSocial.Contains(nombre)
                        group v by new { v.idProveedor, c.razonSocial, c.ruc } into grupo
                        select new CuentaCliente
                        {
                            idCliente = grupo.Key.idProveedor,
                            nombre = grupo.Key.razonSocial,
                            dni = "",
                            ruc = grupo.Key.ruc,
                            monto = grupo.Sum(x => x.montoN - x.montoPagado),
                        }
                      ).ToList();
            }
        }
        
    }
}
